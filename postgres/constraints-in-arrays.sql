-- -- psql -U postgres -d kong -a -f constraints-in-arrays.sql
-- BEGIN;
-- CREATE extension if NOT EXISTS intarray;
-- CREATE extension if NOT EXISTS btree_gist;
-- CREATE TABLE foo(id INTEGER PRIMARY KEY, myarray text[], NAME text, arr2 INT[]);
-- INSERT INTO foo VALUES(1, ARRAY ['123'], '1', ARRAY [1]);
-- UPDATE foo SET myarray = myarray || ARRAY ['456'] WHERE id = 1;
-- UPDATE foo SET myarray = array_append(myarray, '789') WHERE id = 1;

-- ALTER TABLE foo
-- ADD CONSTRAINT unique_per_ws
--   exclude USING gist(
--   NAME WITH = ,
--   arr2 WITH &&
--   );

-- INSERT INTO foo VALUES(2, ARRAY ['123'], '2', ARRAY [2]);
-- INSERT INTO foo VALUES(3, ARRAY ['123'], '3', ARRAY [1]);
-- INSERT INTO foo VALUES(4, ARRAY ['123'], '2', ARRAY [1]);

-- TABLE foo ;
-- INSERT INTO foo VALUES(5, ARRAY ['123'], '1', ARRAY [1]); -- kaboom

-- TABLE foo ;
-- ROLLBACK;






--- Trying with uuids
-- XXX this approach is doomed because it needs superuser powers and
-- RDS doesn't allow it, so it's a nope. Shit

--  psql -U postgres -d kong -a -f constraints-in-arrays.sql
BEGIN;
CREATE extension if NOT EXISTS intarray;
CREATE extension if NOT EXISTS btree_gist;
CREATE extension if NOT EXISTS "uuid-ossp";

-- CREATE OPERATOR CLASS _uuid_ops DEFAULT FOR TYPE _uuid USING gist AS
--       OPERATOR 1 &&(anyarray, anyarray),
--       OPERATOR 2 @>(anyarray, anyarray),
--       OPERATOR 3 <@(anyarray, anyarray),
--       OPERATOR 4 =(anyarray, anyarray),
--       FUNCTION 1 uuid_cmp(uuid, uuid),
--       FUNCTION 2 ginarrayextract(anyarray, internal, internal),
--       FUNCTION 3 ginqueryarrayextract(anyarray, internal, smallint, internal, internal, internal, internal),
--       FUNCTION 4 ginarrayconsistent(internal, smallint, anyarray, integer, internal, internal, internal, internal),
--       STORAGE uuid;


CREATE TABLE foo(id uuid PRIMARY KEY, myarray text[], NAME text, arr2 uuid[]);
INSERT INTO foo VALUES('A6C9690D-B2B2-4B6E-915B-ACA3AAE4FFC9', ARRAY ['123'], '1', ARRAY ['A6C9690D-B2B2-4B6E-915B-ACA3AAE4FFC9']::uuid[]);

ALTER TABLE foo
ADD CONSTRAINT unique_per_ws
  exclude USING gist(
  NAME WITH = ,
  arr2 WITH &&
  );

-- INSERT INTO foo VALUES(2, ARRAY ['123'], '2', ARRAY [2]);
-- INSERT INTO foo VALUES(3, ARRAY ['123'], '3', ARRAY [1]);
-- INSERT INTO foo VALUES(4, ARRAY ['123'], '2', ARRAY [1]);

-- TABLE foo ;
-- INSERT INTO foo VALUES(5, ARRAY ['123'], '1', ARRAY [1]); -- kaboom

TABLE foo ;
ROLLBACK;
