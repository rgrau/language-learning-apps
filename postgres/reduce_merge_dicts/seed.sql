-- psql -U postgres -d kong -a -f seed.sql

BEGIN;

CREATE TABLE hars(id serial PRIMARY KEY, har jsonb);

INSERT INTO hars(har) VALUES ('{"foo": "asaa"}'), ('{"bar": 1}');

SELECT json_agg(jsonb_build_object('hola', 1));
--SELECT json_agg (to_jsonb(SELECT * FROM hars));


CREATE aggregate jsonb_object_agg(jsonb) (
  SFUNC = 'jsonb_concat',
  STYPE = jsonb,
  INITCOND = '{}'
);

SELECT jsonb_object_agg(har) FROM hars;


-- CREATE OR REPLACE FUNCTION jsonb_update(jsonb1 JSONB, jsonb2 JSONB)
-- RETURNS JSONB  LANGUAGE sql IMMUTABLE
-- AS $$
--   SELECT json_object_agg(merged.key, merged.value)::jsonb FROM
--   (
--     WITH existing_object AS (
--       SELECT key, value FROM jsonb_each($1)
--         WHERE NOT (value IS NULL OR value in ('[]', 'null', '{}') )
--     ),
--     new_object AS (
--       SELECT key, value FROM jsonb_each($2)
--     ),
--     deep_merge AS (
--       SELECT lft.key as key, jsonb_merge( lft.value, rgt.value ) as value
--         FROM existing_object lft
--         JOIN new_object rgt ON ( lft.key = rgt.key)
--           AND jsonb_typeof( lft.value ) = 'object'
--           AND jsonb_typeof( rgt.value ) = 'object'
--     )

--     -- Any non-empty element of jsonb1 that's not in jsonb2 (the keepers)
--     SELECT key, value FROM existing_object
--       WHERE key NOT IN (SELECT key FROM new_object )
--     UNION
--     -- Any non-empty element from jsonb2 that's not to be deep merged (the simple updates and new elements)
--     SELECT key, value FROM new_object
--       WHERE key NOT IN (SELECT key FROM deep_merge )
--         AND NOT (value IS NULL OR value in ('[]', 'null', '{}') )
--     UNION
--     -- All elements that need a deep merge
--     SELECT key, value FROM deep_merge
--   ) AS merged
-- $$;

ROLLBACK;
