-- psql -U postgres -d kong -a -f seed.sql

BEGIN;

  CREATE TABLE rabo(store_id INT, customer_id INT);
  INSERT INTO rabo VALUES(1,123),(2, 456), (1, 789), (2, 111), (2, 222);
  SELECT * FROM rabo;

  select jsonb_agg(to_jsonb(x))
    from (
      select store_id, jsonb_agg(jsonb_build_object('customer_id', customer_id)) as customers
      from rabo
      group by store_id
  ) x;

  select jsonb_agg(to_jsonb(x))
    from (
      select store_id, jsonb_agg(customer_id) as customers
      from rabo
      group by store_id
    ) x;

ROLLBACK;
