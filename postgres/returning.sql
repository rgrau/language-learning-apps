-- psql -U postgres -d kong -a -f returning.sql

BEGIN;

CREATE TABLE foo(id uuid PRIMARY KEY , store_id INT, customer_id INT);
INSERT INTO foo VALUES
       ('8db29a99-428a-413a-aef5-e1c40f46600d', 1, 2),
       ('53f60296-939f-467b-b863-d2fef292bb80', 1, 2);
TABLE foo;

CREATE TABLE bar AS TABLE foo LIMIT 1;
SELECT * FROM bar;

CREATE TABLE baz AS TABLE bar WITH NO DATA;
SELECT * FROM baz;

CREATE TABLE qux AS (SELECT * FROM foo);
SELECT * FROM qux;


CREATE TABLE bla AS (SELECT * FROM qux ) WITH NO data;
-- CREATE TABLE bla AS (SELECT * FROM qux limit 0 );

WITH tmp AS (DELETE FROM qux returning *)
         INSERT INTO bla (SELECT * FROM tmp);

TABLE bla;
ROLLBACK;
