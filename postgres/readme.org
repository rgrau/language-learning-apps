* create table from...
  - https://kccoder.com/postgres/fabricating-data-with-generate_series/
  - https://dataschool.com/learn-sql/generate-series/
  - https://regilero.github.io/postgresql/english/2017/06/26/postgresql_advanced_generate_series/
  - https://popsql.com/learn-sql/postgresql/how-to-use-generate-series-to-avoid-gaps-in-data-in-postgresql/
** copy table
 #+BEGIN_SRC sql
 create table foo AS
 table bar [WITH NO DATA];
 #+END_SRC
** create table from data
   #+BEGIN_SRC sql
   create table bar as
   select id from foo;
   #+END_SRC
** create from random data
 #+BEGIN_SRC sql
 create table baz as
 select a,b from generate_series(1,10) as a, generate_series(1,2) as b
 #+END_SRC
