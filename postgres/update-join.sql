-- subtract removed entities from counters
UPDATE workspace_entity_counters w
SET count = COALESCE(w.COUNT, 0) - c.count
FROM
(SELECT workspace_id, entity_type, COUNT(DISTINCT entity_id)
   FROM workspace_entities w INNER JOIN ]]
     .. self:escape_identifier(table_name) ..[[ c
   ON w.entity_id = c.id::text
   WHERE w.entity_type=]] .. self:escape_literal(table_name) ..
   [[ AND ]] .. column_name .. [[ < CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
   GROUP BY workspace_id, entity_type) c
WHERE w.workspace_id = c.workspace_id
  AND w.entity_type = c.entity_type;
