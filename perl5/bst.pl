use warnings;
use Data::Dumper;
my %h = ();

# while(chomp($_ = <>)){
#   for $_ (split) {
#     $h{$_}++;
#   }
# }
# print($h{'a'}, "\n");

my $hash = {foo => 'bar'};

for my $v (keys(%$hash)) {
  print($v, $hash->{$v}, "\n");
}

my $node = {left => 1, right => 2} ;

sub insert {
  my ($t, $v) = @_;
  if (!defined($t)) {
    return {
            val => $v,
            left => undef,
            right => undef
           }
  }
  $dir = ($v < $t->{'val'}) ? 'left' : 'right';
  $t->{$dir} = insert($t->{$dir}, $v);
  $t
}

my $t = undef;
$a = insert($t, 3);
print Dumper($a), "\n";
$b = insert($a, 4);
print Dumper($b), "\n";
print Dumper($a), "\n";
