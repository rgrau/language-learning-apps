sub say {
  print @_, "\n";
}

sub qsort {
  my ($pivot, @arr) = @_;
  if (0 == @arr ) { return ($pivot) }
  return qsort(grep {$_ < $pivot} @arr), $pivot, qsort(grep {$_ >= $pivot} @arr);
}

say qsort((3,2,1,5,3,3,7,5,3,4));
