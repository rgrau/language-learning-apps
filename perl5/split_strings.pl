use strict;
use warnings;

sub say {
  print @_, "\n";
}

sub splitit {
  my $a = shift;
  my @splitted = split(/ /, $a);
  print @splitted, "\n";
}

my @dict = qw(hola adeu);

sub try_to_split {
  my $phrase = shift or die "no param";
  if ($phrase eq "" ) {
    return "";
  }
  foreach my $a (@dict) {
    if ($phrase =~ m/^$a/) {
      if (length($a) == length($phrase)) {
        return "$a";
      }
      my $rest = try_to_split(substr($phrase, length($a)));
      if (defined($rest)) {
        return "$a $rest";
      }
    }
  }
  return undef;
}

while(<>) {
  chomp(my $a = $_);
  say(try_to_split($a));
}
