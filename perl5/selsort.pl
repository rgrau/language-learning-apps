use strict;
use warnings;
use Data::Dumper;

sub say {
  print @_, "\n";
}

sub sel_sort {
  my $a = shift;
  for my $from (0..@$a-2) {
    my $min_pos = undef;
    for my $curr ($from+1.. @$a-1) {
      if ((!defined($min_pos)) or (@$a[$min_pos] > @$a[$curr] )) {
        $min_pos = $curr;
      }
    }

    if (@$a[$min_pos] < @$a[$from]) {
      (@$a[$from], @$a[$min_pos]) = (@$a[$min_pos], @$a[$from]);
    }
  }
}

# my @a = (4,0,2,5);
# sel_sort(\@a);
# say @a;

my $a = [4,0,2,5];
sel_sort($a);
say Dumper($a);

# CUIDAO!!!! assigning array creates a new array with a copy of the values
