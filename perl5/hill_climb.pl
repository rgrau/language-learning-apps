use Data::Dumper;

sub say {
  print @_, "\n";
}

sub mutate {
  my $arr = shift;
  my @arr2 = @$arr;
  $arr2[int(rand(scalar @arr2))] = int(rand(10));
  return \@arr2;
}

sub hill_climb {
  my ($num_it, $ranges, $fitness) = @_;
  my $arr = [map {int(rand($ranges))} 1..5];
  my $best_fit = $fitness->($arr);
  for my $it (1..$num_it){
    my $a = mutate($arr);
    my $c_fit = $fitness->($a);
    if ($best_fit < $c_fit) {
      ($arr, $best_fit) =  ($a, $c_fit);
    }
  }
  return $arr
}

sub fit {
  my $arr = shift;
  my $r = 0;
  map { $r += $_ } @$arr;
  return $r;
}

say Dumper(hill_climb(10, 10, \&fit));
