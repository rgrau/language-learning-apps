use strict;
use warnings;
use Data::Dumper;

sub say {
  print @_, "\n";
}

sub Heap {
  return [undef];
}

sub float {
  my $tree = shift;
  my $pos = shift;
  return if $pos == 1;
  my $parent_pos = $pos/2;
  if( $tree->[$parent_pos] > $tree->[$pos]) {
    ($tree->[$pos], $tree->[$parent_pos]) = ($tree->[$parent_pos], $tree->[$pos]);
    float($tree, $parent_pos);
  }
}

sub sink {
  my $heap = shift;
  my $pos = shift;
  my $val = $heap->[$pos];
  my $h_size = scalar @$heap;
  my $min = $pos;
  if ($pos * 2 >= $h_size) {
    return $heap;
  }
  if ($val > ($heap->[$pos*2] || $val) ) {
    $min = $pos*2;
  }
  if ($val > ($heap->[($pos*2) +1] || $val) ) {
    $min = ($pos*2) +1;
  }
  if ($min != $pos) {
    ($heap->[$pos] , $heap->[$min] ) =  ($heap->[$min] , $heap->[$pos]);
    sink($heap, $min);
  }
  return $heap;
}

sub insert {
  my ($heap, $v ) = @_;
  push(@$heap, $v);
  float($heap , 0+@$heap-1);
  return $heap;
}

sub pop_min {
  my $heap = shift;
  say Dumper $heap;
  my $res = $heap->[1];

  $heap->[1] = $heap->[(scalar @$heap) -1 ] ;
  pop(@$heap);
  sink($heap, 1);
  return $res
}

my $h = insert(insert(insert(Heap, 2), 5), 6);

map {say pop_min($h)} 1..5;
