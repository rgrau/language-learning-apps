USING: combinators combinators.smart io kernel math math.parser
math.ranges sequences ascii random ;

IN: scratchpad

: LOL ( x -- x ) 1 + ;

: NEIN ( x -- x ) 1 - ;

: FOO ( x -- x ) NEIN LOL ;

: rand-array ( n elt -- arr )
    <array> [ random ] map ;

: game-choices ( -- arr )
    100 3 rand-array ;

: no-change ( x y -- b )
    = ;

: change ( x y -- b )
    no-change not ;

: main ( -- arr )
    game-choices game-choices
    [ [ change ] [ no-change ]  2bi 2array ] 2map [ 0 swap nth ] count;

: simple-main ( -- arr )
    game-choices game-choices
    [ change ] 2map [ ] count  ;
