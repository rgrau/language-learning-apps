USE: random
USE: math.parser

: factorial ( x -- y )  dup 2 < [ ] [ dup 1 - factorial * ] if ;

: gt? ( x y -- b  ) >  ;

: lt? ( x y -- b  ) >  ;

: readInt ( -- x ) readln string>number ;

: pick-rand ( -- x ) 100 random ;

: pick-lower ( -- ) "pick lower " print  flush ;

: pick-higher ( -- ) "pick higher " print flush ;

: say-so ( x y -- )  >  [ pick-higher ] [ pick-lower ] if ;

: dupover ( x y -- x x y ) over swap ;

: MAIN ( -- ) pick-rand dup "pick" print flush readInt [ 2dup = not ] [ say-so readInt dupover ]  while . . . "found!" print ;

: MAIN ( -- ) pick-rand  "pick" print flush [ dup readInt  2dup = not ] [ say-so ]  while . . . "found!" print ;
