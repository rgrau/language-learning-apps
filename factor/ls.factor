
USING: inspector io.files.info io.pathnames system tools.files ;

: visible-files ( dir -- ary )
    <listing-tool> list-files [ 0 swap nth ] map [ hidden? not ] filter ;

: hidden? ( f -- b )
    "^\\..*" <regexp> matches? ;
