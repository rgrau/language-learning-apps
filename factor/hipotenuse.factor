#!/home/rgrau/programmingStuff/factor/factor -script
USING: kernel command-line formatting prettyprint sequences formatting math.parser math.functions regexp io math ;
IN: hipotenuse

: read-num ( -- x ) readln string>number ;
: hipotenuse ( x y -- h ) sq swap sq + sqrt ;
: print-it ( x -- ) "%.3f" sprintf R/ .?0*$/ "" re-replace . ;

read-num read-num hipotenuse print-it
