#include <elf.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* #include <iostream.h> */

int main(int argc, char **argv){
  Elf64_Ehdr ehdr;
  int fd = open(argv[1], O_RDWR);
  read(fd, &ehdr, sizeof(ehdr));
  printf("Entry: %lx\n", ehdr.e_entry);
  ehdr.e_entry =0x00400807;
  lseek(fd, 0, SEEK_SET);
  write(fd, &ehdr, sizeof(ehdr));
  close(fd);
}

int other(int argc, char **argv){
  printf("This is the patched version!!\n");
}
