#include <stdio.h>

/* expand a-z to abc....yz */
void expand(char s1[], char s2[]) {
  char to, from, subindex;
  int s2i = 0;
  int i = 0;
  while(s1[i] != '\0')
    {
    if (s1[i+1] == '-' && s1[i+2] != '\0'){
      from = s1[i];
      to = s1[i+2];
      i+=3;
      for (subindex = from; subindex <= to; ++subindex)
        {
          s2[s2i++] = subindex;
        }
    }
    else
      {
        printf("%d\n", i);
        s2[s2i++] = s1[i++];
      }
    }
  s2[s2i++] = '\0';


  return;
}

int main(int argc, char *argv[])
{
  char s2[1000];
  expand(argv[1], s2);
  printf("%s\n", s2);
  return 0;
}
