;;; url-retrieve-async.el ---                        -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Raimon Grau

;; Author: Raimon Grau <rgrau@raikong>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


(defun my-cb (status)
  (message "status: %s" status))

(defun my-cb-sync (b status)
  (message "%s" status))

(defvar my-flag nil)

(defadvice url-http-handle-authentication (around authack activate)
  (if url-http-auth-flag2
      (message "yess!")
    (message "nope"))
  ad-do-it)

(ad-activate 'url-http-handle-authentication)

(defun bla ()
  (message "yeah"))

(defun rgc-url-retr ()
  "A."
  (let ((url-request-method "HEAD")
        (url-request-authentication-handler #'oauth-url-auth))
    (my-cb-sync (url-retrieve-synchronously "http://httpbin.org/status/401" 'my-cb))))

(rgc-url-retr)
(provide 'url-retrieve-async)
;;; url-retrieve-async.el ends here
