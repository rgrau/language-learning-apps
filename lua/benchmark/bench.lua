ffi = require "ffi"

ffi.cdef[[
    typedef long time_t;

    typedef struct timeval {
        time_t tv_sec;
        time_t tv_usec;
    } timeval;

    int gettimeofday(struct timeval* t, void* tzp);
]]

gettimeofday_struct = ffi.new("timeval")

local function gettimeofday()
    ffi.C.gettimeofday(gettimeofday_struct, nil)
    return tonumber(gettimeofday_struct.tv_sec) * 1000000 + tonumber(gettimeofday_struct.tv_usec)
end

local ITER_EXP = 7

local fmt    = string.format
local ti     = table.insert
local ok, tc = pcall(require, "table.clear")
if not ok then
    tc = function(t)
        for k, v in pairs(t) do
            t[k] = nil
        end
    end
end
local ok, tn = pcall(require, "table.new")
if not ok then tn = function() return {} end end


local t = tn(10 ^ ITER_EXP, 0)
local x = 0

local start

for j = 1, ITER_EXP do
    start = gettimeofday()
    for i = 1, 10 ^ j do
        t[#t + 1] = "foo"
    end
    print(fmt("# (%s): %s", j, gettimeofday() - start))
    tc(t)
end

t = tn(10 ^ ITER_EXP, 0)
for j = 1, ITER_EXP do
    start = gettimeofday()
    for i = 1, 10 ^ j do
        ti(t, "foo")
    end
    print(fmt("insert (%s): %s", j, gettimeofday() - start))
    tc(t)
end
