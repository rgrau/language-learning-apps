local function foo1(one, two, three)
  if one == one then
    print("hello")
  end

  return one
end

local function foo2(one)
  if one == one then
    print("hello")
    return false
  end

  return one
end
