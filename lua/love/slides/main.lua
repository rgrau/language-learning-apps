
slides = {
  {
    title = "Title 1",
    body = "This is body"
  },
  {
    title = "Title 2",
    body = "This is body 2"
  }
}

current = 1


function print_slide(slide)
  love.graphics.print(slides[current].title,
                      love.graphics:getWidth()/2-50,
                      love.graphics:getHeight()/2-10)

  love.graphics.print(slides[current].body,
                      love.graphics:getWidth()/2-50,
                      love.graphics:getHeight()/2)
end


function love.load()

end

function love.update()
  if love.keyboard.isDown('n')  then
    current = 2
  end
end

function love.draw()
  love.graphics.print(current,
                      love.graphics:getWidth()/2-50,
                      love.graphics:getHeight()/2+ 15)

  love.graphics.print(slides[current].title,
                      love.graphics:getWidth()/2-50,
                      love.graphics:getHeight()/2-10)

  love.graphics.print(slides[current].body,
                      love.graphics:getWidth()/2-50,
                      love.graphics:getHeight()/2)
end
