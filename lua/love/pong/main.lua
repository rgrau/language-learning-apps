do
  local speed = 10
  local ball_2dspeed = 200
  local state = ({[("dx")] = 2, [("dy")] = 1, [("left")] = 10, [("right")] = 10, [("x")] = 100, [("y")] = 100})
  local w = love[("graphics")][("getWidth")]()
  local h = love[("graphics")][("getHeight")]()
  local function _0_()
      return ((((state[("x")]) < (20)) and ((state[("left")]) < (state[("y")])) and ((state[("y")]) < ((state[("left")] + 100)))) or ((((w - 20)) < (state[("x")])) and ((state[("right")]) < (state[("y")])) and ((state[("y")]) < ((state[("right")] + 100)))))
  end
  local on_2dpaddle_3f = _0_
  local function update(dt)
      state[("x")] = (state[("x")] + (state[("dx")] * dt * ball_2dspeed))
      do local _ = nil end
      state[("y")] = (state[("y")] + (state[("dy")] * dt * ball_2dspeed))
      do local _ = nil end
      local function _1_()
        
        if love[("keyboard")][("isDown")](("a")) then
            do
                state[("left")] = (state[("left")] - speed)
                return nil
            end
        end
      end
      _1_()
      local function _2_()
        
        if love[("keyboard")][("isDown")](("z")) then
            do
                state[("left")] = (state[("left")] + speed)
                return nil
            end
        end
      end
      _2_()
      local function _3_()
        
        if love[("keyboard")][("isDown")](("up")) then
            do
                state[("right")] = (state[("right")] - speed)
                return nil
            end
        end
      end
      _3_()
      local function _4_()
        
        if love[("keyboard")][("isDown")](("down")) then
            do
                state[("right")] = (state[("right")] + speed)
                return nil
            end
        end
      end
      _4_()
      local function _5_()
        
        if (((state[("y")]) < (0)) or ((state[("y")]) > (h))) then
            do
                state[("dy")] = (0 - state[("dy")])
                return nil
            end
        end
      end
      _5_()
      local function _6_()
        
        if on_2dpaddle_3f() then
            do
                state[("dx")] = (0 - state[("dx")])
                return nil
            end
        end
      end
      _6_()
      local function _7_()
        
        if ((state[("x")]) < (0)) then
            do
                print(("Right player wins"))
                return love[("event")][("quit")]()
            end
        end
      end
      _7_()
      
      if ((state[("x")]) > (w)) then
          do
              print(("Left player wins"))
              return love[("event")][("quit")]()
          end
      end
  end
  love[("update")] = update
  do local _ = nil end
  local function _1_(key)
      assert(((nil) ~= (key)), ("Missing argument key on main.fnl:40"))
      
      if ((("escape")) == (key)) then
          do
              return love[("event")][("quit")]()
          end
      end
  end
  love[("keypressed")] = _1_
  do local _ = nil end
  local function _2_()
      love[("graphics")][("rectangle")](("fill"), 10, state[("left")], 10, 100)
      love[("graphics")][("rectangle")](("fill"), (w - 10), state[("right")], 10, 100)
      return love[("graphics")][("circle")](("fill"), state[("x")], state[("y")], 10)
  end
  love[("draw")] = _2_
  return nil
end
