;; run `fennel --compile pong.fnl > main.lua; love .` to run this code
(let [speed 10 ball-speed 200
      state {:x 100 :y 100 :dx 2 :dy 1 :left 10 :right 10}
      w (love.graphics.getWidth) h (love.graphics.getHeight)

      on-paddle?
      (fn []
       (or (and (< state.x 20)
            (< state.left state.y) (< state.y (+ state.left 100)))
        (and (< (- w 20) state.x)
         (< state.right state.y) (< state.y (+ state.right 100)))))]
 (set love.update
  (fn update [dt]
   (set state.x (+ state.x (* state.dx dt ball-speed)))
   (set state.y (+ state.y (* state.dy dt ball-speed)))

   (when (love.keyboard.isDown "a")
    (set state.left (- state.left speed)))
   (when (love.keyboard.isDown "z")
    (set state.left (+ state.left speed)))
   (when (love.keyboard.isDown "up")
    (set state.right (- state.right speed)))
   (when (love.keyboard.isDown "down")
    (set state.right (+ state.right speed)))

   (when (or (< state.y 0) (> state.y h))
    (set state.dy (- 0 state.dy)))

   (when (on-paddle?)
    (set state.dx (- 0 state.dx)))

   (when (< state.x 0)
    (print "Right player wins")
    (love.event.quit))
   (when (> state.x w)
    (print "Left player wins")
    (love.event.quit))))

 (set love.keypressed
  (λ [key]
   (when (= "escape" key)
    (love.event.quit))))

 (set love.draw
  (fn []
   (love.graphics.rectangle "fill" 10 state.left 10 100)
   (love.graphics.rectangle "fill" (- w 10) state.right 10 100)
   (love.graphics.circle "fill" state.x state.y 10))))
