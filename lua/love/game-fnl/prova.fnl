(global add (fn [x y] (+ x y)))
(global sub (fn [x y] (- x y)))
(print (add 32 12))


(global print-calculation
        (lambda [x ?y ?z]
          (print (- x (or ?y 1) (or z 0)))))

;; (print-calculation 5)

;;; let is let*
(let [x 54
      y (+ x 1)]
  (print y))

(print (+ 1 2 3 4))

(print (.. "hola" (+ 1 3) "ya ves"))

(print (string.format "%s" "yueaaa"))

(print (table.concat ["hola" "tiu"] ", "))

;; (print (table.concat { "hola" "tiu" "ya" "ves" } ", "))


(each [k v (pairs {:key1 1 :key2 2})]
  (print k v))

(each [k v (ipairs [ :key1 1 :key2 2 ])]
  (print k v))

(for [i 1 10]
  (print i))

(global abs
        (fn [x]
          (* (if (< x 0) -1 1) x)))

(print (abs -4))
(print (abs 4))

(print "aveeerrr")

;; + and - are not first class
(print ((if (= 0 1) add sub) 1 2))

(for [i 1 10]
 (let [x (% (math.random 100) 2)]
   (if (= 0 x)
       (print "even!!")
       (print "odd!!"))))

(global map
  (fn [fun array]
    (let [x []]
      (each [i v (ipairs array)]
        (tset x i (fun v)))
      x)))

(map (fn [x] (+ x 1))  [1 2 3])
