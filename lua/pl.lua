local pl_config = require("pl.config")
local pl_stringio = require("pl.stringio")
local inspect = require('inspect')

s = pl_stringio.open "hoila=off #"
local from_file_conf, err = pl_config.read(s ,
                                           {
                                             smart = false,
                                             list_delim = "_blank_",
})
print(inspect(from_file_conf))
