-- Queremos explotar el mundo:
--   0. definir que mundo queremos explotar
--   1. saber donde vamos a poner  las cargas
--   2. poner las cargas de dinamita
--   2.1 cosas que afectan a esta funcion
--     - donde vamos a poner las cargas (resultado de 1)
--   2.2 cosas que hace esta funcion
--     - comprarlas
--     - guardarlas
--     - colocarlas
--   3. salvar a todos los buenos
--      Determina quien es bueno y quien no y guarda los buenos y descarta los malos
--   4. irnos del mundo
--      Nos saca a nosotros con todos los buenos de los paises
--   5. explotar el mundo
--      Boom
--   6. saber si exploto
--      Testea que no queda nadie vivo en los paises

function cargar_paises_de_planeta(planeta)
  if planeta == "tierra"  then
    return { "afganistan", "espanya", "argentina" }
  elseif planeta == "marte" then
    return { "este", "el otro"}
  else
    return {}
  end
end

function cargar_habitantes_de_planeta()

end

function definir_mundo(nombre)
  paises = cargar_paises_de_planeta(nombre)
  habitantes = cargar_habitantes_de_planeta(nombre)
  print(paises[1])
end

definir_mundo("tierra")
