* table.sort in place
#+BEGIN_SRC lua
  local t = {t = {"a", "b", "0"}}
  table.sort(t.t)
  return table.concat(t.t, ", ")
#+END_SRC

#+RESULTS:
: 0, a, b
* table.concat of hash
  it just doesn't work. only for seq
#+BEGIN_SRC lua
--return table.concat({a = 1, b = 2})
return table.concat({1,2})
#+END_SRC

#+RESULTS:
: 12
* ensure_list and join
  #+BEGIN_SRC lua

    function ensure_list(l)
      if type(l) == "table" then
        return l
      else
        return {l}
      end
    end

    local list = ensure_list({"a"})
    return table.concat(list, ",")
  #+END_SRC

  #+RESULTS:
  : a
