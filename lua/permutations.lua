local function map(f, t)
  local r = {}
  for _, x in ipairs(t) do
    r[#r+1] = f(x)
  end
  return r
end


-- helper function for permutations
local function inc(t, pos)
  if t[pos][3] == t[pos][2] then
    if pos == 1 then return nil end
    t[pos][3] = 1
    return inc(t, pos-1)
  else
    t[pos][3] = t[pos][3] + 1
    return true
  end
end

-- permutations iterator
-- local c = 0
-- for i in permutations({1,2,3,5,6} , {3,4,5,6,7,6}) do
--   c = c+1
--   if c == 10 then break end
--   print(i)
-- end

local function permutations(...)

  local sets = {...}
  local state = map(function(x)
                      return {x, #x , 1}
                    end , sets)
  state[#state][3] = 0

  local curr = #state

  return function()
    while true do
      if inc(state, curr) then
        return map(function(s)
                     return s[1][s[3]] end,
                   state)
      else
        return nil
      end
    end
  end
end


local function pinspect(x)
  print(require("inspect")(x))
end


-- for i in permutations({1,2}, {3,4}) do
--   print(i[1], i[2])
-- end

for perm in permutations({"GET" , "POST"}
  nil or {"/"},
  "hola"
                        )
