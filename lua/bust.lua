describe("a", function()
  setup(function()
    print("setup for a")
  end)
  before_each(function()
    print("before a")
  end)
  after_each(function()
    print("after a")
  end)
  it("does a", function()
     print("test for a")
  end)
  it("does another a", function()
     print("test for another a")
  end)

  describe("aa", function()
    setup(function()
      print("setup for aa")
    end)
    before_each(function()
      print("before aa")
    end)
    after_each(function()
        print("after aa")
    end)

    it("does aa", function()
      print("test for aa")
    end)

    it("does another aa", function()
      print("test for another aa")
    end)
  end)
end)

describe("b ", function()
  setup(function()
    print("setup for b")
  end)
  it("does b", function()
    print("test for b")
  end)
end)

-- setup for a
-- before a
-- test for a
-- after a
-- setup for aa
-- before a
-- before aa
-- test for aa
-- after aa
-- after a
-- setup for b
-- test for b
