package main

import (
	"github.com/codegangsta/martini"
	"labix.org/v2/mgo"
	"strings"
	"net/http"
	"fmt"
//	"labix.org/v2/mgo/bson"
 )


func get_db(session *mgo.Session) *mgo.Database {
	session.SetMode(mgo.Monotonic, true)
	db := session.DB("test")
	return db
}

func main() {
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

  m := martini.Classic()
  m.Get("/collections", func() string {
		db := get_db(session)
		names, _ := db.CollectionNames()
    return strings.Join(names, ", ")
  })

	m.Post("/collections/:col", func(params martini.Params, req http.Request) string {
	//	db := get_db(session)
		// db.C(params["col"]).Insert()
		//return fmt.Sprintf("%d", req.ContentLength)
		a := []byte{1,2,3,4,5}
		n, _ := req.Body.Read(a)
		return fmt.Sprintf("%d", n)
	})


  m.Run()
}
