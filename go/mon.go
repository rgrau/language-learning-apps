package main
import ( "fmt"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

func get_db(session *mgo.Session) *mgo.Database {
	session.SetMode(mgo.Monotonic, true)
	db := session.DB("test")
	return db
}

func main() {
	var m []bson.M

	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)

	db := get_db(session)
	err = db.C("people").Find(bson.M{}).All(&m)

	// check(err)
	for key, value := range m {
    fmt.Println(key, value)
	}

	var o bson.M
	err = db.C("people").Find(bson.M{"name" : "Ale" }).One(&o)

	// check(err)
	for key, value := range o {
    fmt.Println(key, value)
	}


}
