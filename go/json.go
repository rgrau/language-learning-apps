package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Message struct {
    Name string
    Body string
    Time int64
}

func main() {
	var buffer [256]string

	slice := buffer[0:2]
	slice[0] = "sa"
	slice[1] = "osaf"
	fmt.Printf(strings.Join(slice, ", "))
	fmt.Println("")

	m := Message{"Alice", "Hello", 1294706395881547000}
	other := map[string]interface{"hola" : "tiu"}
	b, _ := json.Marshal(other)
	fmt.Printf("%s", b)
	fmt.Println("")


	b = []byte(`{"Name":"Wednesday","Age":6,"Parents":["Gomez","Morticia"]}`)
	var f interface{}
	_ = json.Unmarshal(b, &f)
	fmt.Println(f)
}
