package main

import("fmt"
	"io"
	"regexp"
	"os")


func conta_a(filename string) int {
	cont := 0
	c2 := 0
	buff_size := 100
	data := make([]byte, buff_size)
	file, err := os.Open(filename)

	var as = regexp.MustCompile(`a`)

	numbytes, err := file.Read(data)
	for !(err == io.EOF) {
		c2 += numbytes
		cont += len(as.FindAll(data, -1))
		numbytes, err = file.Read(data)
	}

	return cont
}

func main() {
	fmt.Println("HOLA")
	//fmt.Println(os.Open("basic.go"))
	var as = regexp.MustCompile(`a`)
	var foo = []byte("aaa123fsaaaa")
	cont := len(as.FindAll(foo, -1))
	fmt.Println(cont)
	fmt.Println(conta_a("basic.go"))

}
