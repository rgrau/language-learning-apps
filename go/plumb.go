package main

// Import declaration declares library packages referenced in this file.
import (
	"fmt"       // A package in the Go standard library.
	"regexp"
	"os/exec"
    // "io/ioutil" // Implements some I/O utility functions.
    // m "math"    // Math library with local alias m.
    // "net/http"  // Yes, a web server!
    // "os"        // OS functions like working with the file system
    // "strconv"   // String conversions.
)


type command struct {
	  regex string
	  run_cmd string
}

func run(prog, arg string) {
	cmd := exec.Command(prog)
	cmd.Run()
}

func main() {

	// var commands []command

	// commands["^.*git.*$"] = func(path string) int {
	// 	run("/usr/bin/xclock", "a")
	// }
	input := "git@git.com"
	fmt.Println("HOOLA")

	// i := 0
	// for i <= 3 {
	// 	fmt.Println(i)
	// 	i = i+1
	// }

	match, _ := regexp.MatchString("^git@git.*kong.*\\.git$", input)
	if match {
		run(fmt.Sprintf("git clone %s ~/workspace", input))
	} else if regexp.MatchString("", input) {
		fmt.Println("A")
	} else {
		fmt.Println("nope")
	}

	return
}
