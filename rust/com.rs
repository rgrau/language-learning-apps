
use std::process::Command;
use std::path::Path;
fn main() {
    // Create our own local event loop
    let mut core = Core::new().unwrap();

    // Use the standard library's `Command` type to build a process and
    // then execute it via the `CommandExt` trait.
    let v : Vec<String> = vec!["xclock".to_string(), "-digital".to_string()];
    let mut &child = Command::new(&v[0]);


        // for i in 1..c.command.len()-1 {
        //     child.arg(&c.command[i]);
        // }

        ;
    child.current_dir("/tmp").spawn_async(&core.handle());

    // Make sure our child succeeded in spawning
    let child = child.expect("failed to spawn");

    match core.run(child) {
        Ok(status) => println!("exit status: {}", status),
        Err(e) => panic!("failed to wait for exit: {}", e),
    }
}
