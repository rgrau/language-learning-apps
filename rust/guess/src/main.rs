use std::io;

fn printit(it: &u16) {
    //it = *it + 1;
    println!("{}", it+1);
}

fn guess() {
    println!("Guess the number!");
    println!("Input your guess:");

    let mut guess = String::new();
    let mut foo = String::from("a");
    let mut bar = 1;

    foo = String::from("b");

    printit(&bar);
    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    let guess: u32 = guess.trim().parse()
        .expect("Please type a number!");

     println!("You guessed: {}", guess);

}

fn fib(n: u32) -> u32 {
    43
}

fn ref_tests() {
    let mut s = String::from("hello");

    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
    println!("{}", some_string);
    println!("{}", *some_string);
}

fn pref(r: &String) {
    // let a : () = r;
    println!("{}", r.len()); // len is defined for String refs?
    println!("{}", (*r).len()); // len is defined for String
    println!("{}", r);
    println!("{}", *r);
    let rr = (*r).clone();
    pstr(rr);
    // pstr(*r);  ///  cannot move out of borrowed content
}

fn pstr(r: String) {
    println!("{}", r.len());
    println!("{}", r);
}


#[derive(Debug)]
struct Person {
    name: String,
    age: u32
}

impl Person {
    fn foo(&self) {
        println!("AAA")
    }

    fn bar(self) -> Person {
        println!("AAA");
        return self
    }
}

// My big question is about the expression problem: I have 2 kinds of
// commands, and for all them I want to be able to call 'run'.

// The different enum values have different attributes, so I need a
// different function for each one of those. in this case I'm printing
// the command or the cwd. If I'd like to call different functions
// depending on those.

// if they would be unit types like in
// https://internals.rust-lang.org/t/impl-trait-for-enum-variant/4131,
// everything can be implemented under EAppCommand, but having
// different attibutes and values, I can't have a function that gets
// an EAppCommand and does TheRightThing without the compiler
// complaining.

#[derive(Debug)]
enum EAppCommand {
    Start{command: Vec<String>, cwd: String},
    Stop{command: Vec<String>}
}

impl EAppCommand {
    fn run(&self){
        match self {
            &EAppCommand::Start{command: ref x, cwd: ref y} => println!("{:?}", y),
            &EAppCommand::Stop{command: ref x} => println!("{:?}", x)
        }
    }
}

// impl EAppCommand::Start {
//     fn run(&self) {
//         println!("start!");
//     }
// }

// impl EAppCommand::Stop {
//     fn run(&self) {
//         println!("stop!");
//     }
// }

fn commands() {
    let c = EAppCommand::Start{
        command: vec!["a".to_string(), "b".to_string()],
        cwd: String::from("hola")
    };
    c.run();
    let c = EAppCommand::Stop{
        command: vec!["a".to_string(), "b".to_string()]
    };
    c.run();
}

fn main() {
    // let mut ss = String::from("ss");
    let s = String::from("db");
    //println!("{}, {}", (&s).len(), s.len());
    // pstr(s);
    // ref_tests();
    // pref(&ss);
    // pstr(ss);

    let p1 = Person{
        name: String::from("hubba bubba"),
        age: 12
    };

    println!("{}: {:?}", s, p1);

    let p2 = Person{
        age: 12,
        ..p1

    };
    println!("{}: {:?}", s, p2);
    p2.foo();
    p2.bar();


    commands();
}
