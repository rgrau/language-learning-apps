fn main() {
    let mut mine: Box<i32> = Box::new(3);
    *mine = 5;
    println!("{}", &mine);
    let mut now_its_mine = mine;
    *now_its_mine +=1;
    println!("{}", now_its_mine);
    // println!("{}", &mine); -> error
    // value used here after move
    let mut var = 4;
    println!("{}", var);
    var = 3;
    println!("{}", var);
    let ref_var: &i32 = &var;
    println!("{}", var);
    println!("{}", ref_var);
    let mut ref_var2 = ref_var;
    println!("{}", ref_var2);
    *ref_var2 += 1;

}
