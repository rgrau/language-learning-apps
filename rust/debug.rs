#[derive(Debug)]
struct Structure(i32);

#[derive(Debug)]
struct Deep(Structure);

fn main() {
    println!("{} is my name", "Raimon");
    println!("Although my full name is {0} {1}, sometimes they write {1}, {0}", "Raimon", "Grau");
    println!("This is {amazing}", amazing="Amaaaaaaazinnng!");
    println!("{:?} months in a year.", 12);
    println!("{:?}", Structure(3));
}
