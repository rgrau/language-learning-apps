(load "@lib/debug.l")
(de fact (n)
    (if ( = 1 n) 1
        (* n (fact (- n 1)))))

(de fact-a (n a)
    (if ( = 1 n) 1
        (fact-a (- n 1) (* n a))))

(de fact-r (n a)
    (recur (n a)
           (if (= 1 n) a
               (recurse (dec n) (* n a)))))

(de fact-r1 (n)
    (recur (n)
           (if (= 1 n) 1
               (* n (recurse (dec n))))))

(de fact-it (n)
    (apply * (range 1 n)))


#(bench (fact-r 10000 1))
#(bench (fact-r1 10000 ))
#(bench (fact-a 10000 ))
#(bench (fact 10000 ))

#(bench (fact-r 100000 1))
#(bench (fact-r1 100000 ))
#(bench (fact-a 100000 ))
#(bench (fact 100000 ))

(de product (From To)
    (let (delta (- To From)
                foo delta)
      (if (< 5 (- To From ))
          (apply * (range (+ 1 From) To))
          (* (product From (- (/ (+ From To) 2) 1))
             (product (/ (+ From To) 2) To)))))

(bench (product 1 100000))
(bench (fact-it 100000 1))

(de cadddr (x)
    (car (cdr (cdddr x))))

#(prinl
# (bench (prog
#           (product 1 10000)
#           (product 1 10000)
#           (product 1 10000)
#           (product 1 10000)
#           (product 1 10000)
#           (product 1 10000)
#           (product 1 10000))))
#
#(prinl
# (bench (prog
#           (fact 10000)
#           (fact 10000)
#           (fact 10000)
#           (fact 10000)
#           (fact 10000)
#           (fact 10000)
#           (fact 10000))))


(bye)
