(de sleep (ms)
    (wait ms))

(de cat (f)
    (in f (line T)))

(unless (fork) (call 'mplayer "/home/pi/intergalactic.mp3") (bye))

(loop
  (T (=0 (format (cat "/sys/class/gpio/gpio4/value")))
         (call 'pkill "-9" "mplayer"))
  (sleep 500)
  (prinl "waitingggg"))
(bye)

echo "7" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio7/direction
echo "4" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio4/direction
