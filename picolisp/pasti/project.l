# (setq lisp-indent-offset 3)
(setq Milli "//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css")
# pil @lib/http.l @lib/xhtml.l @lib/form.l project.l -go -em +

(de encode (Str)
    (pack
     (mapcar '((B) (pad 2 (hex B)))
             (native "libcrypto.so.1.0.0" "MD5" '(B . 16) Str (length Str) '(NIL (16))) ) ))

(de pastie ()
   (app)
   (action
      (html 0 "Form" Milli NIL
         (form NIL
            (gui 'cntnt '(+TextField) 200 30)
            (gui '(+Chk +Button)
               '(or (and
                       (< (length (chop (val> (: home cntnt)))) 1)
                       "EMPTY Field!"))
               "Pastie!"
               '(url "!show"
                   (basename (store-pastie (val> (: home cntnt))))))))))

(de basename (Str)
   (pack (car (split (chop Str) "."))))

(de show (Fname)
   (html 0 "nose" Milli NIL
      (<href> "back" "!pastie")
      (<pre> NIL
         (in (pack Fname ".txt")
            (until (eof)
               (prinl (line T)))))))

(de store-pastie (Content)
   (let
      (Id (encode Content)
       ts (pack (dat$ (date))
             (time)
             "-" Id ".txt"))
      (out (pack (pwd) "/" ts)
         (prin Content))
      ts))

(de go ()
   (server 8080 "!pastie"))
