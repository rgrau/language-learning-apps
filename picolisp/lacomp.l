# http://pleac.sourceforge.net/pleac_picolisp/index.html
# Naming Conventions
#
# It was necessary to introduce - and adhere to - a set of conventions for PicoLisp symbol names. Because all (internal) symbols have a global scope (there are no packages or name spaces), and each symbol can only have either a value or function definition, it would otherwise be very easy to introduce name conflicts. Besides this, source code readability is increased when the scope of a symbol is indicated by its name.
#
# These conventions are not hard-coded into the language, but should be so into the head of the programmer. Here are the most commonly used ones:
#
#     Global variables start with an asterisk "*"
#     Global constants may be written all-uppercase
#     Functions and other global symbols start with a lower case letter
#     Locally bound symbols start with an upper case letter
#     Local functions start with an underscore "_"
#     Classes start with a plus-sign "+", where the first letter
#         is in lower case for abstract classes
#         and in upper case for normal classes
#     Methods end with a right arrow ">"
#     Class variables may be indicated by an upper case letter
#
# For example, a local variable could easily overshadow a function definition:
#
#
# : (de max-speed (car)
#    (.. (get car 'speeds) ..) )
# -> max-speed
#
# Inside the body of max-speed (and all other functions called during that execution) the kernel function car is redefined to some other value, and will surely crash if something like (car Lst) is executed. Instead, it is safe to write:
#
#
# : (de max-speed (Car)            # 'Car' with upper case first letter
#    (.. (get Car 'speeds) ..) )
# -> max-speed
#
# Note that there are also some strict naming rules (as opposed to the voluntary conventions) that are required by the corresponding kernel functionalities, like:
#
#     Transient symbols are enclosed in double quotes (see Transient Symbols)
#     External symbols are enclosed in braces (see External Symbols)
#     Pattern-Wildcards start with an at-mark "@" (see match and fill)
#     Symbols referring to a shared library contain a colon "lib:sym"
#
# With that, the last of the above conventions (local functions start with an underscore) is not really necessary, because true local scope can be enforced with transient symbols.


# Fact
(de fact (X)
    (if (=0 X) 1
        (* X (fact (- X 1)))))

(prinl (fact 10))


(when (=0 0) (prinl "yes"))
(unless (=0 1) (prinl "yes"))
(let foo 1
     (println (+ 2 foo)))

(let (foo 1 bar 2)
     (println (+ bar foo)))

(def 'param1 '(b 1 2))
(def 'autoquotes-number-lists (1 1 2))
(println autoquotes-number-lists)
#(println (pp 'fact))

(let (i 0 j 0)
 (while (=0 i)
   (setq j (+ j 1))
   (when (= 10 j) (inc 'i 2)))
 (println i j))

#unevaled params
(de my-fun X
    X)

(println (my-fun '((a b c) (d e f))))

(setq no-lambda '((X Y) (* (+ X Y) (+ X Y))))
(println (no-lambda 3 4))

(class +Animal)
(dm T (N)
    (=: name N))

(dm talk> ()
    "...")

(class +Dog +Animal)
(dm talk> ()
    "guau")

(class +Cat +Animal)
(dm talk> ()
    "Meowww")

(setq Bobby (new '(+Dog) "Bobby"))
(show Bobby)
(println (talk> Bobby))

(setq Fauna (list Bobby (new '(+Cat) "frisky") (new '(+Animal) "no-name") ))

(println (mapcar 'talk> Fauna ))

(de compose (F G)
   (curry (F G) (X)
      (F (G X)) ) )

# https://rosettacode.org/wiki/Function_composition#PicoLisp
# http://software-lab.de/doc/faq.html#dynamic
(def 'ptalk (compose 'println 'talk>))

((compose 'println 'talk>) Bobby)  # everything quoted cause otherwise dynamic scope trumps us
(ptalk Bobby)

(class +SuperPowered)
(dm talk> ()
    (uppc (extra)))

(setq Milo (new '(+SuperPowered +Dog) "Milo"))

(setq Fauna (cons Milo Fauna))
(println "---------")
(mapcar 'ptalk Fauna)

(pool "test.db")


#(bye)

#(class Episode)
#(setq )



(bye)
