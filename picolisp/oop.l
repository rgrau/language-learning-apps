(class +Animal)
(class +Person +Animal)
(dm talk> ()
    "Hello")

(class +Dog +Animal)
(dm talk> ()
    "Woof")

(class +Loud)

(dm talk> ()
    (uppc (extra)))

(println (talk> (new '( +Person))))
(println (talk> (new '(+Loud +Person))))
(bye)
