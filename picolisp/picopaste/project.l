(de encode (Str)
    (pack
     (mapcar '((B) (pad 2 (hex B)))
             (native "libcrypto.so.1.0.0" "MD5" '(B . 16) Str (length Str) '(NIL (16))) ) ))

(when (app)                # On start of session
   (class +Pasti +Entity)    # Define data model
   (rel nm (+String))      # with a name
   (rel content (+String))      # and a content
   (pool (tmp "db"))       # Create temporary DB

   (push '*Mimes (list (chop"txt") "text/plain"))

   (prinl *Obj)
   (setq *Obj              # and a single object
      (new! '(+Pasti) 'nm "" 'content "New Object") ))

(action
 (html 0 "+E/R" "@lib.css" NIL
       (form NIL
             (gui 'a '(+E/R +TextField) '(nm . *Obj) 70)
             (gui 'file '(+TextField) 20 )
             (gui 'b '(+JS +Button) "Show"
                  '(let ( Fname (glue "." (list (encode (val> (: home c))) "txt")))
                    (out Fname
                     (prinl (val> (: home c))))
                    (url Fname)))
             (----)
              (gui 'c '(+E/R +TextField) '(content . *Obj) 100 50)
              )))
