(class +User +Entity)
(rel nm (+Need +Sn +Idx +String))
(rel pasties (+List +Joint) author (+Pastie))

(class +Pastie +Entity)
(rel content (+Ref +String))
(rel author (+Joint) pasties (+User))

(de main ()
    (pool "pasties.db")
    (let (a (new! '(+User) 'nm "rgrau"))
      (new! '(+Pastie) 'content "This is my content" 'author a)
      (new! '(+Pastie) 'content "This is my content2" 'author a)
      (new! '(+Pastie) 'content "This is my content232" 'author a)))

(main)
