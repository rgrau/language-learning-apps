(app)
## (pool "test.db")
## (class +Pastie +Entity)
## (rel text (+String))
## (set *DB (request '(+Pastie) 'text *Text))
## (commit)

#(action
# (html 0 "Form" "@lib.css" NIL
#       (form NIL
#             (<h1> NIL "Hello, web")
#         (<br> "")
#         (<p> NIL
#              *Text)
#         (gui 'a '(+TextField) 10)
#         (gui '(+Button) "Pring"
#              '(msg (val> (: home a)))))))

(action
   (html 0 "Case Conversion" "@lib.css" NIL
      (form NIL
         (<grid> 2
            "Source" (gui 'src '(+TextField) 30)
            "Destination" (gui 'dst '(+Lock +TextField) 30) )
         (gui '(+JS +Button) "Upper Case"
            '(set> (: home dst)
               (uppc (val> (: home src))) ) )
         (gui '(+JS +Button) "Lower Case"
            '(set> (: home dst)
               (lowc (val> (: home src))) ) ) ) ) )
