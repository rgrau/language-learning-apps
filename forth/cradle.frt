
0 VALUE Look


: getchar  ( -- )          EKEY TO Look ;
: error    ( c-addr u -- ) CR  EMIT ." Error: " TYPE ." ." ;
: aborts   ( c-addr u -- ) error ABORT ;
: expected ( c-addr u -- ) S"  Expected" $+ aborts ;
: alpha?   ( char -- tf )  >UPC 'A' 'Z' 1+ WITHIN ;
: digit?   ( char -- tf )  '0' '9' 1+ WITHIN ;
: emits    ( c-addr u -- ) Tab EMIT TYPE ;
: emitln   ( c-addr u -- ) CR emits ;
: init     ( -- )          getchar ;




: match ( char -- )
        DUP Look = IF  DROP getchar
                 ELSE  S" `" ROT CHAR-APPEND &' CHAR-APPEND expected
                ENDIF ;


: getname ( -- char )
        Look alpha? 0= IF  S" Name" expected  ENDIF
        Look >UPC
        getchar ;


: getnum ( -- char )
        Look digit? 0= IF  S" Integer" expected  ENDIF
        Look '0' -
        getchar ;



: cradle ( -- ) init ;

:ABOUT  CR ." Try: cradle -- eat a single character" ;

DEPRIVE
.ABOUT -cradle CR
