class Foo
  attr_accessor :param

  def initialize(p)
    self.param = p
  end

  def ret(x = self.param)
    x
  end

  def accum_params(x, y = x.ret)
    y
  end
end

one = Foo.new(4)
p one.ret()
two = Foo.new(5)
p two.accum_params(one)
