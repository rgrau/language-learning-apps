class Heap
  attr_accessor :data
  def initialize
    @data = [:ghost]
  end

  def insert(e)
    data.push(e)
    float(data.size-1)
    self
  end

  def extract_min
    data[1], data[-1] = data[-1], data[1]
    r = data.pop
    sink(1)
    r
  end

  def inspect
    puts "from here: ", data , "to here."
  end


  protected

  def switch(pos1, pos2)
    data[pos1], data[pos2] = data[pos2], data[pos1]
  end

  def float(pos)
    return if pos == 1
    parent = pos / 2
    if data[pos] < data[parent]
      switch(pos, parent)
      float(parent)
    end
  end

  def sink(pos)
    elem = data[pos]
    return if data[pos*2] == nil
    if data[(pos*2)+1] == nil
      smallest = pos*2
    else
      left, right = [data[pos*2], data[(pos*2)+1]]
      smallest =  (left < right) ? pos*2 : (pos*2)+1
    end

    if data[smallest] != nil && data[smallest] < elem
      switch(smallest, pos)
      sink(smallest)
    end
  end
end

mh = Heap.new
#[1,5,3,4,0].each{|x| mh.insert(x)}
[1,5,3,4,0].each{|x| mh.insert(x)}
puts mh.inspect
5.times {puts(mh.extract_min) }

# a =  Heap.new.insert(2).insert(1).insert(5).insert(0)
# # a.inspect
# 5.times{ puts a.extract_min }

class PQueue
  def initialize
    @heap = Heap.new
  end
end

class Graph
  attr_accessor :vertices
  def initialize
    @vertices = {}
  end

  # vertex[n] = {'A' => 4, 'B' => 2}
  def add_vertex(n, v)
    @vertices[n] = v
  end
end

class Search

  def setup(g, from)
    @distances = {}
    @nodes = Heap.new
    @prev = {}
    g.vertices.each do |v, n|
      @distances[v] = Float::INFINITY
      @distances[v] = Float::INFINITY

    end
  end

  def dijkstra(g, from, to)
    setup(g, from)
  end
end
