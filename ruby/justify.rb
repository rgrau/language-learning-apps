$str1 = "hola tiu que tal anempue smoltbe i tu com vas"
$words = $str1.split(' ')
$fill_at = 20

def sum_length(a)
  a.map{|x| x.length}.reduce(:+)
end

def line_fits(w_l)
  if (w_l.join(' ').length <= $fill_at) or (w_l.length == 1)
    return w_l
  end
end

def spaces_for(w_l, spaces)
  return [] if w_l.length == 1
  gaps = Array.new((w_l.length)-1){0}
  spaces.times do |i|
    gaps[i%gaps.length] +=1
  end
  gaps.map{|a| " " * a}
end

def inject_spaces(w_l)
  spaces = w_l.map{|line| spaces_for(line, $fill_at - sum_length(line))}
  res = []
  w_l.each_with_index do |line,i|
    res << line.zip(spaces[i]+[""]).flatten.join
  end
  res
end

def fill_par(word_list)
  if line_fits(word_list)
    return [word_list]
  else
    -2.downto(-1 * word_list.length).each do  |i|
      if ( cur = line_fits(word_list[0..i]) &&
                 ( others = fill_par(word_list[i+1..-1])))

        return word_list[0..i], *others
      end
    end
    return nil
  end
end



puts inject_spaces(fill_par($words)).join("\n")
