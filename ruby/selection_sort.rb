class SelSort
  attr_reader :data

  def initialize(data)
    @data = data
  end

  def sort
    sel_sort(@data); self
  end

  def sel_sort(arr)
    s = arr.size
    s.times do |from|
      min_pos = nil
      from.upto(s-1) do |checked_pos|
        min_pos = ((arr[min_pos || checked_pos]) < arr[checked_pos]) ? min_pos : checked_pos
      end
      arr[from], arr[min_pos] = arr[min_pos], arr[from]
    end
  end
end

a = [4,2,1]
SelSort.new(a).sort
puts a

class Qsort
  def sort(arr)
    return [] if arr.size == 0
    x, *xs = *arr
    # less, more = xs.partition {|y| y < x}
    less, more = mpartition(xs) {|y| y < x}
    sort(less) + [x] + sort(more)
  end

  def mpartition(list, &block)
    less, more = [], []
    list.each do |elem|
      if block.call(elem)
        less << elem
      else
        more << elem
      end
    end
    [less, more]
  end
end
a = [4,2,1]
puts Qsort.new.sort(a)

class HillClimb
  attr_accessor :ranges, :num_it, :fitness
  def initialize(num_it, ranges, &block)
    @num_it = num_it
    @ranges = ranges
    @fitness = block
  end

  def mutate(code)
    pos = Random.rand(code.size)
    code[pos] = Random.rand(ranges[pos][0]..ranges[pos][1])
    code
  end

  def find()
    initial = {}
    best = { code: Array.new(ranges.size) { |i| Random.rand(ranges[i][0]..ranges[i][1]) },
             score: nil}

    num_it.times do |n|
      current = {}
      current[:code] = mutate(best[:code])
      current[:score] = fitness.call(current[:code])
      if best[:score].nil? or current[:score] > best[:score]
        best = current
      end
    end
    best
  end
end

puts HillClimb.new(10, [[0,10], [8, 18], [-10, 10]]) {|x| x.reduce(:+) }.find


# Fallos
# NO Array .sum ! array.reduce(:+)
# nils!
# multidimensional
# attr_accessors
# return values of the elements!
# Random.rand(a..b)

dict = ['hola', 'adeu', 'adios']

str="holaadeu"

def startswith(str, word, pos = 0)
  str.index(word, pos) == 0
end

def split_to_strings(str, dict)
  if str == ''
    return ""
  end
  dict.each do |word|
    if startswith(str, word)
      puts "starts with #{word}"
      if suffix = split_to_strings(str[word.size..-1], dict)
        return "#{word} #{suffix}"
      end
    end
  end
  nil
end

puts(split_to_strings(str, dict))


def is_pal?(w)
  if w == w.reverse
    w
  end
end

def sip(str)
  return '' if str == ''
  l = str.size
  (l-1).downto(0) do |pos|
    if prefix = is_pal?(str[0..pos])
      if sufix = sip(str[pos+1..-1])
        return "#{prefix} #{sufix}"
      end
    end
  end
  nil
end

# return values of all subfunctions, careful with bools
# upto vs downto
# offbyones

puts(sip("bbaabaaabb"))

class Node
  attr_accessor :left, :right, :content
  def initialize(n)
    @content = n
  end

  def sprint
    puts @content
    puts "  #{@left.sprint}"
    puts "  #{@right.sprint}"
  end

end

def insert(t, n)
  return Node.new(n) if t.nil?
  if t.content > n
    t.left = insert(t.left, n)
  else
    t.right = insert(t.right, n)
  end
  t
end

def traverse(t, &block)
  if t.nil?
    return ""
  end
  traverse(t.left, &block)
  block.call(t.content)
  traverse(t.right, &block)
end

t_vals = [1,2,3,4,5,6,7,8,9,0]
t_vals = [5,2,6]
tree = []
tree = Node.new(t_vals.shift)
t_vals.each {|n| insert(tree, n)}
traverse(tree){|n| puts n}      # should be printed sorted

def valid?(b)
  # puts b.inspect
  pairs = []
  b.each_with_index do |val, index|
    pairs << [index, val]
  end

  pairs.each do |(i,v)|
    (i+1).upto(b.size-1) do |other|
      if (i-other).abs == (v-b[other]).abs # DIAGONAL must be the
                                           # absolute of the
                                           # diferences
        return false
      end
    end
  end

  b.uniq == b
end

def queen(n, total)
  return nil if total < 4 or n > total
  b = []
  fill_b(b, n, total)
end

def fill_b(b, n, total)
  return b if n == 0
  (0..total-1).each do |a|
    if valid?(b+[a])
      puts (b+[a]).inspect
      if suffix = fill_b(b+[a], n-1, total)
        return suffix
      end
    end
  end
  nil
end

print queen(8, 8)
# Backtracking has following phases
# 1) setup of the problem
# 2) validity of partial solution
# 3) recursiveness (try to use 'persistent' instead of in-place)
# 4) for all choices, I try to choose mine, that validates with what I've been passed
# 5) if valid, I call the recursive call with my choice.
# 6) IF THE RECURSIVE CALL SUCCEEDS I return the recursive solution
# 7) else, I keep searching another valid solution for me
# 8 return nil if couldn't find any of my solutions that is compatible with future solutions

puts()

class MinHeap
  attr_accessor :block
  def initialize
    @data = [nil]
  end

  def insert(v)
    @data << v
    float(@data.size-1)
  end

  def float(last_pos)
    return if last_pos == 1
    if last_pos.odd?
      against = (last_pos-1)/2
    else
      against = last_pos/2
    end
    if @data[last_pos] < @data[against]
      @data[last_pos], @data[against] =  @data[against], @data[last_pos]
    end

    float(against)
  end

  def extract
    return nil if @data.size == 2
    res = @data[1]
    @data[1] = @data.pop
    sink(1)
    return res
  end

  # Sink MUST look at the 2 children and pick the smallest!!
  def sink(from)
    return if from*2 > @data.size-1
    if @data[from*2+1]
      smallest_child = @data[from*2] < @data[from*2+1] ? from*2 : from*2+1
    else
      smallest_child = from*2
    end

    if @data[from] > @data[smallest_child]
      @data[from], @data[smallest_child] = @data[smallest_child], @data[from]
       sink(smallest_child)
    end
  end
end

mh = MinHeap.new

[1,5,3,4,0].each{|x| mh.insert(x)}
10.times {puts(mh.extract)}

# <rbistolfi> rgrau: hey  [14:39]
# <rbistolfi> rgrau: prueba usando "time complexity of backtracking" como search term. Parece que las
#             implementaciones naive son O(n!) y quizas O(n^n) pero se puede hacer en O(n^2)  [14:54]
# <rbistolfi> aha, parece que la optimizacion no esta disponible en todos los casos  [14:56]
# <rbistolfi> creo que si uno quita todo el fuzz, se queda calculando permitaciones adentro de un for
#             loop, que es O(n!) no? for X1 .. Xni: permutar(Xi)  [15:02]
# <rgrau> puede que si  [15:15]
# <rgrau> si, tiene sentido
# <rgrau> pero confundo n^n con n*n
# <rgrau> unque claramente es mas que n^2
# <rgrau> tio, me doy cuenta haciendo ejercicios que he perdido mucha soltura en este tipo de
#         problemas  [15:17]



# https://en.wikipedia.org/wiki/Smoothsort
# and count sort

# Astar.
# Heuristic has to be admissible (not overestimate cost)
# if the heuristic is not consistent, using closedSet can lead to unoptimal solution (c(a,a') = h(a')-h(a) )
