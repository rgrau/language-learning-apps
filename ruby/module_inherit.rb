module Inh
  def foo
    conflict
  end

  def conflict
    "Inh"
  end
end

class Cl
  include Inh
  def baz
    foo()
  end

  def conflict
    "CL"
  end
end

puts Cl.new.baz
