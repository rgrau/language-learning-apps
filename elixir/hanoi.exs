defmodule Hanoi do
  def hanoi(from,to,aux, 1), do: move(from, to)
  def hanoi(from,to,aux, n) do
    hanoi(from, aux, to, n - 1)
    move(from, to)
    hanoi(aux, to, from, n - 1)
  end

  def move(from, to) do
      IO.puts("move from #{from} to #{to}")
  end
end

Hanoi.hanoi(1,3,2 , 3)
