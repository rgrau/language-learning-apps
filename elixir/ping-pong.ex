defmodule PingPong do
  def ping(n) when n > 0 do
    receive do
      {sender, :ping} ->
        IO.puts("getting ping with number #{n}. sending pong")
        sender <- {self, :pong}
      {sender, :pong} ->
        IO.puts("getting pong with number #{n}. sending ping")
        sender <- {self, :ping}
    end
    ping(n - 1)
  end
  def ping(n) when n <= 0 do
    IO.puts("STOP!")
  end
end

pid = spawn(PingPong, :ping , [3])
pid <- { pid , :ping}
