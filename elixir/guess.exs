defmodule Guess do
  def guess(correct_value // :random.uniform) do
    IO.puts("Choose number:")
    {result , rest } = String.to_integer(String.rstrip(IO.readline()))
    say_what(result , correct_value)
  end

  def say_what(try, correct_value) when correct_value < try do
    IO.puts "correct_value => #{correct_value}, try => #{try}"
    IO.puts "too big"
    guess(correct_value)
  end
  def say_what(try, correct_value) when correct_value > try do
    IO.puts "too small"
    guess(correct_value)
  end

  def say_what(try, correct_value), do: IO.puts("YEAH!")

  def start(range) do
    from..to = range
    :random.seed(:erlang.now)
    guess(:random.uniform(to - from) + from )
  end
end

r = 1..100
Guess.start(r)

defmodule Player do
  def guess(opponent_pid, range) do
    IO.puts("hello")
  end

end
