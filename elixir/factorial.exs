defmodule Fact do
  def factorial(0), do: 0
  def factorial(1), do: 1
  def factorial(n) when n > 0 do
    n * factorial(n - 1)
  end
end
IO.puts(Fact.factorial(10))
