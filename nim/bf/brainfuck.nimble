# Package

version     = "0.1.0"
author      = "Raimon Grau"
description = "BF implementation"
license     = "MIT"
bin         = @["brainfuck"]

# Dependencies

requires "nim >= 0.16.0", "docopt >= 0.1.0"
