#!/usr/bin/env bash

VAR=0
on_exit() {
    # if ! [ -z $1 ]; then
    #     echo "exit $1"
    # fi
    echo $VAR
    export PS1='>> '
    bash
}

foo() {
    local a=1
    trap "on_exit $a" RETURN
    echo "foo"
}

bar () {
    echo "bar"
}

main () {

    echo "pre"
    ((VAR++))
    foo
    ((VAR++))
    bar
    ((VAR++))
    echo "post"
    ((VAR++))
}

main

# ./trap_fun.sh
# pre
# foo
# exit 1
# bar
# post
# exit 1
