#!/usr/bin/env bash
source ~/programmingStuff/bash-fun/src/fun.sh

flags() {
    echo "$@" | filter lambda a . '[[ $a == -* ]] && ret true || ret false'
}

main () {
    f=$(flags "$@")
}

main "$@"
