#!/bin/sh

tap_inited=false
tap_i=0
tap_exitstatus=0

tap_printline()
{
    [ "$1" -eq 0 ] && printf "ok %d - %s\n" $tap_i "$current" && return;

    printf "not ok %d - %s\n" $tap_i "$current"
    tap_exitstatus=1
}

tap_test() {
    lastexitstatus=$?

    ! $tap_inited && current="$1" && tap_inited=true && tap_pretest && return;

    tap_i=$((tap_i+1))

    tap_printline $lastexitstatus
    tap_posttest
    current=$1
    tap_pretest
}

tap_printplan() {
    printf "1..%s\n" "$tap_i"
}

tap_finish() {
    lastexitstatus=$?
    [ -z "$current" ] && exit
    tap_printline $lastexitstatus
    tap_printplan
    exit $tap_exitstatus
}

trap tap_finish EXIT

tap_pretest() {
    true
}


tap_posttest() {
    true
}

# tap_test "one plus zero is zero"
# sum=$((1+0))
# [ $sum -eq 0 ]

tap_test "one plus one is two"
sum=$((1+1))
[ $sum -eq 2 ]

tap_test "one plus one is three"
sum=$((1+1))
[ $sum -eq 3 ]

tap_test "two plus two is four"
sum=$((2+2))
[ $sum -eq 4 ]
