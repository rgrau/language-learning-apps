module Main where
  allEven :: [Integer] -> [Integer]
  allEven [] = []
  allEven (h:t) = if even h then h:allEven t else allEven t

  evenPos :: [a] -> [a]
  evenPos [] = []
  evenPos (h:t) = oddPos t

  oddPos :: [a] -> [a]
  oddPos [] = []
  oddPos (h:t) = h:evenPos t