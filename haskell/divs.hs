module Main where
import System.Environment
import System.IO

len :: String -> Int
len s = length s


openfile :: String -> IO ()
openfile s = do fh <- openFile s ReadMode
                contents <- hGetContents fh
                hClose fh

main :: IO ()
main = do l <- getLine
          contents <- openfile l
          putStrLn contents
          -- args <- getArgs
          -- fh <- openFile (args !! 0) ReadMode
          -- contents <- hGetContents fh
          -- mapM_ putStrLn (lines contents)
          -- hClose fh
