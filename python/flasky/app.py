from flask import Flask
import click


app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.cli.command()
@click.argument('name')
def create_user(name):
    print(name)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
