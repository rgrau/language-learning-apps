;;;; helloworld.lisp

(in-package #:helloworld)

(defun hello-world ()
  (scan "a(a|b)*" "aaaaba"))

;;; "helloworld" goes here. Hacks and glory await!
