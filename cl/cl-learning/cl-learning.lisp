;;;; cl-learning.lisp

(in-package #:cl-learning)

(defclass <person> ()
  ((name :accessor name :initform 'name :initarg :name)
   (id :accessor id :initform 'id :initarg :id)))

(defclass <underage> (<person>) ())

(defgeneric shout (p what)
  (:documentation "shouts things, according to its age"))

(defmethod shout ((p <person>) what)
  (format nil "I am ~a and I say ~a." (name p) what))

(define-condition this-is-an-exception (error)
  ((text :initarg :text :reader text)))

(defun do-exception ()
  (error 'this-is-an-exception :text text))

(defun divide (x y)
  (handler-case (/ x y)
    (division-by-zero () nil)))

(divide 1 0)
;;; "cl-learning" goes here. Hacks and glory await!
