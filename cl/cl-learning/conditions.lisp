(in-package #:conditions)
(use #:cl)

;;; capturing a system condition
(defun divide (x y)
  (handler-case (/ x y)
    (division-by-zero () nil)))

(divide 1 0)

;;; Creating a custom condition and capturing it
(define-condition division-by-one (error)
  ((text :initarg :message :reader message)
   (dividend :initarg :divident :reader divident)))

(defun divide-custom (x y)
  (case y
    (1 (error 'division-by-one :message "division by one" :divident x))
    (t (divide x y))))

;;; division-by-one is raised
(divide-custom 1 1)

;;; adding restarts. change handler-case to restart-case
(defun divide-custom-with-restart (x y)
  (restart-case (divide-custom x y)
    (custom-restart-option () "restarted from here!")
    (use-value (value) value)))

(divide-custom-with-restart 1 1)
;;; can't offer it just for some errors. it's offered always
(divide-custom-with-restart 1 0)


;;; connecting handlers to restarts
(defun divide-custom-invoking-restart (x y)
  (handler-bind ((division-by-one #'(lambda (_) (invoke-restart 'custom-restart-option))))
    (divide-custom-with-restart x y)))

(divide-custom-invoking-restart 1 1)    ;calls the restart automatically
(divide-custom-invoking-restart 1 0)    ;offers debugger (with the custom-restart-option available)

;;; idiomatic way for connecting handlers to restarts with restart functions
(defun custom-restart-option (x)
  (invoke-restart 'custom-restart-option))

(handler-bind ((division-by-one #'custom-restart-option))
  (divide-custom-with-restart 1 1))

;;; continue
(handler-bind ((division-by-one
                #'(lambda (x) (use-value (divident x)))))
  (divide-custom-with-restart 32 1))

;;; rethrow TBD



;;; Example from http://c2.com/cgi/wiki?CommonLispConditionSystem that
;;; explains the special points of CL's condition system

(define-condition on-zero-denominator (error)
  ((message :initarg :message :reader message)))


(defun high-level-code ()
  (handler-bind
      ((on-zero-denominator
        #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'return-zero))))
    (determine-infinity))


  (handler-bind
      ((on-zero-denominator
        #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'return-value 1))))
    (determine-infinity))


  (handler-bind
      ((on-zero-denominator
        #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'recalc-using 2))))
    (determine-infinity))


  (handler-bind
      ((on-zero-denominator
        #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'just-continue))))
    (determine-infinity))


  (format t "Done."))


(defun determine-infinity ()
  (restart-case
      (let ((result 0))
        (setf result (reciprocal-of 0))
        (format t "Value: ~a~%" result))
    (just-continue () nil)))


(defun reciprocal-of (value)
  (restart-case
      (if (/= value 0)
          (/ 1 value)
          (error 'on-zero-denominator :message "cannot use zero"))


    (return-zero () 0)
    (return-value (r) r)
    (recalc-using (v) (reciprocal-of v))))
