(in-package :cl-user)

;;; Tests on what can be hot reloaded and what cannot

(defun tight-loop ()
  (loop for i from 1
    do (sleep 1)
    do (callit2 i)))

(defun callit (i)
                                        ;  (grab i)
  (grab i)
  (format t "HI! ~a~%"i))


(defvar hola nil)
(setq hola 43)

(defun callit2 (i)
  (progn
    (defvar hola nil)
    (when (null hola)
      (push i grabbed-stuff)
      (setf hola t)))
  (format t "HI! ~a~%"i))

(defparameter grabbed-stuff '())

(defmacro grab (var)
  (let ((unique-symbol (gensym)))
    (format t "~A~%" unique-symbol)
    `(progn
       (defvar ,unique-symbol nil)
       (if ,unique-symbol
           (progn
             (push ,var grabbed-stuff)
             (setf ,unique-symbol t))))))
