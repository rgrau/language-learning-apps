(defun range (max &key (min 0) (step 1))
   (loop for n from min upto max by step
         collect n))

(defun fact (n a)
  (if ( = n 1) a
      (fact (1- n) (* n a))))

(defun ff (n)
  (if ( = n 1) 1
      (* n(ff (- n 1)))))

(defun product (from to)
  (if (< 5 (- to from))
      (apply #'* (range to :min (+ 1 from)))
      (* (product from (floor (/ (+ from to) 2)))
         (product (ceiling (/ (+ from to) 2))  to))))
