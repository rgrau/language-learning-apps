(in-package :screamer)



(all-values
            (let ((a (an-integer-between 1 9))
                  (b (an-integer-between 1 9)))
              (if (= a b)
                  (list a b)
                  (fail))))

;; get a magic square, where all rows, cols, and diagonals sum the
;; same
;; a b c
;; d e f
;; g h i
(one-value
    (let ((a (an-integer-between 1 9))
          (b (an-integer-between 1 9))
          (c (an-integer-between 1 9))
          (d (an-integer-between 1 9))
          (e (an-integer-between 1 9))
          (f (an-integer-between 1 9))
          (g (an-integer-between 1 9))
          (h (an-integer-between 1 9))
          (i (an-integer-between 1 9)))
      (if (and
           (= (+ a b c) (+ d e f) (+ g h i)
              (+ a d g) (+ b e h) (+ c f i)
              (+ a e i) (+ g e c))
           (/=v a b c d e f g h i)))
      (list a b c d e f g h i)
      (fail)))




(defmacro distinct (&rest args)
  `(loop
     for i in ,args
     collect (every #'/= (remove i ,args)))
  )

(distinct a b)
