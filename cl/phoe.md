# Common Lisp Condition System

## Abstract

This book is intended to be a tutorial that teaches the functioning and example uses of the Common Lisp condition system. It is aimed at beginning and intermediate Lisp programmers, as well as intermediate programmers of other programming languages. It is intended to supplement already existing material for studying Common Lisp as a language by providing detailed information about the Lisp condition system and its control flow mechanisms, as well as description of an example ANSI-conformant implementation of the condition system.

## Introduction

Let us, programmers, daydream for a moment, daydream about a system of handling special situations in our code.

Some of us already know exception handling systems of popular programming languages, such as C++ or Java. When an exception is signaled, it bubbles up and immediately begins travelling upwards. Not only the state of the program is destroyed during the process of attempting to find a handler for that exception; if it is not handled, the program is in no state to continue since all of its stack has been unwound, and it crashes without any means of recovering, only sometimes leaving a core dump in its wake.

We could do better than that.

Let us imagine a system where stack unwinding is a choice, not a necessity. When an exceptional situation is detected, then the stack is wound further instead of being immediately unwound; the point of signaling an error inspects the program state for all handlers that are applicable for handling that situation, executing them and letting them choose either to repair the program state or to execute some code, transfer control to a known place in the program, and continue execution from there.

Such a system could even use that mechanism for situations that are not errors, but would nonetheless benefit from being treated in such a way. A program could, at some point during its execution, announce that a given condition has just happened, and access the list of callbacks suitable for that particular situation.

These callbacks might have been declared completely outside of the program in question and passed to it dynamically, from outside, as applicable to the wider context in which a given program is run. The program can then either simply call all of these callbacks in succession, or it could perform some more logic, perhaps only calling the functions after verifying that they apply to a given situation, or attempting to call a particular class of callbacks first before resorting to a less preferable method of recovery.

It would also be perfect if an error situation, once it ran out of means of being handled, would not crash the program. Instead, an internal debugger could pop up and allow the programmer to inspect the full state of the program when the error happened, offering him some predefined means of handling the error; it could also allow them to issue arbitrary commands to inspect, recover, or even modify the program's state.

Ideally, we could express such a system in the programming language for which we design it. Even more ideally, we could construct such a system from scratch in that programming language and seamlessly integrate it into the rest of that language, ensuring that it is a good fit for the particular domain for which our program is meant to work.

-----------------

The good thing about daydreaming of such a system is that it already exists. Yes, even outside our daydreams.

It is called the Common Lisp condition system. It is the main focus of this book.

## Foreword

The Common Lisp condition system is based on very different principles than the exception handling mechanisms in other programming languages, such as C++, Java, or Python. This often creates misunderstanding and confusion about how the condition system functions and what are its possible and actual use cases. The awareness about the internal functioning and usefulness of all aspect of the condition system is not widely spread, which causes Common Lisp programmers to not utilize the condition system, even in situations where using it would be overall beneficial.

This is the *raison d'etre* of the book whose foreword you are reading right now. *Common Lisp Condition System* is one more attempt to explain the functioning and usefulness of Common Lisp conditions, handlers, and restarts, using an approach that is different to former work on the topic. We acknowledge the multitude of other authors who have attempted to explain the Common Lisp condition system to novice and intermediate Common Lisp programmers: among them, [Peter Seibel](http://www.gigamonkeys.com/book/beyond-exception-handling-conditions-and-restarts.html), [Chaitanya Gupta](https://lisper.in/restarts), [Justin Grant](https://imagine27.com/error-handling-or-the-emporors-old-clothes/), [Timmy Jose](https://z0ltan.wordpress.com/2016/08/06/conditions-and-restarts-in-common-lisp/), [Jacek Złydach](http://jacek.zlydach.pl/blog/2019-07-24-algebraic-effects-you-can-touch-this.html), and [Robin Schroer](https://sulami.github.io/posts/common-lisp-restarts/) as well as the collaborative effort behind the [Lisp Cookbook](https://lispcookbook.github.io/cl-cookbook/error_handling.html); nonetheless, this book takes an approach that differs from the one exhibited by the aforementioned previous work.

Instead of describing the condition system from an outside perspective, we implement the basics of the handler and restart subsystems piecewise from scratch in order to demonstrate how such subsystems can be bootstrapped from basic language features: dynamic variables and simple data structures. We then rewrite these examples to use standard Common Lisp functionality to draw parallels between our implementation and the Common Lisp-provided tools. In addition, we elaborate on all aspects of the condition system that are defined by the standard, including the lesser used and not commonly understood ones. At the end, we propose a portable set of extensions to the condition systems that augment the tools defined by the standard.

This book is supposed to be read in order. Some topics are simplified or skipped in the earlier chapters, only to be defined and expanded upon later in the book. Therefore, it would be unwise to use the main content of this book as a reference of the functioning of the condition system and Common Lisp control flow operators; such a reference, with proper examples, is available in an appendix at the end of this book.

In addition, this book is meant to show the basic flow of working with Common Lisp as an interactive programming language. We frequently test expressions in the REPL before defining them as concrete functions or do the inverse and test individual functions in the REPL right after defining them. We also make use of Common Lisp's ability to redefine functions at runtime in order to define function stubs that help us test our programs.

All the code demonstrated in this book will be published online as a git repository. FIXME when this book is finished.

The beginning of this book assumes that you know the basics of the C language and basics of Common Lisp. If you don't, but know the general basics of programming, you should nonetheless be able to infer the meaning of the operations from the text. In case of questions, feel free to throw them at me via [mail](mailto:phoe@disroot.org).

We will begin with the very feature of Common Lisp that makes the condition system possible and easy to implement: dynamic variables. Instead of defining *what* does the term "dynamic variable" mean, I will try to convey it through examples instead, and only later come up with definitions.

## Dynamic variables in C

Let's start with an example in C - the *lingua franca* of our profession, in a way.

(As a part of explaining dynamic variables, I am going to lie to you at some point of this demonstration; that fact will become evident later in the text.)

```c
int x = 5;

int foo() {
  return x;
}

foo(); // -> 5
```

In the example above, we have a global variable named `x`, with the function `foo` returning the value of that variable. The result of calling `foo()` will be an integer `5`.

```c
int x = 5;

int bar() {
  return x;
}

int foo() {
  return bar();
}

foo(); // -> 5
```

In the example above, we have a global variable named `x`, with the function `bar` returning the value of that variable and the function `foo` calling the function `bar`. Compared to the previous example, we have added a level of indirection, but the result is still the same: calling `foo()` still gives us `5`.

```c
int x = 5;

int bar() {
  return x;
}

int foo() {
  int x = 42;
  return bar();
}

foo(); // -> 5
bar(); // -> 5
```

The above example modifies one more thing: a new variable binding for `x` is introduced in the body of `foo`, giving it the value of `42`. That variable is *lexically scoped*, though, meaning that it is only effective within the body of the block that it is declared in. The variable `x` is not used within the block of function `foo`; this means that the definition `int x = 42;` is effectively unused.

```c
dynamic int dynamic_x = 5;

int bar() {
  return dynamic_x;
}

int foo() {
  int dynamic_x = 42;
  return bar();
}

int quux() {
  int x = 42;
  return x;
}

foo(); // -> 42
bar(); // -> 5
```

The above example adds one more modification: the global variable definition for `dynamic_x` is now `dynamic`. The `dynamic` modifier means that the scope of this variable is no longer lexical, but *dynamic*; the scope of the variable is not affected by curly braces (or a lack thereof), but by the *environment* in which that variable is accessed.

This modification is enough to cause `foo()` to return `42`, even though calling `bar()` alone still returns 5!

Each new binding of a dynamic variable creates a new *environment*, in which the name of that variable (in this case, `dynamic_x`) is bound to a value (in this case, `5` for the global definition, and `42` for the definition in `foo()`). The environments are always accessed in order from the last-defined one to the first-defined one; looking up the value of a dynamic variable means going through the environments until we find the first environment that has the binding for that dynamic variable, and the value of that binding is then accessed.

(One can notice that `x` is no longer named `x`, and instead it is named `dynamic_x`. Such a distinction is naming is important for reasons that should become evident later; for now, let's get used to the fact that dynamic variables have a `dynamic_` name infix as a naming convention.)

If one knows the [stack data structure](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)), one can think of the set of environments as a stack. Each variable binding is looked up on the stack, going from the top to the bottom. This means that, in a program without any dynamic variables, the stack of environments is empty:

```

            (nothing here)

-------------b-o-t-t-o-m--------------
```

(It is therefore an error to access a dynamic variable named `x`; it is unbound, meaning, there is no value whatsoever associated with it!)

But, if we wanted to illustrate the environment of the toplevel of the earlier example with `dynamic_x`, it would look like this:

```

        --------------------
        |   dynamic_x: 5   |
-------------b-o-t-t-o-m--------------
```

There is only one (global) dynamic environment that contains the binding of the variable `dynamic_x`. If we called the function `bar()`, which returns the dynamic value of `dynamic_x`, that environment would be accessed, and `5` would be returned.

The situation changes, however, if we call `foo()`, because of the `int dynamic_x = 42` binding found there. `x` has been declared dynamic at the toplevel, which "infects" all future binding of that variable; it means that it becomes a dynamic variable, and its value therefore becomes stored on the environment stack when `foo()` is called. The situation inside `foo()`, after *rebinding* the dynamic variable but before calling `bar()`, looks like this:

```

        --------------------
        |   dynamic_x: 42  |
        --------------------
        |   dynamic_x: 5   |
-------------b-o-t-t-o-m--------------
```

When `bar()` is called, it returns the value of `dynamic_x`. It is looked up in the environment stack, starting from the top. The first environment that contains a binding for `dynamic_x` is the one with the value `42`, which is then returned from `bar()`, which is then returned from `foo()`.

It means that, with this simple change, calling `bar()` still gives us `5`, but calling `foo()` gives us `42` instead of `5`!

It also means, indirectly, that dynamic variables give us a way to *dynamically* affect the environment of functions that we execute. *Dynamically*, which means, depending on *where* a given function was run from. If it is run from the toplevel, or a `main()` function that itself has no dynamic bindings, then only the global dynamic bindings shall be in effect; if it is called from another scope in the code, then it will be called with all the dynamic bindings that this particular scope has defined *on top of* all the dynamic bindings that have been defined in previous dynamic environments.

The difference between lexical variables and dynamic variables can be summed in one more way: _a lexical variable can't be seen outside its scope_. When a dynamic scope ends, the *environment* defined in it is discarded. It means that, after control leaves `foo()`, the dynamic environment defined in it is removed from the environment stack, which leaves us in the following situation:

```

        --------------------
        |   dynamic_x: 5   |
-------------b-o-t-t-o-m--------------
```

This means that, if we tried to call `bar()` again right after calling `foo()`, the dynamic environment created inside `foo()` will not affect the subsequent execution of `bar()`. When `foo()` finishes, it cleans up the dynamic environment that it created, and therefore `bar()` sees only the global environment that it then accesses.

What if there was no global environment though? Let us try the following example.

```c
int bar() {
  return dynamic_x;
}

int foo() {
  dynamic int dynamic_x = 42;
  return bar();
}
```

In this example, we only define `dynamic_x` inside of `foo()`. This means that the `return x` inside `bar()` has no value whatsoever to refer to; `dynamic_x` is an undeclared variable. That is a compilation error even if we do not use any dynamic variables or remove the definition of `foo()` altogether:

```
example5.c: In function ‘bar’:
example5.c:2:10: error: ‘dynamic_x’ undeclared (first use in this function)
    2 |   return dynamic_x;
      |          ^
```

The situation changes, though, if we *locally declare* the variable `dynamic_x` to be dynamic.

```c
int bar() {
  dynamic int dynamic_x;
  return dynamic_x;
}

int foo() {
  dynamic int dynamic_x = 42;
  return bar();
}

foo(); // -> 42
```

In this example, the compiler now knows that `dynamic_x` inside `bar()` is a `dynamic int`: we declare `dynamic_x` to be so. The `dynamic` modifier means that the value of `dynamic_x` needs to be fetched at runtime *from the environment*. Therefore, `bar()` does not need to know its value at compilation time; when it needs to access `dynamic_x` (in order to return its value), it can simply look at the stack of environments, find the first environment that binds `dynamic_x`, and return the respective value. If it doesn't find it, that's (obviously) a runtime error, which is going to happen if we call `bar()` directly; however, calling `foo()` is *still* going to return `42`. Let us look at the state of the environments to better understand this:

```

            (nothing here)

-------------b-o-t-t-o-m--------------
```

```

        --------------------
        |   dynamic_x: 42  |
-------------b-o-t-t-o-m--------------
```

Once control reaches `bar()`, there is an environment that binds `dynamic_x` to `42`. That environment is accessed and the value of `dynamic_x` in that environment is returned. And then, when `foo()` finishes, the created environment is discarded, and we are back to an empty environment stack.

This means that dynamic environment is preserved even in situations with more nested functions. If we have a function `foo` that calls `bar` that calls `baz` that calls `quux`, and we define a `dynamic int dynamic_x = 42` at the top of `foo()`'s body, and then we try to access a `dynamic int dynamic_x` inside `quux`, then the access is going to work. Of course, `bar` and `baz` can introduce their own bindings of `dynamic int dynamic_x` that are then going to affect the value that `quux` is going to find; all that is necessary for a dynamic variable access to *always* work is at least one binding for that particular variable.

------------------

Nothing prevents us from using multiple dynamic variables, too. For simplicity, we will use an environment model in which a single environment frame always holds only one variable-value binding. Let us assume that we have three variables, `dynamic_x` (which is later rebound), `dynamic_y`, and `dynamic_z`. At one point in execution of the program, the environment stack is going to look like the following:

```

        ------------------------------
        | dynamic_z: [2.0, 1.0, 3.0] |
        ------------------------------
        |      dynamic_x: 1238765    |
        ------------------------------
        |      dynamic_y: "Hello"    |
        ------------------------------
        |        dynamic_x: 42       |
------------------b-o-t-t-o-m--------------------
```

This means that accessing a `dynamic char* dynamic_y` will return `"Hello"`, accessing a `dynamic int dynamic_x` will return `1238765` (it was rebound once, and the previous value of `42` is ignored), and accessing a `dynamic float dynamic_z[3]` will return the vector `[2.0, 1.0, 3.0]`. Note that I mentioned "accessing", which means not just getting the values, but *setting* them, too: it is possible, for example, to execute the following body of code that sets the values of these variables.

```c
{
  dynamic int dynamic_x;
  dynamic char* dynamic_y;
  dynamic float dynamic_z[3];

  dynamic_x = 2000;
  dynamic_y = "World";
  dynamic_z[1] = 123.456;
}
```

The resulting environment is going to look like this:

```

     ----------------------------------
     | dynamic_z: [2.0, 123.456, 3.0] |
     ----------------------------------
     |        dynamic_x: 2000         |
     ----------------------------------
     |       dynamic_y: "World"       |
     ----------------------------------
     |         dynamic_x: 42          |
----------------b-o-t-t-o-m--------------------
```

It is noteworthy that the last environment, containing the original binding for `dynamic_x`, was not affected by the operation of setting. When the code tried to set the value of `dynamic_x`, it searched for the first environment where `dynamic_x` was bound from top to bottom and modified that environment only.

A consequence of this is that only the last binding for each dynamic variable is visible to code that is being executed; all environments and bindings defined previously are inaccessible. This is useful in case we expect that some code might modify the value of a dynamic variable; for instance, if we want to save the previous value of `dynamic_x` from modification, we can define a new binding in form of `dynamic int dynamic_x = dynamic_x`, which binding is then going to be affected by subsequent `dynamic_x = ...` setters.

(One can notice that `dynamic int dynamic_x = dynamic_x` would be a no-op if it wasn't for the `dynamic` part; it is however not, since that is the syntax for *creating new bindings*, as opposed to *setting* them. That is also why, in the example above, we separated the `dynamic int dynamic_x;` declarations from the `dynamic_x = 2000` setting; with dynamic variables, we need a way to tell apart the situations of creating a new dynamic binding, accessing a previous dynamic binding, and setting a new value to the last dynamic binding.)

-----------------------------------------------------

The only sad thing about dynamic variables in C is that of the above code that uses `dynamic` variables won't compile. It's invalid.

Think about it for a second.

I'm sorry. It was a lie. Yes, you can stop googling through the C reference now.

C has no notion of `dynamic` variables whatsoever, so the `dynamic` examples we've shown above are written in pseudo-C that I sometimes wish we had. We don't, though, so even though they (hopefully!) illustrate the concept of dynamic variables, they will not work with any C compiler whatsoever.

To be absolutely honest, though, it is actually possible to emulate dynamic variables in C and other languages that do not have them in standard. The technique involves the following steps: saving the original value of a variable in a temporary stack-allocated variable, setting the variable with the new value, executing user code, and then restoring the variable with its original value.

This technique is commonly used to implement dynamic variables (e.g. in Emacs C code) and it replaces the explicit, separate stack of environments with an *implicit* stack, embedded within the temporary variables that are allocated by the program.

Example:

```c
int x = 5;

int bar() {
  return x;
}

int foo() {
  int temp_x = x;  // Save the original value of X
  x = 42;          // Set the new value to X
  int ret = bar(); // Execute user code and save its return value
  x = temp_x;      // Restore the original value of X
  return ret;      // Return the user code's return value
}

foo(); // -> 42
bar(); // -> 5
```

I have called this approach "emulation", because we are not really creating new bindings of dynamic variables; we are creating new lexical variables and temporarily store the old dynamic values in there, while the original lexical variable overwritten with a new value and restored when the scope is being left. Not only this approach is particularly messy and prone to errors, it does not work at all in situations with multiple threads attempting to read or bind a single dynamic variable.

We do have languages that have dynamic variables in their standard, though. One of them is Common Lisp, in which I will rewrite the above examples. I will do so to demonstrate how they work in a language that actually supports dynamic scope; it will be possible to execute all of the examples in any standard-conforming Common Lisp implementation.

## Dynamic variables in Common Lisp

Let us begin with a simple example of how Lisp variables work in general. Compared to C, it is possible in Lisp to declare functions that access local lexical variables; therefore, for non-dynamic variables, we will not need to define globals.

```lisp
(let ((x 5))
  (defun foo ()
    x))

(foo) ; -> 5
```

In the example above, we have a local lexical variable named `x`, with the function `foo` returning the value of that variable. Exactly as in the related C example, the result of calling `foo` will be `5`.

```lisp
(let ((x 5))
  (defun bar ()
    x)
  (defun foo ()
    (bar)))

(foo) ; -> 5
```

After adding a level of indirection to accessing the variable, this example behaves the same way as in C. `(foo)` evaluates to `5`.

```lisp
(let ((x 5))
  (defun bar ()
    x)
  (defun foo ()
    (let ((x 42))
      (bar)))
  (defun quux ()
    (let ((x 42))
      x))

(foo) ; -> 5
(quux) ; -> 42
```

If we experiment with introducing new lexically-scoped variables in Lisp, the result is still consistent with what C produces: calling `(foo)` gives us `5`, calling `(quux)` gives us the shadowed value of `42`, and the variable definition `(let ((x 42)) ...)` inside the body of `foo` is effectively unused.

```lisp
(defvar *x* 5)

(defun bar ()
  *x*)

(defun foo ()
  (let ((*x* 42))
    (bar)))

(foo) ; -> 42
(bar) ; -> 5
```

The above example changes two things. First, the name of our variable is now modified from `x` to `*x*` - the "earmuff notation" with asterisks on both side of the variable name is the canonical Lisp notation for dynamic variables; second, the variable we define is now global, as defined via `defvar`.

The environment stack works in the same way as in the C example: calling `(bar)` gives us the original, toplevel value of `5`, but calling `(foo)` will give us the rebound value of `42`.

If we decide to use local dynamic variables in Lisp instead, the following example is an incorrect way of doing that:

```lisp
(defun bar ()
  *x*)

(defun foo ()
  (let ((*x* 42))
    (declare (special *x*))
    (bar)))
```

Inside `foo`, we locally declare the variable `*x*` to be *special*, which is Common Lisp's notation for declaring that it denotes a dynamic variable. However, we have not done the same in the body of `bar`; because of that, the compiler treats `*x*` inside the body of `bar` as an undefined variable and produces a warning:

```lisp
; in: DEFUN BAR
;     (BLOCK BAR *X*)
;
; caught WARNING:
;   undefined variable: COMMON-LISP-USER::*X*
```

The correct version is:

```lisp
(defun bar ()
  (declare (special *x*))
  *x*)

(defun foo ()
  (let ((*x* 42))
    (declare (special *x*))
    (bar)))

(foo) ; -> 42
```

Similarly, calling `(bar)` is an error, since the variable `*x*` does not have a global dynamic binding.

An environment stack with multiple dynamic variables from Example 7 (named `*x*`, `*y*`, and `*z*` in Lisp) may look like the following:

```

        ------------------------------
        |     *z*: #(2.0 1.0 3.0)    |
        ------------------------------
        |       *x*: 1238765         |
        ------------------------------
        |       *y*: "Hello"         |
        ------------------------------
        |         *x*: 42            |
-----------------b-o-t-t-o-m--------------------
```

We may set the values of the three:

```lisp

(locally (declare (special *x* *y* *z*))
  (setf *x* 2000)
  (setf *y* "World")
  (setf (aref *z* 1) 123.456))
```

And produce the following environment this way:

```

     ----------------------------------
     |   *z*: #(2.0 123.456 3.0)      |
     ----------------------------------
     |          *x*: 2000             |
     ----------------------------------
     |         *y*: "World"           |
     ----------------------------------
     |           *x*: 42              |
----------------b-o-t-t-o-m----------------------
```

To sum up, the actual syntax for working with dynamic variables in Lisp is threefold: we need means of declaring that a Lisp symbol denotes a dynamic variable, means of creating new dynamic bindings between a Lisp symbol and a Lisp value, and means of setting the topmost visible binding. The first is achieved via `declare special` (if we want to work with a local dynamic variable) or via `defvar` (which *proclaims* the variable to be globally special); the second we achieve via `let`, among others; the third is achievable with Common Lisp's general setting mechanism, named `setf`.

(An alternative to `defvar` is `defparameter`, which is a slightly different way to create global dynamic variables. For clarity, we will not use it in this tutorial nor explain the differences between the two; a curious reader may read the related chapter in [Practical Common Lisp](http://www.gigamonkeys.com/book/variables.html#dynamic-aka-special-variables).)

## A simple system of hooks

We will utilize this knowledge of how dynamic variables work in Lisp on a simple code example that we will extend over time.

Let's meet Tom.

Tom tends to call various people often and for various puroposes. He tends to do various things before calling them, such as launching Counter Strike when calling some of his schoolmates who play that game, flipping a coin before calling his parents to see if he should actually call them, maybe calling his girlfriend two times in a row, and so on. He does not always want to perform all of these everyday tasks, though; sometimes, e.g. on holidays, he wants to call all of them (except his ex) and wish them happy holidays.

From a programming point of view, we could express this problem in the following way. We have a group of people that we could express as a list of objects. Let's describe each person as a set of Lisp keywords that describe who a given person is to Tom.

```lisp
(defvar *phonebook*
  '((:mom :parent)
    (:dad :parent)
    (:alice :classmate :csgo :homework)
    (:bob :classmate :homework)
    (:catherine :classmate :ex)
    (:dorothy :classmate :girlfriend :csgo)
    (:eric :classmate :homework)
    (:dentist)))
```

(For brevity, we will refer to Counter Strike: Global Offensive in our code as `csgo`.)

Let us assume that each time Tom wants to make calls, he goes through the whole phonebook. Programmatically, we could express this as:

```lisp
(defun call-person (person)
  (format t ";; Calling ~A.~%" (first person)))

(defun call-people ()
  (dolist (person *phonebook*)
    (call-person person)))
```

An example execution of that function looks like the following:

```lisp
CL-USER> (call-people)
;; Calling MOM.
;; Calling DAD.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

We see that this method of calling everyone works. However, we also want to account for a situation when we want to do something else before calling some people. What kind of "something"? That depends on the current situation in which Tom is at the moment; we are unable to define that up front and would like to leave that to the caller of `(call-people)`. For that, we can use a technique called [hooking](https://en.wikipedia.org/wiki/Hooking).

### Hook #1: launching Counter Strike

Let us use an example where Tom is a Counter Strike player. Just in case someone else wants to have a quick match, he wants to launch Counter Strike before calling any `:csgo` people. In code, it could be expressed as a hook function that will be executed for each person that Tom is about to call. That function will check if the person is a Counter Strike player and will execute some code based on the result of that check.

This approach needs us to define a variable that tells us whether Counter Strike was already launched. We will also want to ensure that, before Tom tries to call anyone, Counter Strike is not turned on, even if some of the previous calls have turned it back on. we will add this to the beginning of `call-people` to take that precondition into account.

```lisp
(defvar *csgo-launched-p* nil)

(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (call-person person)))
```

Now that we have this done, we can focus on the hook itself. For example, such a hook could be represented in code as:

```lisp
(lambda (person)
  (when (member :csgo person)
    (unless *csgo-launched-p*
      (format t ";; Launching Counter Strike for ~A.~%" (first person))
      (setf *csgo-launched-p* t))))
```

We would like the call site of the function, `(call-people)`, to remain unchanged; the function must still accept zero arguments. This means that we need to pass the data about the currently present hooks into that function through other means - in our case, we will do it via a dynamic variable.

Let us define the special variable `*hooks*` with a default value of an empty list, representing the default case of no special situations.

```lisp
(defvar *hooks* '())
```

We will use it in the following way. If we bind the hook we have created above to the value of `*hooks*` around the call of `(call-people)`, it means that we want to launch Counter Strike before calling any people who play Counter Strike.

```lisp
(let ((*hooks*
        (list
         (lambda (person)
           (when (member :csgo person)
             (unless *csgo-launched-p*
               (format t ";; Launching Counter Strike for ~A.~%" (first person))
               (setf *csgo-launched-p* t)))))))
  (call-people))
```

Obviously, calling it like that at the moment has no effect, since `call-people` does not yet refer to the value of `*hooks*` in any way. Let us change that and reimplement that function to take that into account.

```lisp
(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (dolist (hook *hooks*)
      (funcall hook person))
    (call-person person)))
```

Let us try to evaluate the previous form, then:

```lisp
CL-USER> (let ((*hooks*
                 (list
                  (lambda (person)
                    (when (member :csgo person)
                      (unless *csgo-launched-p*
                        (format t ";; Launching Counter Strike for ~A.~%" (first person))
                        (setf *csgo-launched-p* t)))))))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

We can see that before we called the first person marked with `:csgo`, we have launched Counter Strike. That is exactly the behaviour that we wanted. It can become unwieldy, however; the body of each function is present in each invocation of `(call-people)`. In order to avoid verbosity, we can define the function with a name and use that name to refer to that function in the list of hooks.

```lisp
(defun ensure-csgo-launched (person)
  (when (member :csgo person)
    (unless *csgo-launched-p*
      (format t ";; Launching Counter Strike for ~A.~%" (first person))
      (setf *csgo-launched-p* t))))
```

```lisp
CL-USER> (let ((*hooks* (list #'ensure-csgo-launched)))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

### Hook #2: only call Counter Strike players

In the next example, in addition to launching Counter Strike before calling the first CSGO-playing person, we would like to *only* call Counter Strike-playing people and not call any other ones. This will require us to have some logic that will *prevent* a person from being called; in other words, logic that will prevent execution of the `(call-person person)` form inside the body of `call-people`. We will also want the prevention to occur only on a per-person basis; we do not want to cease calling people altogether if we run on a person that we do not want to call.

First of all, we will modify the function `call-people` once more to add logic that prevents a person from being called. We will use a pair of operators named `throw` and `catch` for that.

(One other reason why I have decided to use these operators is to show how they are different from the throw/catch operators commonly found in languages such as C++ or Java. In Lisp, `throw` and `catch` are analogous to `go` and `tagbody` or `return-from` and `block` - the operators that allow non-local transfers of control within lexical scope. The difference is that `throw` and `catch` work in dynamic scope instead, and that `throw` requires a value to be thrown that is then returned from the `catch` form.)

Our next modification of the function `call-people` will look like this:

```lisp
(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (catch :nope
      (dolist (hook *hooks*)
        (funcall hook person))
      (call-person person))))
```

We have additionally wrapped the forms inside `dolist (person *phonebook*)` in a `catch` form, with the *catch tag* being the symbol `:nope`. This means that every `throw` form that throws a value to the catch tag `:nope` within the dynamic scope of this block will transfer control to the end of that block; in our case, it means that if the hook called by `(funcall hook person)` throws anything to the catch tag `:nope`, execution of any remaining hooks stops, and the `(call-person person)` form is not executed.

This modification allows us to add a second hook. For people who are *not* playing Counter Strike, we would like to completely avoid making a call.

```lisp
(defun skip-non-csgo-people (person)
  (unless (member :csgo person)
    (format t ";; Nope, not calling ~A.~%" (first person))
    (throw :nope nil)))
```

```lisp
CL-USER> (let ((*hooks* (list #'ensure-csgo-launched
                              #'skip-non-csgo-people)))
           (call-people))
;; Nope, not calling MOM.
;; Nope, not calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Nope, not calling BOB.
;; Nope, not calling CATHERINE.
;; Calling DOROTHY.
;; Nope, not calling ERIC.
;; Nope, not calling DENTIST.
NIL
```

This has worked, too. If the second hook throws anything at the `:nope` catch tag, it prevents the respective person from being called. The first hook on the list still takes care of launching Counter Strike for the first Counter Strike player that we call.

### Hook #3: only call parents... maybe

Let us go for a third example: Tom wants to only call his parents, and he only wants to *maybe* call them. That is, for each of them, he flips a coin, and only calls them if he gets heads.

```lisp
(defun maybe-call-parent (person)
  (when (member :parent person)
    (when (= 0 (random 2))
      (format t ";; Nah, not calling ~A this time.~%" (first person))
      (throw :nope nil))))

(defun skip-non-parents (person)
  (unless (member :parent person)
    (throw :nope nil)))
```

(In order to reduce output verbosity, we decide not to print any information from within the body of `skip-non-parents`. This will mean that, from now on, the only output shall come from people whom we actually call.)

```lisp
CL-USER> (let ((*hooks* (list #'maybe-call-parent
                              #'skip-non-parents)))
           (call-people))
;; Nah, not calling MOM this time.
;; Calling DAD.
NIL

;;;;;;;;;;; Or...
;; Calling MOM.
;; Calling DAD.
NIL

;;;;;;;;;;; Or...
;; .........
```

In the above example, the output is randomized; it is possible to call both parents, or either of them, or none of them, based on the output of the `random` call that is embedded in the body of `maybe-call-parent`.

### Hook #4: holiday wishes

In another situation, perhaps Tom wants to call everyone (excluding Tom's ex) and wish them happy holidays.

```lisp
(defun skip-ex (person)
  (when (member :ex person)
    (throw :nope nil)))

(defun wish-happy-holidays (person)
  (format t ";; Gonna wish ~A happy holidays!~%" (first person)))
```

```lisp
CL-USER> (let ((*hooks* (list #'skip-ex
                              #'wish-happy-holidays)))
           (call-people))
;; Gonna wish MOM happy holidays!
;; Calling MOM.
;; Gonna wish DAD happy holidays!
;; Calling DAD.
;; Gonna wish ALICE happy holidays!
;; Calling ALICE.
;; Gonna wish BOB happy holidays!
;; Calling BOB.
;; Gonna wish DOROTHY happy holidays!
;; Calling DOROTHY.
;; Gonna wish ERIC happy holidays!
;; Calling ERIC.
;; Gonna wish DENTIST happy holidays!
;; Calling DENTIST.
NIL
```

In the second hook in the form above, there are no conditional checks. The technical meaning of this is that we want to use this hook for all persons that we call.

A somewhat trained eye may then notice that we have an unconditional hook that should execute for every person, and yet it is not executed for Catherine - Tom's ex. This is because `(call-people)` walks the list of hook in order, which is an important property. The first hook throws at the `:nope` catch tag, which transfers control out of the `dolist` that walks the hook list; this prevents the second hook function from being executed. If we reversed the order of the two hook, we would not get the intended behaviour.

### Accumulating hooks

One important matter is the accumulation of hook functions. In more complex code, we might want to have multiple layers of bindings that add more and more hook functions onto the list, but does not override the previously established hooks. In practice, this is achieved by *appending* new hooks on top of the previously established ones. For example, this previous form:

```lisp
(let ((*hooks* (list #'skip-ex
                     #'wish-happy-holidays)))
  (call-people))
```

Could be rewritten in the following way, if we first decided that we want Tom to wish everyone happy holidays *and only then remembered* that he should not call his ex, and added that on top of the previous hooks:

```lisp
CL-USER> (let ((*hooks* (list #'wish-happy-holidays)))
           (let ((*hooks* (append (list #'skip-ex) *hooks*)))
             (call-people)))
;; Gonna wish MOM happy holidays!
;; Calling MOM.
;; Gonna wish DAD happy holidays!
;; Calling DAD.
;; Gonna wish ALICE happy holidays!
;; Calling ALICE.
;; Gonna wish BOB happy holidays!
;; Calling BOB.
;; Gonna wish DOROTHY happy holidays!
;; Calling DOROTHY.
;; Gonna wish ERIC happy holidays!
;; Calling ERIC.
;; Gonna wish DENTIST happy holidays!
;; Calling DENTIST.
NIL
```

(This approach ensures that the hook function `skip-ex` will be called before `wish-happy-holidays`, since the resulting list appends `skip-ex` before the previous hooks. This means that, if we modify the `append` call, we can change this behaviour as required.)

### Hook #5: calling Tom's girlfriend again

Let us suppose that we are okay with the current behaviour and would like to add one more thing; after calling Tom's girlfriend, he would like to call her again, since one time is not enough for him. As simple as such a situation sounds, it is currently unrepresentable in our code: the list of hooks that we have created is executed inside `(call-people)` *before* calling each person, whereas we require a method to execute hooks *after* calling them.

Let us therefore modify our code to take that into account. Instead of using a singular variable `*hooks*`, let us use twin variables `*before-hooks*` and `*after-hooks*`.

```lisp
(defvar *before-hooks* '())

(defvar *after-hooks* '())

(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (catch :nope
      (dolist (hook *before-hooks*)
        (funcall hook person))
      (call-person person)
      (dolist (hook *after-hooks*)
        (funcall hook person)))))
```

This approach will allow us to execute code after a given person is called:

```lisp
(defun call-girlfriend-again (person)
  (when (member :girlfriend person)
    (format t ";; Gonna call ~A again.~%" (first person))
    (call-person person)))
```

```lisp
CL-USER> (let ((*after-hooks* (list #'call-girlfriend-again)))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Gonna call DOROTHY again.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

It will also let us compose before- and after-hooks:

```lisp
CL-USER> (let ((*before-hooks* (list #'ensure-csgo-launched))
               (*after-hooks* (list #'call-girlfriend-again)))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Gonna call DOROTHY again.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

### Multiple types of hooks

One issue that is evident with such an approach is that defining a new point of hooking requires us to define a new variable. So far, we have defined `*before-hooks*` and `*after-hooks*`; it will become clumsy when we, for example, complicate the logic inside `call-person` and allow hooks to be called during the call. Such an approach will require us to handle multiple variables, which may quickly become unwieldy.

We will propose a somewhat different mechanism, which reintroduces the singular variable `*hooks*` for storing *all* hooks that we create. This will require us to have some way of discriminating the individual groups of hooks that, in our previous approach, would belong to different variables. To achieve that, each value in `*hooks*` will actually be a list of two values. The second value will be the hook function itself as it is now; the first value, however, will be a symbol that denotes the *kind* of that given hook.

This will allow us to invoke hooks based on the *kind* of hooks that we want to invoke. For instance, if we have some hooks of kind `before-call` and some of kind `after-call`, we would like the form `(call-hooks 'before-call)` to only call the former ones, and not the latter ones.

We can remove the old `*before-hooks*` and `*after-hooks*` variables, define the new `*hooks*` variable, and implement the function `call-hooks` that iterates over the list of all hooks and only calls the ones of kind which is relevant to us.

```lisp
(makunbound '*before-hooks*)

(makunbound '*after-hooks*)

(defvar *hooks* '())

(defun call-hooks (kind &rest arguments)
  (dolist (hook *hooks*)
    (destructuring-bind (hook-kind hook-function) hook
      (when (eq kind hook-kind)
        (apply hook-function arguments)))))
```

This allows us to redefine `call-people` to take this new function into account.

```lisp
(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (catch :nope
      (call-hooks 'before-call person)
      (call-person person)
      (call-hooks 'after-call person))))
```

We now need to adjust the way in which our dynamic environment is created around `(call-people)`. For brevity, we will use *backquote notation* to build our list this time. (A brief tutorial of that notation is available in an appendix to this book related to the basics of macro writing.)

```lisp
CL-USER> (let ((*hooks* `((before-call ,#'ensure-csgo-launched)
                          (after-call ,#'call-girlfriend-again))))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Gonna call DOROTHY again.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

This approach allows us to define multiple hooks on a single variable. Let us, at once, skip calling Tom's ex, ensure that Counter Strike is launched, wish everyone happy holidays, and ensure that Tom calls his girlfriend again.

```lisp
CL-USER> (let ((*hooks* `((before-call ,#'skip-ex)
                          (before-call ,#'ensure-csgo-launched)
                          (before-call ,#'wish-happy-holidays)
                          (after-call ,#'call-girlfriend-again))))
           (call-people))
;; Gonna wish MOM happy holidays!
;; Calling MOM.
;; Gonna wish DAD happy holidays!
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Gonna wish ALICE happy holidays!
;; Calling ALICE.
;; Gonna wish BOB happy holidays!
;; Calling BOB.
;; Gonna wish DOROTHY happy holidays!
;; Calling DOROTHY.
;; Gonna call DOROTHY again.
;; Calling DOROTHY.
;; Gonna wish ERIC happy holidays!
;; Calling ERIC.
;; Gonna wish DENTIST happy holidays!
;; Calling DENTIST.
NIL
```

If we want to complicate this example further, we can define new hook sites inside the body of `(call-people)`. For instance, we could define hooks that are meant to be run before we start calling anyone (such hooks may e.g. inspect the list of people that we are about to call) and after we finish calling altogether (such hooks may e.g. inspect the list of people that have actually been called). Perhaps we might want to stop calling altogether at some point: if, for example, Tom's mother tells him that he needs to show up in the living room this instant, he should cease all further calling (even if she was the first person he called!) and go to the living room instead to await his fate. Implementing such functionality can require us to bind additional hook kinds in the body of `(call-people)` to collect the people that we have actually called or to insert additional `catch` forms in different places to short-circuit the algorithm further. Such complication is left as an exercise for the reader.

### Summary

To summarize: starting with dynamic variables and simple code, we have implemented a simple system of hooks, which are places that allow the user to *extend the behaviour of an existing system* with their own code at predefined points. All of the particular hooks are called in inverse order in which were bound; the "newest" ones are called first, the "oldest" ones - last.

In truth, we are able to translate the above example into another system of dynamically-scoped hooks. More: that system is a part of standard Common Lisp, and is ready for us to use.

## A simple system of condition handlers

As we said in the chapter above, the Lisp condition system has the same basis as the system we have created in the previous chapter: a dynamically-scoped hook system. It is a more complicated system than the one we have constructed earlier, but the principles of its functioning ultimately boil down to the same thing. To demonstrate this, this chapter will reimplement all of the examples we have shown so far, but via the condition system instead of our homegrown code.

[The Common Lisp Hyperspec](http://clhs.lisp.se/Body/09_a.htm) states:

> A situation is the evaluation of an expression in a specific context. A *condition* is an object that represents a specific situation that has been detected. (...) *Signaling* is the process by which a condition can alter the flow of control in a program by raising the condition which can then be *handled*.

In this context, a *situation* is not really a technical term: it bears its usual meaning of a state of affairs or a set of circumstances. To reword the above statement slightly in our context: *handlers* contain the actual code for hooks. The action of invoking the hooks is called *signaling*. *Conditions* are the objects that may trigger some of the hooks when they are signaled, allowing handlers to access arbitrary *data* that is a part of the given condition that was signaled.

Let us start from the same initial codebase, where we initially call everyone on the list, and where we already have the code required for checking whether Counter Strike was launched.

```lisp
(defvar *phonebook*
  '((:mom :parent)
    (:dad :parent)
    (:alice :classmate :csgo :homework)
    (:bob :classmate :homework)
    (:catherine :classmate :ex)
    (:dorothy :classmate :girlfriend :csgo)
    (:eric :classmate :homework)
    (:dentist)))

(defun call-person (person)
  (format t ";; Calling ~A.~%" (first person)))

(defvar *csgo-launched-p* nil)

(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (call-person person)))
```

Calling this code produces the expected result:

```lisp
CL-USER> (call-people)
;; Calling MOM.
;; Calling DAD.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

Previously, our hook system called each hook with the person that was about to be called. In the Common Lisp condition system, there is one more layer of indirection: *condition types*. In order to run our hooks using the condition system, we need to create an instance of a *condition*, equip it with arbitrary data that we want to pass to the handlers, and call the function `signal` on that condition.

(There is an analogy between condition types and hook kinds which we have constructed earlier. Instead of creating hook kinds, which are symbols, we define new condition types, which denote *Lisp types*. Operations on Lisp types are more complex, since Lisp types are an implementation of mathematical sets; therefore operating on those allows for more complexity, compared to matching symbols by equality. An example of this extended capability will be demonstrated later in the book, just like the case of dealing with multiple condition types.)

For now, we want to define one condition type for the situation where we are about to call someone.

```lisp
(define-condition before-call ()
  ((%person :reader person :initarg :person)))
```

This creates a condition type named `before-call`. We will want to pass the person that we are about to call to the code, so we create a single *slot* on that condition type. The internal name of that slot is `%person`. We can read its value using the reader function named `person` and we can set the initial value of that slot by using the initialization argument `:person`, like this:

```lisp
(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (signal 'before-call :person person)
    (call-person person)))
```

This modification ensures that a `before-call` condition is going to be signaled before calling each person from Tom's phonebook.

Now that we have that additional layer of indirection, we need to slightly modify our hook function. Each hook function that will be called will accept a single argument that is the condition object; in order to fetch the person from it, we will need to call the `person` function on the condition object.

```lisp
(lambda (condition)
  (let ((person (person condition)))
    (when (member :csgo person)
      (unless *csgo-launched-p*
        (format t ";; Launching Counter Strike for ~A.~%" (first person))
        (setf *csgo-launched-p* t)))))
```

The only remaining issue is to associate that function with the signaled condition. We can use the standard Lisp macro `handler-bind` to achieve that. The act of associating a given condition type with its associated function is called *binding* a *handler*.

(Technically speaking, a Common Lisp condition handler is a pair of two elements: a condition type that the handler should wait for, and the actual code that gets executed. Contrary to the hook system that we have created ourselves, not all handlers are executed in turn; only handlers that successfully match the condition's type are run for a given condition object.)

```lisp
CL-USER> (handler-bind
             ((before-call
                (lambda (condition)
                  (let ((person (person condition)))
                    (when (member :csgo person)
                      (unless *csgo-launched-p*
                        (format t ";; Launching Counter Strike for ~A.~%" (first person))
                        (setf *csgo-launched-p* t)))))))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

In order to avoid verbosity, again, we can define that hook function with a name.

```lisp
(defun ensure-csgo-launched (condition)
  (let ((person (person condition)))
    (when (member :csgo person)
      (unless *csgo-launched-p*
        (format t ";; Launching Counter Strike for ~A.~%" (first person))
        (setf *csgo-launched-p* t)))))
```

```lisp
CL-USER> (handler-bind ((before-call #'ensure-csgo-launched))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

Our next modification was short-circuiting and not calling some people; we need to modify `(call-people)` for that.

```lisp
(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (catch :nope
      (signal 'before-call :person person)
      (call-person person))))
```

Now, let's define the handler for skipping people and attempt skipping them:

```lisp
(defun skip-non-csgo-people (condition)
  (let ((person (person condition)))
    (unless (member :csgo person)
      (format t ";; Nope, not calling ~A.~%" (first person))
      (throw :nope nil))))
```

```lisp
CL-USER> (handler-bind ((before-call #'ensure-csgo-launched)
                        (before-call #'skip-non-csgo-people))
           (call-people))
;; Nope, not calling MOM.
;; Nope, not calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Nope, not calling BOB.
;; Nope, not calling CATHERINE.
;; Calling DOROTHY.
;; Nope, not calling ERIC.
;; Nope, not calling DENTIST.
NIL
```

The syntax for `handler-bind` requires us to specify the condition type for each handler, which is why we duplicate the condition type `before-call`. For every condition of type `before-call` that is signaled, `ensure-csgo-launched` will be called first, and `skip-non-csgo-people` will be called second.

Next goes the example of not calling anyone but parents, and then only calling them sometimes:

```lisp
(defun maybe-call-parent (condition)
  (let ((person (person condition)))
    (when (member :parent person)
      (when (= 0 (random 2))
        (format t ";; Nah, not calling ~A this time.~%" (first person))
        (throw :nope nil)))))

(defun skip-non-parents (condition)
  (let ((person (person condition)))
    (unless (member :parent person)
      (throw :nope nil))))
```

```lisp
CL-USER> (handler-bind ((before-call #'maybe-call-parent)
                        (before-call #'skip-non-parents))
           (call-people))
;; Nah, not calling MOM this time.
;; Calling DAD.
NIL

;;;;;;;;;;; Or...
;; Calling MOM.
;; Calling DAD.
NIL

;;;;;;;;;;; Or...
;; .........
```

Wishing happy holidays to everyone who is not Tom's ex?

```lisp
(defun skip-ex (condition)
  (let ((person (person condition)))
    (when (member :ex person)
      (throw :nope nil))))

(defun wish-happy-holidays (condition)
  (let ((person (person condition)))
    (format t ";; Gonna wish ~A happy holidays!~%" (first person))))
```

```lisp
CL-USER> (handler-bind ((before-call #'skip-ex)
                        (before-call #'wish-happy-holidays))
           (call-people))
;; Gonna wish MOM happy holidays!
;; Calling MOM.
;; Gonna wish DAD happy holidays!
;; Calling DAD.
;; Gonna wish ALICE happy holidays!
;; Calling ALICE.
;; Gonna wish BOB happy holidays!
;; Calling BOB.
;; Gonna wish DOROTHY happy holidays!
;; Calling DOROTHY.
;; Gonna wish ERIC happy holidays!
;; Calling ERIC.
;; Gonna wish DENTIST happy holidays!
;; Calling DENTIST.
NIL
```

Adding different handlers at different moments?

```lisp
CL-USER> (handler-bind ((before-call #'wish-happy-holidays))
           (handler-bind ((before-call #'skip-ex))
             (call-people)))
;; Gonna wish MOM happy holidays!
;; Calling MOM.
;; Gonna wish DAD happy holidays!
;; Calling DAD.
;; Gonna wish ALICE happy holidays!
;; Calling ALICE.
;; Gonna wish BOB happy holidays!
;; Calling BOB.
;; Gonna wish DOROTHY happy holidays!
;; Calling DOROTHY.
;; Gonna wish ERIC happy holidays!
;; Calling ERIC.
;; Gonna wish DENTIST happy holidays!
;; Calling DENTIST.
NIL
```

(The above form differs from the hook-based implementation in one detail: the handler mechanism has one property called *clustering* that the hook system does not have. We will elaborate on that later in the book.)

Doing different things before calling people, and different things after calling people?

```lisp
(define-condition after-call ()
  ((%person :reader person :initarg :person)))

(defun call-people ()
  (setf *csgo-launched-p* nil)
  (dolist (person *phonebook*)
    (catch :nope
      (signal 'before-call :person person)
      (call-person person)
      (signal 'after-call :person person))))

(defun call-girlfriend-again (condition)
  (let ((person (person condition)))
    (when (member :girlfriend person)
      (format t ";; Gonna call ~A again.~%" (first person))
      (call-person person))))
```

```lisp
CL-USER> (handler-bind ((before-call #'ensure-csgo-launched)
                        (after-call #'call-girlfriend-again))
           (call-people))
;; Calling MOM.
;; Calling DAD.
;; Launching Counter Strike for ALICE.
;; Calling ALICE.
;; Calling BOB.
;; Calling CATHERINE.
;; Calling DOROTHY.
;; Gonna call DOROTHY again.
;; Calling DOROTHY.
;; Calling ERIC.
;; Calling DENTIST.
NIL
```

We can see that the hook system that we have defined so far maps perfectly into the Lisp condition system, to the extent which we have used it for so far. Further extending the function `call-people` is analogous to extending it in the previous case, except instead of iterating through the different hook variables, we signal distinct condition types.

### Exception handling

So far, we have implemented the situation where Tom is in full control of what is going on; he is the one calling other people. But, other people have the possibility to call us as well; let us try to program this situation.

Let us assume that every person on Tom's phonebook is able to call him; they can make Tom's phone ring. Generally, he will want to answer phone calls from everyone - except for his ex. We do not want to think what happens if Tom answers a call from her - we must simply assume that this is an erroneous situation which Tom cannot recover from.

Programmatically, we could describe the naïve, always-answering code in the following way:

```lisp
(defun receive-phone-call (person)
  (format t ";; Answering a call from ~A.~%" (first person))
  (when (member :ex person)
    (format t ";; About to commit a grave mistake...~%")
    (we do not want to be here)))
```

We purposefully do not define the `(we do not want to be here)` for. If control ever reaches that form, Tom's program has failed him, and that is due to a mistake on the programmer's side; therefore, we need to ensure that this form is never reached under any circumstances.

To achieve this, we can use a method similar to the one that we used in the previous chapter. Using `throw` and `catch`, we have performed a *non-local transfer of control*; in other words, we have escaped from a part of code inside `call-people` before it led to an erroneous situation.

The Common Lisp condition system has accounted for such. There is a *subtype* of all conditions named `serious-condition`s that are signaled in these situations. Contrary to the usual, non-serious conditions, serious conditions in Common Lisp are defined so that they **must** be handled in some way.

So far, we have not discussed condition subtypes. This is a part of the Common Lisp condition system that is more powerful than hook kinds which we have implemented earlier. While hook kinds only provide lookup by means of symbol equality (e.g. calling all hooks with kind `before-call` will only invoke hooks whose kind is *equal* to the symbol `before-call`), the Common Lisp condition system allows for a *hierarchy* of condition types, in which one condition type is allowed to *subtype* one or more other condition types.

These condition types can then be utilized to trigger handlers with *distinct* condition types; for instance, a handler for the system-defined condition type `error` is going to be triggered whenever an error of any kind is signaled, no matter if it is e.g. an `undefined-function` error, a `program-error`, a plain `error` or any other condition type which itself is a subtype of `error`. Further, we may define a handler on `serious-condition`, which is going to handle all `error`s as well - since the condition type `error` is a subtype of `serious-condition`.

The inheritance model in Common Lisp condition types allows a condition type to be a subtype of *more than one condition*; for instance, it is common to define custom error types which are themselves subtypes of `error` and a different condition type that is specific to the class of problems we are in. This means that the inheritance model of conditions (which is, actually, the inheritance model of *Common Lisp Object System* in general) solves [the diamond problem](https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem) that occurs with multiple inheritance. (Details about multiple inheritance are out of scope of this book.)

We could extend the above example to illustrate the knowledge of inheritance in condition types.

```lisp
(define-condition grave-mistake (error)
  ((%reason :reader reason :initarg :reason)))

(defun receive-phone-call (person)
  (format t ";; Answering a call from ~A.~%" (first person))
  (when (member :ex person)
    (format t ";; About to commit a grave mistake...~%")
    (signal 'grave-mistake :reason :calling-your-ex)
    (we do not want to be here)))
```

We have defined the condition `grave-mistake` to be an `error`. This means, among others, that handlers that expect an `error` will now be notified when a `grave-mistake` is signaled within their scope, and their code will be run. This means that we can now write two code examples that safely defuse the situation: one that binds a handler to all `error` conditions, and another, more specialized, which binds a handler to `grave-mistake` conditions only. Specializing like this allows the programmer to query the condition objects for properties that only `grave-mistake`s have: for instance, the `reason` that each grave mistake condition has.

```lisp
(defun defuse-error (condition)
  (declare (ignore condition))
  (format t ";; Nope nope nope, not answering!~%")
  (throw :nopenopenope nil))

(defun defuse-grave-mistake (condition)
  (let ((reason (reason condition)))
    (format t ";; Nope nope nope, not answering - ~A!~%" reason))
  (throw :nopenopenope nil))
```

```lisp
CL-USER> (handler-bind ((error #'defuse-error))
           (dolist (person *phonebook*)
             (catch :nopenopenope
               (receive-phone-call person))))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope nope nope, not answering!
;; Answering a call from DOROTHY.
;; Answering a call from ERIC.
;; Answering a call from DENTIST.
NIL

CL-USER> (handler-bind ((grave-mistake #'defuse-grave-mistake))
           (dolist (person *phonebook*)
             (catch :nopenopenope
               (receive-phone-call person))))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope nope nope, not answering - CALLING-YOUR-EX!
;; Answering a call from DOROTHY.
;; Answering a call from ERIC.
;; Answering a call from DENTIST.
NIL
```

So far, so good. However, handling this kind of errors is left solely to the handlers, and so the `receive-phone-call` function is itself dangerous. Let us imagine what happens if Tom ever forgets to handle that condition, and simply answers calls from everyone, in turn, from his phonebook...

```lisp
CL-USER> (dolist (person *phonebook*)
           (catch :nopenopenope
             (receive-phone-call person)))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;;
;;
;;
;; Phone call answered.
;;
;;
;;
;; Grave mistake successfully committed.
;;
;;
;;
;; ...so, what do we do now?
```

We have passed control to a form that should not have executed under any circumstances. We had an erroneous situation in our program, and we have nonetheless allowed the program to proceed. At this point, *the behaviour of the program is undefined*, and so are the results of it. We no longer know what is the state of Tom or what has the call been like. We may never know it. Or we may, after which we would wish we never got to know it.

This is why `signal` is not powerful enough to handle erroneous situations. There is a function that is more powerful than that, though: powerful enough to invoke the ultimate means of saving us from the problem. That function is named `error`, and using it in place of `signal` is enough to prevent the ultimate from happening.

```lisp
(defun receive-phone-call (person)
  (format t ";; Answering a call from ~A.~%"
          (first person))
  (when (member :ex person)
    (format t ";; About to commit a grave mistake...~%")
    (error 'grave-mistake :reason :calling-your-ex)
    (we do not want to be here)))
```

The function `error` works by first `signal`ing the condition in question. This allows the external handlers to function just like they would with standard `signal`: they can intercept the control flow and route it out on their own terms. However, if no handlers decide to transfer control outside, and therefore `signal` returns, `error` then calls the ultimate means of saving Tom from making regrettable life choices: it calls `invoke-debugger` with the condition object as its argument.

The function `invoke-debugger`, and the Lisp debugger, will be described in later chapters. For now, all we need to know about it is that it is the equivalent of a turtle falling on its back and wiggling its limbs hopelessly in the air; the program has exhausted all chances of handling an error gracefully and, therefore, has no choice but to handle it disgracefully.

The debugger is an *interactive* condition handler; it is called to prevent crashing the system by giving the programmer an interface to manually handle conditions. Once it is invoked, execution of the program is effectively paused, and Lisp requires programmer attention in order to resume the program. That means that there is no way to return programmatically from the debugger whatsoever.

It is not a pleasant situation to be in by any means, but it is at least a defined one - for all imaginable cases, we would rather want Tom to think, "damn, now I need to call a Lisp technician to fix that program for me" than think, "damn, why in the heavens have I even answered this call from her".

For now, let us consider the debugger as a place of controlled programmer failure. We can assume the debugger will prevent us from doing unwise things, such as adding `42` to `"42"` or having Tom answer a call from someone he should not answer a call from. Nonetheless, it is a programmer failure - and so we will discuss one more tool that helps us defend against errors in places where we expect them to happen.

Let's once more look at the previous, safe way of calling `receive-phone-call`, in which we simplify the previously passed `defuse-grave-mistake` to its very minimum.

```lisp
(handler-bind ((grave-mistake (lambda (condition)
                                (declare (ignore condition))
                                (throw :nopenopenope nil))))
  (dolist (person *phonebook*)
    (catch :nopenopenope
      (receive-phone-call person))))
```

The routing of control is as follows. The function `receive-phone-call` is called on every member of the `*phonebook*`. However, it is possible that `receive-phone-call` may signal an `error`; if this happens, we want to immediately stop whatever was going on in `receive-phone-call` and return to the `catch` form. That form is inside the `dolist` call, which means that walking the phonebook will continue; if the `catch` form encompassed all of `dolist`, signaling an error would instead stop iterating through the phonebook.

This is an idiom that is frequent in programming: attempt to do something, and if doing that something would result in an error, recover and do something else instead. That pattern is called *exception handling*, or *try-catch* from the most famous keywords that implement this behaviour in many programming languages.

In Lisp, the macro that implements this idiom is named `handler-case`. In our example, it will have the following syntax:

```lisp
(dolist (person *phonebook*)
  (handler-case (receive-phone-call person)
    (grave-mistake (condition)
      (format t ";; Nope, not this time: ~A~%" (reason condition)))))
```

If, in the above example, `(receive-phone-call person)` returns normally, then - like in `handler-bind` - no handlers whatsoever are invoked. If, however, an error is signaled and a matching `handler-case` handler is found, the condition object becomes bound to the variable `condition` in the matching handler, and control continues inside that handler. The body of that handler is executed, and the return value of `handler-case` is computed from it.

(Detail: in Lisp, `handler-case` returns a value, as opposed to the `try`/`catch` of C-like languages. If the main form returns normally, its return value is returned from `handler-case`; otherwise, it is the value returned by the handler that handled the signaled condition. There is one exception to this rule, which requires the special `:no-error` handler to be defined; if it is present, then its return value is returned in case of no error and the return value of the main form is ignored.)

We can execute this code snippet and see that it performs as intended:

```lisp
CL-USER> (dolist (person *phonebook*)
           (handler-case (receive-phone-call person)
             (grave-mistake () (format t ";; Nope, not this time.~%"))))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope, not this time.
;; Answering a call from DOROTHY.
;; Answering a call from ERIC.
;; Answering a call from DENTIST.
NIL
```

In other words, `handler-case` is a shorter way of performing a non-local transfer of control, when compared to `handler-bind`, but is also somewhat less powerful. While `handler-bind` allows one to execute hook functions at the site where a condition is signaled before continuing standard function execution, `handler-case` *always* performs a non-local transfer of control *outside* the expression that was executing, which unconditionally short-cirtuits the original control flow.

In other words: `handler-case` *always unwinds the stack*, whereas `handler-bind` *does not necessarily unwind the stack*, as it leaves the choice of whether and how to do that to each individual handler.

In yet another words: if `handler-bind` is a way to execute hooks in Lisp, then `handler-case` is a way to execute exception handling. It is possible to implement `handler-case` via `handler-bind` (we have done a simple version of that in the earlier chapter!), but not the other way around.

Finally: if we would like to make our code *very* dirty (for example, when we are writing quick, temporary hacks that will only last forever), we can use one more operator that is equivalent to creating a `handler-case` handler on all errors: `ignore-errors`. It does what its name says: in case an error is signaled, control is unconditionally transferred outside of the form that is being executed, and no other actions are performed.

```lisp
CL-USER> (dolist (person *phonebook*)
           (ignore-errors (receive-phone-call person)))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Answering a call from DOROTHY.
;; Answering a call from ERIC.
;; Answering a call from DENTIST.
NIL
```

(Technically speaking, `ignore-errors` is equivalent to a `handler-case` with a single handler for `error` which returns two values: `nil`, and the error object that was being signaled.)

### Protection against transfers of control

We could complicate the above scenario in one more way. Let us assume that Tom has a very broken phone that locks up every time the call finishes. It doesn't matter whether the call was answered and lasted for an hour (for instance, if Tom's girlfriend was calling), whether is was immediately rejected (for instance, if Tom's mother was calling), or even if Tom pretended he did not hear his phone whatsoever (for instance, if Tom's ex was calling): after each of these situations, he needs to pry the battery out of his phone, put it back inside, and start it anew.

Technically speaking, this programming idiom is an extension of the `try`/`catch` pattern - usually, it is named `finally`, giving the pattern the final name of `try`/`catch`/`finally`. The code marked as `finally` executes unconditionally after the `try`/`catch` part, no matter whether the code block in `try` has returned normally or whether control was transferred out by means of handling an exception.

The Common Lisp standard lists a rich collection of ways to perform non-local transfers of control (for short, *NLToCs*). The standard operators that always perform a NLToC include `go` (in tandem with `tagbody`), `throw` (in tandem with `catch`), `return`/`return-from` (in tandem with `block`) `handler-case`, `ignore-errors`, and `with-simple-restart` (which we will discuss later, in the chapter related to restarts). The form `unwind-protect` "protects" against all of these means of transferring control outside of the *protected form* and forces the *cleanup forms* to be executed right after control leaves the protected form, before any other code is run.

We could therefore say that `unwind-protect` is Lisp's way of implementing the `finally` part of that idiom. And so, we may utilize that form to implement Tom needing to restart the phone each single time.

Let us work on this example:

```lisp
CL-USER> (dolist (person *phonebook*)
           (catch :nopenopenope
             (handler-case (receive-phone-call person)
               (grave-mistake (e) (defuse-grave-mistake e)))))
;; Answering a call from MOM.
;; Answering a call from DAD.
;; Answering a call from ALICE.
;; Answering a call from BOB.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope nope nope, not answering - CALLING-YOUR-EX
;; Answering a call from DOROTHY.
;; Answering a call from ERIC.
;; Answering a call from DENTIST.
NIL
```

(In the above example, we use `handler-case`, but `handler-bind` would work as well. The function `defuse-grave-mistake` performs a NLToC by `throw`ing at the `:nopenopenope` catch tag, therefore it does not matter whether it is called directly, as via `handler-bind`, or with one more indirection, as via `handler-case`.)

It is possible to perform a NLToC by means of `throw`ing anything to the `:nopenopenope` catch tag. This means that the following code (with a reduced phonebook for clarity) will not work correctly in case of an error:

```lisp
CL-USER> (dolist (person '((:bob :classmate :homework)
                           (:catherine :classmate :ex)
                           (:dorothy :classmate :girlfriend :csgo)))
           (catch :nopenopenope
             (handler-case (receive-phone-call person)
               (grave-mistake (e) (defuse-grave-mistake e)))
             (format t ";; Restarting phone.~%")))
;; Answering a call from BOB.
;; Restarting phone.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope nope nope, not answering - CALLING-YOUR-EX
;; Answering a call from DOROTHY.
;; Restarting phone.
NIL
```

We can see that Tom restarted his phone after Bob and Dorothy called him, but we do not see the same happening for Catherine. This is fixable by adding `unwind-protect`:

```lisp
CL-USER> (dolist (person '((:bob :classmate :homework)
                           (:catherine :classmate :ex)
                           (:dorothy :classmate :girlfriend :csgo)))
           (catch :nopenopenope
             (unwind-protect
                  (handler-case (receive-phone-call person)
                    (grave-mistake (e) (defuse-grave-mistake e)))
               (format t ";; Restarting phone.~%"))))
;; Answering a call from BOB.
;; Restarting phone.
;; Answering a call from CATHERINE.
;; About to commit a grave mistake...
;; Nope nope nope, not answering - CALLING-YOUR-EX
;; Restarting phone.
;; Answering a call from DOROTHY.
;; Restarting phone.
NIL
```

To summarize: using `handler-bind` with an unconditional transfer of control, we have constructed `handler-case`: a means of handling errors by means of transferring control outside of the erroring forms. In addition, we have introduced `unwind-protect`, a means of running cleanup code that is executed even if control leaves a form in an unnatural way.

### Clustering

One property of handlers which we have not yet touched is *clustering*. In short, clustering handler together means that a handler does not "see" any handlers bound in the same `handler-bind` form - meaning that it cannot cause itself or its "neighbors" to become invoked.

Let us use a short synthetic example for that. We establish one "outer" handler in one `handler-bind` form, and then we establish three "inner" handlers inside it, with the second "inner" handler resignaling the condition that it gets. Finally, as the final form, we signal a single condition. (The same rule applies for `handler-case`.)

```lisp
CL-USER> (handler-bind ((condition (lambda (condition)
                                     (declare (ignore condition))
                                     (format t ";; Outer handler~%"))))
           (handler-bind ((condition (lambda (condition)
                                       (declare (ignore condition))
                                       (format t ";; Inner handler A~%")))
                          (condition (lambda (condition)
                                       (format t ";; Inner handler B~%")
                                       (signal condition)))
                          (condition (lambda (condition)
                                       (declare (ignore condition))
                                       (format t ";; Inner handler C~%"))))
             (signal 'condition)))
;; Inner handler A
;; Inner handler B
;; Outer handler
;; Inner handler C
;; Outer handler
```

The result is as following. First, inner handler A is invoked. Second, inner handler B is invoked, and it resignals that condition. However, due to clustering rules, all of the inner handlers become "invisible" to the `signal` call within the inner handler B; this is why only the outer handler is invoked. Next, the inner handler C is invoked, and then, finally, the outer handler is invoked *for the second time*. (The second invocation of the outer handler came from the innermost `signal` call, whereas the first invocation - from the `signal` call embedded in the inner handler B.)

An important corollary of this fact is that a handler may never call its own self when using standard handler-based mechanisms.

Such a rule, even though it is not fully intuitive, allows for better structuring of distinct layers of handlers. We can rely on all handlers within a single cluster not getting in each other's (and their own) way if they decide to (re)signal a condition.

### Summary

This concludes the part of this book about the handler subsystem of the Common Lisp condition system. In the earlier chapters, we have constructed the basic system of hooks, which turned out to be very similar to the system of condition handlers which we have described in these chapters.

In the next part of the book, we will focus on constructing another subsystem of Common Lisp, which has a different principle of working.

## A simple system of choices

Let us leave Tom for a moment and focus on a different person of the story: Catherine. Catherine, or Kate, had a boyfriend named Tom. That is no longer the case, though, and she has moved on with her life. She has met a marvelous young programmer named Mark, whom she meets in her house when her parents are not home in order to indulge in intense Lisp studying sessions.

There is a problem, however. Kate's parents are a pair of hardcore COBOL mainframe programmers of the old generation, who strongly despise dynamic programming and would promptly garbage-collect Mark and sweep him into the trash bin the moment they saw him in their household. Then, they would proceed to ruthlessly scold Kate about her life and professional choices as a growing mainframe programmer.

Kate therefore needs to be particularly cautious about her first steps with Mark in the world of interactive programming. If her parents unexpectedly return home while she is in the middle of analyzing a Lisp program with Mark, she needs that fact to not be discovered, no matter the costs. Since there are many ways in which her mother and father may notice her relationship with Mark, she needs a versatile and adaptive strategy of covering it up. The most important requirement for that new strategy is that Kate needs to be able to **arbitrarily choose** a single option from the various ones she has at any given moment instead of utilizing them all in order.

Because of this last constraint, the previously constructed mechanism of hooks or handlers are not suited for this kind of use case. Let us construct a mechanism that allows us to inspect the list of available *choices* we have at any given moment and arbitrarily select the ones that we choose to be the most proper.

### Kate and Mark

First, we need to set the scene. There is a house in which Kate lives. This house has two pairs of doors: front door and back door, both of which may be locked or unlocked. Kate's room is on the first floor, and in every scene it initially contains Mark.

We can represent that state as a set of dynamic variables that we will then rebind around calls to our main function.

```lisp
(defvar *mark-safe-p* nil)
(defvar *front-door-locked-p* t)
(defvar *back-door-locked-p* t)
```

We shall also define the main function that will illustrate a situation in which Kate's parents come home. The value of `*mark-safe-p*` should be true by the time we have finished covering Mark up, which is visible in the following body of code:

```lisp
(defun parents-come-back ()
  (format t ";; Uh oh - Kate's parents are back!~%")
  (try-to-hide-mark)
  (if *mark-safe-p*
      (format t ";; Whew... We're safe! For now.~%")
      (we do not want to be here)))
```

Once again, we do not define the `(we do not want to be here form)`; in the event control reaches this form, we kindly declare that the behaviour is undefined, both for Mark and for Kate. To keep our code modular, the functionality for recovering from that situation should stay within the `try-to-hide-mark` function and within the dynamic scope in which `parents-come-back` is called.

Therefore, we need to split our implementation between the dynamic scope of `parents-come-back` and the body of `try-to-hide-mark`. The former will contain the cover-up choices available for Kate and Mark at any given moment; the latter, the actual logic for choosing the proper choice and utilizing it.

### Choice #1: escape

In order to implement our system of choices, we will first need to model an individual choice as well as means of interacting with it. First, we will need some means of referring to any given choice, perhaps by a name that could be a Lisp symbol. Second, we will need to be able to execute arbitrary code associated with such a choice, in order to actually affect the state of our code and be able to cover Kate and Mark up. Third, we will need means of checking whether a given option is applicable to a given moment; there is no worse situation than e.g. Mark trying to escape through a window only to find that Kate's father, standing next to it, gives Mark a surprised stare.

Therefore, each individual choice will have three components to it: a name, by which we will refer to that given choice; an effect function, which will be executed whenever that choice is chosen; and a test function, which will be called to check if a given choice is applicable to the current situation.

While we could model our choice as a list that contains these three elements, we should instead define a proper choice structure that we will later use for code clarity. For that, we will use a Common Lisp facility named `defstruct`. This tutorial will not go in depths about how to use `defstruct`; all that matters for us is that evaluating the following form will define the function `make-choice` for creating new choice objects, as well as functions `choice-name`, `choice-effect-function`, and `choice-test-function` for reading the values from a choice object.

```lisp
(defstruct choice
  (name (error "Must provide :NAME."))
  (effect-function (error "Must provide :EFFECT-FUNCTION."))
  (test-function (constantly t)))
```

In the above `defstruct`form, the `(error "...")` subforms mean that assigning a `name` and `effect-function` to a newly created choice is mandatory; it is a *default initial form* that, upon evaluation, will signal an error. The `(constantly t)` subform returns a function which, in turn, always returns true. Therefore, passing as the default initial form for `test-function` means that the test function will always return true, and the choice will always be visible. We choose it as a default since we decide that choices, by default, should always be visible - that is, unless a programmer decides otherwise.

The above means that we can now create choice objects that represent means of covering up Kate and Mark's Lisp studies. Let us define one means of escaping for the time being: escaping through the front door. Its effect should be setting `*mark-safe-p*` to a true value, and it should only be available when `*front-door-locked-p*` is false, as Mark cannot escape through a closed door.

Let us define a pair of helper functions that will perform these actions along with printing debug information for us.

```lisp
(defun perform-escape-through-front-door ()
  (format t ";; Escaping through the front door.~%")
  (setf *mark-safe-p* t))

(defun escape-through-front-door-p ()
  (format t ";; The front door is~:[ not~;~] locked.~%" *front-door-locked-p*)
  (not *front-door-locked-p*))
```

This allows us to create a choice object and interact with it in the following manner:

```lisp
CL-USER> (defvar *our-first-choice*
           (make-choice
            :name 'escape-through-front-door
            :effect-function #'perform-escape-through-front-door
            :test-function #'escape-through-front-door-p))
*OUR-FIRST-CHOICE*

CL-USER> *our-first-choice*
#S(CHOICE
   :NAME ESCAPE-THROUGH-FRONT-DOOR
   :EFFECT-FUNCTION #<FUNCTION PERFORM-ESCAPE-THROUGH-FRONT-DOOR>
   :TEST-FUNCTION #<FUNCTION ESCAPE-THROUGH-FRONT-DOOR-P>)

CL-USER> (choice-name *our-first-choice*)
ESCAPE-THROUGH-FRONT-DOOR

CL-USER> (choice-effect-function *our-first-choice*)
#<FUNCTION PERFORM-ESCAPE-THROUGH-FRONT-DOOR>

CL-USER> (choice-test-function *our-first-choice*)
#<FUNCTION ESCAPE-THROUGH-FRONT-DOOR-P>
```

Since Kate's house is symmetrical and we treat the front and back door the same, we can define a pair of helper functions for handling the back door as well.

```lisp
(defun perform-escape-through-back-door ()
  (format t ";; Escaping through the back door.~%")
  (setf *mark-safe-p* t))

(defun escape-through-back-door-p ()
  (format t ";; The back door is~:[ not~;~] locked.~%" *back-door-locked-p*)
  (not *back-door-locked-p*))
```

This means that we can now also create a choice for escaping through the back door via the following form:

```lisp
(make-choice
 :name 'escape-through-back-door
 :effect-function #'perform-escape-through-back-door
 :test-function #'escape-through-back-door-p)
```

Now that we have values that we can bind to a variable listing all available choices, we should define the variable in question and give it an empty initial value.

```lisp
(defvar *choices* '())
```

Only one thing prevents us from executing `parents-come-back`; we have not yet implemented the function `try-to-hide-mark` that is called therein. We can do it now.

```lisp
(defun try-to-hide-mark ()
  (let ((choices (loop for choice in *choices*
                       when (funcall (choice-test-function choice))
                         collect choice)))
    (if choices
        (let ((choice (first choices)))
          (format t ";; Performing ~A.~%" (choice-name choice))
          (funcall (choice-effect-function choice)))
        (format t ";; Kate cannot hide Mark!~%"))))
```

The algorithm has the following path. First of all, all the choices from `*choices*` are scanned for whether they apply to a given situation. If yes, such a choice is collected for later processing. Once we have the list of applicable choices, we ensure that it is not empty; once that is done, we perform our logic for choosing and invoking the choices.

For the time being, we will choose the choice object that we use in a simple manner: we will use the first one that we find. This logic will, however, become more complex later in the chapter.

The `loop` form in the above function is somewhat large. We can factor it into a separate function for clarity.

```lisp
(defun compute-choices ()
  (loop for choice in *choices*
        when (funcall (choice-test-function choice))
          collect choice))

(defun try-to-hide-mark ()
  (let ((choices (compute-choices)))
    (if choices
        (let ((choice (first choices)))
          (format t ";; Performing ~A.~%" (choice-name choice))
          (funcall (choice-effect-function choice)))
        (format t ";; Kate cannot hide Mark!~%"))))
```

We have implemented the function `try-to-hide-mark`; this means that we can now construct an example form that represents a situation when Kate's parents come back, in which we would like to have the options of escaping through the front and back doors.

```lisp
(let ((*choices*
        (list (make-choice
               :name 'escape-through-front-door
               :effect-function #'perform-escape-through-front-door
               :test-function #'escape-through-front-door-p)
              (make-choice
               :name 'escape-through-back-door
               :effect-function #'perform-escape-through-back-door
               :test-function #'escape-through-back-door-p))))
  (let ((*mark-safe-p* nil)
        (*front-door-locked-p* nil)
        (*back-door-locked-p* nil))
    (parents-come-back)))
```

That form is rather unwieldy to type. We can notice that the most of that code consists of a part that is somewhat static: a list of all choices that we offer for successfully covering up Mark's presence in Kate's home. We can factor it out into a separate function that establishes the proper dynamic environment and then calls a [thunk function](https://en.wikipedia.org/wiki/Thunk) in that environment.

```lisp
(defun call-with-home-choices (thunk)
  (let ((*choices*
          (list (make-choice
                 :name 'escape-through-front-door
                 :effect-function #'perform-escape-through-front-door
                 :test-function #'escape-through-front-door-p)
                (make-choice
                 :name 'escape-through-back-door
                 :effect-function #'perform-escape-through-back-door
                 :test-function #'escape-through-back-door-p))))
    (funcall thunk)))
```

(In Lisp, this behaviour is usually achieved by writing a *macro*, rather than a function; it could be called `with-home-choices` and accept code that we would want to run in the dynamic scope of that form. However, since this is not a tutorial on macro-writing, we use a function instead to achieve the same result.)

The above allows us to shorten our form to the following:

```lisp
(call-with-home-choices
 (lambda ()
   (let ((*mark-safe-p* nil)
         (*front-door-locked-p* nil)
         (*back-door-locked-p* nil))
     (parents-come-back))))
```

We can now attempt executing this form and seeing how it behaves.

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil)
                  (*back-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is not locked.
;; The back door is not locked.
;; Performing ESCAPE-THROUGH-FRONT-DOOR.
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL
```

If only the front door is unlocked, not much changes:

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is not locked.
;; The back door is locked.
;; Performing ESCAPE-THROUGH-FRONT-DOOR.
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL
```

If only the back door is unlocked, Mark still has a way of escaping:

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*back-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is locked.
;; The back door is not locked.
;; Performing ESCAPE-THROUGH-BACK-DOOR.
;; Escaping through the back door.
;; Whew... We're safe! For now.
NIL
```

If both doors are locked... well. That is a situations that we do not want to evaluate in practice. We can, however, evaluate `try-to-hide-mark` instead of `parents-come-back` in such an environment, assuming that it is a practice exercise for later.

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil))
              (try-to-hide-mark))))
;; The front door is locked.
;; The back door is locked.
;; Kate cannot hide Mark!
NIL
```

### Choice #2: excuses

The above mechanism is based on the assumption that all means of escape are equal and do not require any data passed to them from the call site. We are doing it this way since both choices we have established have functions that accept zero arguments; we can call their effect functions without passing any data to them.

Additionally, both of the choices we have created perform essentially the same thing: they allow Mark to escape without confronting Kate's parents, even if through different means. We shall establish a convention of naming choices that perform the same action with the same symbol. This way, client code could check for presence of *any* choice named `escape` - without needing to know the exact details of the escape route.

Let us rewrite our code to take this into account.

```lisp
(defun call-with-home-choices (thunk)
  (let ((*choices*
          (list (make-choice
                 :name 'escape
                 :effect-function #'perform-escape-through-front-door
                 :test-function #'escape-through-front-door-p)
                (make-choice
                 :name 'escape
                 :effect-function #'perform-escape-through-back-door
                 :test-function #'escape-through-back-door-p))))
    (funcall thunk)))
```

The function `call-with-home-choices` only requires the choice names to be changed. This means that we can also change the implementation of `try-to-hide-mark` to be more precise: instead of calling the first available choice, we can instead try to *find* a choice named `escape`. For this, we introduce a new function, `find-choice`, and a convenience function `invoke-choice` that will call the effect function of the choice with a given name while passing arguments to it.

```lisp
(defun find-choice (name)
  (loop for choice in *choices*
        when (and (funcall (choice-test-function choice))
                  (eq name (choice-name choice)))
          return choice))

(defun invoke-choice (name &rest arguments)
  (let ((choice (find-choice name)))
    (apply (choice-effect-function choice) arguments)))

(defun try-to-hide-mark ()
  (if (find-choice 'escape)
      (invoke-choice 'escape)
      (format t ";; Kate cannot hide Mark!~%")))
```

We can check that our code still works, albeit slightly differently:

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is not locked.
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*back-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is locked.
;; The back door is not locked.
;; Escaping through the back door.
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil))
              (try-to-hide-mark))))
;; The front door is locked.
;; The back door is locked.
;; Kate cannot hide Mark!
NIL
```

We have unified escape-related choices under a single name, `escape`. We will now extend our means of Mark's survival with another tactic: allowing him to talk his way out of the difficult situation he found himself in.

```lisp
(defvar *excuses*
  '("Kate did not divide her program into sections properly!"
    "I was borrowing Kate's books on mainframe programming!"
    "I had COBOL-related homework and hoped Kate could help me!"))

(defun perform-excuse (excuse)
  (format t ";; Mark excuses himself before leaving:~%;; \"~A\"~%" excuse)
  (setf *mark-safe-p* t))
```

We assume that Mark, when he says one of the above excuses, should be able to calm down Kate's parents' suspicion and successfully sneak out of their household. Confronting Kate's parents is nonetheless an option of last resort: if preferable, they should not even be aware that Mark visited their daughter. Our logic must take this into account and first try and attempt for an escape.

```lisp
(defun try-to-hide-mark ()
  (cond ((find-choice 'escape)
         (invoke-choice 'escape))
        (t
         (format t ";; Kate cannot hide Mark!~%")
         (when (find-choice 'excuse)
           (let ((excuse-text (elt *excuses* (random (length *excuses*)))))
             (invoke-choice 'excuse excuse-text))))))
```

The above code will work, but we have not yet actually established an `excuse` choice. To do so, we need to modify `call-with-home-choices`.

```lisp
(defun call-with-home-choices (thunk)
  (let ((*choices*
          (list (make-choice
                 :name 'excuse
                 :effect-function #'perform-excuse)
                (make-choice
                 :name 'escape
                 :effect-function #'perform-escape-through-front-door
                 :test-function #'escape-through-front-door-p)
                (make-choice
                 :name 'escape
                 :effect-function #'perform-escape-through-back-door
                 :test-function #'escape-through-back-door-p))))
    (funcall thunk)))
```

(As mentioned before, we want the `excuse` choice to be always available, therefore we do not provide it with a `:test-function`.)

With this in place, we can check whether Kate's and Mark's love for Lisp and each other can survive in the inevitable scenario where the doors are locked and Mark needs to confront Kate's mother and father:

```lisp
CL-USER> (call-with-home-choices
          (lambda ()
            (let ((*mark-safe-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; The front door is locked.
;; The back door is locked.
;; Kate cannot hide Mark!
;; Mark excuses himself before leaving:
;; "Kate did not divide her program into sections properly!"
;; Whew... We're safe! For now.
NIL
```

Looks like Mark is safe... for now.

We are allowed to further complicate this example, both by defining more choices inside the body of `call-with-home-choices` and by complicating the choice-invoking logic inside `try-to-hide-mark`. Perhaps there should be an option of jumping out of the window in Kate's room, if there is a conveniently placed haystack just beneath it and if Mark feels like performing a leap of faith; maybe there could be a laundry pile in Kate's room that Mark could hide in... unless that particular day is laundry day, and Kate's mother comes in to pick the clothes up for washing. Just as in the example with hooks; this means that only these two functions need to be adjusted: one, to actually offer choice objects to code executed inside its dynamic scope, and the other - to compute the available choices and decide on which of them, and under what conditions, should be used. Such complication is left as an exercise for the reader.

### Summary

To summarize: we have created a system that internally functions in a similar way to the hook system that we have implemented earlier, given that it works based on a single dynamic variable and a list of choice objects bound to it. Invoking a given choice has exactly the same main effect as calling a hook - executing arbitrary code that was provided to us earlier in the dynamic scope.

However, the functioning of that system is distinct from the hook system. In the system of hooks, client code merely calls all hooks that were provided to it; in the system of choices, client code is given much more capability as to which choices it wants to interact with, both because of the choice objects having names and because of them not being always applicable to a given dynamic situation of the program (via the test function).

In addition to the above, there is one aspect of the choice system which we have not used in the above example. A choice is allowed to merely perform some computation and - contrary to hooks - *return a value*, which is then returned from the place where the choice was invoked. This, in addition to the differences described above, makes choices a more complex and therefore more versatile toolset than hooks, better suited for situations where client code needs to compute recovery strategies more complex than "let's hope that one of the hooks gets us out of there, and if not - invoke the debugger".

As we have seen so far, choices, just like hooks, are not required to transfer control outside of the normal program control flow. If a choice does not perform a NLToC and instead returns, the `invoke-choice` call site returns immediately without performing any other actions.

Therefore, choices express a different mechanism of control than hooks. With hooks, a given situation which has occurred in code may cause one or more externally specified bodies of code to be executed in an order defined by the dynamic scope. With choices, the bodies of code are still provided dynamically, but it is *the invoking code* that chooses which of these code bodies, under what circumstances, and in which order are utilized in order to work with a given situation.

## A simple system of restarts

Truth be told, in the previous chapter, we have done a lot of work that was, strictly speaking, not necessary for production code. This is because, through constructing the choice system from its smallest parts, we have constructed a simple version of a system that already exists in standard Common Lisp - a system of *restarts*. A restart in Common Lisp is defined by things similar to the choice objects defined by us: a name (usually), a function utilized for invoking its effects (called a *restart function* in Common Lisp), and a test function that checks whether a given restart should be visible, given the current state of the program. (There are other qualities to restarts that I omit here for clarity; we will talk about them later, when we discuss the Common Lisp debugger.)

To show this, we may rewrite the earlier examples while using restarts. The initial code for setting the stage for us is going to be the same:

```lisp
(defvar *mark-safe-p* nil)
(defvar *front-door-locked-p* t)
(defvar *back-door-locked-p* t)
```

We do not need to manually create the restart structure, since Common Lisp has it created for us in form of the `restart` system class. We do not manually create instances of that class, though; Common Lisp provides us with a macro named `restart-bind` which creates these objects for us. (The reason for this is that restart objects in Common Lisp have *dynamic extent*; this means that accessing them is only legal within the dynamic scope in which they were created.)

We want to establish a restart named `escape-through-front-door`, which prints a debug message and unconditionally sets `*mark-safe-p*` to true. In addition, it must only be visible if the front door is unlocked. An example syntax for such

```lisp
(restart-bind ((escape-through-front-door
                 (lambda ()
                   (format t ";; Escaping through the front door.~%")
                   (setf *mark-safe-p* t))
                 :test-function
                 (lambda (condition)
                   (declare (ignore condition))
                   (not *front-door-locked-p*))))
  ...)
```

(The test function for a restart must accept one argument, since, in some circumstances, the Lisp restart system passes a condition object to that function. What this object is for will be covered in a later part of this book; for now, this function ignores the argument passed to it.)

For code clarity, let us once again define the helper functions for our restart for escaping through the front door - this time, skipping some verbosity in the test function.

```lisp
(defun perform-escape-through-front-door ()
  (format t ";; Escaping through the front door.~%")
  (setf *mark-safe-p* t))

(defun escape-through-front-door-p (condition)
  (declare (ignore condition))
  (not *front-door-locked-p*))
```

This allows us to shorten our code to the following:
```lisp
(restart-bind ((escape-through-front-door
                 #'perform-escape-through-front-door
                 :test-function #'escape-through-front-door-p))
  ...)
```

We may now compute the restarts available within the dynamic scope of that form. We will print their names to ensure that the restart we have created is on top of that list. For that, we will utilize the predefined `compute-restarts` function, offered to us by Common Lisp. It behaves in the same way as `compute-choices` did for choices from the previous example, offering us a list of restarts objects that we can query for their names using the `restart-name` function.

```lisp
CL-USER> (restart-bind ((escape-through-front-door
                          #'perform-escape-through-front-door
                          :test-function #'escape-through-front-door-p))
           (let ((*mark-safe-p* nil)
                 (*front-door-locked-p* nil))
             (let* ((restarts (compute-restarts))
                    (names (mapcar #'restart-name restarts)))
               (format t "~{;; ~A~%~}" names))))
;; ESCAPE-THROUGH-FRONT-DOOR
;; RETRY
;; ABORT
;; ABORT
NIL
```

The `escape-through-back-door` restart is available on that list as the first restart, but we also have several restarts that have been established earlier by the Lisp system we program in. (Since they are established by the system, the list may look different in another Lisp image.) These restarts are not of any relevance to us and are used by the Lisp system as the ultimate means of recovering from errors. We will therefore perform a bit of additional work to filter the system-provided restarts out from out from the restarts that we establish ourselves.

```lisp
(defvar *toplevel-restarts* '())

(defun compute-relevant-restarts (&optional condition)
  (set-difference (compute-restarts condition) *toplevel-restarts*))
```

(The reason for the existence of that `&optional condition` argument will be explained later in the book.)

We can bind the variable `*toplevel-restarts*` to compute the restarts available before entering our dynamic scope, and then use our new function `compute-relevant-restarts` to filter these out from the list.

```lisp
CL-USER> (let ((*toplevel-restarts* (compute-restarts)))
           (restart-bind ((escape-through-front-door
                            #'perform-escape-through-front-door
                            :test-function #'escape-through-front-door-p))
             (let ((*mark-safe-p* nil)
                   (*front-door-locked-p* nil))
               (let* ((restarts (compute-relevant-restarts))
                      (names (mapcar #'restart-name restarts)))
                 (format t "~{;; ~A~%~}" names)))))
;; ESCAPE-THROUGH-FRONT-DOOR
NIL
```

As long as we use the `compute-relevant-restarts` function, we can only access the restart we have created ourselves. We can similarly expand our code with a restart available for escaping through the back door.

```lisp
(defun perform-escape-through-back-door ()
  (format t ";; Escaping through the back door.~%")
  (setf *mark-safe-p* t))

(defun escape-through-back-door-p (condition)
  (declare (ignore condition))
  (not *back-door-locked-p*))
```

```lisp
CL-USER> (let ((*toplevel-restarts* (compute-restarts)))
           (restart-bind ((escape-through-front-door
                            #'perform-escape-through-front-door
                            :test-function #'escape-through-front-door-p)
                          (escape-through-back-door
                            #'perform-escape-through-back-door
                            :test-function #'escape-through-back-door-p))
             (let ((*mark-safe-p* nil)
                   (*front-door-locked-p* nil)
                   (*back-door-locked-p* nil))
               (let* ((restarts (compute-relevant-restarts))
                      (names (mapcar #'restart-name restarts)))
                 (format t "~{;; ~A~%~}" names)))))
;; ESCAPE-THROUGH-BACK-DOOR
;; ESCAPE-THROUGH-FRONT-DOOR
NIL
```

Our code is becoming unwieldy. We can separate it into three logical parts: the first, where we capture the toplevel restarts and establish new restarts of our own; the second, where we declare the state of the world; and the third, where we utilize the available restarts in some way (so far: to print their names). We may separate them in order to achieve greater code clarity and separate the different concerns of our program.

Let us separate the first part from the rest. We will factor it out into a function `call-with-home-restarts` that, just like `call-with-home-choices` from the chapter above, will establish the new restarts for us; in addition, however, it will also set the `*toplevel-restarts*` variable for filtering out the restarts not relevant for us.

```lisp
(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts)))
    (restart-bind ((escape-through-front-door
                     #'perform-escape-through-front-door
                     :test-function #'escape-through-front-door-p)
                   (escape-through-back-door
                     #'perform-escape-through-back-door
                     :test-function #'escape-through-back-door-p))
      (funcall thunk))))
```

We can verify that our code, when called by `call-with-home-restarts`, behaves in the expected manner. If we call it with both doors unlocked, we have both restarts available:

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil)
                  (*back-door-locked-p* nil))
              (let* ((restarts (compute-relevant-restarts))
                     (names (mapcar #'restart-name restarts)))
                (format t "~{;; ~A~%~}" names)))))
;; ESCAPE-THROUGH-BACK-DOOR
;; ESCAPE-THROUGH-FRONT-DOOR
NIL
```

On the other hand, if both doors are locked, we have no way to let Mark escape:

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (let* ((restarts (compute-relevant-restarts))
                     (names (mapcar #'restart-name restarts)))
                (format t "~{;; ~A~%~}" names)))))
NIL
```

This brings our restart-based implementation on par with the choice-based one that we have created ourselves. We will use the same main function as we did in the earlier chapter:

```lisp
(defun parents-come-back ()
  (format t ";; Uh oh - Kate's parents are back!~%")
  (try-to-hide-mark)
  (if *mark-safe-p*
      (format t ";; Whew... We're safe! For now.~%")
      (we do not want to be here)))
```

The `try-to-hide-mark` function will also be very similar to the one we have created for choices. Other than performing a `s/choice/restart/g` inside it and substituting `compute-restarts` with `compute-relevant-restarts`, we are allowed to use the system-defined function `invoke-restart` on it from the very start:

```lisp
(defun try-to-hide-mark ()
  (let ((restarts (compute-relevant-restarts)))
    (if restarts
        (let ((restart (first restarts)))
          (format t ";; Performing ~A.~%" (restart-name restart))
          (invoke-restart restart))
        (format t ";; Kate cannot hide Mark!~%"))))
```

This means that we can execute `parents-come-back` in the dynamic environment created by `call-with-home-restarts` in various state of the doors' openness:

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Performing ESCAPE-THROUGH-FRONT-DOOR.
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*back-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Performing ESCAPE-THROUGH-BACK-DOOR.
;; Escaping through the back door.
;; Whew... We're safe! For now.
NIL
```

How about the situation in which no doors are open? (Note that, again, for Kate's and Mark's well-being, we demonstrate this in a safe environment - we do not call `parents-come-back`, but only `try-to-hide-mark`.)

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (try-to-hide-mark))))
;; Kate cannot hide Mark!
NIL
```

Similarly, we can refactor the above code to allow us to use a single `escape` restart name for the different escape methods that we have. We will also modify `try-to-hide-mark` to use the system-provided `find-restart` function, analogous to `find-choice` which we have defined ourselves earlier.

```lisp
(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts)))
    (restart-bind ((escape #'perform-escape-through-back-door
                           :test-function #'escape-through-back-door-p)
                   (escape #'perform-escape-through-front-door
                           :test-function #'escape-through-front-door-p))
      (funcall thunk))))

(defun try-to-hide-mark ()
  (cond ((find-restart 'escape)
         (invoke-restart 'escape))
        (t (format t ";; Kate cannot hide Mark!~%"))))
```

And these are the examples of the above code in use:

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*front-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil)
                  (*back-door-locked-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Escaping through the back door.
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (try-to-hide-mark))))
;; Kate cannot hide Mark!
NIL
```

This also allows us to easily bind a restart with a different name and a different implementation: our `excuse` restart from earlier.

```lisp
(defvar *excuses*
  '("Kate did not divide her program into sections properly!"
    "I was borrowing Kate's books on mainframe programming!"
    "I had COBOL-related homework and hoped Kate could help me!"))

(defun perform-excuse (excuse)
  (format t ";; Mark excuses himself before leaving:~%;; \"~A\"~%" excuse)
  (setf *mark-safe-p* t))
```

We will modify `try-to-hide-mark` and `call-with-home-restarts` in the same way we have modified the choice-based implementation. (The latter function, obviously, will need a slightly different syntax provided by the `restart-bind` macro.)

```lisp
(defun try-to-hide-mark ()
  (cond ((find-restart 'escape)
         (invoke-restart 'escape))
        (t
         (format t ";; Kate cannot hide Mark!~%")
         (when (find-restart 'excuse)
           (let ((excuse-text (elt *excuses* (random (length *excuses*)))))
             (invoke-restart 'excuse excuse-text))))))

(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts)))
    (restart-bind ((escape #'perform-escape-through-back-door
                           :test-function #'escape-through-back-door-p)
                   (escape #'perform-escape-through-front-door
                           :test-function #'escape-through-front-door-p)
                   (excuse #'perform-excuse))
      (funcall thunk))))
```

(One difference from the choice system: in `restart-bind`, restarts are established in order from the last one to the first one; this means that the `excuse` restart will be the on top of the list returned by `compute-relevant-restarts`, even though here it is the last.)

Is Mark safe in a restart-based situation where no escape is possible? Let us find out!

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Kate cannot hide Mark!
;; Mark excuses himself before leaving:
;; "I was borrowing Kate's books on mainframe programming!"
;; Whew... We're safe! For now.
NIL
```

Again - we can see that the system of choices that we have created maps into the restart system perfectly. (Almost as if the author has done that on purpose!...) Of course, that is to the extent in which we have used the restart system so far; while the above exercises display the internal structure and working of the restart system, it is *not* the canonical way of using restarts. The next chapter will elaborate on that.

### Interactive restarts

There is one more function provided by standard Common Lisp that we have not implemented in the choice system from the previous chapter; we will instead discuss it now. It is most commonly used from within the debugger, since it is used to *interactively* prompt the user for arguments that are then used to call a given restart. However, user code is also allowed to call this function on its own.

That function is named `invoke-restart-interactively` and it does not accept any arguments other than the *restart designator* for a restart that it is meant to invoke. The restart arguments are instead retrieved by calling one aspect of the restart system that we have not touched - the restart's `*interactive function*`.

Let us imagine a situation in which we may interactively supply Mark with new excuses. Only if the excuse we supply is empty, Mark will resort to one of the classical ones which we have stored in the value of `*excuses*`. This requires us to change the function `try-to-hide-mark` so it calls `invoke-restart-interactively` instead of a non-interactive `invoke-restart`.

```lisp
(defun try-to-hide-mark ()
  (cond ((find-restart 'escape)
         (invoke-restart 'escape))
        (t
         (format t ";; Kate cannot hide Mark!~%")
         (when (find-restart 'excuse)
           (invoke-restart-interactively 'excuse)))))
```

This function will compile correctly; however, attempting to execute it will not work. The default interactive function for all restarts returns an empty list of arguments, whereas our `excuse` restart needs exactly one argument to be passed to it. This means that we need to define an interactive function for our `excuse` restart and then pass it to the created `excuse` restart by means of modifying `call-with-home-restarts`.

```lisp
(defun provide-excuse ()
  (format t ";; Mark is thinking of an excuse...~%")
  (let ((excuse-text (read-line)))
    (list (if (string/= "" excuse-text)
              excuse-text
              (elt *excuses* (random (length *excuses*)))))))
```

Calling this function and passing it some text causes this text to be returned, wrapped in a list. (The outer list is required, since `invoke-restart-interactively` is expected to return a list of arguments.)

```lisp
CL-USER> (provide-excuse)
;; Mark is thinking of an excuse...
I was discussing the distinct aspects of mainframe repair!     ; text input by the user
("I was discussing the distinct aspects of mainframe repair!")
```

If we instead provide no input (an empty line), this function instead picks a random predefined excuse.

```lisp
CL-USER> (provide-excuse)
;; Mark is thinking of an excuse...
                                                               ; empty line provided
("I was borrowing Kate's books on mainframe programming!")
```

Now that the `provide-excuse` function works as intended, we can add it to the `excuse` restart...

```lisp
(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts)))
    (restart-bind ((escape #'perform-escape-through-back-door
                           :test-function #'escape-through-back-door-p)
                   (escape #'perform-escape-through-front-door
                           :test-function #'escape-through-front-door-p)
                   (excuse #'perform-excuse
                           :interactive-function #'provide-excuse))
      (funcall thunk))))
```

...and try running our code!

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Kate cannot hide Mark!
;; Mark is thinking of an excuse...
I was working on getting a mainframe installed in Kate's room! ; text input by the user
;; Mark excuses himself before leaving:
;; "I was working on getting a mainframe installed in Kate's room!"
;; Whew... We're safe! For now.
NIL

CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*mark-safe-p* nil))
              (parents-come-back))))
;; Uh oh - Kate's parents are back!
;; Kate cannot hide Mark!
;; Mark is thinking of an excuse...
                                                               ; empty line provided
;; Mark excuses himself before leaving:
;; "Kate did not divide her program into sections properly!"
;; Whew... We're safe! For now.
NIL
```

Of course, the interactive function for a given restart may call `read-line` multiple times in order to get more than one textual argument. I may also call other query functions, such as `read` for reading Lisp data, or `parse-integer` to try and parse a user-input string into an integer usable by Lisp. In more creative cases, the `invoke-restart-interactively` function may also, for example, invoke GUI toolkits to prompt the user for data input or for selecting some of the options available to them by using mouse.

## A simple system of actually-restarting restarts

While a restart is not strictly required to "restart" any abstract process in the code, the usual way of using restarts is exactly this: "rolling back" the code to some previously established good point, with an optional step of performing some recovery action earlier.

If we used an analogy to handler macros, it would mean that, `handler-case` is much, much more commonly used in practice than `handler-bind`. While there are common practical uses for both `handler-bind` and `handler-case`, it is the macro `restart-case` (which, like `handler-case`, always performs a non-local transfer of control) which is the more commonly used part of the restart facility, compared to `restart-bind`.

This is so because restarts which do *not* perform non-local transfers of control do not integrate well with one more Common Lisp facility: the debugger. As mentioned earlier, there is no programmatic way of leaving the debugger; and even with a programmer interacting with the debugger, the only way of escaping it is performing a non-local transfer of control outside the debugger. If a restart function returns normally, then such a normal return is not enough to leave a debugger, even if that function performs some side effects which otherwise modify the state of the program.

We will describe the debugger itself in a later part of this book; for now, we will focus on the two macros which establish restarts that always perform a NLToC: `restart-case` and `with-simple-restart`.

### Restarts that perform a NLToC

First, we need to describe the means by which a restart established by `handler-bind` is capable to transfer control outside of a form executed within

Instead of using `catch` and `throw`, as we did with handlers, we will instead use a different pair of operators: `block` and `return-from`. This pair of operators is lexical in scope and therefore less footgun-shaped than `catch` and `throw`.

We will first begin by creating a *named block*. A block, in Common Lisp, is a form that gives special meaning to `return-from` forms that execute within its lexical scope; it becomes possible to short-circuit the execution of code inside the block in order to return from it (and, optionally, return a value). (If a block is not named (or rather, named `nil`), then `return` can be used in place of `return-from`. `return` is a shorthand for `return-from nil`.)

Therefore, the following code will work correctly and will return without printing anything:

```lisp
CL-USER> (block restart
           (restart-bind ((abort (lambda () (return-from restart))))
             (invoke-restart 'abort)
             (format t ";; We do not want to be here.~%")))
NIL
```

However, let us suppose that we want to additionally perform some action other rather than a simple NLToC; let us say that, after transferring control outside of the affected form, we want to print a different debug message. For this, we may use one more lexical mechanism of control flow: `tagbody` and `go`. Just like the dreaded `goto` of other programming languages, the lexically scoped `tagbody` and `go` allow control to be transferred out of arbitrary places in the code and into one of the *tagbody tags* stored in the body of `tagbody`.

The following example illustrates this:

```lisp
CL-USER> (block restart
           (tagbody
              (restart-bind ((abort (lambda () (go :abort))))
                (invoke-restart 'abort)
                (format t ";; We do not want to be here.~%"))
            :abort
              (format t ";; Whew. That was close.~%")
              (return-from restart)))
;; Whew. That was close.
NIL
```

It is possible to extend this example into multiple restarts. This requires the list of restarts bound in `restart-bind` to be appropriately extended with new restart forms, and the body of `tagbody` to be similarly expanded with new tags and the bodies that execute associated restart cases.

However, a keen eye may notice that this example assumes that the lambda list of a given restart is always empty; or, in other words, that the `abort` restart defined above does not accept any arguments. For example, if our `abort` restart accepted a single argument, `reason` for the abort, the above mechanism would not be able to properly handle that.

We may begin fixing this issue by wrapping the restart case in a function that accepts such a single argument. Once that is done, we may route the arguments that are actually passed to the bound restart by means of a *local variable* whose value will be the arguments that the restart function was called with. Once that is done, we will call the restart case with these arguments.

```lisp
CL-USER> (block restart
           (let (restart-arguments)
             (tagbody
                (restart-bind ((abort (lambda (&rest arguments)
                                        (setf restart-arguments arguments)
                                        (go :abort))))
                  (invoke-restart 'abort :about-to-error)
                  (format t ";; We do not want to be here.~%"))
              :abort
                (return-from restart
                  (apply (lambda (reason) (format t ";; Whew: ~A.~%" reason))
                         restart-arguments)))))
;; Whew: ABOUT-TO-ERROR.
NIL
```

This implementation outlines the difference between a *restart function* and a *restart case*. In `restart-bind`, all code is allowed to run within the restart function, because there is no requirement to leave the dynamic scope of `restart-bind` before executing our code. For `restart-case`, however, the stack must be first unwound in order to, among others, disestablish the original restart that we bind via `handler-bind` and to execute any cleanup forms established by `unwind-protect` in the dynamic scope. Therefore, the only effective responsibility of the *restart function* inside `restart-case` is to pass the arguments it was called with outside and transfer control to code that calls the *restart case*.

### From RESTART-BIND to RESTART-CASE

We can see that the code from the last example is complex and therefore unwieldy to write on one's own. This is why the standard macro `restart-case` implements that idiom. First, a non-local transfer of control is made; second, the restart forms are executed; third, the value returned by the restart forms is returned from the outermost `block` created by the `restart-case`.

The `restart-case` code equivalent to the above form looks like the following:

```lisp
CL-USER> (restart-case (progn (invoke-restart 'abort :about-to-error)
                              (format t ";; We do not want to be here.~%"))
           (abort (reason) (format t ";; Whew: was ~A.~%" reason)))
;; Whew: was ABOUT-TO-ERROR.
NIL
```

We can see that it is much shorter than the above. It also fulfills our requirement of first disestablishing the dynamic scope and only then calling the code contained in the restart case:

```lisp
CL-USER> (restart-case
             (unwind-protect
                  (progn (invoke-restart 'abort :about-to-error)
                         (format t ";; We do not want to be here.~%"))
               (format t ";; Leaving the dynamic scope.~%"))
           (abort (reason)
             (format t ";; Whew: was ~A.~%" reason)))
;; Leaving the dynamic scope.
;; Whew: was ABOUT-TO-ERROR.
NIL
```

Let us utilize that knowledge to rework the earlier example of Kate and Mark trying to cover themselves up. Because the restarts established by `restart-case` always transfer control outside of the restarting forms, we need to modify `parents-come-back` in order to move the established restarts inside its body. That, in turn, will require us to modify `call-with-home-restarts`, where the restarts will actually be established.

```lisp
(defun parents-come-back ()
  (format t ";; Uh oh - Kate's parents are back!~%")
  (call-with-home-restarts
   (lambda ()
     (try-to-hide-mark)
     (we do not want to be here)))
  (format t ";; Whew... We're safe! For now.~%"))
```

The above function now forces `call-with-home-restarts` to accept a single lambda that will be called. The function `try-to-hide-mark` is now forced to transfer control outside of the lambda, in order to prevent the `(we do not want to be here)` form from being evaluated and wreak havoc upon the professional lives of Mark and Kate.

This also means that the variable which we have used previously, `*mark-safe-p*`, is now unnecessary. If we want our code to be clean, we can remove the global variable definition from our code and remove now-dangling references to it from the bodies of all the functions that used it: `perform-escape-through-front-door`, `perform-escape-through-back-door`, and `perform-excuse`.

```lisp
(makunbound '*mark-safe-p*)

(defun perform-escape-through-front-door ()
  (format t ";; Escaping through the front door.~%"))

(defun perform-escape-through-back-door ()
  (format t ";; Escaping through the back door.~%"))

(defun perform-excuse (excuse)
  (format t ";; Mark excuses himself before leaving:~%;; \"~A\"~%" excuse))
```

Let us appropriately modify `call-with-home-restarts` to utilize `restart-case` instead of `restart-bind`.

```lisp
(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts)))
    (restart-case (funcall thunk)
      (escape () :test escape-through-back-door-p
        (perform-escape-through-back-door))
      (escape () :test escape-through-front-door-p
        (perform-escape-through-front-door))
      (excuse (reason) :interactive provide-excuse
        (perform-excuse reason)))))
```

(A keen eye can notice that the test/interactive functions are now provided in a different way. Where `handler-bind` had keywords `:test-function` and `:interactive-function`, `handler-case` has `:test` and `:interactive`; in addition, we now pass function names - without the `#'` reader macro - instead of passing function objects. This disparity is an unfortunate result of Common Lisp syntax being the product of a compromise and backwards compatibility towards earlier Lisp dialects.)

This modification is enough to trigger the new behaviour: what is noteworthy is that we do not have to perform any modifications to the body of `try-to-hide-mark`. If either door is unlocked, Mark is able to escape that way:

```lisp
CL-USER> (let ((*front-door-locked-p* nil))
           (parents-come-back))
;; Uh oh - Kate's parents are back!
;; Escaping through the front door.
;; Whew... We're safe! For now.
NIL

CL-USER> (let ((*back-door-locked-p* nil))
           (parents-come-back))
;; Uh oh - Kate's parents are back!
;; Escaping through the back door.
;; Whew... We're safe! For now.
NIL
```

In case Mark needs to excuse himself:

```lisp
CL-USER> (parents-come-back)
;; Uh oh - Kate's parents are back!
;; Kate cannot hide Mark!
;; Mark is thinking of an excuse...
I was checking if floor is even in all the rooms!          ; text input by the user
;; Mark excuses himself before leaving:
;; "I was checking if floor is even in all the rooms!"
;; Whew... We're safe! For now.
NIL
```

We can see that our restart-binding code is now fully contained within the body of `parents-come-back` and the the only modifications to the dynamic environment around it are forms that state which doors are unlocked. This has an effect of simplifying the control flow of our functions: when a suitable restart is visible, invoking it inside `try-to-hide-mark` now causes an unconditional transfer control back to `call-with-home-restarts` where a proper restart case is executed.

Whether such simplification is beneficial for us depends on our exact use case: in some cases we may want a more intricate flow of possible actions where `restart-bind` may be of more use, but in other situations - especially when we get to deal with the Lisp debugger - we might prefer the more "get me out of here" style of `restart-case`.

### Simple restarts

There is one more operator that simplifies the syntax even `restart-case` even further. It allows only to bind a single restart, and if that restart is invoked, it returns two values: `nil` as the primary value, and `t` as the secondary one.

The name of that macro is `with-simple-restart` and its syntax is even simpler than the one of `restart-case`.

```lisp
CL-USER> (with-simple-restart (escape "Wake up from the bad dream.")
           (parents-come-back))
;; Uh oh - Kate's parents are back!
NIL
T
```

Inside the macro, we define the name of that restart, but also *a format control, and optional format arguments*. (Why - we will explain in the next chapters.) The forms executed within the scope of `with-simple-restart` will have this new restart bound. In the above case, the `escape` restart that we bind immediately transfers control outside of `parents-come-back` without printing anything else to the screen, returning values `nil` and `t`.

(These return values are meaningful if the forms within `with-simple-restart` are supposed to return a non-`nil` primary value and either return `nil` as the secondary value or not to return a secondary value at all. This means that it is possible to check the secondary value for whether the particular call has been restarted with the restart bound by `with-simple-restart`; if that is the case, the code that invoked a `with-simple-restart`-wrapped form may use that information for recovery.)

We can therefore see the parallel between `ignore-errors` and `with-simple-restart`. Both of them establish a respective dynamic binding: `ignore-errors` binds a condition handler, `with-simple-restart` - a restart. Both of these, when the object from that respective binding is invoked, immediately transfer control outside of their form, returning `nil` as the first value and a meaningful non-`nil` secondary value: a condition object for `ignore-errors` and `t` for `with-simple-restart`. (It is impossible to return a restart object as a secondary value, since restart objects always have *dynamic extent*; to repeat the definition, it is not valid to return them outside the dynamic scope in which they were bound.)

### Standard restarts and restart-invoking functions

Before we explain the unusual syntax of `with-simple-restart`, let us take a small detour to talk the standard restarts which portable Common Lisp programs may expect to use. There are five such restarts: `abort`, `continue`, `use-value`, `store-value`, and `muffle-warning`, and they are always meant to perform non-local transfers of control.

The first restart, `abort`, should be always bound by the Lisp system. Its role is to terminate the current computation and transfer control to a known location in the program where it may be possible to specify another command for the Lisp system. In case of interactive usage, such as communicating with Lisp via the read-eval-print loop, the `abort` restart should make it possible to input another command into the REPL; in case of programmatic or batch usage, such a restart is allowed to terminate the Lisp system altogether and return control to the operating system.

The restart `continue` is meant to allow the computation to proceed. It is up to the restart function of an individual `continue` restart to define what "proceeding" means in its particular context; it might mean simply continuing the standard control flow from some defined point in the program, but it might also mean interactively querying the programmer for some data before execution is continued.

The restarts `use-value` and `store-value` are usually bound when some block of code decides that a piece of Lisp data that was passed to it may be, for whatever reason, unfit for processing; for instance, they may be bound by a Lisp function that accepts only strings, in order to protect against a case when someone passes it e.g. an integer. Invoking such a restart then means that the original datum that the function was invoked with is discarded, and another datum is supplied in its stead to continue program execution. The difference between the two is that `use-value` only utilizes the value once and then discards it, whereas `store-value` also stores the value in some proper place of the Lisp system, so it will also be used again in the future. (The precise *place* where the datum is stored in the second case is, again, defined by the piece of code that binds the `store-value` restart.)

The restart `muffle-warning` is established by the `warn` function; they will both be discussed in detail in the next chapter.

The programmer is free to bind these standard restarts on their own. This allows the programmer to implement these restarts in their own way that makes sense in a given program; as long as they are bound to the standard symbols listed above, the respective standard restart-invoking functions will be able to find these restarts and invoke them.

### Defining custom restart-invoking functions

Each of the aforementioned alarms has an associated standard function which invokes the respective alarm: the functions `abort`, `continue`, and `muffle-warning` each invoke the respective alarm without requiring any arguments, whereas `use-value` and `store-value` must be given one mandatory argument. (For completeness: all of these functions accept an optional argument, which must be a condition object. The reason for that will be explained in the later chapters, since it is the same as the reason why `compute-restarts` and `find-restart` also accept condition objects as optional arguments.)

In addition, the functions `abort` and `muffle-warning` are unique, as they explicitly expect their respective restart to be bound, otherwise, they signal a `control-error`. Invoking `continue`, `use-value`, and `store-value` is safe, however; these functions do nothing if their respective restarts are not bound.

It is actually a Lisp idiom to define functions named after individual restarts. These functions invoke their respective restarts with any arguments they might need.

Let us utilize that knowledge to further refactor the one function that was untouched by our earlier transition from `handler-bind` to `handler-case`: `try-to-hide-mark`. Its implementation looked like this:

```lisp
(defun try-to-hide-mark ()
  (cond ((find-restart 'escape)
         (invoke-restart 'escape))
        (t
         (format t ";; Kate cannot hide Mark!~%")
         (when (find-restart 'excuse)
           (invoke-restart-interactively 'excuse)))))
```

We can see that there is a pair of restarts which are invokable within its body. We shall define a pair of restart-invoking functions for restarts `escape` and `excuse`. The former is a non-interactive restart; for the latter, we will specify an option of invoking that restart interactively. If an excuse is provided, it will be used to invoke the restart function; otherwise, the user will be queried for the excuse.

```lisp
(defun escape (&optional condition)
  (let ((restart (find-restart 'escape condition)))
    (when restart (invoke-restart restart))))

(defun excuse (&optional excuse-text condition)
  (if excuse
      (invoke-restart restart excuse-text)
      (invoke-restart-interactively restart)))
```

(These functions accept the optional `condition` argument and pass it over to `find-restart`; again, explanation of this phenomenon will become available later in the book.)

We can see that these two functions follow the logic of our program. The restart `escape` is not always available, e.g. when both doors are locked, therefore it makes sense for its restart-invoking function to first check for whether such a restart is available. The restart-invoking function for `excuse`, though, assumes that it is always possible for Mark to excuse himself; it therefore assumes that an `excuse` restart is always bound.

Now that these functions are available, we can rewrite `try-to-hide-mark` to use them.

```lisp
(defun try-to-hide-mark ()
  (escape)
  (format t ";; Kate cannot hide Mark!~%")
  (excuse))
```

We see that the control-flow logic of `try-to-hide-mark` has disappeared altogether, having been offloaded onto the restart system. A keen eye may notice that our restarts have been established via `restart-case`; therefore, we can depend on the fact that they will always perform a non-local transfer of control. That is why simply calling `(escape)` will either unwind the stack to the matching `escape` handler or return if no such restart is available. Once that happens, a debug information is printed, and `(excuse)` is called to ultimately save Mark and Kate from the doom of becoming mainframe programmers for the rest of their lives.

We have not utilized the optional `excuse-text` argument to the function `excuse` in the above scenario; however, it is worth to keep that option inside the function body, since it may be utilized in other situations. For instance, Kate may notice that a particular set of excuses - e.g. talking about C++ - works particularly well on her father. To model that behaviour, the only required modification will be changing `try-to-hide-mark` to explicitly call the `excuse` function with a suitable, C++-mentioning excuse text.

-------------

This mechanism is a part of a whole another part of the condition system which we have not yet touched: reporting condition objects and restarts.

## Reporting conditions and restarts

One thing that we have not explained in the previous chapter is the fact that `with-simple-restart` requires us to pass *a format control string* and allows us to pass in optional *format arguments*. This ties in with the fact that, so far, we have been discussing conditions and restarts in a non-interactive context. The conditions we have signaled and the restarts we have invoked were all handled programmatically. Even in such a fully automated environment, though, it might be worth to have observability into what is happening inside the program.

One common use of condition handlers is to insert logging into programs that is triggered by signaled conditions. In addition, such logging might also be a part of restart functioning; the program logs may want to contain information about which restart is being invoked in which context.

Common Lisp provides functionality that composes well with these goals. Both condition objects and restarts have the ability to be *reported* - which means the process of extracting human-readable information about the details of a given condition or restart. The details of programming this process is what this chapter shall elaborate on.

### Printing versus reporting

If we attempted to print a condition object or a restart object, it is most likely that we would get an *unreadable* object, which is, printed by `print-unreadable-object`. It would likely look similar to `#<CONDITION 012345ABCDEF>` or `#<RESTART FOO 012345FEDCBA>`. (This behaviour is not mandated by the Common Lisp standard, but is nonetheless exhibited by all Common Lisp implementations the author is aware of.) This syntax is called "unreadable" since it is impossible to read these objects back into Lisp by using the standard `read` function; the Lisp reader signals an error every time it encounters the character `#<` at the beginning of the read object.

The way to force a condition or a restart to be *reported* is by setting the global variable `*print-escape*`. It is a system variable which is initially set to true; that is what causes the condition and restart objects to be printed in the unreadable way. Binding it to `nil` and then printing causes the respective object's report function to be invoked instead, and the data output by that function to the reporting stream then becomes the report of that respective condition.

(The variable `*print-escape*` is implicitly bound by some of the printing functions and some of the `format` directives. It is implicitly bound to true by e.g. the function `prin1` and the `format` directive `~S`; it is implicitly bound to false by e.g. the function `princ` and the `format` directive `~A`. Functions `write`, `write-string`, `write-to-string` and the format directive `~W` do not bind that variable themselves; they use its value from the dynamic environment in which they are executed.)

For conditions and restarts with no specified report function, the reported data is implementation-dependent. Therefore, to not only provide proper content to be reported, but to ensure overall consistency across all conditions and restarts which may be reported in our program, it is important to specify the report functions in all `define-condition`, `restart-bind`, and `restart-case` macros which we write.

This fact also means that the code examples from the earlier chapters are not suited for situations where the condition or restart objects might have been reported. No such situation occurred in the above examples; however, from now on, our code will contain proper report functions for all defined conditions and bound restarts.

### Custom condition reports

Condition objects are printed in their unreadable form if the dynamic variable `*print-escape*` is bound to `t`. However, when that same variable is bound to `nil`, printing a restart objects causes its *report function* to be invoked. That report function accepts a pair of arguments: the condition object itself and the reporting stream that the report should be written to. This means that the report function is capable of querying the condition object for all the information that is needed for generating a proper report; it is good style for condition reporting functions to be self-contained, which means, to not depend on any Lisp data other than the condition object itself.

Let us come back to the earlier example of Tom and his phonebook. We will not focus on the calling algorithm this time; we will instead spend our time on the condition objects themselves, in particular: on ensuring that they are reported properly.

The three conditions we have defined in that example are:

```lisp
(define-condition before-call ()
  ((%person :reader person :initarg :person)))

(define-condition after-call ()
  ((%person :reader person :initarg :person)))

(define-condition grave-mistake (error)
  ((%reason :reader reason :initarg :reason)))
```

We see that the earlier two are similar, since both of them refer to a person who is about to be called or who was just called. We will come back to the third one in a moment; for now, we will utilize condition inheritance that is possible in Common Lisp to slightly refactor the first two conditions.

```lisp
(define-condition person-condition ()
  ((%person :reader person :initarg :person)))

(define-condition before-call (person-condition) ())

(define-condition after-call (person-condition) ())
```

We have extracted the common slot into a single condition type named `person-condition` and caused `before-call` and `after-call` to inherit from it. This allows us to slightly reduce code duplication , since now report functions for both `before-call` and `after-call` can call the function `person` on the condition object in order to be able to fetch the person who was, respectively, about to be called or actually called.

Let us write report functions for these two conditions.

```lisp
(define-condition before-call (person-condition) ()
  (:report (lambda (condition stream)
             (format stream "We are about to call ~A." (person condition)))))

(define-condition after-call (person-condition) ()
  (:report (lambda (condition stream)
             (format stream "We have just called ~A." (person condition)))))
```

This ensures that the conditions we have created are reported correctly.

```lisp
CL-USER> (let ((*print-escape* nil))
           (write-to-string (make-condition 'before-call :person :mom)))
"We are about to call MOM."

CL-USER> (let ((*print-escape* nil))
           (write-to-string (make-condition 'after-call :person :mom)))
"We have just called MOM."
```

As we come back to the condition `grave-mistake`, let us focus for a moment on the `:report` option. These three means of defining are all valid (even though the first one does not utilize the `reason` slot in our condition:)

```lisp
(define-condition grave-mistake (error)
  ((%reason :reader reason :initarg :reason))
  (:report "We are committing a grave mistake."))
```

```lisp
(define-condition grave-mistake (error)
  ((%reason :reader reason :initarg :reason))
  (:report (lambda (condition stream)
             (format stream "We are committing a grave mistake: ~A."
                     (reason condition)))))
```

```lisp
(defun report-grave-mistake (condition stream)
  (format stream "We are committing a grave mistake: ~A."
          (reason condition)))

(define-condition grave-mistake (error)
  ((%reason :reader reason :initarg :reason))
  (:report report-grave-mistake))

```

In the first case, the report for the given condition is a static string. (Even though the report does not utilize the `reason` slot in any way, this slot is still accessible to handlers; this is why removing it altogether is not a valid choice.) The second case passes a lambda form as the option value, and the third one passes the name of a function.

From this, we are able to infer that the `:report` option has multiple ways of specifying the actual report contents. First of all, it accepts a string; the meaning of it is that this string then becomes the condition report as-is. That string is written to the reporting stream as if by `write-string` - which is the case for the second case. That case also shows us that the `:report` option also accepts anonymous functions. The third condition supplies `:report` with the symbol `report-excuse` - which is the name of a function bound elsewhere. (The actual function may be bound globally, as via `defun`, or locally, as via `flet` or `labels`.)

### Custom restart reports

For restarts, the situation is similar to the one we have encountered in conditions. Restart objects are printed in their unreadable form if the dynamic variable `*print-escape*` is bound to `t`. However, when that same variable is bound to `nil`, printing a restart objects causes its *report function* to be invoked. However, since restarts are objects of dynamic extent that are not usually utilized directly, report functions suitable for use in restarts only accept one argument: the stream that they should output their information to.

Let us consider a very simplified version of the earlier example with Kate and Mark. For brevity, we will completely ignore the restart functions (which will be empty) and test functions (which will be defaulted). We will only focus on the parts of code that are relevant to *reporting* the restarts in question and completely neuter all logic related to actually handling the situations Kate and Mark are finding themselves in. ~~(Poor Mark.)~~

The simplified code from the earlier example is:

```lisp
(defvar *toplevel-restarts* '())

(defun compute-relevant-restarts (&optional condition)
  (set-difference (compute-restarts condition) *toplevel-restarts*))

(defvar *excuses*
  '("Kate did not divide her program into sections properly!"
    "I was borrowing Kate's books on mainframe programming!"
    "I had COBOL-related homework and hoped Kate could help me!"))

(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts))
        (excuse (elt *excuses* (random (length *excuses*)))))
    (flet ((report-excuse (stream) (format stream "Use an excuse, ~S." excuse)))
      (restart-case (funcall thunk)
        (escape ()
          :report "Escape through the back door.")
        (escape ()
          :report (lambda (stream)
                    (write-string "Escape through the front door." stream)))
        (excuse ()
          :report report-excuse)))))
```

The first two forms are still relevant to us, since we only want to concern ourselves with restarts that we bind ourselves.

The third form is a collection of excuses that Mark is allowed to make; for simplification, we will once again only allow Mark to choose from these three excuses. In addition, one can see that now the excuse is randomly chosen inside the body of `call-with-home-restarts`. One reason for that is to simplify our code, but the other is the fact that the excuse is now used inside the local function `report-excuse`.

Instead of starting with a full explanation of the above code, we will instead begin with a more hands-on example.
```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*print-escape* nil))
              (format t ";; Available restarts:~%;;~%~{;; ~W~%~}"
                      (compute-relevant-restarts)))))
;; Available restarts:
;;
;; Use an excuse, "I had COBOL-related homework and hoped Kate could help me!".
;; Escape through the front door.
;; Escape through the back door.
NIL
```

The anonymous function we call here first binds the dynamic variable `*print-escape*` to `nil`, causing the report function of the restart objects to be invoked. This is how we are able to print human-readable reports for each individual restart that is returned from `compute-relevant-restarts`.

Let us take a closer look at the `restart-case` form from the above.

```lisp
(...
 (restart-case (funcall thunk)
   (escape ()
     :report "Escape through the back door.")
   (escape ()
     :report (lambda (stream)
               (write-string "Escape through the front door." stream)))
   (excuse ()
     :report report-excuse)))
```

We can see that the `:report` option for each restart case accepts three different kinds of arguments, just like `:define-condition`. This "do-what-I-mean"-style richness in is not shared by `restart-bind`, however. The two differences in how `restart-bind` treats restart reports are the difference in keyword used for report functions (`restart-case` uses `:report`, whereas `restart-bind` uses `:report-function`) and that `restart-bind` explicitly requires *function objects* to be passed as their arguments. (This means that, contrary to `restart-case`, it is not possible to pass function names there; this inconsistency is yet another compromise made by the Common Lisp standard committee.)

If we wanted to replicate the above functionality via `restart-bind` instead of `restart-case` (while still ignoring all actual restarting behaviour), then we could implement `call-with-home-restarts` in the following way.

```lisp
(defun call-with-home-restarts (thunk)
  (let ((*toplevel-restarts* (compute-restarts))
        (excuse (elt *excuses* (random (length *excuses*)))))
    (flet ((report-excuse (stream) (format stream "Use an excuse, ~S." excuse)))
      (restart-bind
          ((escape (lambda ())
                   :report-function
                   (lambda (stream)
                     (write-string "Escape through the back door." stream)))
           (escape (lambda ())
                   :report-function
                   (lambda (stream)
                     (write-string "Escape through the front door." stream)))
           (excuse (lambda ())
                   :report-function #'report-excuse))
        (funcall thunk)))))
```

We can evaluate the same form as before to ensure that this behaviour is consistent with the previous example.

```lisp
CL-USER> (call-with-home-restarts
          (lambda ()
            (let ((*print-escape* nil))
              (format t ";; Available restarts:~%;;~%~{;; ~W~%~}"
                      (compute-relevant-restarts)))))
;; Available restarts:
;;
;; Use an excuse, "I was borrowing Kate's books on mainframe programming!".
;; Escape through the front door.
;; Escape through the back door.
NIL
```

One more notable thing is that the local function `report-excuse` accesses the `excuse` variable earlier from its scope in order to print its value inside the report. (It will also mean that, every time the restart is invoked, the excuse inside the report function will be chosen randomly from the three available ones.) Creating *closures* such as this is the way to work around the fact that report functions for restarts, unlike those for conditions, accept no user-programmable objects to refer to for more information about the report. (Restarts, unlike conditions, are not subclassable or otherwise extensible by the user.)

## Warnings

The fact that we now know how to properly report conditions allows us to describe one more standard Common Lisp operator that makes use of that information. The function `warn` is one more means of signaling a condition, in addition to the functions `signal` and `error`. If the signaled condition is not handled, `signal` simply returns `nil` and `error` invokes the debugger; `warn` instead reports the condition to the stream that is the value of `*error-output*` before returning `nil`. Additionally, it must be used to signal conditions of type `warning`, which is a subtype of `condition`.

One more fact that we have not talked about yet is that the data accepted by all these functions - `signal`, `warn`, and `error` - is versatile. Each function may accept either a condition object itself, or a set of arguments suitable for `make-condition`, or a format string and arguments that are then used to construct a condition of type `simple-warning`.

We will utilize the above knowledge to demonstrate `warn`; however, for that, we need to define a `warning` condition. Let us modify the above `grave-mistake` condition, we will need to define a condition that is not an `error`, but a `warning`.

```lisp
(defun report-grave-warning (condition stream)
  (format stream "We are committing a grave warning: ~A."
          (reason condition)))

(define-condition grave-warning (warning)
  ((%reason :reader reason :initarg :reason))
  (:report report-grave-warning))
```

Now, let us attempt to `warn` this condition in two cases: without handling it, and while handling it to instead return its report.

```lisp
CL-USER> (warn 'grave-warning :reason :about-to-call-your-ex)
;; WARNING: We are committing a grave warning: ABOUT-TO-CALL-YOUR-EX.
NIL

CL-USER> (handler-case (warn 'grave-warning :reason :about-to-call-your-ex)
           (warning (condition)
             (let ((*print-escape* nil))
               (write-to-string condition))))
"We are committing a grave warning: ABOUT-TO-CALL-YOUR-EX."
```

The same can be achieved if we instead explicitly use `make-condition`:

```lisp
CL-USER> (warn (make-condition 'grave-warning
                               :reason :about-to-call-your-ex))
;; WARNING: We are committing a grave warning: ABOUT-TO-CALL-YOUR-EX.
NIL
CL-USER> (handler-case (warn (make-condition 'grave-warning
                                             :reason :about-to-call-your-ex))
           (warning (condition)
             (let ((*print-escape* nil))
               (write-to-string condition))))
"We are committing a grave warning: ABOUT-TO-CALL-YOUR-EX."
```

Finally, if we do not care about signaling a particular warning type (and are therefore fine with `simple-warning` that `warn` will implicitly create for us), we may pass a format control (with optional format arguments) to `warn`.

```lisp
CL-USER> (warn "Example warning with no arguments.")
;; WARNING: Example warning with no arguments.
NIL

CL-USER> (handler-case (warn "Example warning with no arguments.")
           (warning (condition)
             (let ((*print-escape* nil))
               (write-to-string condition))))
"Example warning with no arguments."
NIL

CL-USER> (warn "Example warning with argument ~S." 42)
;; WARNING: Example warning with argument 42.
NIL

CL-USER> (handler-case (warn "Example warning with argument ~S." 42)
           (warning (condition)
             (let ((*print-escape* nil))
               (list (write-to-string c) (type-of condition)))))
("Example warning with argument 42." SIMPLE-WARNING)
```

Additionally, there is a particular restart, and its matching restart-invoking function, which is of particular interest when considering the function `warn`. The name of that restart is `muffle-warning` and it is established inside `warn` in a way that makes it visible in the dynamic scope of the signaled condition. Invoking the `muffle-warning` restart informs the Lisp system that the warning has been accounted for in some way and that execution of the program may continue without any other actions, such as e.g. reporting the warning to `*query-io*`. We can observe this behaviour on the following example:

```lisp
CL-USER> (warn "Example warning with argument ~S." 42)
;; WARNING: Example warning with argument 42.
NIL

CL-USER> (handler-bind ((warning #'muffle-warning))
           (warn "Example warning with argument ~S." 42))
NIL
```

## Assertions

Now that we have covered the topic of restarts, we can touch one more topic that utilizes both condition handling and restarts. There are operators in Common Lisp which act as *assertions*: unless some condition is met, they signal errors, which - unless they are handled - force entry into the debugger. Some of these assertions bind restarts around the error sites, which allows execution to proceed if these restarts are invoked and - in case of `assert` and `check-type` - if some additional conditions are met.

The macro `assert`, in its simplest form, checks if its test form evaluates to true. If it does, it returns `nil`; if it doesn't, it signals an error.

```lisp
CL-USER> (handler-case (assert (= (+ 2 2) 4)))
NIL

CL-USER> (handler-case (assert (= (+ 2 2) 5))
           (error () :nope))
:NOPE
```

However, `assert` also establishes a `continue` restart that allows the programmer to retry the assertion. It is most useful in interactive programming environments where the programmer may e.g. *redefine* a failing function while still in the debugger and then invoke that restart; however, it can also be used to e.g. retry some process in a brute-force manner.

```lisp
CL-USER> (handler-bind ((error (lambda (condition)
                                 (declare (ignore condition))
                                 (format t ";; Retrying...~%")
                                 (continue))))
           (assert (= (random 10) 0)))
;; Retrying...
;; Retrying...
;; Retrying...
;; Retrying...
;; Retrying...                                                 ; the number of retries
;; Retrying...                                                 ; will vary randomly
;; Retrying...
;; Retrying...
;; Retrying...
;; Retrying...
NIL
```

The optional arguments to `assert` allow the programmer to further alter the functionalities of the restart: if the programmer passes a non-empty collection of Lisp *places* to the macro, then the `continue` restart will prompt the programmer if they want to set these places to new, interactively-provided values.

```lisp
CL-USER> (handler-bind ((error #'continue))
           (let ((x nil))
             (assert x (x))))
;; The old value of X is NIL.
;; Do you want to supply a new value? (y or n) y               ; user input here
;; Type a form to be evaluated: t                              ; user input here
NIL
```

The further optional arguments are a format control and format arguments that should be passed to the condition reporter.

```lisp
CL-USER> (handler-case (assert (= (+ 2 2) 5) ()
                               "The numbers ~D and ~D do not sum up to ~D." 2 2 5)
           (error (condition)
             (let ((*print-escape* nil))
               (format t ";; ~W~%" condition))))
;; The numbers 2 and 2 do not sum up to 5.
NIL
```

The macro `check-type` functions similarly to `assert`, however it requires specifying exactly one place and a type form. A check is then made if the value of that place is of the provided type; if not, a `type-error` is signaled.

```lisp
CL-USER> (let ((x 42))
           (check-type x integer))
NIL

CL-USER> (let ((x "42"))
           (handler-case (check-type x integer)
             (type-error () :oops)))
:OOPS
```

This macro binds a `store-value` restart around the error site; invoking that restart causes the user to be prompted for a new value of that place, after which the type check is repeated.

It is noteworthy that, while `assert` binds a `continue` restart, `check-type` binds a `store-value` restart; this distinction is important. `check-type` always works on a single places, whereas the number of places passed to `assert` is arbitrary.

```lisp
CL-USER> (let ((x "42"))
           (handler-bind ((type-error (lambda (condition)
                                        (declare (ignore condition))
                                        (store-value 42))))
             (check-type x integer)
             x))
42
```

The only optional argument to `check-type` is a string that describes the type that is checked in a human-readable form; it is used during the construction of the condition report for the signaled error.

```lisp
CL-USER> (let ((x 24))
           (handler-case (check-type x (eql 42) "the ultimate answer to everything")
             (type-error (condition)
               (let ((*print-escape* nil))
                 (format t ";; ~W" condition)))))
;; The value of X is 24, which is not the ultimate answer to everything.
NIL
```

The macros `ecase` and `etypecase` are variants of, respectively, `case` and `typecase`. The latter two return `nil` when no case matches the test key they are provided; the former two instead signal a `type-error`.

```lisp
CL-USER> (let ((x 24))
           (handler-case (ecase x
                           (42 :integer)
                           (:forty-two :keyword)
                           ("42" :string))
             (type-error (condition)
               (let ((*print-escape* nil))
                 (format t ";; ~W" condition)))))
;; 24 fell through ECASE expression. Wanted one of (42 :FORTY-TWO "42").
NIL
```

`ecase` ensures that the test key is *of type* `(member key-1 key-2 key-3 ...)`, where `key-n` are all the keys provided to `ecase` in case forms.

```lisp
CL-USER> (let ((x nil))
           (handler-case (etypecase x
                           (integer :integer)
                           (keyword :keyword)
                           (string :string))
             (type-error (condition)
               (let ((*print-escape* nil))
                 (format t ";; ~W" condition)))))
;; NIL fell through ETYPECASE expression. Wanted one of (INTEGER KEYWORD STRING).
NIL
```

`etypecase` ensures that the test key is of type `(or type-1 type-2 type-3 ...)`, where `type-n` are all the types provided to `etypecase` in typecase forms.

The macros `ccase` and `ctypecase` are variants of, respectively, `ecase` and `etypecase`. They function in the same way as their `e*` counterparts, except they also establish a `store-value` restart around the `type-error` that they signal. This means that it is possible to continue program execution even after the error is signaled by invoking the `store-value` restart with the proper argument. The `ccase` or `ctypecase` is then retried with the newly stored value.

```lisp
CL-USER> (let ((x nil))
           (handler-bind ((type-error (lambda (condition)
                                        (declare (ignore condition))
                                        (store-value 42))))
             (ctypecase x
               (integer :integer)
               (keyword :keyword)
               (string :string))))
:INTEGER

CL-USER> (let ((x nil))
           (handler-bind ((type-error (lambda (condition)
                                        (declare (ignore condition))
                                        (store-value :forty-two))))
             (ccase x
               (42 :integer)
               (:forty-two :keyword)
               ("42" :string))))
:KEYWORD
```

In Lisp jargon, the behaviour of `ccase` and `ctypecase` is called signaling a *correctable error*. It is implemented by the function `cerror`, which is, again, a variant of `error`. The function `cerror` takes one more required argument: a format control, which is used by the report function of the established restart.

### Arguments for continuable errors

An interesting thing about `cerror` is that the optional arguments passed to the function are used *both* for constructing the condition object and as format arguments for the passed format string. Let us analyze the following form:

```lisp
(cerror "Continue after signaling a SIMPLE-ERROR ~
         with arguments: ~S ~S ~S ~S."
        'simple-error
        :format-control "A simple error signaled with ~A."
        :format-arguments '(42))
```

Executing this form will cause a `simple-error` to be signaled; we expect the report of that condition object to be `A simple error signaled with 42.`. In addition, we expect a new restart to be bound around the error site; the restart will have a longer report form, reading `Continue after signaling a SIMPLE-ERROR with arguments:` and then listing all the optional arguments that `cerror` was called with.

We can verify our assumptions by handling the signaled `simple-error` and printing out information about the condition object and the most recently established restart. This is one of the situations where `handler-bind` is required over `handler-case`; if we used the latter, we would manage to successfully inspect the condition object, but the `continue` restart bound by `cerror` would have already been disestablished by the time control reached the handler case.

```lisp
CL-USER> (block nil
           (flet ((print-reports (condition)
                    (let ((*print-escape* nil))
                      (format t ";; Condition report: ~W~%;; Restart report: ~W~%"
                              condition (first (compute-restarts)))
                      (return))))
             (handler-bind ((simple-error #'print-reports))
               (cerror "Continue after signaling a SIMPLE-ERROR ~
                        with arguments:~%;;   ~S ~S ~S ~S"
                       'simple-error
                       :format-control "A simple error signaled with ~A."
                       :format-arguments '(42)))))
;; Condition report: A simple error signaled with 42.
;; Restart report: Continue after signaling a SIMPLE-ERROR with arguments:
;;   :FORMAT-CONTROL "A simple error signaled with ~A." :FORMAT-ARGUMENTS (42)
NIL
```

## A simple debugger

So far in the book we have avoided talking in detail about the debugger. More, we have completely avoided invoking it. However, in the end, there is no escaping the fact that error handling is never going to be good enough to take care of all erroneous situations. Sometimes, the only resolve that a program may have is to be put into the hands of the programmer. The debugger is the utility via which Lisp achieves that goal.

### A very very very simple debugger

To follow the general spirit of our book, we will not begin by describing the Lisp debugger; instead, we will begin by introducing the system-defined variable `*debugger-hook*`. Its full description will come later - for now, let us define a very simple *debugger function* that produces some output and then handles all conditions by invoking the `abort` restart.

```lisp
(defun debugger (condition hook)
  (declare (ignore hook))
  (let ((*print-escape* nil))
    (format t ";; Aborting: ~W~%" condition))
  (abort condition))
```

We will, for the time being, ignore the second argument passed to that function. Let us rebind `*debugger-hook*` and explicitly calling `invoke-debugger` for the first time in this book:

```lisp
CL-USER> (let ((*debugger-hook* #'debugger))
           (invoke-debugger (make-condition 'simple-error
                                            :format-control "We are in trouble.")))
;; Aborting: We are in trouble.
;;
;; ABORT restart invoked; returning to toplevel.
```

We can see that the code inside our `debugger` function was evaluated and that the `abort` restart established by the Lisp system was invoked. (The message printed to the screen by that second part, if any, is implementation-dependent.) This has effectively brought us back to the read-eval-print loop, allowing us to specify the next command to be evaluated.

By performing this, we have demonstrated the functioning of the `*debugger-hook*` variable. The function `invoke-debugger` first checks if that variable is bound; if yes, then the debugger hook function is called with the condition passed to `invoke-debugger`. If the debugger hook function returns, for any reason, then the system-provided debugger is invoked to ultimately handle the condition.

One more step done by `invoke-debugger` is binding the variable `*debugger-hook*` to `nil`. This is to ensure that, if an unexpected error is signaled *inside* the debugger hook, the debugger that is invoked recursively is the debugger provided by the Lisp system; this allows system-provided recovery from errors in the user-provided debugger. The value of `*debugger-hook*` is passed as the second argument to the debugger hook function - which, in our case, is the `hook` variable which we have ignored.

In other words: by defining one function and by binding one variable, we have defined our own custom Lisp debugger. It is a very primitive one, however; it provides absolutely no interaction with the programmer and tells them little to nothing about what exactly happened. It also utilizes only one restart strategy, which is invoking the `abort` restart; the programmer has no chance of attempting to utilize a different one.

### A very very simple debugger

Let us breathe a little bit more life into our debugger function. First of all, let us both print the condition report and condition type before invoking the `abort` restart.

```lisp
(defun debugger (condition hook)
  (declare (ignore hook))
  (let ((*print-escape* nil))
    (format t ";;~%;; Debugger entered on ~S:~%" (type-of condition))
    (format t ";; ~W~%" condition))
  (abort condition))
```

The above adds another bit of information to our program: now we know the condition type of the signaled condition.

```lisp
CL-USER> (let ((*debugger-hook* #'debugger))
           (invoke-debugger (make-condition 'simple-error
                                            :format-control "We are in trouble.")))
;;
;; Debugger entered on SIMPLE-ERROR:
;; We are in trouble.
;;
;; ABORT restart invoked; returning to toplevel.
```

### A very simple debugger

Let us now list the restarts available to the user.

```lisp
(defun debugger (condition hook)
  (declare (ignore hook))
  (let ((*print-escape* nil))
    (format t ";;~%;; Debugger entered on ~S:~%" (type-of condition))
    (format t ";; ~W~%" condition)
    (let ((restarts (compute-restarts condition)))
      (format t ";;~%;; Available restarts:~%")
      (dolist (restart restarts)
        (format t ";; [~W] ~W~%" (restart-name restart) restart))))
  (abort condition))
```

This gives the programmer even more information about what restarting options are available.

```lisp
CL-USER> (let ((*debugger-hook* #'debugger))
           (invoke-debugger (make-condition 'simple-error
                                            :format-control "We are in trouble.")))
;;
;; Debugger entered on SIMPLE-ERROR:
;; We are in trouble.
;;
;; Available restarts:
;; [RETRY] Retry evaluating the form.
;; [ABORT] Return to the toplevel.
;;
;; ABORT restart invoked; returning to toplevel.
```

(The above restarts are established by the Lisp system and therefore implementation-dependent; they are allowed to look differently on other implementations.)

Now that the restart options are listed, we should allow the programmer to choose them in some simple way. We will assign numbers to the restarts and define a function that reads input from the user until it manages to read a valid restart number; at that moment, our debugger will invoke this restart.

```lisp
(defun read-valid-restart-number (number-of-restarts)
  (loop (format t ";;~%;; Invoke restart number: ")
        (let* ((line (read-line *query-io*))
               (integer (parse-integer line :junk-allowed t)))
          (when (and integer (< -1 integer number-of-restarts))
            (return integer)))))

(defun debugger (condition hook)
  (let ((*print-escape* nil))
    (format t ";;~%;; Debugger entered on ~S:~%" (type-of condition))
    (format t ";; ~W~%" condition)
    (let ((restarts (compute-restarts condition)))
      (format t ";;~%;; Available restarts:~%")
      (loop for i from 0
            for restart in restarts do
              (format t ";; ~D [~W] ~W~%" i (restart-name restart) restart))
      (let ((chosen-restart (read-valid-restart-number (length restarts)))
            (*debugger-hook* hook))
        (invoke-restart-interactively (nth chosen-restart restarts)))
      (debugger condition hook))))
```

This allows the programmer the most basic amount of interactivity, allowing them to choose which restart should be invoked. (We also rebind the `*debugger-hook*` variable; in case the restart function signals an error, this will cause our debugger function to be recursively invoked instead of escalating to the system-provided debugger.)

One more change we add in this situation is recursively callilng the debugger at the very end of the code. This is required in case a restart returns normally instead of performing a non-local transfer of control; if a restart returns normally, we must assume that the condition was not handled and that the programmer should still remain in the debugger.

```lisp
CL-USER> (let ((*debugger-hook* #'debugger))
           (invoke-debugger (make-condition 'simple-error
                                            :format-control "We are in trouble.")))
;;
;; Debugger entered on SIMPLE-ERROR:
;; We are in trouble.
;;
;; Available restarts:
;; 0 [RETRY] Retry evaluating the form.
;; 1 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here
;;
;; Debugger entered on SIMPLE-ERROR:
;; We are in trouble.
;;
;; Available restarts:
;; 0 [RETRY] Retry evaluating the form.
;; 1 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 3                                    ; user input here
;;
;; Invoke restart number: forty-two                            ; user input here
;;
;; Invoke restart number:                                      ; empty line provided
;;
;; Invoke restart number: 1                                    ; user input here
;;
;; ABORT restart invoked; returning to toplevel.
```

We can see that this simple input loop is robust enough to survive the programmer attempting to break it; the only available options that allow the programmer to leave this debugger is `0` (which immediately casts the programmer back inside, as evaluating that form again immediately invokes the debugger once more) and `1` (which aborts evaluation and brings the programmer back to the read-eval-print loop).

### Installing a custom debugger

We have come quite far in our debugger voyages - and yet, there is a technique possible in standard Common Lisp that will defeat our current approach. Our debugger above depends on the Lisp system calling it when the debugger is invoked by means of the variable `*debugger-hook*`. It means that if some code binds this variable differently - for example, to another debugger function, or to `nil` in order to directly invoke the system debugger - we will not be able to handle that error via our custom debugger function.

There are even worse news: there is a standard Lisp function that does exactly that. That function is named `break` and it is utilized to insert temporary *breakpoints* into a program for debugging purposes. This function internally binds `*debugger-hook*` to `nil` right before calling `invoke-debugger`. This means not only that no handlers are allowed to fire (since no condition is being signaled), but also that our custom debugger function has no chance of being executed!

This means that we need to reach for means that are stronger than the `*debugger-hook*`. Such means, however, are not defined by the Common Lisp standard; it is up to each Lisp implementation to define means of replacing the standard Lisp debugger in a way that makes it possible to capture `break`.

For this purpose, the author of this book has created a *portability library* named `trivial-custom-debugger`, available on [GitHub](https://github.com/phoe/trivial-custom-debugger) at the time of writing this book. If the reader is a [Quicklisp](https://quicklisp.org) user, it should be possible to load this library via `(ql:quickload :trivial-custom-debugger)`; otherwise, it should be enough to `(load "trivial-custom-debugger.lisp")` contained within that repository.

We can then attempt to use the `with-debugger` macro from that package to tame the uncivilized beast that is `break` by using our custom `debugger` function which we previously passed to `*debugger-hook*`:

```lisp
CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (break "Breaking with ~D." 42))
;;
;; Debugger entered on SIMPLE-CONDITION:
;; Breaking with 42.
;;
;; Available restarts:
;; 0 [CONTINUE] Return from BREAK.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here
NIL
```

We can observe that the function `break` establishes a single `continue` restart, which is the standard way of quitting the debugger from inside a breakpoint. (It is not possible to customize the report of that restart, unless the reader performs the exercise of reimplementing `break` from scratch; an example implementation of it, along with the whole condition system, is available in the last chapters of this book.)

The function `break` is also useful in one case which we have not mentioned earlier: in some cases, it can be directly invoked by the function `signal` (and its derivatives, such as `warn`, `error`, or `cerror`). Namely, `signal` is equipped with a capability to directly enter the debugger for some condition types before they are actually signaled; this allows the programmer to quickly generate `break` calls for a whole group of conditions at once.

This behaviour is driven by a standard variable named `*break-on-signals*`. Its initial value is `nil`, which matches no condition types; however, when we set it to an actual condition type, all `signal` calls that signal a condition matching that condition type will first instead `break` with that condition.

```lisp
CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (let ((*break-on-signals* 'error))
             (handler-case (signal 'simple-error :format-control "Error: ~D."
                                                 :format-arguments '(42))
               (error (condition)
                 (format t ";; Handling a ~W.~%" (type-of condition))))))
;;
;; Debugger entered on SIMPLE-CONDITION:
;; Error: 42.
;; (BREAK was entered because of *BREAK-ON-SIGNALS*.)
;;
;; Available restarts:
;; 0 [CONTINUE] Return from BREAK.
;; 1 [RESET] Set *BREAK-ON-SIGNALS* to NIL and continue.
;; 2 [REASSIGN] Return from BREAK and assign a new value to *BREAK-ON-SIGNALS*.
;; 3 [RETRY] Retry evaluating the form.
;; 4 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here
;; Handling a SIMPLE-ERROR.
NIL
```

We can see that with `*break-on-signals*` bound, merely `signal`ing a condition is enough to directly enter the debugger by means of a `break`. Only after the `continue` restart is invoked are the condition handlers for that condition allowed to execute.

We can also see that our Lisp implementation has established additional, non-standard restarts around the `*break-on-signals*`-invoked `break` call: in this case, they are named `reset` and `reassign`. We will not go in detail into them, instead leaving their presence as an example of permitted implementation-defined behaviour. Implementing them is left as an exercise for the reader.

### Recursive debugger

It might be desirable to equip the debugger function with even more functionalities. For instance, the current version of our debugger is not particularly well equipped to deal with recursive errors, which may happen e.g. when one of the restarts chosen by the programmer enters the debugger as a part of the respective restart function without first routing control out of the previous instance of the debugger.

One of the issues with our current debugger is that the programmer does not know how many recursive invocations of the debugger have happened so far. We will implement that by creating a new dynamic variable that stores the current debugger level. We will rebind it on every successive debugger invocation.

In addition, our debugger function has grown rather much, especially with I/O-specific code. Let us try to factor that function and abstract specific parts of it into new functions.

```lisp
(defvar *debugger-level* 0)

(defun print-banner (condition)
  (format t ";;~%;; Debugger level ~D entered on ~S:~%"
          *debugger-level* (type-of condition))
  (format t ";; ~W~%" condition))

(defun print-restarts (restarts)
  (format t ";;~%;; Available restarts:~%")
  (loop for i from 0
        for restart in restarts do
          (format t ";; ~D [~W] ~W~%" i (restart-name restart) restart)))

(defun read-valid-restart-number (number-of-restarts)
  (loop (format t ";;~%;; Invoke restart number: ")
        (let* ((line (read-line *query-io*))
               (integer (parse-integer line :junk-allowed t)))
          (when (and integer (< -1 integer number-of-restarts))
            (return integer)))))

(defun debugger (condition hook)
  (let ((*print-escape* nil)
        (*debugger-level* (1+ *debugger-level*)))
    (print-banner condition)
    (let ((restarts (compute-restarts condition)))
      (print-restarts restarts)
      (let ((chosen-restart (read-valid-restart-number (length restarts)))
            (*debugger-hook* hook))
        (invoke-restart-interactively (nth chosen-restart restarts)))
      (debugger condition hook))))
```

We have factored the code and introduced a new dynamic variable, `*debugger-level*`; we rebind it inside `debugger` and print it inside `print-banner`. Let's try it out with `check-type`:

```lisp
CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (let ((x 42))
             (check-type x string)
             x))
;;
;; Debugger level 1 entered on SIMPLE-TYPE-ERROR:
;; The value of X is 42, which is not of type STRING.
;;
;; Available restarts:
;; 0 [STORE-VALUE] Supply a new value for X.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here
;;
;; Enter a form to be evaluated:                               ; user input here

(let ((maybe-result "some string"))
  (cerror "Continue." "About to supply ~S." maybe-result)
  maybe-result)

;;
;; Debugger level 2 entered on SIMPLE-ERROR:
;; About to supply "some string".
;;
;; Available restarts:
;; 0 [CONTINUE] Continue.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here

"some string"
```

This is better; we can see that the programmer can now see the debugger level inside the banner printed by the debugger, just above the condition. However, the programmer still does not have an easy way of routing control from the nested debugger back to a lower level of nesting.

We can fixing it by wrapping the `invoke-restart-interactively` form inside the debugger with a `with-simple-restart` form that will provide a NLToC-performing `abort` restart that allows the programmer to return to an earlier level of the debugger.

```lisp
(defun debugger (condition hook)
  (let ((*print-escape* nil)
        (*debugger-level* (1+ *debugger-level*)))
    (print-banner condition)
    (let ((restarts (compute-restarts condition)))
      (print-restarts restarts)
      (let ((chosen-restart (read-valid-restart-number (length restarts)))
            (*debugger-hook* hook)
            (current-debugger-level *debugger-level*))
        (with-simple-restart
            (abort "Return to level ~D of the debugger." current-debugger-level)
          (invoke-restart-interactively (nth chosen-restart restarts))))
      (debugger condition hook))))
```

(The new lexical variable, `current-debugger-level`, is required to remember the debugger level at the moment of establishing the restart. If we instead supplid `*debugger-level*` directly as a format argument, it will have been rebound with a new value by the time this restart is reported.)

We can verify that the new restart is available and works as intended.

```lisp
CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (let ((x 42))
             (check-type x string)
             x))
;;
;; Debugger level 1 entered on SIMPLE-TYPE-ERROR:
;; The value of X is 42, which is not of type STRING.
;;
;; Available restarts:
;; 0 [STORE-VALUE] Supply a new value for X.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 0                                    ; user input here
;;
;; Enter a form to be evaluated:                               ; user input here

(let ((maybe-result "some string"))
  (cerror "Continue." "About to supply ~S." maybe-result)
  maybe-result)

;;
;; Debugger level 2 entered on SIMPLE-ERROR:
;; About to supply "some string".
;;
;; Available restarts:
;; 0 [CONTINUE] Continue.
;; 1 [ABORT] Return to level 1 of the debugger.
;; 2 [RETRY] Retry evaluating the form.
;; 3 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 1                                    ; user input here
;;
;; Debugger level 1 entered on SIMPLE-TYPE-ERROR:
;; The value of X is 42, which is not of type STRING.
;;
;; Available restarts:
;; 0 [STORE-VALUE] Supply a new value for X.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Invoke restart number: 2                                    ; user input here
;;
;; ABORT restart invoked; returning to toplevel.
```

We see that the programmer has left the level-2 debugger via the newly established `abort` restart which has once again invoked the level-1 debugger with its respective condition. This approach will also work with more debugger nesting; for instance, a programmer from a level-5 debugger will have restarts that make it possible to drop to any debugger from levels 1 to 4.

### Adding a REPL to the debugger

Another noteworthy idea is to allow the programmer to evaluate arbitrary Lisp code instead of only picking a restart. The moment the programmer has a read-eval-print loop available inside the debugger, they can not only manually compute, find, and invoke restarts, but also inspect and modify their Lisp environment in order to better deal with the erroneous situation. For that reason, debuggers used in real life situations often offer such evaluation functionality.

At the same time, we would like to keep the convenience of selecting a restart by only typing its number. For this reason, we would like to slightly modify the standard behaviour of a Lisp read-eval-print loop by introducing one more stage between its read- and eval-steps. If the programmer enters an integer, we will attempt to match it against our list of restarts; otherwise, we the read form will be normally evaluated.

This will require us to modify `read-valid-restart-number` and change it to handle a wider variety of user input. At this moment, we may even notice that this function will not be expected to read a number anymore: it will be used to query the programmer for general Lisp expressions. We can still keep the restart-number-checking logic in that function, though, and keep evaluating Lisp forms until the user evaluates a number that matches an available restart.

```lisp
(makunbound 'read-valid-restart-number)

(defun read-debug-expression (number-of-restarts)
  (format t ";; Enter a restart number to be invoked~%")
  (format t ";; or an expression to be evaluated.~%")
  (loop (format t "Debug> ")
        (let* ((form (read)))
          (if (and (integerp form) (< -1 form number-of-restarts))
              (return form)
              (print (eval form))))))

(defmacro with-abort-restart (&body body)
  (let ((level (gensym)))
    `(let ((,level *debugger-level*))
       (with-simple-restart (abort "Return to level ~D of the debugger." ,level)
         ,@body))))

(defun debugger (condition hook)
  (let ((*print-escape* nil)
        (*debugger-level* (1+ *debugger-level*)))
    (print-banner condition)
    (let ((restarts (compute-restarts condition)))
      (print-restarts restarts)
      (let* ((*debugger-hook* hook)
             (chosen-restart
              (with-abort-restart (read-debug-expression (length restarts)))))
        (when chosen-restart
          (with-abort-restart
            (invoke-restart-interactively (nth chosen-restart restarts)))))
      (let ((*debugger-level* (1- *debugger-level*)))
        (debugger condition hook)))))
```

We have additionally introduced a *macro* named `with-abort-restart` which establishes an `abort` restart around the function `read-debug-expression` (which contains our debug REPL) and around the restart invocation. Both of these places are now allowed to signal arbitrary errors and therefore invoke the debugger recursively; therefore, we will want a way to reduce the debugger level from both of these places. (Additionally, now that `with-abort-restart` is allowed to return `nil`, we have wrapped the restart invocation in an additional `when` to check for cases where we instead immediately want to drop into the debugger again. We also bind `*debugger-level*` to its previous value before calling the debugger again.)

We can now utilize this REPL to change the state of the program without leaving the debugger. This, in turn, will allow us to e.g. continue from a simple `assert` form, from which we would not previously be able to continue normally.

```lisp
CL-USER> (defvar *x* 24)
*X*

CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (assert (= *x* 42)))
;;
;; Debugger level 1 entered on SIMPLE-ERROR:
;; The assertion (= *X* 42) failed.
;;
;; Available restarts:
;; 0 [CONTINUE] Retry assertion.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Enter a restart number to be invoked
;; or an expression to be evaluated.

Debug> (setf *x* 42)                                           ; user input here
42

Debug> 0                                                       ; user input here
NIL

CL-USER>
```

### Back trace

One more utility that is frequently available in a debugger is a *backtrace*. It is a list of all functions that were *on the stack* at the moment of entering the debugger; programmers frequently utilize these as a debugging aid. A backtrace often contains the names of functions that were previously called, their arguments, and sometimes the values local variables within these functions.

The Common Lisp standard does not contain any standardized functions related to accessing backtraces at any point in program execution; it is therefore up to every implementation to implement such functionality and optionally provide it for programmers. The library [Dissect](https://shinmera.github.io/dissect/) is a portability library, which means that it is similar to `trivial-custom-debugger` that we have mentioned above; but while `trivial-custom-debugger` provides the ability to portably modify the system debugger, Dissect provides a portable means of retrieving and analyzing detailed stack information, such as function names, arguments, or source locations for individual forms on the backtrace.

If we use Quicklisp, then, after issuing `(ql:quickload :dissect)`, we can begin utilizing the Dissect functionality. For instance, we can ask Dissect to print everything that it knows about our environment. A fragment of example output of `(dissect:present t)` may look like the following:

```lisp
CL-USER> (dissect:present t)
#<ENVIRONMENT {10059B6B83}>
   [Environment of thread #<THREAD "new-repl-thread" RUNNING {100237A133}>]

Available restarts:
 0: [RETRY] Retry evaluating the form.
 1: [ABORT] Return to the toplevel.

Backtrace:
 13: (DISSECT:PRESENT T T)
 14: (SI:SIMPLE-EVAL (DISSECT:PRESENT T) #<NULL-LEXENV>)
 15: (EVAL (DISSECT:PRESENT T))
 16: (REPL-EVAL-FROM-STRING "(dissect:present t)")
     ...
```

Extending our hand-written debugger with a Dissect-provided backtrace (returned via calling `(dissect:with-capped-stack () (dissect:stack))`) is left as an exercise for the reader.

### Associating conditions with restarts

The interactive REPL within our debugger allows the programmer to execute arbitrary code: among others, this means that the programmer is now allowed to commit arbitrary programming mistakes - such as, for example, common typos.

```lisp
CL-USER> (setf *x* 42)
42

CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (check-type *x* string))
;;
;; Debugger level 1 entered on SIMPLE-TYPE-ERROR:
;; The value of *X* is 42, which is not of type STRING.
;;
;; Available restarts:
;; 0 [STORE-VALUE] Supply a new value for *X*.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Enter a restart number to be invoked
;; or an expression to be evaluated.

Debug> (setg *x* "forty-two")                                  ; user input here

;;
;; Debugger level 2 entered on UNDEFINED-FUNCTION:
;; The function COMMON-LISP-USER::SETG is undefined.
;;
;; Available restarts:
;; 0 [CONTINUE] Retry using SETG.
;; 1 [USE-VALUE] Use specified function
;; 2 [ABORT] Return to level 1 of the debugger.
;; 3 [RETRY] Retry evaluating the form.
;; 4 [ABORT] Return to the toplevel.
;;
;; Enter a restart number to be invoked
;; or an expression to be evaluated.

Debug>
```

Even though everything seems normal in the level-2 debugger which was invoked, a particularly keen eye might notice a unique restart-related behaviour. Before continuing with the next paragraph, the reader may want to pause for a moment and focus on the list of restarts available in the level-2 debugger and ask themselves: *hasn't something gone missing in meanwhile?*

*(\*jazz music plays\*)*

The interesting thing is that the `store-value` restart that was available inside the level-1 debugger is no longer visible once we have entered the level-2 debugger - even though that we are clearly in the dynamic scope in which that restart has been established, due to the level-1 debugger (in which the `store-value` restart was visible) being still active when the level-2 debugger was invoked. At the same time, we can be sure that the `store-value` restart has no `:test-function` specified and therefore should be always visible regardless of the state of the dynamic environment.

Therefore, we observe a particular behaviour that is related to the *condition* with which the debugger is invoked. The moment we invoked the level-1 debugger with the original `type-error` coming from `check-type`, the `store-value` restart was accessible. However, when we entered the debugger one more time, with the `undefined-function` error coming from us attempting to use an undefined function `setg`, the `store-value` restart has disappeared!

This behaviour is actually defined in Common Lisp. It has been the reason why we have been passing around that optional `condition` argument in all of our restart-based functions: most notably, `compute-restarts` and `find-restart`. We are now ready to add one more modification to our debugger, so it makes the condition object accessible to us via a newly created `*debugger-condition*` dynamic variable.

```lisp
(defvar *debugger-condition* nil)

(defun debugger (condition hook)
  (let ((*print-escape* nil)
        (*debugger-condition* condition)
        (*debugger-level* (1+ *debugger-level*)))
    (print-banner condition)
    (let ((restarts (compute-restarts condition)))
      (print-restarts restarts)
      (let* ((*debugger-hook* hook)
             (chosen-restart
               (with-abort-restart (read-debug-expression (length restarts)))))
        (when chosen-restart
          (with-abort-restart
            (invoke-restart-interactively (nth chosen-restart restarts)))))
      (let ((*debugger-level* (1- *debugger-level*)))
        (debugger condition hook)))))
```

This modification will allow us to see the difference that we get when calling `compute-restarts` with and without the condition argument passed to it.

```lisp
CL-USER> (trivial-custom-debugger:with-debugger (#'debugger)
           (check-type *x* string))
;;
;; Debugger level 1 entered on SIMPLE-TYPE-ERROR:
;; The value of *X* is 42, which is not of type STRING.
;;
;; Available restarts:
;; 0 [STORE-VALUE] Supply a new value for *X*.
;; 1 [RETRY] Retry evaluating the form.
;; 2 [ABORT] Return to the toplevel.
;;
;; Enter a restart number to be invoked
;; or an expression to be evaluated.

Debug> (setg *x* "forty-two")                                  ; user input here

;;
;; Debugger level 2 entered on UNDEFINED-FUNCTION:
;; The function COMMON-LISP-USER::SETG is undefined.
;;
;; Available restarts:
;; 0 [CONTINUE] Retry using SETG.
;; 1 [USE-VALUE] Use specified function
;; 2 [ABORT] Return to level 1 of the debugger.
;; 3 [RETRY] Retry evaluating the form.
;; 4 [ABORT] Return to the toplevel.
;;
;; Enter a restart number to be invoked
;; or an expression to be evaluated.

Debug> *debugger-condition*
#<UNDEFINED-FUNCTION SETG {100D2AF073}>

Debug> (let ((*print-escape* nil))
         (format t "~&~{;; ~W~%~}" (compute-restarts)))
;; Return to level 2 of the debugger.
;; Retry using SETG.
;; Use specified function
;; Return to level 1 of the debugger.
;; Supply a new value for *X*.
;; Retry evaluating the form.
;; Return to the toplevel.
NIL

Debug> (let ((*print-escape* nil))
         (format t "~&~{;; ~W~%~}" (compute-restarts *debugger-condition*)))
;; Return to level 2 of the debugger.
;; Retry using SETG.
;; Use specified function
;; Return to level 1 of the debugger.
;; Retry evaluating the form.
;; Return to the toplevel.
NIL
```

Let us differentiate these two lists of restarts via `set-difference` to find restarts which appear in the first list but don't in the other:

```lisp
Debug> (let ((*print-escape* nil))
         (format t "~&~{;; ~W~%~}"
                   (set-difference
                    (compute-restarts)
                    (compute-restarts *debugger-condition*))))
;; Supply a new value for *X*.
NIL
```

We can see that calling `compute-restarts` *without* passing it the `undefined-function` condition object has caused one more restart to appear - the missing `store-value` restart, with a report "Supply a new value for \*X\*.", which was bound by the `store-value` form which we originally evaluated. From this, we may infer that there must be some sort of relationship between condition objects and restart objects that is checked by `compute-restarts` and `find-restart`, among other functions. This relationship in Common Lisp is named *condition-restart association*.

In the example above, we have been first dealing with a `type-error` coming from `check-type`, but then the object of our focus has changed: we have started to deal with an `undefined-function` coming from our typo while typing `setf`. This is the reason for the existence of condition-restart association, which works by *hiding* the restarts which are *not associated* with the *currently handled* condition.

In this particular situation, the `store-value` restart was associated with the `type-error`; when the debugger is invoked on `undefined-function`, this restart stops being relevant to the currently handled condition, and is therefore hidden from the programmer's view to let them see only restarts which are either explicitly associated with the new `undefined-function` error or not associated with any condition in particular.

This association is always established by `with-simple-restart` and, sometimes, by `handler-case`. (Detailed explanation: `restart-case` is required to detect when the form that it directly invokes is a `signal`, `warn`, `error`, or `cerror` call, or a form which macroexpands into such a call. In such a case, the condition signaled by that form becomes associated with all restarts bound by that particular `handler-case`.)

The programmer is also allowed to manually associate conditions with restarts when they decide that the association automatically performed by `with-simple-restart` and `handler-case` is not enough for their cases. The operator in question is named `with-condition-restarts`. To demonstrate it, we will revive one variable and one function which we have defined in the restart-related chapters.

```lisp
(defvar *toplevel-restarts* '())

(defun compute-relevant-restarts (&optional condition)
  (set-difference (compute-restarts condition) *toplevel-restarts*))
```

A synthetic example for the association behaviour can be seen here:

```lisp
CL-USER> (let ((*toplevel-restarts* (compute-restarts))
               (condition-1 (make-instance 'condition))
               (condition-2 (make-instance 'condition)))
           (restart-bind ((restart-1 (lambda ()))
                          (restart-2 (lambda ())))
             (with-condition-restarts condition-1 (list (find-restart 'restart-1))
               (with-condition-restarts condition-2 (list (find-restart 'restart-2))
                 (format t ";; All restarts:~%")
                 (format t ";; ~S~%" (mapcar #'restart-name
                                             (compute-relevant-restarts)))
                 (format t ";; Restarts associated with condition-1:~%")
                 (format t ";; ~S~%" (mapcar #'restart-name
                                             (compute-relevant-restarts condition-1)))
                 (format t ";; Restarts associated with condition-2:~%")
                 (format t ";; ~S~%" (mapcar #'restart-name
                                             (compute-relevant-restarts condition-2)))))))
;; All restarts:
;; RESTART-2 RESTART-1
;; Restarts associated with condition-1:
;; (RESTART-1)
;; Restarts associated with condition-2:
;; (RESTART-2)
NIL
```

We first create two distinct condition objects, then bind two distinct restarts, and then associate the restart objects with the conditions. This association is then reflected when we list all restarts, then restarts associated with the first condition, and then - with the second condition.

(It is noteworthy that, while many other Lisp forms operate on condition types and restart names as monikers for the actual objects, `with-condition-restarts` operates on the actual *condition/restart instances themselves*. The association does not happen between condition types or restart names: it happens between individual condition and restart objects, because association based on condition types or restart names would be too granular e.g. if we made several typos in a row and ended up with recursively invoking the debugger with several `undefined-function` errors in a row.)

## Implementing the Common Lisp condition system

(I kind of wish I could title this chapter "A simple Common Lisp condition system" - unfortunately, the matters that we touch in this chapters are not as simple anymore.)

So far in this book, we have built the handler and restart subsystems of standard Common Lisp, constructing the basics of these from scratch. This knowledge allows us to proceed into the ultimate chapters of this book, in which we describe an example implementation of a full, standard-compliant condition system for Common Lisp.

The condition system defined in this book is adapted from [Portable Condition System](https://github.com/phoe/portable-condition-system) (henceforth referred to as PCS), which, in turn, has been adapted from the [original condition system](http://www.nhplace.com/kent/CL/Revision-18.lisp) written for the first versions of Common Lisp by Kent M. Pitman. The "portable" in its name refers to the fact that it is defined without using the CL-provided versions of any operators that it defines itself, and therefore its code suitable for e.g. implementing condition systems in Common Lisp systems which do not have a condition system themselves. It also means that it is completely independent from the condition system of the host, allowing for easier introspection and debugging.

PCS purports to comply with the ANSI Common Lisp standard and, at the time of writing, passes the respective tests from the [ANSI-TEST](https://gitlab.common-lisp.net/ansi-test/ansi-test) test suite related to the condition system and assertion operators. (If any issues are found and fixed in PCS code, then this book shall be amended and/or errata shall be published to take that into account.)

For brevity, we will omit the less interesting and more repetitive parts of the full code, such as package definition and the list of definitions for the standard condition types. Readers interested in these parts of the code may see them in the full repository linked earlier or inspect it themselves by issuing `(ql:quickload :portable-condition-system)` on a Quicklisp-enabled Lisp REPL.

If the reader instead would like to follow the book and evaluate forms listed in here, it will be enough for them to only load the file `package.lisp` from that repository, in order to establish the aforementioned package structure. Then, they can do `(in-package :portable-condition-system)` to have a fresh package that is suitable to evaluate our code in.

### Before

One more issue that will become visible in this part of the book is that we will put much greater case to use proper *input/output streams* when reading input and communicating to the user. Previously, all of our `format` calls were using the `t` value for the stream, meaning that they printed to the standard output; same thing happened for our `read` calls.

That is a big simplification of the Common Lisp stream structure which are now going to fix: from now on, depending on the particular situation, we will choose the particular stream with which we want to interact. In some cases, we will interact with `*debug-io*`, which is a stream meant for interactive debugging; in some cases, we will print to `*error-output*`, an output stream meant for logging error information; in some cases, we will interact with `*query-io*`, which is a stream responsible for interactively querying the user for data.

Such care is required to comply with the ANSI Common Lisp standard, and to ensure that our condition system works with Common Lisp systems that rebind these variables, e.g. routing error streams to a different file/window than standard output, and e.g. showing query windows for the user to type in when input is requested from `*query-io*`.

### Package definition

The package for our condition system is allowed to use all Common Lisp symbols, except for symbols related to the condition system. For this, we need to define a list of all these symbols, *shadow* them in our package, and export them to allow other people to use them. The shadowing is to ensure that our package uses its own symbols with the provided names instead of importing them from the host's `common-lisp` package, while exporting them will ensure that the people using our condition system will be able to use our package's functionality.

Our package will be named `portable-condition-system`. Here is the complete list of all symbols shadowed and exported by it, grouped by their role within the condition system.

* Creating conditions: `define-condition`, `make-condition`;
* Standard condition types and their readers: `condition`, `warning`, `serious-condition`, `error`, `style-warning`, `simple-condition`, `simple-warning`, `simple-error`, `simple-condition-format-control`, `simple-condition-format-arguments`, `storage-condition`, `type-error`, `type-error-datum`, `type-error-expected-type`, `simple-type-error`, `control-error`, `program-error`, `cell-error`, `cell-error-name`, `unbound-variable`, `undefined-function`, `unbound-slot`, `unbound-slot-instance`, `stream-error`, `stream-error-stream`, `end-of-file`, `parse-error`, `reader-error`, `package-error`, `package-error-package`, `arithmetic-error`, `arithmetic-error-operands`, `arithmetic-error-operation`, `division-by-zero`, `floating-point-invalid-operation`, `floating-point-inexact`, `floating-point-overflow`, `floating-point-underflow`, `file-error`, `file-error-pathname`, `print-not-readable`, `print-not-readable-object`;
* Handling conditions: `*break-on-signals*`, `signal`, `warn`, `cerror`, `error`, `cerror`, `handler-case`, `handler-bind`, `ignore-errors`;
* Assertions: `assert`, `check-type`, `etypecase`, `ctypecase`, `ecase`, `ccase`;
* Binding and accessing restarts: `restart`, `restart-name`, `find-restart`, `compute-restarts`, `invoke-restart`, `invoke-restart-interactively`, `with-condition-restarts`, `restart-case`, `restart-bind`, `with-simple-restart`, `abort`, `continue`, `muffle-warning`, `store-value`, `use-value`;
* Interfacing with the debugger:  `*debugger-hook*`, `break`, `invoke-debugger`.

In addition, we will prepare a package named `common-lisp+portable-condition-system`, which imports and reexports all the symbols from `common-lisp` *except* for symbols from the above list, which will be imported and reexported from `portable-condition-system`. Such package will be useful for people who want to start using PCS in their programs right away.

(For brevity, this book will also nickname this package `pcs`; this nickname is not present in the package definitions present in the repository.)

### Conditions

In the previous chapters, we have used a Common Lisp macro named `defstruct` to define a structure for our choice objects. It was a tool good enough for the job, given that choices (just like restart objects) are very static in Common Lisp: they have a limited, strictly defined set of functionalities that cannot be extended in any way. Contrary to restarts, however, condition types can be defined with a variety of options: they may exhibit *multiple inheritance* by being subtypes of multiple distinct condition types; their slots may have multiple *reader*, *writer*, and/or *accessor* functions; their slots can be *class-allocated* or *instance-allocated*; they may have a *default initialization argument* list; and so on, and so on.

All that is because Common Lisp condition types defined via `define-condition` are very closely related to classes defined by Common Lisp Object System's `defclass`. The only practical difference between these two macros is the presence of an additional `:report` option in `define-condition`, used to define a report function for condition objects. This allows `define-condition` to be elegantly implemented via `defclass`, which is exactly what many Common Lisp implementations do. (As for impractical differences, the ANSI specification for `define-condition` contradicts itself on the details of how this macro should work on matters like *slot inheritance*; Common Lisp implementors generally agree that `define-condition` should work like `defclass`.)

(Teaching the details of classes, `defclass`, or Common Lisp Object System in general is not in scope of this book; the readers who would like to learn more about these topics can consult *Practical Common Lisp* by Peter Seibel or *Object-Oriented Programming in Common Lisp: A Programmer's Guide to CLOS* by Sonja Keene.)

#### Base class for conditions

First of all, let us define the root of our condition tree: the `condition` class, along with a pair of methods that describe the printing and reporting mechanism.

```lisp
(defclass condition () ())

(defmethod print-object ((object condition) stream)
  (format stream "Condition ~S was signaled." (type-of object)))

(defmethod print-object :around ((object condition) stream)
  (if *print-escape*
      (print-unreadable-object (object stream :type t :identity t))
      (call-next-method)))
```

We define a pair of methods on `print-object`, which is is the generalized object printer that is invoked every time `prin1`, `print`, `princ`, or `format` is used to print Lisp objects.

The first method *specializes* on all objects of class `condition` which we define in the `defclass` form; it means that, whenever `print-object` is called with an object of class `condition`, it is possible that this method will be invoked. Note that it is *possible*, not sure - that is because the second method we define, while it also specializes on the `condition` class, is in addition an `:around` method. In this context, it means that it will be called *before* any method that is not an `:around` method, and therefore will take precedence over the first method we define.

The second method defines a "wrapper" over condition reporting. In particular, we want to ensure that the condition is reported (by means of `call-next-method`) only if the special variable `*print-escape*` is false. If it is true, we want to print the condition object in an unreadable way - which is implemented by the `print-unreadable-object` macro.

We can immediately test this behaviour in our REPL:

```lisp
PCS> (defvar *condition* (make-instance 'condition))
*CONDITION*

PCS> (let ((*print-escape* t))
       (format nil "~W" *condition*))
"#<CONDITION {100F8112B3}>"

PCS> (let ((*print-escape* nil))
       (format nil "~W" *condition*))
"Condition CONDITION was signaled."
```

One issue that we have is the fact that `make-instance` is not the standard-defined way to create condition objects; `make-condition` is. However, since the syntax used in both operators is identical, we can define `make-condition` in terms of `make-instance`.

```lisp
(defun make-condition (type &rest args)
  (apply #'make-instance type args))
```

We may immediately test it:

```lisp
PCS> (make-condition 'condition)
#<CONDITION {1010744EB3}>
```

#### Defining new condition types

We can now create individual condition objects. The next step is to be able to define new condition types, which requires us to *define a new macro*, `define-condition`. In particular, if we have the following `define-condition` form:

```lisp
(define-condition foo-condition (condition)
  ((foo-slot :reader foo-condition-slot :initarg :slot)))
```

Then we would like it to expand into the equivalent of the following form:

```lisp
(defclass foo-condition (condition)
  ((foo-slot :reader foo-condition-slot :initarg :slot)))
```

In the general case, it seems simple - we need to translate `define-condition` into `defclass`. However, there are two special cases that we need to support inside our macro:
* The first one is that all condition types must be supertypes of the `condition` class; therefore, if the programmer provides an empty list of condition supertypes, we must ensure that the list passed to `defclass` contains the class `condition`.
* The other is that we need to specially handle the `:report` option, which may not be directly passed to `defclass`. We need to extract that option and define a `print-object` method specializing on the newly created condition class.

Therefore, if we have the following code:

```lisp
(defun report-foo-condition (condition stream)
  (format stream "A foo happened: ~A"
          (foo-condition-slot condition)))

(define-condition foo-condition ()
  ((foo-slot :reader foo-condition-slot :initarg :slot))
  (:report report-foo-condition))
```

We may expect the above `define-condition` form to expand into an equivalent of the following code:

```lisp
(progn
  (defclass foo-condition (condition)
    ((foo-slot :reader foo-condition-slot :initarg :slot)))
  (defmethod print-object ((condition foo-condition) stream)
    (funcall #'report-foo-condition condition stream))
  'foo-condition)
```

Our macro now needs to expand into a `progn` in order to accumulate more than one generated form. We can see that the superclass list in `defclass` now contains name the of the `condition` class, and that the `:report` option has been extracted into its separate `print-object` method. In addition, we return the name of the condition from the final form, which is required by the Common Lisp standard.

For clarity, we will separate our macro into two logical parts. One of them will be a function named `define-condition-make-report-method` that will be used for generating the `defmethod print-object` form.

```lisp
(defun define-condition-make-report-method (name report-option)
  (let* ((condition (gensym "CONDITION"))
         (stream (gensym "STREAM"))
         (report (second report-option))
         (report-form (if (stringp report)
                          `(write-string ,report ,stream)
                          `(funcall #',report ,condition ,stream))))
    `(defmethod print-object ((,condition ,name) ,stream)
       ,report-form)))
```

It is not in scope of this book to provide explanation about the art of writing Lisp macros, backquote notation, or the use of symbols returned by `gensym` calls. (Readers interested in the topic can consult *On Lisp* by Paul Graham or *Let over Lambda* by Doug Hoyte.) We will therefore innstead focus on treating macros and code-generating functions as black boxes, only outlining important details of their internal structure and testing them in the REPL. An appendix to this book describes the most important parts of the new syntax; the reader may want to read it before proceeding with this chapter, in case they are not used to working with Lisp macros.

The function `define-condition-make-report-method` accepts two arguments: the name of the condition being defined and the whole `:report` option  passed to `define-condition`. Its output is a *backquoted* `defmethod print-object` form that defines a proper reporting method on the new condition.

We see inside the backquoted structure that the condition variable (represented by the unquoted `condition` variable) is going to be specialized to the unquoted `name`, which is the name of the condition class which we define. The body of the defined method is going to be the value of `report-form`, which - as we can see in the variable bindings - is going to be calculated depending on the type of the `report` variable, extracted from the report option. This is consistent with the behaviour of `define-condition`: if the value passed to `:report` is a string, then it should be written to the stream (and so a `write-string` form is generated). Otherwise, that value denotes a function, which must be called with the condition and stream passed as their arguments (represented by the `funcall` form).

We can test the above function by giving it the same arguments that we have passed into the example form above which should define a `foo-condition`: the symbol `foo` and the report option containing a function name.

```lisp
PCS> (define-condition-make-report-method
      'foo '(:report report-foo-condition))
(DEFMETHOD PRINT-OBJECT ((#:CONDITION576 FOO) #:STREAM577)
  (FUNCALL #'REPORT-FOO-CONDITION #:CONDITION576 #:STREAM577))
```

The *gensyms* in the above form denote the variables that the method will be called with; they are passed as-is to the `funcall` form which calls the function named `report-foo-condition`. If we instead passed a lambda form, it would work as well, since the `#'` syntax also accepts lambda forms. However, the situation will change if we decide that `foo-condition` should instead have a static report:

```lisp
PCS> (define-condition-make-report-method
      'foo '(:report "A foo happened."))
(DEFMETHOD PRINT-OBJECT ((#:CONDITION578 FOO) #:STREAM579)
  (WRITE-STRING "A foo happened." #:STREAM579))
```

We can see that the `funcall` form has been replaced with a `write-string` form, suitable for reporting static strings passed to the `:report` option. This works for creating new methods or *redefining* old ones; however, `define-condition` can also be used to *redefine* a condition, meaning that the `:report` option can disappear from the `define-condition` form. To account for this, we must also have an option for removing the method that we define: that is implemented by our `define-condition-remove-report-method` function.

```lisp
(defun define-condition-remove-report-method (name)
  (let ((method (gensym "CONDITION")))
    `(let ((,method (find-method #'print-object '() '(,name t) nil)))
       (when ,method (remove-method #'print-object ,method)))))
```

We may test this with the name of our `foo-condition` to see the generated code:

```lisp
PCS> (define-condition-remove-report-method 'foo-condition)
(LET ((#:CONDITION617 (FIND-METHOD #'PRINT-OBJECT 'NIL '(FOO-CONDITION T) NIL)))
  (WHEN #:CONDITION617 (REMOVE-METHOD #'PRINT-OBJECT #:CONDITION617)))
```

We see that we test for presence of a method defined on `print-object` with no qualifiers and specialized on `foo-condition`. If such a method is found, we call `remove-method` on it to remove it from the system.

The choice of whether we should generate a `defmethod` form or a `remove-method` form depends on whether the `:report` option was passed to `define-condition`. We will check for presence of that form inside the body of our next function, `expand-define-condition`; we will use that function to generate the full expansion of the `define-condition` macro.

```lisp
(defun expand-define-condition (name supertypes direct-slots options)
  (let* ((report-option (find :report options :key #'car))
         (other-options (remove report-option options))
         (supertypes (or supertypes '(condition))))
    `(progn (defclass ,name ,supertypes ,direct-slots ,@other-options)
            ,@(if report-option
                  `(,(define-condition-make-report-method name report-option))
                  `(,(define-condition-remove-report-method name)))
            ',name)))

(defmacro define-condition (name (&rest supertypes) direct-slots &rest options)
  "Defines or redefines a condition type."
  (expand-define-condition name supertypes direct-slots options))
```

This function accepts four arguments: the condition name, a list of supertypes of that condition, a list of direct slots and a list of options. All four are passed from the macro, which additionally performs proper structuring of them inside its *macro lambda list*.

In the first two lines of the body of `expand-define-condition`, we can see that we separate the `:report` option, if any, from the list of all other options provided to the macro. The third line checks if the `supertypes` argument is empty, and, if yes, replaces it with a static list containing only the `condition` symbol.

This takes care of the two edge cases that our implementation of `define-condition` must take care of, and we can proceed to generate the expansion itself. The expansion contains a `defclass` form, and a conditional for handling the `print-object` method. If the `report-option` was found, we will want it to expand into a `defmethod print-object` form, and otherwise, into a `remove-method print-object` form.

(We have separated the function `expand-define-condition` from `define-condition` for slightly easier testing in the REPL. It is more common to include macro bodies directly within `defmacro` forms.)

We can test `expand-define-condition` in the REPL now, checking both for presence and for lack of the `:report` option.

```lisp
PCS> (expand-define-condition
      'foo-condition '()
      '((foo-slot :reader foo-condition-slot :initarg :slot))
      '((:report report-foo-condition)))
(PROGN
  (DEFCLASS FOO-CONDITION (CONDITION)
    ((FOO-SLOT :READER FOO-CONDITION-SLOT :INITARG :SLOT)))
  (DEFMETHOD PRINT-OBJECT ((#:CONDITION624 FOO-CONDITION) #:STREAM625)
    (FUNCALL #'REPORT-FOO-CONDITION #:CONDITION624 #:STREAM625))
 'FOO-CONDITION)

PCS> (expand-define-condition
      'foo-condition '()
      '((foo-slot :reader foo-condition-slot :initarg :slot))
      '())
(PROGN
  (DEFCLASS FOO-CONDITION (CONDITION)
    ((FOO-SLOT :READER FOO-CONDITION-SLOT :INITARG :SLOT)))
  (LET ((#:CONDITION626 (FIND-METHOD #'PRINT-OBJECT 'NIL '(FOO-CONDITION T) NIL)))
    (WHEN #:CONDITION626 (REMOVE-METHOD #'PRINT-OBJECT #:CONDITION626)))
 'FOO-CONDITION)
```

Now that we are sure that our functions for code generation work correctly, we can use `macroexpand-1` on our macro forms to ensure that our macro will expand in the way we expect it to.

```lisp
PCS> (macroexpand-1
      '(define-condition foo-condition ()
         ((foo-slot :reader foo-condition-slot :initarg :slot))
         (:report report-foo-condition)))
(PROGN
  (DEFCLASS FOO-CONDITION (CONDITION)
    ((FOO-SLOT :READER FOO-CONDITION-SLOT :INITARG :SLOT)))
  (DEFMETHOD PRINT-OBJECT ((#:CONDITION627 FOO-CONDITION) #:STREAM628)
    (FUNCALL #'REPORT-FOO-CONDITION #:CONDITION627 #:STREAM628))
 'FOO-CONDITION)
T

PCS> (macroexpand-1
      '(define-condition foo-condition ()
         ((foo-slot :reader foo-condition-slot :initarg :slot))))
(PROGN
  (DEFCLASS FOO-CONDITION (CONDITION)
    ((FOO-SLOT :READER FOO-CONDITION-SLOT :INITARG :SLOT)))
  (LET ((#:CONDITION629 (FIND-METHOD #'PRINT-OBJECT 'NIL '(FOO-CONDITION T) NIL)))
    (WHEN #:CONDITION629 (REMOVE-METHOD #'PRINT-OBJECT #:CONDITION629)))
 'FOO-CONDITION)
T
```

This concludes the implementation of our first macro, `define-condition`, which defines new condition types. (When `defclass` defines a class, it also defines a matching Common Lisp type with the same name as the class). This operator is enough for us to define all of the Common Lisp condition type hierarchy, including matching condition readers and report functions.

(For brevity, we omit the actual definition from the book; interested readers can inspect the file `condition-hierarchy.lisp` inside the PCS repository. The only part of this file that we are going to include are two non-standard condition types, which we define for the convenience of implementing case assertions and restart functions; they shall be defined in their respective chapters.)

### Coercing data to conditions

Before we continue implementing the outwards-visible parts of the condition system, we will focus on one internal utility that will be used by `signal`, `warn`, `error`, `cerror`, and - implicitly - by `restart-case`. The interface to the condition system allows the user to create condition objects in a multitude of ways: for instance, it is possible to `(signal "foo")`, `(signal 'condition)`, `(signal (formatter "foo"))`, or `(signal (make-condition 'condition))`; all of these are valid ways of signaling a condition within `signal`, `warn`, `error`, and `cerror`. To sum up, the datum argument that is passed to one of these four functions may legally be a string, a symbol, a function, or a condition object.

That is why it is useful to have a single function that can handle parsing the arguments for all four of these cases and return condition objects that are then good to properly signal. We will create such a function, named `coerce-to-condition`, and equip it with means of dealing with all of these four situations, as well as with a fifth situation - in which the user has passed a datum that is not of the aforementioned four types.

To separate these four cases from one another, and to demonstrate a little bit of Lisp syntax, by the way, we will create a new *generic function* - a function that, just like the `print-object` function which we have seen before, may specialize on its arguments.

```lisp
(defgeneric coerce-to-condition (datum arguments default-type name))
```

This function will accept four arguments. Obviously, it needs the datum and arguments passed to the signaling function that will be used for constructing the condition object. In case a string is passed as datum, though, the function will need a default simple condition type that will be constructed, which will be passed as the third argument. The fourth argument is the name of the signaling function that is calling `coerce-to-condition`: its purpose is only to be displayed in error messages, if any occur.

Let us start with the seemingly simplest case: passing a condition object to the signaling function, exemplified by `(signal (make-condition 'condition))`.

```lisp
(defmethod coerce-to-condition ((datum condition) arguments default-type name)
  (when arguments
    (cerror "Ignore the additional arguments." 'simple-type-error
            :datum arguments
            :expected-type 'null
            :format-control "You may not supply additional arguments when giving ~S to ~S."
            :format-arguments (list datum name)))
  datum)
```

We can see that the datum is returned, but not immediately; first, a check is made that the argument list is empty (since it is illegal to e.g. `(signal (make-condition 'condition) 42)`); if not, a continuable error is signaled.

```lisp
(defmethod coerce-to-condition ((datum symbol) arguments default-type name)
  (apply #'make-condition datum arguments))
```

The second case is the *actually* simplest one: if we receive a condition type and a list of arguments, we can directly call `make-condition` with them.

```lisp
(defmethod coerce-to-condition ((datum string) arguments default-type name)
  (make-condition default-type :format-control datum :format-arguments arguments))

(defmethod coerce-to-condition ((datum function) arguments default-type name)
  (make-condition default-type :format-control datum :format-arguments arguments))
```

The third and fourth cases are identical: if a string or a function is passed as the datum, then we instantiate a simple condition of `default-type`, passing the datum and arguments as, respectively, the format control and format arguments. (This is possible because `format` accepts *formatter functions* as format controls; the reader may verify this by playing with forms such as `(format t (lambda (stream &rest args) (declare (ignore args)) (write-string "Test!" stream)))`.)

```lisp
(defmethod coerce-to-condition (datum arguments default-type name)
  (error 'simple-type-error :datum datum
                            :expected-type '(or condition symbol function string)
                            :format-control "~S is not coercable to a condition."
                            :format-arguments (list datum)))
```

The fifth and final case is the saddest one; if the datum is not of any recognized type, `coerce-to-condition` signals a type error of its own.

A possible issue that we will encounter here is the fact that this method refers to an undefined function, `error`. This is a function that we will properly define later, since we need `coerce-to-condition` for it to properly function. However, Common Lisp allows functions to be *redefined*, which means that we can create a stub implementation of `error` now and use it for our testing, only to replace it with a proper definition of `error` later in the code.

```lisp
(defun error (datum &rest arguments)
  (declare (ignore arguments))
  (format *error-output* ";; Error: ~A~%" datum)
  (throw :error nil))
```

Our stub implementation requires a `catch` tag to be present in order to transfer control, which means that testing our error situations will require wrapping the tested form with `(catch :error ...)` until we define `error` the proper, standard-compliant way. This stub will work well for us until that time.

We can now test all five cases of this function in the REPL.

```lisp
PCS> (coerce-to-condition (make-condition 'condition) '() 'simple-condition 'signal)
#<CONDITION {1003271693}>

PCS> (coerce-to-condition 'condition '() 'simple-condition 'signal)
#<CONDITION {1003272D63}>

PCS> (coerce-to-condition "A condition was signaled." '() 'simple-condition 'signal)
#<SIMPLE-CONDITION {1003273B23}>

PCS> (coerce-to-condition (lambda (stream) (write-string "A condition was signaled." stream))
                          '() 'simple-condition 'signal)
#<SIMPLE-CONDITION {100336B663}>

PCS> (catch :error (coerce-to-condition 42 '() 'simple-condition 'signal))
;; Error: SIMPLE-TYPE-ERROR
NIL
```

### Restart basics

The next part of the condition system that we are going to touch is the restart subsystem. This does not strictly follow the order in which we have discussed the condition system in the book; however, it is necessary for bootstrapping reasons. In order to implement assertions like `cerror` or `check-type`, which bind restarts as a part of their functioning, we need to implement the macros that are used to bind restarts; and for that, we need to implement restarts themselves.

#### Restart class

Let us start with the `restart` structure, which is very similar to the choice structure we have defined in the earlier chapters. (To repeat: using `defstruct` in place of `defclass` is okay, since we will not allow the user to extend the restart structure.) We will follow it with a pair of `print-object` methods, similar in functioning to the methods we have defined on the `condition` class.

```lisp
(defstruct restart
  (name (error "NAME required."))
  (function (constantly nil))
  (report-function nil)
  (interactive-function nil)
  (test-function (constantly t))
  (associated-conditions '()))

(defmethod print-object :around ((restart restart) stream)
  (if *print-escape*
      (print-unreadable-object (restart stream :type t :identity t)
        (prin1 (restart-name restart) stream))
      (call-next-method)))

(defmethod print-object ((restart restart) stream)
  (cond ((restart-report-function restart)
         (funcall (restart-report-function restart) stream))
        ((restart-name restart)
         (format stream "Invoke restart ~A." (restart-name restart)))
        (t
         (format stream "Invoke the anonymous restart ~S." restart))))
```

We can see that the main reporting method for the restart is somewhat larger than the condition one. This is because restarts, while not being extensible by the user, contain more logic than conditions by default. A part of this logic is the fact that each restart has a report function that, if present, is used to report the restart. Otherwise, if the restart has a name, we use that name to report the restart; and finally, if we have a truly "incognito" restart without a name or a report function, we also need to take that fact into account in the report.

(A keen eye or a working compiler may also notice that we are using the `error` function, which we have not yet defined. That is permitted by the Common Lisp standard; this function will work when it is compiled later, even if it will not work at the moment. We are nonetheless still able to create restart objects right now, as long as we are careful to always provide `:name` argument for them.)

(Additionally, the standard specifies that restart objects have *dynamic extent*, meaning that accessing them outside the dynamic scope in which they are bound has undefined consequences. For ease of testing and clarity of code, we will not apply that trait in our implementation of restarts.)

#### Restart visibility and computing restarts

We may now implement a test for restart visibility. First and foremost, a restart should not be visible if its test function returns `nil`. If it returns true, then we may begin checking the condition-restart association. For a restart to be visible, either we have no condition to test against, or the restart has no conditions associated with it, or the condition must be present among the associated conditions. The function `restart-visible-p` implements exactly this logic.

```lisp
(defun restart-visible-p (restart condition)
  (and (funcall (restart-test-function restart) condition)
       (or (null condition)
           (let ((associated-conditions (restart-associated-conditions restart)))
             (or (null associated-conditions)
                 (member condition associated-conditions))))))
```

One implementation detail for the above function is that `associated-conditions` is a slot within each restart that is modified each time the association is required to change. The change occurs within macro `with-condition-restarts`, which we may now implement.

The macro will work in the following way. Given a condition object and a list of restart objects, it will `push` the condition object to each restart's list of associated conditions, and only then it will execute the body. The task of disassociating them is delegated to another form that `pop`s the conditions off the lists; it is additionally ensured to always run upon leaving the body's dynamic scope by means of `unwind-protect`.

```lisp
(defun expand-with-condition-restarts (condition restarts body)
  (let ((condition-var (gensym "CONDITION"))
        (restarts-var (gensym "RESTARTS"))
        (restart (gensym "RESTART")))
    `(let ((,condition-var ,condition)
           (,restarts-var ,restarts))
       (unwind-protect
            (progn
              (dolist (,restart ,restarts-var)
                (push ,condition-var (restart-associated-conditions ,restart)))
              ,@body)
         (dolist (,restart ,restarts-var)
           (pop (restart-associated-conditions ,restart)))))))

(defmacro with-condition-restarts (condition (&rest restarts) &body body)
  (expand-with-condition-restarts condition restarts body))
```

This macro, in effect, will transform a form like this:

```lisp
(with-condition-restarts condition (list restart-1 restart-2 ...)
  ...)
```

Into the equivalent of a form like this:

```lisp
(let ((restarts (list restart-1 restart-2 ...)))
  (unwind-protect
       (progn (dolist (restart restarts)
                (push condition (restart-associated-conditions restart)))
              ...)
    (dolist (restart restarts)
      (pop (restart-associated-conditions restart)))))
```

In order to test this macro, we need some means of finding restarts based on their association with condition objects. This means that we need to implement `compute-restarts` and `find-restart` - and, in order to implement these, we will need a dynamic variable that shall host our restarts.

```lisp
(defvar *restart-clusters* '())

(defun compute-restarts (&optional condition)
  (loop for restart in (apply #'append *restart-clusters*)
        when (restart-visible-p restart condition)
          collect restart))

(defun find-restart (name &optional condition)
  (dolist (cluster *restart-clusters*)
    (dolist (restart cluster)
      (when (and (or (eq restart name)
                     (eq (restart-name restart) name))
                 (restart-visible-p restart condition))
        (return-from find-restart restart)))))
```

(While `*restart-clusters*` holds restarts in clusters, restarts do not follow the same clustering behaviour as handlers. The specification is not very clear on this issue, but within contemporary Common Lisp implementations, a restart function is allowed to call any restart available within the dynamic environment - including its "neighbours" and the restarts which were defined later in dynamic scope.)

We can see that `compute-restarts` first appends the list of all restart clusters into a flat list of restarts, and then walks it, collecting all restarts which should be visible into a new list. `find-restarts` works slightly differently, as it does not need to collect its results; it walks each cluster and then checks if each restart inside that cluster is eligible for being returned from the function.

We may now test these functions by manually rebinding `*restart-clusters*` to some meaningful value. What is important for us are their names, their test functions, and their association with conditions.

Our sandbox will have the following contents:

```lisp
(defvar *abort-restart-2-visible-p* t)
(defvar *test-condition-1* (make-instance 'condition))
(defvar *test-condition-2* (make-instance 'condition))

(defvar *example-clusters*
  (let ((abort-1 (make-restart :name 'abort
                               :function (lambda () 'abort-1)))
        (retry (make-restart :name 'retry))
        (fail (make-restart :name 'fail
                            :associated-conditions
                            (list *test-condition-1*)))
        (abort-2 (make-restart :name 'abort
                               :function (lambda () 'abort-2)
                               :test-function
                               (lambda (condition)
                                 (declare (ignore condition))
                                 *abort-restart-2-visible-p*))))
    (list (list abort-2 retry)
          (list fail)
          (list abort-1))))
```

Let us first compute all restarts and verify that they are listed in the correct order.


```lisp
PCS> (let ((*restart-clusters* *example-clusters*))
       (format t "~{;; ~S~%~}" (compute-restarts)))
;; #<RESTART ABORT {1014C89C13}>
;; #<RESTART RETRY {1014C89B53}>
;; #<RESTART FAIL {1014C89BB3}>
;; #<RESTART ABORT {1014C89AF3}>
NIL
```

Then, we should ensure that binding the `*abort-restart-2-visible-p*` variable affects the visibility of one of the `abort` restarts.

```lisp
PCS> (let ((*restart-clusters* *example-clusters*)
           (*abort-restart-2-visible-p* nil))
       (format t "~{;; ~S~%~}" (compute-restarts)))
;; #<RESTART RETRY {101759EB53}>
;; #<RESTART FAIL {101759EBE3}>
;; #<RESTART ABORT {101759EAD3}>
NIL
```

Finally, let us verify that the `fail` restart stays visible when we look for restarts associated with `*test-condition-1*`, but disappears - for `*test-conditon-2*`.

```lisp
PCS> (let ((*restart-clusters* *example-clusters*))
       (format t "~{;; ~S~%~}" (compute-restarts *test-condition-1*)))
;; #<RESTART ABORT {1017184ED3}>
;; #<RESTART RETRY {1017184DE3}>
;; #<RESTART FAIL {1017184E73}>
;; #<RESTART ABORT {1017184D63}>
NIL

PCS> (let ((*restart-clusters* *example-clusters*))
       (format t "~{;; ~S~%~}" (compute-restarts *test-condition-2*)))
;; #<RESTART ABORT {1017184ED3}>
;; #<RESTART RETRY {1017184DE3}>
;; #<RESTART ABORT {1017184D63}>
NIL
```

#### Invoking restarts

Now that we can find restarts, we should also be able to invoke them. Let us implement `invoke-restart` and `invoke-restart-interactively`, starting with the first one.

```lisp
(defun invoke-restart (restart &rest arguments)
  (typecase restart
    (restart (apply (restart-function restart) arguments))
    (symbol (let ((real-restart (find-restart restart)))
              (unless real-restart (error "Restart ~S is not active." restart))
              (apply #'invoke-restart real-restart arguments)))
    (t (error "Wrong thing passed to INVOKE-RESTART: ~S" restart))))
```

The above code dispatches based on the type of the `restart` argument passed to it. If it is a restart object, then its restart function is applied to the provided arguments; if it is a symbol, it is treated as a restart name, and the first restart with such name is found and invoked. In case there is no visible restart with such name, or when `restart` is not a *restart designator*, calling `invoke-restart` results in an error.

```lisp
(defun invoke-restart-interactively (restart)
  (typecase restart
    (restart (let* ((function (restart-interactive-function restart))
                    (arguments (if function (funcall function) '())))
               (apply (restart-function restart) arguments)))
    (symbol (let ((real-restart (find-restart restart)))
              (unless real-restart (error "Restart ~S is not active." restart))
              (invoke-restart-interactively real-restart)))
    (t (error "Wrong thing passed to INVOKE-RESTART-INTERACTIVELY: ~S"
              restart))))
```

The implementation of `invoke-restart-interactively` is the same in two `typecase` branches out of three. The third one is the most interesting: first, the restart is queried for its interactive function. If it is present, it is called to retrieve the list of arguments that will be used to apply the restart function to; otherwise, the argument list is assumed to be the empty list. The restart function is then called.

We may now test invoking our restarts. First, let us attempt to invoke a restart non-interactively:

```lisp
PCS> (let* ((restart (make-restart :name 'add-42
                                   :function (lambda (x) (+ 42 x))))
            (*restart-clusters* (list (list restart))))
       (invoke-restart 'add-42 4200))
4242
```

Then, let us add a trivial interactive function to the restart, and call `invoke-restart-interactively`:

```lisp
PCS> (let* ((restart (make-restart :name 'add-42
                                   :function (lambda (x) (+ 42 x))
                                   :interactive-function (lambda () '(4200))))
            (*restart-clusters* (list (list restart))))
       (invoke-restart-interactively 'add-42))
4242
```

Let us also try to invoke non-existing restarts:

```lisp
PCS> (catch :error (invoke-restart 'foo))
;; Error: Restart ~S is not active.
NIL

PCS> (catch :error (invoke-restart-interactively 'foo))
;; Error: Restart ~S is not active.
NIL
```

### Binding restarts

By now, we have all the parts required to find and invoke our restart objects, as well as to manually create them. That last part, however, is what the Common Lisp standard does not provide the programmer with. Instead, restarts must be established within dynamic scope by means of restart-binding macros, which we will now implement.

The simplest one of the three is `restart-bind`, which accepts a list of restart bindings, creates new restart objects based on them, and attached a list of newly created restarts to the overall list of restart clusters. This is exactly what our macro is going to do.

```lisp
(defun restart-bind-transform-binding (binding)
  (destructuring-bind (name function . arguments) binding
    `(make-restart :name ',name :function ,function ,@arguments)))

(defun expand-restart-bind (bindings body)
  (let ((cluster (mapcar #'restart-bind-transform-binding bindings)))
    `(let ((*restart-clusters* (cons (list ,@cluster) *restart-clusters*)))
       ,@body)))

(defmacro restart-bind (bindings &body body)
  (expand-restart-bind bindings body))
```

We see that there are two logical parts to our macro. The first one is a function that accepts a raw binding the way it is listed inside `handler-bind` and turns it into a `make-restart` form that will construct our restart object with the provided data. The second part is the main body of the macro which calls `restart-bind-transform-binding` on all bindings passed to `handler-bind` and then rebinds `*restart-clusters*` by adding a new list element - our new handler cluster - atop them.

Let us perform some basic tests of these functions, starting with the binding transform.

```lisp
PCS> (restart-bind-transform-binding
      '(return-42 (lambda () 42)))
(MAKE-RESTART :NAME 'FOO-RESTART
              :FUNCTION (LAMBDA () 42))

PCS> (restart-bind-transform-binding
      '(return-42 (lambda () 42)
                  :test-function (lambda (condition) (typep condition 'error))
                  :report-function "Return 42 if an error is being signaled."))
(MAKE-RESTART :NAME 'RETURN-42
              :FUNCTION (LAMBDA () 42)
              :TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
              :REPORT-FUNCTION "Return 42 if an error is being signaled.")
```

Then, the body generator:

```lisp
PCS> (expand-restart-bind
      '((return-42 (lambda () 42)
         :test-function (lambda (condition) (typep condition 'error))
         :report-function "Return 42 if an error is being signaled."))
      '((let ((restart (find-restart 'return-42)))
          (when restart (invoke-restart restart)))))
(LET ((*RESTART-CLUSTERS*
       (CONS
        (LIST
         (MAKE-RESTART :NAME 'RETURN-42
                       :FUNCTION (LAMBDA () 42)
                       :TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
                       :REPORT-FUNCTION "Return 42 if an error is being signaled."))
        *RESTART-CLUSTERS*)))
  (LET ((RESTART (FIND-RESTART 'RETURN-42)))
    (WHEN RESTART (INVOKE-RESTART RESTART))))
```

And finally, the macro body (which is, in a way, just a formality; we only effectively test the macro lambda list, which ensures that the arguments inside the macro are structured properly).

```lisp
PCS> (macroexpand-1
      '(restart-bind
           ((return-42 (lambda () 42)
             :test-function (lambda (condition) (typep condition 'error))
             :report-function "Return 42, but only if an error is being signaled."))
         (let ((restart (find-restart 'return-42)))
           (when restart (invoke-restart restart)))))
(LET ((*RESTART-CLUSTERS*
       (CONS
        (LIST
         (MAKE-RESTART :NAME 'RETURN-42
                       :FUNCTION (LAMBDA () 42)
                       :TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
                       :REPORT-FUNCTION "Return 42 if an error is being signaled."))
        *RESTART-CLUSTERS*)))
  (LET ((RESTART (FIND-RESTART 'RETURN-42)))
    (WHEN RESTART (INVOKE-RESTART RESTART))))
T
```

Now that we have verified our expansions, we can test the macro itself.

```lisp
PCS> (restart-bind
         ((return-42 (lambda () 42)
                     :test-function (lambda (condition) (typep condition 'error))
                     :report-function "Return 42 if an error is being signaled."))
       (let ((restart (find-restart 'return-42)))
         (when restart (invoke-restart restart))))
NIL

PCS> (restart-bind
         ((return-42 (lambda () 42)
                     :test-function (lambda (condition) (typep condition 'error))
                     :report-function "Return 42 if an error is being signaled."))
       (let ((restart (find-restart 'return-42 (make-condition 'error))))
         (when restart (invoke-restart restart))))
42
```

### Restart cases

A functioning `restart-bind` is the most important construction block for the more commonly used restart operator, `restart-case`. It is not the only construction block required, however; due to the keyword differences between `restart-bind` and `restart-case`, the need to parse keyword-value pairs from the body of each restart case, and the requirement to detect common signaling operators for automatic condition-restart association, `restart-case` is the most complex macro in the whole condition system.

Therefore, we will construct the fully conformant macro incrementally. We will start with a basic version that only performs a non-local transfer of control, and then add the above differences one by one.

#### First iteration: basics

The non-expanded `restart-case` accepts its input as an expression to execute (which, for now, we may pass as-is without any transformations) and a list of cases. Each case need to be transformed into a pair of subforms: one that will be a `restart-bind` binding, and another that will form the restart case. The binding is responsible for passing the arguments passed to the restart binding and transferring control to the restart case, which, in turn, executes the body forms of the given case.

Therefore, we will need a function to parse a restart case and annotate it with a unique symbol that will be used to perform the transfer of control. Such a parsed and annotated case can then be passed to a pair of functions that generate proper restart binding and restart case forms ready to be spliced into the macro body. This, along with the function that will generate the proper body of `restart-case`, means that the first iteration of our macro will be containable within four separate functions.

Let us start with the first one. For simplicity, let us assume that the cases passed to `restart-case` have the same form as `restart-bind` bindings.

```lisp
(defun restart-case-parse-case (case)
  (destructuring-bind (name function . options) case
    (let ((tag (gensym (format nil "~A-RESTART-CASE" name))))
      (list tag name function options))))
```

Invoking this function on a restart case gives us a four-element list containing a unique tag symbol, the restart name, restart function, and a list of options.

```lisp
(defun report-return-42 (stream)
  (write-string "Return 42 if an error is being signaled." stream))
```

```lisp
PCS> (restart-case-parse-case
      '(return-42 (lambda () 42)
        :test-function (lambda (condition) (typep condition 'error))
        :report-function #'report-return-42))
(#:RETURN-42-RESTART-BINDING616 RETURN-42 (LAMBDA () 42)
 (:TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
  :REPORT-FUNCTION #'REPORT-RETURN-42))
```

We can use this structure in the pair of functions that will generate our macro parts for us. First, let us make the restart binding.

```lisp
(defun restart-case-make-restart-binding (parsed-case temp-var)
  (destructuring-bind (tag name function options) parsed-case
    (declare (ignore function))
    (let ((lambda-var (gensym "RESTART-ARGS")))
      `(,name
        (lambda (&rest ,lambda-var) (setq ,temp-var ,lambda-var) (go ,tag))
        ,@options))))
```

The above function requires two arguments: the parsed case from `restart-case-parse-case` and the name of the temporary variable that it should set in order to pass the restart arguments outside of the restart binding. We will see this mechanism in action when we come to the main body of `restart-case`; for now, let us assume that the temporary variable is the symbol named `temp-var`.

```lisp
PCS> (let ((case (restart-case-parse-case
                  '(return-42 (lambda () 42)
                    :test-function (lambda (condition) (typep condition 'error))
                    :report-function #'report-return-42))))
       (restart-case-make-restart-binding case 'temp-var))
(RETURN-42
 (LAMBDA (&REST #:RESTART-ARGS622)
   (SETQ TEMP-VAR #:RESTART-ARGS622)
   (GO #:RETURN-42-RESTART-BINDING621))
 :TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
 :REPORT-FUNCTION #'REPORT-RETURN-42)
```

We can see that this `restart-case-make-restart-binding` returned, indeed, a restart binding. It binds a restart named `return-42` with a test function and report function that we have already seen before. The restart function accepts a `&rest` argument, which gathers all arguments that restart was invoked in into a list. This list is then set to the `temp-var` symbol which we have passed, and a `go` call then transfers control to the unique tag the case has been annotated with.

Let us compare this with the function generating the other half of the mechanism.

```lisp
(defun restart-case-make-restart-case (parsed-case temp-var block-name)
  (destructuring-bind (tag name function options) parsed-case
    (declare (ignore name options))
    `(,tag (return-from ,block-name (apply ,function ,temp-var)))))
```

This function accepts three arguments in total: the parsed case, the temporary variable, and a *block name* from which it should return the results of the restart function. We can see that the original function passed to the restart binding is ignored inside the result of `restart-case-make-restart-binding`; it is instead executed here, after the control has been transferred, applied to the arguments from `temp-var` - the temporary variable that has been set by the restart binding.

This function returns a subform that starts with the tag; its purpose is to be spliced into a `tagbody` form, where the unique symbol will be recognized as a `go` tag to which control may be transferred. Because `tagbody` returns `nil`, a valid `block` that wraps the `tagbody` is required in order to transfer return values outside.

We can test this function in order to see the output that will be spliced into `tagbody` from our `return-42` restart.

```lisp
PCS> (let ((case (restart-case-parse-case
                  '(return-42 (lambda () 42)
                    :test-function (lambda (condition) (typep condition 'error))
                    :report-function #'report-return-42))))
       (restart-case-make-restart-case case 'temp-var 'block-name))
(#:RETURN-42-HANDLER-BINDING623
 (RETURN-FROM BLOCK-NAME (APPLY (LAMBDA () 42) TEMP-VAR)))
```

As expected, we can see a unique symbol that will become the `go` tag, and a `return-from` form that will return the values from evaluating `(apply (lambda () 42) temp-var)` outside a block named `block-name`.

The above knowledge allows us to understand the general structure of the code that `restart-case` will expand to. We need to have a temporary variable that will allow us to transfer restart arguments outside the binding; we need to have a `block` to return values from; and we need to have a `tagbody` that will allow us to transfer control outside the restart bindings. Using this knowledge, we may construct the main body of `restart-case`.

```lisp
(defun expand-restart-case (expression cases)
  (let ((block-name (gensym "RESTART-CASE-BLOCK"))
        (temp-var (gensym "RESTART-CASE-VAR"))
        (parsed-cases (mapcar #'restart-case-parse-case cases)))
    (flet ((make-restart-binding (case)
             (restart-case-make-restart-binding case temp-var))
           (make-restart-case (case)
             (restart-case-make-restart-case case temp-var block-name)))
      `(let ((,temp-var nil))
         (declare (ignorable ,temp-var))
         (block ,block-name
           (tagbody
              (restart-bind ,(mapcar #'make-restart-binding parsed-cases)
                (return-from ,block-name ,expression))
              ,@(apply #'append (mapcar #'make-restart-case parsed-cases))))))))

(defmacro restart-case (expression &body cases)
  (expand-restart-case expression cases))
```

First of all, we define gensyms for the `block` name and our temporary variable. Next, we parse all the cases using `resart-case-parse-case`. Then, we define a pair of *local* functions, `make-restart-binding` and `make-restart-case`. We do that in order to have one-argument functions suitable for passing to `mapcar` later in code. (Technical detail: the block name and temporary variable name, which are the additional arguments that `restart-case-make-restart-binding` and `restart-case-make-restart-case` accept, are going to be constant throughout the function call; this is why we may use a technique called *partial application* and construct functions that have these constant arguments already "filled in" and therefore no longer require them to be passed.)

After constructing the local functions, we begin to construct our body. We define our temporary variable (and make it *ignorable*, in case `restart-case` is called without any restarts and therefore the variable is unused), we define our `block` and `tagbody`. Inside, we can see a `restart-bind` that contains a list of restart binding returned by `mapcar make-restart-binding` and a `return-from` form that will return the values of our expression in case none of the freshly bound restarts are invoked. There is also the result of appending all calls to `make-restart-case` that is spliced into our `tagbody` form; this will provide the tags that are recognized by `tagbody` and, therefore, means of transferring control outside of `restart-bind`.

Let us test the expander function with the same restart binding that we have worked with earlier, introducing a small modification to the expression we want to evaluate.

```lisp
PCS> (expand-restart-case
      '(let ((restart (find-restart 'return-42)))
         (when restart (invoke-restart restart))
         24)
      '((return-42
         (lambda () 42)
         :test-function (lambda (condition) (typep condition 'error))
         :report-function #'report-return-42)))
(LET ((#:RESTART-CASE-VAR631 NIL))
  (DECLARE (IGNORABLE #:RESTART-CASE-VAR631))
  (BLOCK #:RESTART-CASE-BLOCK630
    (TAGBODY
      (RESTART-BIND ((RETURN-42 (LAMBDA (&REST #:RESTART-ARGS633)
                                  (SETQ #:RESTART-CASE-VAR631 #:RESTART-ARGS633)
                                  (GO #:RETURN-42-HANDLER-BINDING632))
                     :TEST-FUNCTION
                     (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
                     :REPORT-FUNCTION #'REPORT-RETURN-42))
        (RETURN-FROM #:RESTART-CASE-BLOCK630
          (LET ((RESTART (FIND-RESTART 'RETURN-42)))
            (WHEN RESTART (INVOKE-RESTART RESTART))
            24)))
     #:RETURN-42-HANDLER-BINDING632
      (RETURN-FROM #:RESTART-CASE-BLOCK630
        (APPLY (LAMBDA () 42) #:RESTART-CASE-VAR631)))))
```

Here we can see our `restart-case` expansion in its full glory. We bind a variable, create a `block` and a `tagbody`, we bind a restart by means of `restart-bind`, and we begin evaluating our expression, preparing to return its value from the `block` which we have declared.

If the restart is invoked, the temporary variable (named `#:restart-case-var631`) is set to the list of restart arguments (in this case, empty!), control is transferred outside the dynamic scope of `restart-bind`, and our 42-returning function is applied to the (empty) list of arguments, yielding `42`.

If the restart is not invoked, however, our expression returns `24` - and that value is then immediately returned from the `block`, and therefore, from all of the expanded `handler-case`.

Let us see if that is the case:

```lisp
PCS> (restart-case (let ((restart (find-restart 'return-42)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42
        (lambda () 42)
        :test-function (lambda (condition) (typep condition 'error))
        :report-function #'report-return-42))
24

PCS> (restart-case (let* ((condition (make-condition 'error))
                          (restart (find-restart 'return-42 condition)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42
        (lambda () 42)
        :test-function (lambda (condition) (typep condition 'error))
        :report-function #'report-return-42))
42
```

#### Second iteration: forms instead of a function

Our first iteration of `restart-case` works well. We may proceed to work on shaping it towards compliance with the Common Lisp standard. First, we need to notice that the forms inside a `restart-case` are spliced into the case itself; the keyword-value pairs are placed between the restart case lambda list and the actual body forms. This means that the `restart-case` body from the above example should instead look like this:

```lisp
(restart-case (let* ((condition (make-condition 'error))
                     (restart (find-restart 'return-42 condition)))
                (when restart (invoke-restart restart))
                24)
  (return-42 ()
    :test-function (lambda (condition) (typep condition 'error))
    :report-function #'report-return-42
    42))
```

To do this, we must modify our case-parsing function to separate our case into five distinct parts: the unique tag, restart name, lambda list, keyword-value pairs, and the body of the restart case. The new structure must then be accounted for in the parts that generate restart bindings and the proper restart case.

Let us first construct a function that parses the part of the restart case after the lambda list and returns two values: the keyword-value pairs, if any, and the remaining body forms.

```lisp
(defun restart-case-pop-keywords (case)
  (let ((things case)
        (keywords '(:report-function :interactive-function :test-function)))
    (loop for thing = (first things)
          if (member thing keywords)
            collect (pop things) into pairs
            and collect (pop things) into pairs
          else return (list pairs things))))
```

Our function iterates over the Lisp data inside `things`. If the form matches one of the three recognized keywords, the `loop` collects the keyword and its matching value from the list. If the next form awaiting to be processed is not a known keyword, the iteration stops, and the body and keyword-value pairs are returned.

Let us test this newly modified function on the form of our restart case.

```lisp
PCS> (restart-case-pop-keywords
      '(:test-function (lambda (condition) (typep condition 'error))
        :report-function #'report-return-42
        42))
((:TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
 :REPORT-FUNCTION #'REPORT-RETURN-42)
 (42))
```

We can see that the body, `(42)`, is correctly returned; so are the values for the report function, interactive function (none), and test function.

We can utilize this function inside our `restart-case-parse-case`. We will account for the fact that we now need to pop keyword-value pairs from our case body and return five values instead of four.

```lisp
(defun restart-case-parse-case (case)
  (destructuring-bind (name lambda-list . rest) case
    (destructuring-bind (options body) (restart-case-pop-keywords rest)
      (let ((tag (gensym (format nil "~A-RESTART-BINDING" name))))
        (list tag name lambda-list options body)))))
```

Let us test our function by passing a full restart case to it:

```lisp
PCS> (restart-case-parse-case
      '(return-42 ()
        :test-function (lambda (condition) (typep condition 'error))
        :report-function #'report-return-42
        42))
(#:RETURN-42-RESTART-BINDING743 RETURN-42 NIL
 (:TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
  :REPORT-FUNCTION #'REPORT-RETURN-42)
 (42))
```

We get a tag, restart name, restart lambda list (empty), a list of keywords, and the body of the case - which is exactly what we wanted. Now, we need to adjust `restart-case-make-restart-binding` and `restart-case-make-restart-case` to use the new five-element structure - starting with the former.

```lisp
(defun restart-case-make-restart-binding (parsed-case temp-var)
  (destructuring-bind (tag name lambda-list options body) parsed-case
    (declare (ignore lambda-list body))
    (let ((lambda-var (gensym "RESTART-ARGS")))
      `(,name
        (lambda (&rest ,lambda-var) (setq ,temp-var ,lambda-var) (go ,tag))
        ,@options))))
```

The only required here is splitting the old `function` argument into separate `lambda-list` and `body` arguments; both of them are not used in the binding and therefore ignored. These two arguments will be used the case-making function though:

```lisp
(defun restart-case-make-restart-case (parsed-case temp-var block-name)
  (destructuring-bind (tag name lambda-list options body) parsed-case
    (declare (ignore name options))
    `(,tag
      (return-from ,block-name (apply (lambda ,lambda-list ,@body) ,temp-var)))))
```

Once again, the change here is minor: we have replaced `function` with an explicit `lambda` form which accepts our lambda list and case body.

We may now test both functions with the new form of the restart case.

```lisp
PCS> (let ((parsed-case (restart-case-parse-case
                         '(return-42 ()
                           :test-function (lambda (condition) (typep condition 'error))
                           :report-function #'report-return-42
                           42))))
       (restart-case-make-restart-binding parsed-case 'temp-var))
(RETURN-42
 (LAMBDA (&REST #:RESTART-ARGS756)
   (SETQ TEMP-VAR #:RESTART-ARGS756)
   (GO #:RETURN-42-RESTART-BINDING755))
 :TEST-FUNCTION (LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
 :REPORT-FUNCTION #'REPORT-RETURN-42)

PCS> (let ((parsed-case (restart-case-parse-case
                         '(return-42 ()
                           :test-function (lambda (condition) (typep condition 'error))
                           :report-function #'report-return-42
                           42))))
       (restart-case-make-restart-case parsed-case 'temp-var 'block-name))
(#:RETURN-42-RESTART-BINDING757
 (RETURN-FROM BLOCK-NAME (APPLY (LAMBDA () 42) TEMP-VAR)))
```

Both of these functions return what they should (and what they did in the above example!), which means that no modifications to `expand-restart-case` are required. We may now test whether the `restart-case` macro accepts our new syntax:

```lisp
PCS> (restart-case (let* ((restart (find-restart 'return-42)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42 ()
         :test-function (lambda (condition) (typep condition 'error))
         :report-function #'report-return-42
         42))
24

PCS> (restart-case (let* ((condition (make-condition 'error))
                          (restart (find-restart 'return-42 condition)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42 ()
         :test-function (lambda (condition) (typep condition 'error))
         :report-function #'report-return-42
         42))
42
```

#### Third iteration: managing the keyword differences

The third iteration of our `restart-case` macro will extend the keyword-parsing mechanism that we have created in the second iteration. Not only the keywords used by `restart-case` are different from the ones used by `restart-bind`, but their functioning is distinct as well: whereas for `restart-bind` we may pass any function objects, `restart-case` accepts only lambda forms or function names (or strings, for its `:report` option).

We will modify `restart-case-pop-keywords` to call one more function, `restart-case-transform-subform` on the keyword and value that it finds. In addition, we must modify the set of keywords that it recognizes.

```lisp
(defun restart-case-pop-keywords (case)
  (let ((things case)
        (keywords '(:report :interactive :test)))
    (loop for thing = (first things)
          if (member thing keywords)
            append (restart-case-transform-subform (pop things) (pop things))
              into pairs
          else return (list pairs things))))
```

Now, we need to define `restart-case-transform-subform`.

```lisp
(defun restart-case-transform-subform (keyword value)
  (case keyword
    (:report
     (if (stringp value)
         `(:report-function (lambda (stream) (write-string ,value stream)))
         `(:report-function #',value)))
    (:interactive `(:interactive-function #',value))
    (:test `(:test-function #',value))))
```

The above function dispatches on the keyword. If the keyword is `:interactive` or `:test`, a matching `:interactive-function` or `:test-function` is returned. Situation is slightly more complicated in case of `:report`, since - just like in `define-condition` - we need to account for the possibility of

We may now test `restart-case-transform-subform` to verify that all four of its branches are taken into account - and then, test `restart-case-pop-keywords` on our example `return-42` restart.

```lisp
PCS> (restart-case-transform-subform :report "Test report")
(:REPORT-FUNCTION (LAMBDA (STREAM) (WRITE-STRING "Test report" STREAM)))

PCS> (restart-case-transform-subform :report 'report-return-42)
(:REPORT-FUNCTION #'REPORT-RETURN-42)

PCS> (restart-case-transform-subform :test (lambda (condition) (typep condition 'error)))
(:TEST-FUNCTION #'#<FUNCTION (LAMBDA (CONDITION)) {52E349AB}>)

PCS> (restart-case-transform-subform :interactive (lambda () '(42)))
(:INTERACTIVE-FUNCTION #'#<FUNCTION (LAMBDA ()) {52E34F4B}>)

PCS> (restart-case-pop-keywords
      '(:test (lambda (condition) (typep condition 'error))
        :report report-return-42
        42))
((:TEST-FUNCTION #'(LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
  :REPORT-FUNCTION #'REPORT-RETURN-42)
 (42))
```

That is all that's required. The modification is complete, and our `restart-case` is now syntactically compliant with ANSI Common Lisp.

```lisp
PCS> (restart-case (let* ((condition (make-condition 'error))
                          (restart (find-restart 'return-42 condition)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42 ()
         :test (lambda (condition) (typep condition 'error))
         :report report-return-42
         42))
42

PCS> (restart-case (let* ((restart (find-restart 'return-42)))
                     (when restart (invoke-restart restart))
                     24)
       (return-42 ()
         :test (lambda (condition) (typep condition 'error))
         :report report-return-42
         42))
24
```

#### Fourth iteration: associating conditions with restarts

Our `restart-case` already spans across six distinct functions. We will need to increase that number in order to add one final standard feature of `restart-case`. However, the required modification will be completely separated from the ones we have done so far: the change will be related to the *expression* that is passed to `handler-case`, which, so far, we have spliced into the resulting macroexpansion without any alterations.

That feature is implicit condition-restart association, which we have already described earlier; it is triggered only when the form passed to `restart-case`, *or the result of its macroexpansion*, is a direct call to `signal`, `warn`, `error`, or `cerror`. This implies that `restart-case` must have a capability to macroexpand the expressions that are passed to it, detect if the expression is a call to one of the four aforementioned functions, and *rewrite* such a form into one utilizing `with-condition-restarts`.

This is not a trivial task. Let us start by slightly modifying `restart-case` and `restart-case-expand` to suit our needs.

```lisp
(defun expand-restart-case (expression cases environment)
  (let ((block-name (gensym "RESTART-CASE-BLOCK"))
        (temp-var (gensym "RESTART-CASE-VAR"))
        (parsed-cases (mapcar #'restart-case-parse-case cases)))
    (flet ((make-restart-binding (case)
             (restart-case-make-restart-binding case temp-var))
           (make-restart-case (case)
             (restart-case-make-restart-case case temp-var block-name)))
      `(let ((,temp-var nil))
         (declare (ignorable ,temp-var))
         (block ,block-name
           (tagbody
              (restart-bind ,(mapcar #'make-restart-binding parsed-cases)
                (return-from ,block-name
                  ,(if (restart-case-signaling-form-p expression environment)
                       (restart-case-expand-signaling-form expression environment)
                       expression)))
              ,@(apply #'append (mapcar #'make-restart-case parsed-cases))))))))

(defmacro restart-case (expression &body cases &environment environment)
  (expand-restart-case expression cases environment))
```

We can see that the macro definition has been expanded to include a special *environment* object inside its lambda list; this object is not directly passed to it by the programmer, but is usually provided by the Lisp implementation that calls the macroexpander. While we will not describe environment objects in detail, we will note its traits that are the most important to us in this case. This argument is only available in macros; it holds, among other things, information about local macro definitions, which is information important for passing into the `macroexpand` calls of our new functions. Since they must perform macroexpansion of their own, it is important that they receive this argument to be able to expand macros correctly. That is why `restart-case` passes this argument to `expand-restart-case`, which - in turn - passes it into our yet-undefined functions `restart-case-signaling-form-p` and `restart-case-expand-signaling-form`.

Let us continue by defining the predicate function. It needs to macroexpand the form and return true if it is a list whose first element is a standard condition-signaling operator.

```lisp
(defun restart-case-signaling-form-p (expression env)
  (let ((expansion (macroexpand expression env)))
    (and (consp expansion)
         (member (car expansion) '(signal warn error cerror)))))
```

We can perform quick REPL tests, passing `nil` as the second argument to signify a *null environment*.

```lisp
PCS> (restart-case-signaling-form-p '(+ 2 2) nil)
NIL

PCS> (restart-case-signaling-form-p '(error 2 2) nil)
(ERROR CERROR)
```

(In the second case, the function returned a non-`nil` value, which means that it returned true. Common Lisp has generalized booleans, which mean that anything that is not `nil` is a true value, even though the symbol `t` is designated to be the "canonical" representation of truth - the standard type `boolean`, after all, is defined as `(member nil t)`.)

If `restart-case-signaling-form-p` has returned true, then `restart-case` must rewrite the signaling form. Let us briefly analyze the possibilities here; we can see that the function `signal`, `warn`, and `error` have the same lambda lists, which means that we are allowed to group them together. The fourth function, `cerror` has one more required argument, and so we will treat it separately.

Let us start with the first case, in which we will expand a non-`cerror` form. It will be our first chance to use the `coerce-to-condition` helper function which we have defined in the earlier chapters.

```lisp
(defun restart-case-expand-non-cerror (expansion)
  (destructuring-bind (function-name datum . args) expansion
    (let* ((type (case function-name
                   (signal 'simple-condition)
                   (warn 'simple-warning)
                   (error 'simple-error)))
           (condition (gensym "CONDITION")))
      `(let ((,condition (coerce-to-condition ,datum (list ,@args) ',type ',function-name)))
         (with-condition-restarts ,condition (car *restart-clusters*)
           (,function-name ,condition))))))
```

We expand into a `let` form that binds a new variable and instantiates a condition object via `coerce-to-condition`, based on the arguments that were passed into the function and the name of the function. This condition is then associated with all the restarts from the most recent restart cluster, bound by the `restart-case` form that this expression shall be spliced into. Once that is done, the original function is used to signal the newly created condition object.

We can test this function in the REPL to verify that it generates the output that we expect.

```lisp
PCS> (restart-case-expand-non-cerror '(error "Failure"))
(LET ((#:CONDITION851 (COERCE-TO-CONDITION "Failure" (LIST) 'SIMPLE-ERROR 'ERROR)))
  (WITH-CONDITION-RESTARTS #:CONDITION851 (CAR *RESTART-CLUSTERS*)
    (ERROR #:CONDITION851)))
```

This one works - let us dig into the `cerror` case, which has an optional format control for the `continue` restart that it establishes, but - at the same time - it does not need to compute the resulting simple condition type.

```lisp
(defun restart-case-expand-cerror (expansion)
  (destructuring-bind (function-name format-control datum . args) expansion
    (let* ((type 'simple-error)
           (condition (gensym "CONDITION")))
      `(let ((,condition (coerce-to-condition ,datum (list ,@args) ',type ',function-name)))
         (with-condition-restarts ,condition (car *restart-clusters*)
           (,function-name ,format-control ,condition))))))
```

We may test that it expands into something we want.

```lisp
PCS> (restart-case-expand-cerror '(cerror "Continue from failure." "Failure"))
(LET ((#:CONDITION866 (COERCE-TO-CONDITION "Failure" 'NIL 'SIMPLE-ERROR 'CERROR)))
  (WITH-CONDITION-RESTARTS #:CONDITION866 (CAR *RESTART-CLUSTERS*)
    (CERROR "Continue from failure." #:CONDITION866)))
```

With both branches of our code available, we may finally define the function `restart-case-expand-signaling-form` that decides which branch to take based on the first element of the macroexpanded expression.

```lisp
(defun restart-case-expand-signaling-form (expression env)
  (let ((expansion (macroexpand expression env)))
    (case (car expansion)
      ((signal warn error) (restart-case-expand-non-cerror expansion))
      (cerror (restart-case-expand-cerror expansion)))))
```

And we may test it:

```lisp
PCS> (restart-case-expand-signaling-form '(error "Failure") nil)
(LET ((#:CONDITION868 (COERCE-TO-CONDITION "Failure" (LIST) 'SIMPLE-ERROR 'ERROR)))
  (WITH-CONDITION-RESTARTS #:CONDITION868 (CAR *RESTART-CLUSTERS*)
    (ERROR #:CONDITION868)))

PCS> (restart-case-expand-signaling-form '(cerror "Continue from failure." "Failure") nil)
(LET ((#:CONDITION867 (COERCE-TO-CONDITION "Failure" 'NIL 'SIMPLE-ERROR 'CERROR)))
  (WITH-CONDITION-RESTARTS #:CONDITION867 (CAR *RESTART-CLUSTERS*)
    (CERROR "Continue from failure." #:CONDITION867)))
```

(The above expands into a call of `cerror`, which we have not yet defined; we will do that in the next chapters and test it there.)

Finally, let us look at the example expansion of `restart-case` with `error` to see that the `error` expression indeed expands into a `with-condition-restarts`-wrapped form.

```lisp
(restart-case (error "Failure!")
  (return-42 ()
    :test (lambda (condition) (typep condition 'error))
    :report report-return-42
    42))

(LET ((#:RESTART-CASE-VAR940 NIL))
  (DECLARE (IGNORABLE #:RESTART-CASE-VAR940))
  (BLOCK #:RESTART-CASE-BLOCK939
    (TAGBODY
      (RESTART-BIND ((RETURN-42
                      (LAMBDA (&REST #:RESTART-ARGS942)
                        (SETQ #:RESTART-CASE-VAR940 #:RESTART-ARGS942)
                        (GO #:RETURN-42-RESTART-BINDING941))
                      :TEST-FUNCTION #'(LAMBDA (CONDITION) (TYPEP CONDITION 'ERROR))
                      :REPORT-FUNCTION #'REPORT-RETURN-42))
        (RETURN-FROM #:RESTART-CASE-BLOCK939
          (LET ((#:CONDITION943 (COERCE-TO-CONDITION "Failure!" (LIST) 'SIMPLE-ERROR 'ERROR)))
            (WITH-CONDITION-RESTARTS #:CONDITION943 (CAR *RESTART-CLUSTERS*)
              (ERROR #:CONDITION943)))))
     #:RETURN-42-RESTART-BINDING941
      (RETURN-FROM #:RESTART-CASE-BLOCK939
        (APPLY (LAMBDA () 42) #:RESTART-CASE-VAR940)))))
```

We may not yet test this expansion even though we have defined a dummy implementation of `error`: we would need a handler to route the condition over to the established restart. We will test this behaviour in the next chapters, after our handler-binding mechanism is implemented.

This concludes the implementation of a portable and ANSI-conformant `restart-case` - the most complex macro in the Common Lisp condition system.

#### Implementing simple restarts

Thankfully, the only remaining restart-related macro is a very simple one. `with-simple-restarts` expands into a `restart-case` with a `:report` option based on the format control and optional format arguments. If the restart case is invoked, it returns `nil` and `t` as *multiple values*.

```lisp
(defmacro with-simple-restart ((name format-control &rest args) &body forms)
  (let ((stream (gensym "STREAM")))
    `(restart-case ,(if (= 1 (length forms)) (car forms) `(progn ,@forms))
       (,name ()
         :report (lambda (,stream) (format ,stream ,format-control ,@args))
         (values nil t)))))
```

(In most cases, only the first value returned by an expression, named the *primary value*, is taken into account; however, some Common Lisp operators return more than one value, which some other operators, such as `nth-value` or `multiple-value-bind` are able to accept.)

The macroexpansion is readable without much effort; we can expand the `with-simple-restart` form and read the resulting `restart-case` to verify that it does, indeed, work as intended.

```lisp
(with-simple-restart (fail "Fail the computation.")
  (compute-something))

(RESTART-CASE (COMPUTE-SOMETHING)
  (FAIL NIL :REPORT
   (LAMBDA (#:STREAM846) (FORMAT #:STREAM846 "Fail the computation."))
   (VALUES NIL T)))
```

### System-defined restarts

The Common Lisp standard defines five standard restarts that may (or even should) be bound by standard Common Lisp operators. They are `abort`, `continue`, `muffle-warning`, `store-value`, and `use-value`.

Each of these restarts also has its associated restart-invoking function with the same name. We will implement them now, starting with the least complicated ones - `continue`, `store-value`, and `use-value`.

```lisp
(defun continue (&optional condition)
  (let ((restart (find-restart 'continue condition)))
    (when restart (invoke-restart restart))))

(defun store-value (value &optional condition)
  (let ((restart (find-restart 'store-value condition)))
    (when restart (invoke-restart restart value))))

(defun use-value (value &optional condition)
  (let ((restart (find-restart 'use-value condition)))
    (when restart (invoke-restart restart value))))
```

These functions are the least complicated ones because they are not required to signal any kind of errors if the restart they need to invoke is not active; in such a case they simply return `nil`, contrary to `muffle-warning` and `abort`, which require that a `control-error` be signaled in such a case. For our convenience (and for more meaningful reports), we will define a custom subtype of `control-error` and signal it from within `abort` and `muffle-warning`.

```lisp
(defun report-restart-not-found (condition stream)
  (format stream "Restart ~S is not active."
          (restart-not-found-restart-name condition)))

(define-condition restart-not-found (control-error)
  ((restart-name :reader restart-not-found-restart-name :initarg :restart-name))
  (:report report-restart-not-found))
```

Thanks to these, we may now implement `abort` and `muffle-warning`, which - again - are similar to one another.

```lisp
(defun abort (&optional condition)
  (let ((restart (find-restart 'abort condition)))
    (if restart
        (invoke-restart restart)
        (error 'restart-not-found :restart-name 'abort))))

(defun muffle-warning (&optional condition)
  (let ((restart (find-restart 'muffle-warning condition)))
    (if restart
        (invoke-restart restart)
        (error 'restart-not-found :restart-name 'muffle-warning))))
```

(Some Common Lisp implementations put additional error calls at the ends of `abort` and `muffle-warning` to implement the part of the specification that states that these two functions may *never* return; this might happen e.g. if the respective `abort` or `muffle-warning` restart fails to transfer condition outside of the restart-invoking function. For simplicity, we leave this functionality out of this book.)

We may additionally factor all the common code from the above restart-invoking functions into a single function, `maybe-invoke-restart`, which can then be called by all standard restart-invoking functions. This common function will accept the restart name, a condition object, and an optional parameter that states whether an error should be signaled if the restart is not found.

```lisp
(defun maybe-invoke-restart (restart-name &optional condition errorp &rest arguments)
  (let ((restart (find-restart restart-name condition)))
    (cond (restart (apply #'invoke-restart restart arguments))
          (errorp (error 'restart-not-found :restart-name restart-name)))))

(defun abort (&optional condition)
  (maybe-invoke-restart 'abort condition t))

(defun muffle-warning (&optional condition)
  (maybe-invoke-restart 'muffle-warning condition t))

(defun continue (&optional condition)
  (maybe-invoke-restart 'continue condition))

(defun store-value (value &optional condition)
  (maybe-invoke-restart 'store-value condition nil value))

(defun use-value (value &optional condition)
  (maybe-invoke-restart 'use-value condition nil value))
```

Simple REPL tests of the negative cases follow:

```lisp
PCS> (catch :error (abort))
;; Error: RESTART-NOT-FOUND
NIL

PCS> (catch :error (muffle-warning))
;; Error: RESTART-NOT-FOUND
NIL
```

### Assertions

We have established enough restart-related macrology to be able to implement Common Lisp assertion operators now. They can be grouped into three pairs: non-correctable case assertions (`ecase` and `etypecase`); correctable case assertions (`ccase` and `ctypecase`); and general assertions (`assert` and `check-type`).

#### Case failures

Implementing case assertions will be easier, if we notice that the errors signaled by these assertions are going to be very similar. They are all type errors that have three variables to them: the value that failed the assertion, the kind of assertion that was failed (which could be defined to be the name of that assertion), and a set of possibilities that was not matched.

Let us therefore abstract this into one common condition type, `case-failure`, along with one function that shall expand into an `error case-failure` for us.

```lisp
(defun report-case-failure (condition stream)
  (format stream "~S fell through ~S expression. Wanted one of ~:S."
          (type-error-datum condition)
          (case-failure-name condition)
          (case-failure-possibilities condition)))

(define-condition case-failure (type-error)
  ((name :reader case-failure-name :initarg :name)
   (possibilities :reader case-failure-possibilities :initarg :possibilities))
  (:report report-case-failure))

(defun case-failure (datum complex-type operator-name keys)
  (error 'case-failure :datum datum
                       :expected-type `(,complex-type ,@keys)
                       :name operator-name
                       :possibilities keys))
```

Since `case-failure` is a `type-error`, we should provide it with values for their `:datum` and `:expected-type` keyword arguments. Even though our `report-case-failure` function will not directly use it, sigaling a `case-failure` will nonetheless activate all handlers that are looking for a `type-error`. For this, we use the `complex-type` argument, describing the complex type specifier that should be used when constructuring the type for `:expected-type`. For `case` variants, it will be the symbol `member`; for `typecase` - `or`.

#### Case utilities

Before we begin implementing our case assertions, we need to decide on how we would like to implement them. We may implement them fully from scratch, expanding them into a `cond` with series of `eql` checks (for `ecase` and `ccase`) or a `cond` with a series of `typep` checks (for `etypecase` and `ctypecase`). However, we may also piggyback on our implementations' existing `case` and `typecase` operators, adapting them for our purpose. For simplicity, we will use the latter option.

There is an important difference between `case` and `ecase`/`ccase`, however, that we must account for while implementing the latter two for `case`. The pair of symbols `t` and `otherwise` have special meaning in `case`, for they allow fallthrough and catch all cases that do not match any earlier case. This does not occur in `ecase` or `ccase`, however; these symbols are normal keys there that are expected to be matched by `eql`.

Therefore, in order to implement `ecase` and `ccase`, we need a means of translating the `t`/`otherwise` cases from `ecase`/`ccase` into `(t)`/`(otherwise)` cases for `case`. Wrapping these two symbols in a list is, thankfully, enough to disable their special meaning inside `case` - therefore, our first utility function for case assertions is now ready to be implemented.

```lisp
(defun case-transform-t-otherwise-cases (cases)
  (loop for (key . forms) in cases
        if (member key '(t otherwise)) collect `((,key) ,@forms)
        else collect `(,key ,@forms)))
```

We can immediately test it in the REPL to see if it gives us the expected results.

```lisp
PCS> (case-transform-t-otherwise-cases
      '((24 :foo)
        (:forty-two :bar)
        ((nil) :baz)
        (t :fallthrough)))
((24 :FOO)
 (:FORTY-TWO :BAR)
 ((NIL) :BAZ)
 ((T) :FALLTHROUGH))

PCS> (case-transform-t-otherwise-cases
      '((24 :foo)
        (:forty-two :bar)
        ((nil) :baz)
        (otherwise :fallthrough)))
((24 :FOO)
 (:FORTY-TWO :BAR)
 ((NIL) :BAZ)
 ((OTHERWISE) :FALLTHROUGH))
```

That works well. Another utility that we will need is a way to collect all keys from a list of `ecase`/`ccase` cases, so we have something that we can pass to our `case-failure` function.

```lisp
(defun case-accumulate-keys (cases)
  (loop for case in cases
        for key-or-keys = (first case)
        if (listp key-or-keys) append key-or-keys
        else collect key-or-keys))
```

This function iterates through the cases, checking their first element. When it's a list, its contents are appended to the collection; otherwise, the singular key is collected. We can verify that this approach works:

```lisp
PCS> (case-accumulate-keys
      '(((24 25 26) :foo)
        (:forty-two :bar)
        ((nil) :baz)
        (t :fallthrough)))
(24 25 26 :FORTY-TWO NIL T)
```

It will be useful for `ecase`/`ccase`, where keys are allowed to come in lists or as atoms; we will not use it for `etypecase`/`ctypecase`, since type specifiers are allowed to be lists.

#### Non-correctable case assertions

With the above helper functions, we are now ready to implement `ecase` and `etypecase`. Let us start with the first one. `ecase` accepts a keyform and a list of cases; we will use that to implement `ecase` via `case`, paying attention to transform the cases to get rid of the special meaning of `t`/`otherwise` keys.

```lisp
(defun expand-ecase (keyform cases)
  (let ((keys (case-accumulate-keys cases))
        (variable (gensym "ECASE-VARIABLE")))
    `(let ((,variable ,keyform))
       (case ,variable ,@(case-transform-t-otherwise-cases cases)
         (t (case-failure ,variable 'member 'ecase ',keys))))))

(defmacro ecase (keyform &rest cases)
  (expand-ecase keyform cases))
```

We can see that we add our own `t` case to the body of `case` in order to always signal an error in case of matching failure. In addition, we see that the keyform is bound to a temporary variable that we create around the form; this is to prevent multiple evaluation when we need to pass the value to the `case-failure` form.

We can test the the expansion of our macro...

```lisp
PCS> (expand-ecase 'nil
                   '(((24 25 26) :foo)
                     (:forty-two :bar)
                     ((nil) :baz)
                     (t :no-fallthrough)))
(LET ((#:ECASE-VARIABLE970 NIL))
  (CASE NIL
    ((24 25 26) :FOO)
    (:FORTY-TWO :BAR)
    ((NIL) :BAZ)
    ((T) :NO-FALLTHROUGH)
    (T (CASE-FAILURE #:ECASE-VARIABLE970 'MEMBER 'ECASE '(24 25 26 :FORTY-TWO NIL T)))))
```

...and verify that it indeed works as expected:

```lisp
PCS> (ecase nil
       ((24 25 26) :foo)
       (:forty-two :bar)
       ((nil) :baz)
       (t :no-fallthrough))
:BAZ

PCS> (catch :error
       (ecase :twenty-four
         ((24 25 26) :foo)
         (:forty-two :bar)
         ((nil) :baz)
         (t :no-fallthrough)))
;; Error: CASE-FAILURE
NIL
```

The implementation of `etypecase` is going to be similar, except that we will use `mapcar #'first` instead of `case-accumulate-keys`, we do not need to do anything about `t`, and `otherwise` is a valid type specifier and therefore we do not need to care about it at all.

```lisp
(defun expand-etypecase (keyform cases)
  (let ((keys (mapcar #'first cases))
        (variable (gensym "ETYPECASE-VARIABLE")))
    `(let ((,variable ,keyform))
       (typecase ,keyform ,@cases
         (t (case-failure ,variable 'or 'etypecase ',keys))))))

(defmacro etypecase (keyform &rest cases)
  (expand-etypecase keyform cases))
```

We can test the expansion of `expand-etypecase` in the REPL and then check the macro itself.

```lisp
PCS> (expand-etypecase '"forty-two"
                       '(((or integer rational float) :foo)
                         (keyword :bar)
                         (string :baz)))
(LET ((#:ETYPECASE-VARIABLE978 "forty-two"))
  (TYPECASE "forty-two"
    ((OR INTEGER RATIONAL FLOAT) :FOO)
    (KEYWORD :BAR)
    (STRING :BAZ)
    (T (ERROR 'CASE-FAILURE :DATUM #:ETYPECASE-VARIABLE978
                            :EXPECTED-TYPE '(OR (OR INTEGER RATIONAL FLOAT) KEYWORD STRING)
                            :NAME 'ETYPECASE
                            :POSSIBILITIES '((OR INTEGER RATIONAL FLOAT) KEYWORD STRING)))))

PCS> (etypecase "forty-two"
       ((or integer rational float) :foo)
       (keyword :bar)
       (string :baz))
:BAZ

PCS> (catch :error
       (etypecase 'forty-two
         ((or integer rational float) :foo)
         (keyword :bar)
         (string :baz)))
;; Error: CASE-FAILURE
NIL
```

#### Correctable case assertions

With the first pair of correctable assertions implemented, we may move on to the second one. The correctable case assertions works similar to the non-correctable ones, except they additionally bind a `store-value` restart that allows the programmer to retry the whole assertion with the keyform place set to a new value. This, in turn, can be implemented with a macro that accepts a place to set, a tag to transfer control to, and a set of body forms to execute in an environment where the `store-value` restart is bound.

This abstraction is worth to separate into a separate macro, given that it will be identical in `ccase` and `ctypecase`.

```lisp
(defun store-value-read-evaluated-form ()
  (format *query-io* "~&;; Type a form to be evaluated:~%")
  (list (eval (read *query-io*))))

(defun expand-with-store-value-restart (temp-var place tag forms)
  (let ((report-var (gensym "STORE-VALUE-REPORT"))
        (new-value-var (gensym "NEW-VALUE"))
        (form-or-forms (if (= 1 (length forms)) (first forms) `(progn ,@forms))))
    `(flet ((,report-var (stream)
              (format stream "Supply a new value of ~S." ',place)))
       (restart-case ,form-or-forms
         (store-value (,new-value-var)
           :report ,report-var
           :interactive store-value-read-evaluated-form
           (setf ,temp-var ,new-value-var
                 ,place ,new-value-var)
           (go ,tag))))))

(defmacro with-store-value-restart ((temp-var place tag) &body forms)
  (expand-with-store-value-restart temp-var place tag forms))
```

The macro expands into a `restart-case` form, with a local function defined for reporting the restart.  The function `store-value-read-evaluated-form` is used to query the user for a form to set the place to; it is the interactive function of the `store-value` restart established within `with-store-value-restart`.

(The `(if (= 1 (length forms)) ...)` check is used to ensure that a singular standard signaling form (`signal`, `error`, etc..) will be spliced without a `progn`, so `restart-case` may properly perform its implicit condition-restart association. We can see a similar check earlier, in the relevant functions for `restart-case`.)

The established `store-value` restart case utilizes all three arguments that it is given: the temporary variable and the place passed to it are set with the provided value, and control is then transferred to the appropriate go tag.

Let us see the expansion of that form on sample data.

```lisp
PCS> (expand-with-store-value-restart 'temp-var 'place 'tag '((frobnicate)))
(FLET ((#:STORE-VALUE-REPORT980 (STREAM) (FORMAT STREAM "Supply a new value of ~S." 'PLACE)))
  (RESTART-CASE (FROBNICATE)
    (STORE-VALUE (#:NEW-VALUE981)
      :REPORT #:STORE-VALUE-REPORT980
      :INTERACTIVE STORE-VALUE-READ-EVALUATED-FORM
      (SETF TEMP-VAR #:NEW-VALUE981
            PLACE #:NEW-VALUE981)
      (GO TAG))))
```

We can use this macro to implement our correctable case assertions. Let us start with `ccase`.

```lisp
(defun expand-ccase (keyform cases)
  (let ((keys (case-accumulate-keys cases))
        (variable (gensym "CCASE-VARIABLE"))
        (block-name (gensym "CCASE-BLOCK"))
        (tag (gensym "CCASE-TAG")))
    `(block ,block-name
       (let ((,variable ,keyform))
         (tagbody ,tag
            (return-from ,block-name
              (case ,variable ,@(case-transform-t-otherwise-cases cases)
                    (t (with-store-value-restart (,variable ,keyform ,tag)
                         (case-failure ,variable 'member 'ccase ',keys))))))))))

(defmacro ccase (keyform &rest cases)
  (expand-ccase keyform cases))
```

We can see that the structure of special forms is more complex this time. We establish a `block` to return our value from, a lexical variable that will hold the value of the keyform, and a `tagbody` with a single tag in the beginning. Immediately inside, we attempt to return a value via `return-from`; this returning is meant to succeed if the case succeeds, and otherwise, control is transferred back to the tagbody tag, and `return-from` is entered again.

(The lexical variable is bound before `tagbody` and its name is passed to `with-store-value-restart` in order to fully avoid evaluating the keyform on each entry into `tagbody`.)

Let us attempt to expand `ccase` for place `x` and a single case in which the value `42` returns `:foo`.

```lisp
PCS> (expand-ccase 'x '((42 :foo)))
(BLOCK #:CCASE-BLOCK1025
  (LET ((#:CCASE-VARIABLE1024 X))
    (TAGBODY
     #:CCASE-TAG1026
      (RETURN-FROM #:CCASE-BLOCK1025
        (CASE #:CCASE-VARIABLE1024
          (42 :FOO)
          (T (WITH-STORE-VALUE-RESTART (#:CCASE-VARIABLE1024 X #:CCASE-TAG1026)
               (CASE-FAILURE #:CCASE-VARIABLE1024 'MEMBER 'CCASE '(42)))))))))
```

Let us test the positive and negative cases of our macro.

```lisp
PCS> (let ((x 42))
       (ccase x (42 :foo)))
:FOO

PCS> (catch :error
       (let ((x 24))
         (ccase x (42 :foo))))
;; Error: 24 fell through CCASE expression. Wanted one of (42).
NIL
```

Our `typecase` implementation is going to be very similar in structure.

```lisp
(defun expand-ctypecase (keyform cases)
  (let ((keys (case-accumulate-keys cases))
        (variable (gensym "CTYPECASE-VARIABLE"))
        (block-name (gensym "CTYPECASE-BLOCK"))
        (tag (gensym "CTYPECASE-TAG")))
    `(block ,block-name
       (let ((,variable ,keyform))
         (tagbody ,tag
            (return-from ,block-name
              (typecase ,keyform ,@cases
                        (t (with-store-value-restart (,variable ,keyform ,tag)
                             (case-failure ,variable 'or 'ctypecase ',keys))))))))))

(defmacro ctypecase (keyform &rest cases)
  (expand-ctypecase keyform cases))
```

We can similarly verify its expansion and positive result in the REPL.

```lisp
PCS> (expand-ctypecase 'x '((integer :foo)))
(BLOCK #:CTYPECASE-BLOCK1031
  (LET ((#:CTYPECASE-VARIABLE1030 X))
    (TAGBODY
     #:CTYPECASE-TAG1032
      (RETURN-FROM #:CTYPECASE-BLOCK1031
        (TYPECASE X
          (INTEGER :FOO)
          (T (WITH-STORE-VALUE-RESTART (#:CTYPECASE-VARIABLE1030 X #:CTYPECASE-TAG1032)
               (CASE-FAILURE #:CTYPECASE-VARIABLE1030 'OR 'CTYPECASE '(INTEGER)))))))))

PCS> (let ((x 42))
       (ctypecase x (integer :foo)))
:FOO

PCS> (catch :error
       (let ((x 42))
         (ctypecase x (keyword :foo))))
;; Error: 42 fell through CTYPECASE expression. Wanted one of (KEYWORD).
NIL
```

#### General assertions

With the case assertions ready, we can focus on the remaining two assertions: `assert` itself and `check-type`. The former is the most general Common Lisp assertion: not only it allows arbitrary forms to be evaluated, it is the only Common Lisp assertion capable of allowing the programmer to interactively set multiple places at once. The two support functions that we will write for `assert` will be used exactly for that purpose: one of them will report the `continue` report bound by `assert`, and the other will be responsible for querying the user if they would like to supply a new value for a single place.

```lisp
(defun assert-restart-report (names stream)
  (format stream "Retry assertion")
  (if names
      (format stream " with new value~P for ~{~S~^, ~}." (length names) names)
      (format stream ".")))

(defun assert-prompt (place-name value)
  (cond ((y-or-n-p "~&;; The old value of ~S is ~S.~%~
                    ;; Do you want to supply a new value?"
                   place-name value)
         (format *query-io* "~&;; Type a form to be evaluated:~%")
         (flet ((read-it ()
                  (format *query-io* "> ")
                  (eval (read *query-io*))))
           (cond ((symbolp place-name)
                  (format *query-io*
                          "~&;; (The old value is bound to the symbol ~S.)~%"
                          place-name)
                  (progv (list place-name) (list value) (read-it)))
                 (t (read-it)))))
        (t value)))
```

We will focus on the second function for a moment, since it is non-trivially complex. It contains a conditional expression that first calls a function `y-or-n-p`. That function prints the prompt to `*query-io*` and waits for the user to answer either with a `y` or a `n`. In case of `n`, the function returns false, and `value` is returned; in case of `y`, the function returns true, and the conditional branch is entered.

Once inside, the function checks whether `place-name` is a symbol; if that is true, that symbol is dynamically bound *at runtime* using the special operator `progv`. Then, an expression is read and evaluated at runtime to provide the value to be returned from `assert-prompt`. In case the place is not a symbol, the `progv` binding is skipped, but an expression is still read and evaluated.

It is best to see this function in action in order to understand how it works.

```lisp
PCS> (assert-prompt 'x 42)
;; The old value of X is 42.
;; Do you want to supply a new value? (y or n) y               ; user input here
;; Type a form to be evaluated:
;; (The old value is bound to the symbol X.)
> (* x x)                                                      ; user input here
1764

PCS> (assert-prompt '(car some-cons) 42)
;; The old value of (CAR SOME-CONS) is 42.
;; Do you want to supply a new value? (y or n) y               ; user input here
;; Type a form to be evaluated:
> 84                                                           ; user input here
84

PCS> (assert-prompt 'x 42)
;; The old value of X is 42.
;; Do you want to supply a new value? (y or n) n               ; user input here
42
```

We can see that, in the first case, we can use the symbol `x` inside the query made by `assert-prompt`, even though `x` was never bound as a global dynamic variable. This is allowed by the fact that the `eval` call is executed inside `progv`, and therefore a new dynamic binding for `x` is established. This means that, even if `x` was originally a lexical variable, it is re-created as a dynamic variable that we can use in the form read and evaluated by `assert`.

In addition, we can see that `assert-prompt` always returns a value - either the new one, or the old one. It means that we can use the return value of that function as an argument to `setf` in order to assign values to the place.

We may now work on our implementation of `assert`. It will be somewhat similar to the earlier assertions: we will need a `tagbody` where we will transfer control to in order to retry the assertion, and a place-setting `continue` restart will be established. The main differences are that the only value it returns is `nil`, which will save us from establishing a `block`; in addition, `assert` can set multiple places, which we will need to account for.

```lisp
(defmacro assert (test-form &optional places datum &rest arguments)
  (flet ((make-place-setter (place) `(setf ,place (assert-prompt ',place ,place))))
    (let ((tag (gensym "ASSERT-TAG")))
      `(tagbody ,tag
          (unless ,test-form
            (restart-case ,(if datum
                               `(error ,datum ,@arguments)
                               `(error "The assertion ~S failed." ',test-form))
              (continue ()
                :report (lambda (stream) (assert-restart-report ',places stream))
                ,@(mapcar #'make-place-setter places)
                (go ,tag))))))))
```

The main structure of our assertion is very lightweight: we have a `tagbody` wrapping an `unless`. This means that, first, the test form is evaluated, and if it returns true, then the rest of the form is not entered. Only when an assertion fails, and `test-form` returns true, we get to see the rest of `assert`'s functionality.

Immediately after establishing a `continue` restart, an error is signaled, based on the `datum` and `arguments` that are passed to `assert`. A `continue` restart is available, with its report calling our helper function `assert-restart-report`.

We can see that setting multiple places is taken care of by the local function, `make-place-setter`; it turns the list of places into a list of `setf` forms. The value for each such `setf` form is returned by the interactive function `assert-prompt` which we have defined earlier. Immediately after setting the places, control is transferred back to the `unless` test, retrying the assertion with possibly new values given to our places (if any).

```lisp
PCS> (assert t)
NIL

PCS> (catch :error
       (assert nil))
;; Error: The assertion NIL failed.
NIL
```

(More intricate tests of the full `assert` will come when we are capable of handling the signaled error, for which we need to implement handlers.)

The sixth - and final - assertion macro, `check-type`, has a somewhat similar, if much simpler, structure. We have a `tagbody` over an `unless typep` check; if the check fails, a `store-value` restart is established, and an error is immediately signaled. The actual condition is created in a separate helper function, extracted from the main body of the macro.

```lisp
(defun check-type-error (place value type type-string)
  (error
   'simple-type-error
   :datum value
   :expected-type type
   :format-control (if type-string
                       "The value of ~S is ~S, which is not ~A."
                       "The value of ~S is ~S, which is not of type ~S.")
   :format-arguments (list place value (or type-string type))))

(defun expand-check-type (place type type-string)
  (let ((variable (gensym "CHECK-TYPE-VARIABLE"))
        (tag (gensym "CHECK-TYPE-TAG")))
    `(let ((,variable ,place))
       (tagbody ,tag
          (unless (typep ,variable ',type)
            (with-store-value-restart (,variable ,place ,tag)
              (check-type-error ',place ,variable ',type ,type-string)))))))

(defmacro check-type (place type &optional type-string)
  (expand-check-type place type type-string))
```

By now, the author hopes that the reader has gained enough confidence in reading macros for them to be able to read this one themselves; we shall simply expand it in the REPL and verify that the positive case works well.

```lisp
PCS> (expand-check-type 'x 'integer nil)
(LET ((#:CHECK-TYPE-VARIABLE1034 X))
  (TAGBODY #:CHECK-TYPE-TAG1035
    (UNLESS (TYPEP #:CHECK-TYPE-VARIABLE1034 'INTEGER)
      (WITH-STORE-VALUE-RESTART (#:CHECK-TYPE-VARIABLE1034 X #:CHECK-TYPE-TAG1035)
        (CHECK-TYPE-ERROR 'X #:CHECK-TYPE-VARIABLE1034 'INTEGER NIL)))))

PCS> (let ((x 42))
       (check-type x integer))
NIL

PCS> (catch :error
       (let ((x 42))
         (check-type x keyword)))
;; Error: The value of X is 42, which is not of type KEYWORD.
NIL
```

As mentioned earlier, testing the restarts established by the correctable and general assertions will be performed when we implement handlers, and will therefore be able to handle the `error`s signaled by these operators by invoking a proper restart.

### Signaling

The first such function, `signal`, requires us to build some infrastructure for two future parts of the condition system. In order to be able to process all currently active handlers, we need to declare a variable to hold these handlers; in addition, `signal` is allowed to call `break` when the variable `*break-on-signals*` is non-`nil`. These two variables need to be defined.

```lisp
(defvar *break-on-signals* nil)

(defvar *handler-clusters* '())
```

We are now allowed to define `signal`. It makes use of `coerce-to-condition` to ensure that it is dealing with a condition object; it then checks if `break` should be called before signaling the condition, and finally, it walks the list of handler clusters and invokes any matching handlers found there in order.

```lisp
(defun signal (datum &rest arguments)
  (let ((condition (coerce-to-condition datum arguments 'simple-condition 'signal)))
    (if (typep condition *break-on-signals*)
        (break "~A~%Break entered because of *BREAK-ON-SIGNALS*." condition))
    (loop for (cluster . remaining-clusters) on *handler-clusters*
          do (let ((*handler-clusters* remaining-clusters))
               (dolist (handler cluster)
                 (when (typep condition (car handler))
                   (funcall (cdr handler) condition)))))))
```

One interesting thing we can see here is the fact that `*handler-clusters*` is being rebound as the loop inside `signal` progresses. This is required to implement the clustering mechanism required by handlers: a handler invoked from a given cluster may only see handlers that are "older" than it, and this behaviour is implemented by dynamically binding `*handler-clusters*` to the list of all handler clusters "older" than the cluster that the currently invoked handler belongs to.

Even though we do not have defined any standard means of binding handlers, we may nonetheless bind `*handler-clusters*` manually in order to test our `signal` implementation. We will perform this now.

```lisp
PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (format t ";; Also got a ~S.~%" condition))
            (handler-3 (condition) (format t ";; I too got a ~S.~%" condition)))
       (let ((*handler-clusters* `(((condition . ,#'handler-1)
                                    (error . ,#'handler-2))
                                   ((condition . ,#'handler-3)))))
         (signal (make-condition 'condition))))
;; Got a #<CONDITION {100E885C63}>.
;; I too got a #<CONDITION {100E885C63}>.
NIL

PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (format t ";; Also got a ~S.~%" condition))
            (handler-3 (condition) (format t ";; I too got a ~S.~%" condition)))
       (let ((*handler-clusters* `(((condition . ,#'handler-1)
                                    (error . ,#'handler-2))
                                   ((condition . ,#'handler-3)))))
         (signal (make-condition 'error))))
;; Got a #<ERROR {100E9455D3}>.
;; Also got a #<ERROR {100E9455D3}>.
;; I too got a #<ERROR {100E9455D3}>.
NIL
```

We can now define `warn`, which will call `signal` as a part of its operation. It utilizes `check-type` to ensure that the condition passed to the function is a warning condition, as mandated by the standard: then, it establishes its `muffle-warning` restart, signals the condition, and reports the condition if the signaling did not cause a transfer to control.

```lisp
(defun warn (datum &rest arguments)
  (let ((condition (coerce-to-condition datum arguments 'simple-warning 'warn)))
    (check-type condition warning)
    (with-simple-restart (muffle-warning "Muffle the warning.")
      (signal condition)
      (format *error-output* "~&;; Warning: ~A~%" condition))
    nil))
```

(The `nil` at the end is required by the standard, since `warn` must return `nil` as its only value if it returns normally; `with-simple-restart` will return an additional `t` as the secondary value if its restart was invoked.)

Let us now test the three basic cases: where the warning is not muffled, where `muffle-warning` is called, and when control is transferred outside the handler function (as if in `handler-case`).

```lisp
PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (format t ";; Also got a ~S.~%" condition)))
       (let ((*handler-clusters* `(((condition . ,#'handler-1))
                                   ((warning . ,#'handler-2)))))
         (warn (make-condition 'warning))))
;; Got a #<WARNING {100F669063}>.
;; Also got a #<WARNING {100F669063}>.
;; Warning: Condition WARNING was signaled.
NIL

PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (declare (ignore condition)) (muffle-warning)))
       (let ((*handler-clusters* `(((condition . ,#'handler-1))
                                   ((warning . ,#'handler-2)))))
         (warn (make-condition 'warning))))
;; Got a #<WARNING {100F78B153}>.
NIL

PCS> (block nil
       (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
              (handler-2 (condition) (declare (ignore condition)) (return 42)))
         (let ((*handler-clusters* `(((condition . ,#'handler-1))
                                     ((warning . ,#'handler-2)))))
           (warn (make-condition 'warning)))))
;; Got a #<WARNING {100F5CDB63}>.
42
```

Finally, we may implement `error` (which redefines the earlier stub definition of it) and `cerror`. `error` signals the condition object before entering the debugger, and `cerror` calls `error` with an established simple restart named `continue`.

```lisp
(defun error (datum &rest arguments)
  (let ((condition (coerce-to-condition datum arguments 'simple-error 'error)))
    (signal condition)
    (invoke-debugger condition)))

(defun cerror (continue-string datum &rest arguments)
  (with-simple-restart (continue "~A" (apply #'format nil continue-string arguments))
    (apply #'error datum arguments))
  nil)
```

Once again, we run into bootstrapping issues: we have not yet defined the debugger, so we are unable to invoke it. We will work around the issue in a similar way: we shall stub the debugger out and have it transfer control via `throw`.

```lisp
(defun invoke-debugger (condition)
  (format *error-output* ";; Debugger entered: ~A~%" condition)
  (throw :error nil))
```

A proper implementation of the debugger will come in the next chapters. Until then, we can test it with a `catch` tag and by manually binding condition handlers:

```lisp
PCS> (catch :error
       (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
              (handler-2 (condition) (format t ";; Also got a ~S.~%" condition))
              (handler-3 (condition) (format t ";; I too got a ~S.~%" condition)))
         (let ((*handler-clusters* `(((condition . ,#'handler-1)
                                      (error . ,#'handler-2)
                                      (condition . ,#'handler-3)))))
           (error (make-condition 'error)))))
;; Got a #<ERROR {10100CE1F3}>.
;; Also got a #<ERROR {10100CE1F3}>.
;; I too got a #<ERROR {10100CE1F3}>.
;; Debugger entered: Condition ERROR was signaled.
NIL

PCS> (block nil
       (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
              (handler-2 (condition) (declare (ignore condition)) (return)))
         (let ((*handler-clusters* `(((condition . ,#'handler-1))
                                     ((error . ,#'handler-2)))))
           (error (make-condition 'error)))))
;; Got a #<ERROR {1012A78233}>.
NIL
```

Now that we can manually bind handlers, we may also begin testing our restarts - starting with the `continue` restart established by `cerror` and then performing a tiny test of `restart-bind`.

```lisp
PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (continue condition)))
       (let ((*handler-clusters* `(((condition . ,#'handler-1))
                                   ((error . ,#'handler-2)))))
         (cerror "Continue." (make-condition 'error))
         42))
;; Got a #<ERROR {1012DAD8B3}>.
42

PCS> (restart-bind ((foo (lambda () (format t ";; Restart FOO was invoked.~%"))))
       (flet ((handler-1 (condition) (declare (ignore condition)) (invoke-restart 'foo)))
         (let ((*handler-clusters* `(((condition . ,#'handler-1)))))
           (signal (make-condition 'condition)))))
;; Restart FOO was invoked.
NIL
```

### Handlers

Our variable for handler clusters has been defined while implementing `signal`, and our testing with manually binding `*handler-clusters*` showed that our signaling infrastructure is working well. We can therefore immediately proceed with writing the `handler-bind` macro, which is - maybe unsurprisingly - similar to our previous implementation of `restart-bind`.

#### Binding handlers

```lisp
(defun handler-bind-make-binding (binding)
  (destructuring-bind (condition-type function) binding
    `(cons ',condition-type ,function)))

(defun expand-handler-bind (bindings forms)
  (let ((cluster (mapcar #'handler-bind-make-binding bindings)))
    `(let ((*handler-clusters* (cons (list ,@cluster) *handler-clusters*)))
       ,@forms)))

(defmacro handler-bind (bindings &body forms)
  (expand-handler-bind bindings forms))
```

Other than the function and macro names, the only distinct things are the form of a handler binding (which must have exactly two elements: the condition type and the handler function) and the fact that handlers themselves are not structures, but conses.

We can immediately put this macro to use by rewriting some of the signaling test cases from above to use `handler-bind` - and to test the restarts of `restart-case`, as we promised in the earlier chapters.

```lisp
PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (format t ";; Also got a ~S.~%" condition))
            (handler-3 (condition) (format t ";; I too got a ~S.~%" condition)))
       (handler-bind ((condition #'handler-3))
         (handler-bind ((condition #'handler-1)
                        (error #'handler-2))
           (signal (make-condition 'error)))))
;; Got a #<ERROR {1013AFF0C3}>.
;; Also got a #<ERROR {1013AFF0C3}>.
;; I too got a #<ERROR {1013AFF0C3}>.
NIL

PCS> (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
            (handler-2 (condition) (declare (ignore condition)) (muffle-warning)))
       (handler-bind ((condition #'handler-1)
                      (warning #'handler-2))
         (warn (make-condition 'warning))))
;; Got a #<WARNING {1013BB1E23}>.
NIL

PCS> (block nil
       (flet ((handler-1 (condition) (format t ";; Got a ~S.~%" condition))
              (handler-2 (condition) (declare (ignore condition)) (return)))
         (handler-bind ((condition #'handler-1)
                        (error #'handler-2))
           (error (make-condition 'error)))))
;; Got a #<ERROR {1013E3C563}>.
NIL

PCS> (handler-bind ((error (lambda (condition)
                             (let ((restart (find-restart 'return-42 condition)))
                               (invoke-restart restart)))))
       (restart-case (error "Failure!")
         (return-42 ()
           :test (lambda (condition) (typep condition 'error))
           :report report-return-42
           42)))
42
```

#### Handler cases

With a working `handler-bind`, we can begin working on getting a working `handler-case`. This operator is, thankfully, much simpler than `restart-case`, but it nonetheless introduces one functionality that is completely new: the `:no-error` case, which is called in case the expression passed to `handler-case` returns normally. Our `handler-case` implementation will therefore be split into two parts: one, hich implements the normal, non-`:no-error` behaviour, and another, which handles the `:no-error` case.

The `:no-error` case is only run if the expression from `handler-case` returns normally, and therefore no handlers are triggered. Therefore, it is possible to separate the `:no-error` case and express it via additional code that calls the "standard" `handler-case` which does not contain a `:no-error` case.

```lisp
(defun expand-handler-case-with-no-case-case (form cases)
  (let* ((no-case-case (assoc :no-error cases))
         (other-cases (remove no-case-case cases))
         (normal-return (gensym "NORMAL-RETURN"))
         (case-return  (gensym "CASE-RETURN")))
    `(block ,case-return
       (multiple-value-call (lambda ,@(cdr no-case-case))
         (block ,normal-return
           (return-from ,case-return
             (handler-case (return-from ,normal-return ,form)
               ,@other-cases)))))))
```

First of all, we separate the `:no-error` case from all other cases, and we generate a pair of block names for the two blocks that we establish. The two blocks are nested and intersect with each other: the `form` that was originally passed to `handler-case` is wrapped in a `return-from` that triggers a return from the `normal-return` block, and the values of that form are then passed as arguments to the function created from the `:no-error` case. The recursive `handler-case` call is wrapped in a `return-from` that triggers a return to the outermost `case-return`; this utilizes the fact that the full `handler-case` is now going to return a value if and only if any of the handler cases have been triggered and control has been routed out of the original expression.

```lisp
PCS> (expand-handler-case-with-no-error-case
      '(+ 2 2)
      '((error () (format t ";; Oops~%"))
        (:no-error (x) (format t ";; Got ~A~%" x))))
(BLOCK #:CASE-RETURN1082
  (MULTIPLE-VALUE-CALL (LAMBDA (X) (FORMAT T ";; Got ~A~%" X))
    (BLOCK #:NORMAL-RETURN1081
      (RETURN-FROM #:CASE-RETURN1082
        (HANDLER-CASE (RETURN-FROM #:NORMAL-RETURN1081 (+ 2 2))
         (ERROR NIL (FORMAT T ";; Oops~%")))))))
```

This function will be called if and only if a `:no-error` case is present among the cases passed to `handler-case`, which means that now we may focus on implementing the "standard" situation - where all the cases have condition types for which control shall be transferred and a case should be executed.

The internal structure for our `handler-case` macro is going to be very similar to the first iteration of `restart-case` that we have made. We will want to annotate our cases with a unique tag; we will want to generate handler bindings and handler cases; finally, we will want the main body of our macroexpansion with a `block` for returning values from, a temporary variable for passing arguments around, and a `tagbody` that will allow transferring control to the handler cases.

```lisp
(defun handler-case-parse-case (case)
  (destructuring-bind (type lambda-list . forms) case
    (let ((tag (gensym "HANDLER-TAG")))
      (list tag type lambda-list forms))))

(defun handler-case-make-handler-binding (case temp-var)
  (destructuring-bind (tag type lambda-list forms) case
    (declare (ignore forms))
    (let ((condition (gensym "CONDITION")))
      `(,type (lambda (,condition)
                (declare (ignorable ,condition))
                ,@(when lambda-list `((setf ,temp-var ,condition)))
                (go ,tag))))))

(defun handler-case-make-handler-case (case temp-var block-name)
  (destructuring-bind (tag type lambda-list body) case
    (declare (ignore type))
    `(,tag (return-from ,block-name
             ,(if lambda-list
                  `(let ((,(first lambda-list) ,temp-var)) ,@body)
                  `(locally ,@body))))))
```

One thing that is different in the functions that generate bindings and cases is the fact that the lambda list is much simpler in `handler-case`; the only permitted values are an empty list (if we do not want to use the signaled condition) and a list containing one symbol (if we want to use it). In the former case, no binding for the condition object is made, and in the latter, a `let` is introduced to bind the variable. Because of this simpler structure, the handler case expands into a `locally` or `let` instead of into a `lambda` form.

Let us briefly test these functions to ensure that they return values that we would expect them to return.

```lisp
PCS> (handler-case-parse-case '(error (c) (format t ";; Error: ~A" c)))
(#:HANDLER-TAG1143 ERROR (C) ((FORMAT T ";; Error: ~A" C)))

PCS> (let ((case (handler-case-parse-case '(error (c) (format t ";; Error: ~A" c)))))
       (handler-case-make-handler-binding case 'temp-var))
(ERROR (LAMBDA (#:CONDITION1145)
         (DECLARE (IGNORABLE #:CONDITION1145))
         (SETF TEMP-VAR #:CONDITION1145)
         (GO #:HANDLER-TAG1144)))

PCS> (let ((case (handler-case-parse-case '(error (c) (format t ";; Error: ~A" c)))))
       (handler-case-make-handler-case case 'temp-var 'block-name))
(#:HANDLER-TAG1146
 (RETURN-FROM BLOCK-NAME
   (LET ((C TEMP-VAR))
     (FORMAT T ";; Error: ~A" C))))
```

We can now write a function that expands into the full `handler-case` body.

```lisp
(defun expand-handler-case-without-no-error-case (form cases)
  (let ((block-name (gensym "HANDLER-CASE-BLOCK"))
        (temp-var (gensym "HANDLER-CASE-VAR"))
        (data (mapcar #'handler-case-parse-case cases)))
    (flet ((make-handler-binding (case)
             (handler-case-make-handler-binding case temp-var))
           (make-handler-case (case)
             (handler-case-make-handler-case case temp-var block-name)))
      `(let ((,temp-var nil))
         (declare (ignorable ,temp-var))
         (block ,block-name
           (tagbody
              (handler-bind ,(mapcar #'make-handler-binding data)
                (return-from ,block-name ,form))
              ,@(apply #'append (mapcar #'make-handler-case data))))))))
```

The similarities between `expand-handler-case-no-error-case` and `expand-restart-case` should be visible almost immediately; because of this, we will not comment on this expansion function any further, and instead skip straight to testing its expansion in the REPL.

```lisp
PCS> (expand-handler-case-without-no-error-case
      '(signal 'warning)
      '((error (c) (format t ";; Error: ~A~%" c))
        (warning (c) (format t ";; Warning: ~A~%" c))
        (condition () (format t ";; Condition!~%"))))
(LET ((#:HANDLER-CASE-VAR1153 NIL))
  (DECLARE (IGNORABLE #:HANDLER-CASE-VAR1153))
  (BLOCK #:HANDLER-CASE-BLOCK1152
    (TAGBODY
      (HANDLER-BIND ((ERROR (LAMBDA (#:CONDITION1157)
                              (DECLARE (IGNORABLE #:CONDITION1157))
                              (SETF #:HANDLER-CASE-VAR1153 #:CONDITION1157)
                              (GO #:HANDLER-TAG1154)))
                     (WARNING (LAMBDA (#:CONDITION1158)
                                (DECLARE (IGNORABLE #:CONDITION1158))
                                (SETF #:HANDLER-CASE-VAR1153 #:CONDITION1158)
                                (GO #:HANDLER-TAG1155)))
                     (CONDITION (LAMBDA (#:CONDITION1159)
                                  (DECLARE (IGNORABLE #:CONDITION1159))
                                  (GO #:HANDLER-TAG1156))))
        (RETURN-FROM #:HANDLER-CASE-BLOCK1152 (SIGNAL 'WARNING)))
     #:HANDLER-TAG1154
      (RETURN-FROM #:HANDLER-CASE-BLOCK1152
        (LET ((C #:HANDLER-CASE-VAR1153))
          (FORMAT T ";; Error: ~A~%" C)))
     #:HANDLER-TAG1155
      (RETURN-FROM #:HANDLER-CASE-BLOCK1152
        (LET ((C #:HANDLER-CASE-VAR1153))
          (FORMAT T ";; Warning: ~A~%" C)))
     #:HANDLER-TAG1156
      (RETURN-FROM #:HANDLER-CASE-BLOCK1152
        (LOCALLY (FORMAT T ";; Condition!~%"))))))
```

We have a pair of functions that expand `handler-case` in the presence of a `:no-error` case and in its lack; we now need a function that will detect which case to use and choose one expanding function or the other. Since this is going to be the ultimate expanding function of `handler-case`, we may also define our macro at the same time:

```lisp
(defun expand-handler-case (form cases)
  (if (member :no-error cases :key #'car)
      (expand-handler-case-with-no-error-case form cases)
      (expand-handler-case-without-no-error-case form cases)))

(defmacro handler-case (form &rest cases)
  (expand-handler-case form cases))
```

Now that we have these, we may check our macroexpansions one final time, and then run basic tests in our REPL.

```lisp
PCS> (expand-handler-case
      '(signal 'warning)
      '((warning () (format t ";; Warning!~%"))))
(LET ((#:HANDLER-CASE-VAR1104 NIL))
  (DECLARE (IGNORABLE #:HANDLER-CASE-VAR1104))
  (BLOCK #:HANDLER-CASE-BLOCK1103
    (TAGBODY
      (HANDLER-BIND ((WARNING (LAMBDA (#:CONDITION1106)
                                (DECLARE (IGNORABLE #:CONDITION1106))
                                (GO #:HANDLER-TAG1105))))
        (RETURN-FROM #:HANDLER-CASE-BLOCK1103 (SIGNAL 'WARNING)))
     #:HANDLER-TAG1105
      (RETURN-FROM #:HANDLER-CASE-BLOCK1103 (LOCALLY (FORMAT T ";; Warning!~%"))))))

PCS> (expand-handler-case
      '(signal 'condition)
      '((warning () (format t ";; Warning!~%"))
        (:no-error (&rest args) (declare (ignore args)) (format t ";; No error!~%"))))
(BLOCK #:CASE-RETURN1116
  (MULTIPLE-VALUE-CALL
      (LAMBDA (&REST ARGS) (DECLARE (IGNORE ARGS)) (FORMAT T ";; No error!~%"))
    (BLOCK #:NORMAL-RETURN1115
      (RETURN-FROM #:CASE-RETURN1116
        (HANDLER-CASE (RETURN-FROM #:NORMAL-RETURN1115 (SIGNAL 'CONDITION))
          (WARNING NIL (FORMAT T ";; Warning!~%")))))))

PCS> (handler-case (signal 'warning)
       (warning () (format t ";; Warning!~%")))
;; Warning!
NIL

PCS> (handler-case (signal 'condition)
       (warning () (format t ";; Warning!~%"))
       (:no-error (&rest args) (declare (ignore args)) (format t ";; No error!~%")))
;; No error!
NIL
```

Expanding and testing the final handling macro - `ignore-errors` - is left as an exercise for the reader.

```lisp
(defmacro ignore-errors (&rest forms)
  `(handler-case (progn ,@forms)
     (error (condition) (values nil condition))))
```

#### Testing assertions

At the end of this chapter, it is time to fulfill an old promise. In the earlier chapters, we were unable to properly test our restart-binding assertions, since we were missing a vital component used to connect conditions and restarts. That component - handlers - is available now, which means that we are capable of testing all four of these assertions.

```lisp
PCS> (handler-bind ((error (lambda (condition) (store-value 42 condition))))
       (let ((x 24))
         (ccase x (42 :ok))))
:OK

PCS> (handler-bind ((error (lambda (condition) (store-value 42 condition))))
       (let ((x :forty-two))
         (ctypecase x (integer :ok))))
:OK

PCS> (handler-bind ((error (lambda (condition) (store-value 42 condition))))
       (let ((x :forty-two))
         (check-type x integer)))
NIL

PCS> (handler-bind ((error (lambda (condition) (continue condition))))
       (let ((x :forty-two))
         (assert (typep x 'integer) (x))))

;; The old value of X is :FORTY-TWO.
;; Do you want to supply a new value? (y or n) y               ; user input here

;; Type a form to be evaluated:
;; (The old value is bound to the symbol X.)
42                                                             ; user input here
NIL
```

### Debugger

Our condition system is, at this point, more or less complete: we have fully working conditions, handlers, and restarts. The only missing piece that we have stubbed out is the function `invoke-debugger` that is supposed to call the system debugger to help the programmer handle an otherwise unhandled condition. We will now implement our debugger function and test it piece by piece, finishing our implementation by writing `invoke-debugger` along with `*debugger-hook*` and `break` as soon as we are done.

We will define our debugger in terms of actions that should be possible to perform within the debugger. For example, it should be possible to evaluate arbitrary Lisp forms, to report and return the condition that the debugger has been entered with, and to list and invoke available restarts. We can notice that these commands are fully driven by input from the programmer - the debugger should first receive a *command* that informs the debugger what kind of action it should take, and then, after accepting any additional data, it should perform that command.

#### Debugger commands

Let us model our commands via Common Lisp methods. In order to remain modular, each command should receive a possibly full set of information accessible to our debugger - that is, the command name itself, a debug stream that it should receive input from and print to, the condition object that was invoked, and an optional list of additional arguments that the command would otherwise need to prompt the user for.

Let us define the `run-debugger-command` generic function along with its base case - a command that has not been recognized by the debugger.

```lisp
(defgeneric run-debugger-command (command stream condition &rest arguments))

(defmethod run-debugger-command (command stream condition &rest arguments)
  (declare (ignore arguments))
  (format stream "~&;; ~S is not a recognized command.
;; Type :HELP for available commands.~%" command))
```

A brief test in the REPL shows us how this function is going to be used.

```lisp
PCS> (run-debugger-command :foo *debug-io* (make-condition 'condition))
;; :FOO is not a recognized command.
;; Type :HELP for available commands.
NIL
```

In order to have the function recognize individual commands, which are going to be Lisp keywords, we are going to use a mechanism called *`eql` specializers*; they make it possible to create methods that specialize on individual Lisp objects and are allowed to be called when the argument to the generic function are `eql` to the object that is `eql`-specialized on. Lisp keywords are comparable via `eql`, therefore they are a suitable fit for such a mechanism.

We are free to define commands by using `defmethod` and manually parsing the optional argument list, but for multiple commands, this might become unwieldy. It will be beneficial for us to introduce a more declarative way of defining new debugger commands that hides the implementation details from us; it would accept the command name, the stream and condition arguments and an optional list of arguments in its lambda list, and a set of forms to be executed.

Let us therefore define a macro that abstracts away this syntax; we will name it `define-command`.

```lisp
(defmacro define-command (name (stream condition &rest arguments) &body body)
  (let ((command-var (gensym "COMMAND"))
        (arguments-var (gensym "ARGUMENTS")))
    `(defmethod run-debugger-command
         ((,command-var (eql ,name)) ,stream ,condition &rest ,arguments-var)
       (destructuring-bind ,arguments ,arguments-var ,@body))))
```

We can see that this macro is a wrapper around `defmethod` that creates a new method with an `eql` specializer on the `name` that we provide. In addition, it automatically destructures `arguments-var` into `arguments`, meaning that we will be able to provide arbitrary optional arguments inside `define-command`'s lambda list.

#### Evaluating Lisp forms

Let us demonstrate the above macrology on the first command that the user should encounter upon merely entering the debugger:

```lisp
(defvar *debug-level* 0)

(define-command :eval (stream condition &optional form)
  (let ((level *debug-level*))
    (with-simple-restart (abort "Return to debugger level ~D." level)
      (let* ((real-form (or form (read stream)))
             (- real-form)
             (values (multiple-value-list (eval real-form))))
        (format stream "~&~{~S~^~%~}" values)
        (values values real-form)))))
```

We have defined a new dynamic variable that will be rebound by the main debugger function to state the current debugger level. Inside our `define-command :eval` form, we immediately use the current value of that variable to define a simple restart that will allow the programmer to immediately return to the current level of the debugger Once inside, we either use the Lisp form that was passed via the optional argument or read it from the stream. We bind the *REPL variable* `-` to the form that we are processing (what is a REPL variable - we will define later), and we evaluate that form, memorizing all values returned by it onto a list. We then print all the values to the stream and return these values and the form which we have *actually* evaluated, the reason for which will be explained at the end of the debugger chapter.

```lisp
PCS> (let ((*debug-level* 3))
       (run-debugger-command :eval *debug-io* (make-condition 'error)
                             '(+ 2 2)))
4                                                              ; printed
(4)                                                            ; returned
(+ 2 2)                                                        ; returned

PCS> (let ((*debug-level* 3))
       (run-debugger-command :eval *debug-io* (make-condition 'error)
                             '(values 1 2 3)))
1                                                              ; printed
2                                                              ; printed
3                                                              ; printed
(1 2 3)                                                        ; returned
(VALUES 1 2 3)                                                 ; returned

PCS> (let ((*debug-level* 3))
       (run-debugger-command :eval *debug-io* (make-condition 'error)))
(* 42 42)                                                      ; user input here
1764                                                           ; printed
(1764)                                                         ; returned
(* 42 42)                                                      ; returned
```

(Previously, we have taken care to prepend all output printed by our code with `;;`. In the above example, that is not the case; we have therefore introduced additional comments that show which values were printed by the `:eval` command, which values were returned by the function call, and which values were manually input by the user.)

#### Reporting and returning conditions

Another important thing inside the debugger is having the user realize that a debugger has been entered and informing them why it was entered: therefore, we need to report the condition to the user.

```lisp
(defun split (string)
  (loop for start = 0 then (1+ end)
        for end = (position #\Newline string :start start)
        collect (subseq string start end)
        while end))

(define-command :report (stream condition &optional (level *debug-level*))
  (format stream "~&;; Debugger level ~D entered on ~S:~%" level (type-of condition))
  (handler-case (let* ((report (princ-to-string condition)))
                  (format stream "~&~{;; ~A~%~}" (split report)))
    (error () (format stream "~&;; #<error while reporting condition>~%"))))
```

We have defined a small helper function to split a string by newline characters that will help us report multiline conditions. Finally, inside the method, we inform the programmer that the debugger has been entered, and either report the condition to the stream, or - in case an error has been signaled while reporting the condition - we inform the programmer about that fact.

We may see the macroexpansion of that `define-command` form to understand how it works under the hood.

```lisp
(DEFMETHOD RUN-DEBUGGER-COMMAND
    ((#:COMMAND1216 (EQL :REPORT)) STREAM CONDITION &REST #:ARGUMENTS1217)
  (DESTRUCTURING-BIND (&OPTIONAL (LEVEL *DEBUG-LEVEL*)) #:ARGUMENTS1217
    (FORMAT STREAM "~&;; Debugger level ~D entered on ~S:~%" LEVEL (TYPE-OF CONDITION))
    (HANDLER-CASE (LET* ((REPORT (PRINC-TO-STRING CONDITION)))
                    (FORMAT STREAM "~&~{;; ~A~%~}" (SPLIT REPORT)))
     (ERROR NIL (FORMAT STREAM "~&;; #<error while reporting condition>~%")))))
```

We may now test this command in the REPL to simulate entering the level-3 debugger with an `error` condition.

```lisp
PCS> (let ((*debug-level* 3))
       (run-debugger-command :report *debug-io* (make-condition 'error)))
;; Debugger level 3 entered on ERROR:
;; Condition ERROR was signaled.
NIL
```

Since our debugger is going to have a REPL, it will be beneficial to be able to pass the condition object the debugger was entered with into the REPL, so the programmer may interact with it.

```lisp
(define-command :condition (stream condition)
  (run-debugger-command :eval stream condition condition))
```

Here, we invoke one command from another command. This is to reuse existing code that we have written moments ago: we utilize the fact that condition objects are *self-evaluating* and therefore evaluating them again is a no-op, and that the `:eval` command prints its value to the REPL before returning.

```lisp
PCS> (let ((condition (make-condition 'error)))
       (run-debugger-command :condition *debug-io* condition))
#<ERROR {1011E31A23}>                                          ; printed
(#<ERROR {1011E31A23}>)                                        ; returned
#<ERROR {1011E31A23}>                                          ; returned
```

(The condition object will be accessible in the REPL after invoking this command, since it will be immediately bound to the REPL variable `*` by the function that implements the REPL; we will explain this mechanism later in the book.)

#### Listing and invoking restarts

We have now implemented the basic interaction with the condition object that the debugger has been entered with. We may move to the next important part of the debugger: allowing the programmer to list and invoke individual restarts.

```lisp
(defun restart-max-name-length (restarts)
  (flet ((name-length (restart) (length (string (restart-name restart)))))
    (if restarts (reduce #'max (mapcar #'name-length restarts)) 0)))

(define-command :restarts (stream condition)
  (let ((restarts (compute-restarts condition)))
    (cond (restarts
           (format stream "~&;; Available restarts:~%")
           (loop with max-name-length = (restart-max-name-length restarts)
                 for i from 0
                 for restart in restarts
                 for report = (handler-case (princ-to-string restart)
                                (error () "#<error while reporting restart>"))
                 for restart-name = (or (restart-name restart) "")
                 do (format stream ";; ~2,' D: [~vA] ~A~%"
                            i max-name-length restart-name report)))
          (t (format stream "~&;; No available restarts.~%")))))
```

The above command implements printing the list of all restarts. The big `loop` form is used to print the number, name, and report of each restar. The helper function, `restart-max-name-length`, is used for computing the maximum length of restart names; it is used along with the complex `format` form in order to provide an aesthetically-pleasing list of restarts. If no restarts whatsoever are available, we also inform the programmer about that.

```lisp
PCS> (run-debugger-command :restarts *debug-io* (make-condition 'error))
;; No available restarts.
NIL

PCS> (restart-case (run-debugger-command :restarts *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (retry () :report "Retry the operation." :retry)
       (refrobnicate () :report "Refrobnicate the operands." :refrobnicate))
;; Available restarts:
;;  0: [ABORT       ] Abort the operation.
;;  1: [RETRY       ] Retry the operation.
;;  2: [REFROBNICATE] Refrobnicate the operands.
NIL
```

The restarts have assigned numbers, which will be used to invoke the restarts in question. Let us define a debugger command that will invoke a restart with the given number.

```lisp
(define-command :restart (stream condition &optional n)
  (let* ((n (or n (read stream)))
         (restart (nth n (compute-restarts condition))))
    (if restart
        (invoke-restart-interactively restart)
        (format stream "~&;; There is no restart with number ~D.~%" n))))
```

We can test it in both its interactive and non-interactive forms:

```lisp
PCS> (restart-case (run-debugger-command :restart *debug-io* (make-condition 'error) 0)
       (abort () :report "Abort the operation." :abort)
       (retry () :report "Retry the operation." :retry)
       (refrobnicate () :report "Refrobnicate the operands." :refrobnicate))
:ABORT

PCS> (restart-case (run-debugger-command :restart *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (retry () :report "Retry the operation." :retry)
       (refrobnicate () :report "Refrobnicate the operands." :refrobnicate))
2                                                              ; user input here
:REFROBNICATE

PCS> (restart-case (run-debugger-command :restart *debug-io* (make-condition 'error) 42)
       (abort () :report "Abort the operation." :abort)
       (retry () :report "Retry the operation." :retry)
       (refrobnicate () :report "Refrobnicate the operands." :refrobnicate))
;; There is no restart with number 42.
NIL
```

In addition, we will provide four commands for the most commonly used standard restarts: `abort` and `continue`. In order to abort, the programmer can use `:abort` or `:q`; in order to continue - `:continue` or `:c`.

```lisp
(defun debugger-invoke-restart (name stream condition)
  (let ((restart (find-restart name condition)))
    (if restart
        (invoke-restart-interactively restart)
        (format stream "~&;; There is no active ~A restart.~%" name))))

(define-command :abort (stream condition)
  (debugger-invoke-restart 'abort stream condition))

(define-command :q (stream condition)
  (debugger-invoke-restart 'continue stream condition))

(define-command :continue (stream condition)
  (debugger-invoke-restart 'continue stream condition))

(define-command :c (stream condition)
  (debugger-invoke-restart 'continue stream condition))
```

A series of very brief REPL tests follow in order to verify that these work correctly.

```lisp
PCS> (restart-case (run-debugger-command :abort *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (continue () :report "Continue the operation." :continue))
:ABORT

PCS> (restart-case (run-debugger-command :q *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (continue () :report "Continue the operation." :continue))
:ABORT

PCS> (restart-case (run-debugger-command :continue *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (continue () :report "Continue the operation." :continue))
:CONTINUE

PCS> (restart-case (run-debugger-command :c *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (continue () :report "Continue the operation." :continue))
:CONTINUE
```

#### Debugger help and REPL

The above commands will not be helpful for the programmer if they do not know about them. Therefore, we need a help command that will list all the available commands.

```lisp
(define-command :help (stream condition)
  (format stream "~&~
;; This is the standard debugger of the Common Lisp Condidion System debugger
;; The debugger read-eval-print loop supports the standard REPL variables:
;;   *   **   ***   +   ++   +++   /   //   ///   -
;;
;; Available debugger commands:
;;  :HELP              Show this text.
;;  :EVAL <form>       Evaluate a form typed after the :EVAL command.
;;  :REPORT            Report the condition the debugger was invoked with.
;;  :CONDITION         Return the condition the debugger was invoked with.
;;  :RESTARTS          Print available restarts.
;;  :RESTART <n>, <n>  Invoke a restart with the given number.")
  (when (find-restart 'abort condition)
    (format stream "~&;;  :ABORT, :Q         Invoke an ABORT restart.~%"))
  (when (find-restart 'continue condition)
    (format stream "~&;;  :CONTINUE, :C      Invoke a CONTINUE restart.~%"))
  (format stream "~&~
;;
;; Any non-keyword non-integer form is evaluated.~%"))
```

Other than being very output-heavy, the only interesting thing about this function is that it checks for the visibility of any `abort` and `continue` restarts, and only lists the respective commands if these restarts are available for invocation. We may verify if that last part is true.

```lisp
PCS> (run-debugger-command :help *debug-io* (make-condition 'error))
;; This is the standard debugger of the Common Lisp Condidion System debugger.
;; The debugger read-eval-print loop supports the standard REPL variables:
;;   *   **   ***   +   ++   +++   /   //   ///   -
;;
;; Available debugger commands:
;;  :HELP              Show this text.
;;  :EVAL <form>       Evaluate a form typed after the :EVAL command.
;;  :REPORT            Report the condition the debugger was invoked with.
;;  :CONDITION         Return the condition the debugger was invoked with.
;;  :RESTARTS          Print available restarts.
;;  :RESTART <n>, <n>  Invoke a restart with the given number.
;;
;; Any non-keyword non-integer form is evaluated.
NIL

PCS> (restart-case (run-debugger-command :help *debug-io* (make-condition 'error))
       (abort () :report "Abort the operation." :abort)
       (continue () :report "Continue the operation." :continue))
;; This is the standard debugger of the Common Lisp Condidion System debugger
;; The debugger read-eval-print loop supports the standard REPL variables:
;;   *   **   ***   +   ++   +++   /   //   ///   -
;;
;; Available debugger commands:
;;  :HELP              Show this text.
;;  :EVAL <form>       Evaluate a form typed after the :EVAL command.
;;  :REPORT            Report the condition the debugger was invoked with.
;;  :CONDITION         Return the condition the debugger was invoked with.
;;  :RESTARTS          Print available restarts.
;;  :RESTART <n>, <n>  Invoke a restart with the given number.
;;  :ABORT, :Q         Invoke an ABORT restart.
;;  :CONTINUE, :C      Invoke a CONTINUE restart.
;;
;; Any non-keyword non-integer form is evaluated.
NIL
```

##### REPL variables

The help screen mentions *REPL variables*, a term which we have used before. Let us make a very quick introduction to them, since we will want our REPL to implement that functionality.

The REPL variables are a set of ten symbols: `*`, `**`, `***`, `+`, `++`, `+++`, `/`, `//`, `///`, and `-`. They are bound by the Common Lisp REPL for utility purposes. The `*`-variables contain the results of evaluating the last three expressions, the `+`-variables - the expressions themselves, the `/`-variables - lists of all values returned by the three expressions, and `-` contains the expression that is being evaluated right now. If there were no previous REPL expressions, these variables are bound to is `nil`.

Let us demonstrate this in the REPL:

```lisp
PCS> (+ 2 2)
4

PCS> (let ((x 42)) (check-type x integer))
NIL

PCS> (values :foo :bar :baz)
:FOO
:BAR
:BAZ

PCS> (list (list * / +) (list ** // ++) (list *** /// +++) -)
((:FOO                     ; *
  (:FOO :BAR :BAZ)         ; /
  (VALUES :FOO :BAR :BAZ)) ; +
 (NIL                                    ; **
  (NIL)                                  ; //
  (LET ((X 42)) (CHECK-TYPE X INTEGER))) ; ++
 (4        ; ***
  (4)      ; ///
  (+ 2 2)) ; +++
 (LIST (LIST * / +) (LIST ** // ++) (LIST *** /// +++) -) ; -
 )
```

In order to provide the programmer with a pleasant REPL experience, we will need to account for setting these variables in our REPL. Since our debugger is meant to be used interactively, we will assume that the host's REPL has already established its bindings for these REPL values; therefore, we will only set them instead of creating new bindings for them.

##### Debugger read-eval-print step

Once the debugger has started and is ready to accept input from the programmer, we will want to print a prompt that will inform the user that we are ready to accept input from them. Afterwards, we will read a from provided by them and check its type. If it is a keyword, it shall denote a debugger command to be invoked; if it is an integer, it shall denote the restart that should be invoked. If it is any other kind of form, it shall be equivalent to invoking the `:eval` command on that form.

```lisp
(defun read-eval-print-command (stream condition)
  (format stream "~&[~D] Debug> "*debug-level*)
  (let* ((thing (read stream)))
    (multiple-value-bind (values actual-thing)
        (typecase thing
          (keyword (run-debugger-command thing stream condition))
          (integer (run-debugger-command :restart stream condition thing))
          (t (run-debugger-command :eval stream condition thing)))
      (unless actual-thing (setf actual-thing thing))
      (prog1 values
        (shiftf /// // / values)
        (shiftf *** ** * (first values))
        (shiftf +++ ++ + actual-thing)))))
```

We can see that we bind the two values returned by `run-debugger-command`. While the role of `values` is clear in the above code, the role of `actual-thing` needs a bit of comment. This value is returned only from some commands, and when it is returned, it *supersedes* the form that was input into the REPL. This is e.g. so the user, upon typing `:eval (+ 2 2)` in the debugger, receives a more friendly `(+ 2 2)` form bound to the `+` value, rather than the less useful symbol `:eval`.

We can call this function in the REPL to test the single iteration of our debugger read-eval-print cycle.

```lisp
PCS> (read-eval-print-command *debug-io* (make-condition 'condition))
[0] Debug> (+ 2 2)                                             ; user input here
4                                                              ; printed
(4)                                                            ; returned

PCS> (read-eval-print-command *debug-io* (make-condition 'condition))
[0] Debug> :eval :foo                                          ; user input here
:FOO                                                           ; printed
(:FOO)                                                         ; returned

PCS> (read-eval-print-command *debug-io* (make-condition 'condition))
[0] Debug> :report                                             ; user input here
;; Debugger level 0 entered on CONDITION:
;; Condition CONDITION was signaled.
NIL                                                            ; returned
```

With the single step of our REPL available and tested, we may now implement the standard debugger proper. Upon entry, we will bind the debug level to one more than what it was before (effectively increasing it by one), report the condition, inform the programmer about the existence of the `:help` command, and call `read-eval-print-command` in a loop.

```lisp
(defun standard-debugger (condition &optional (stream *debug-io*))
  (let ((*debug-level* (1+ *debug-level*)))
    (run-debugger-command :report stream condition)
    (format stream "~&;; Type :HELP for available commands.~%")
    (loop (read-eval-print-command stream condition))))
```

We can test this function in the REPL, but we need to be cautious about it: this function never returns normally, which is intended and mandated by the Common Lisp standard. Therefore, we will need some way out of the debugger - for which we will establish a simple restart around the call to the debugger.

```lisp
PCS> (with-simple-restart (abort "Leave the debugger.")
       (standard-debugger (make-condition 'condition)))
;; Debugger level 1 entered on CONDITION:
;; Condition CONDITION was signaled.
;; Type :HELP for available commands.
[1] Debug> (+ 2 2)                                             ; user input here

4
[1] Debug> (list * / +)                                        ; user input here

(4 (4) (+ 2 2))
[1] Debug> :restarts

;; Available restarts:
;;  0: [ABORT] Leave the debugger.
[1] Debug> :restart 0                                          ; user input here

NIL
T
```

(The final two values are returned from the "outer" REPL by `with-simple-restart`.)

#### Debugger interface

Now that our debugger is more or less usable and useful for the programmer, we need to integrate it into the rest of our condition system by implementing the standard functionality related to the debugger: `*debugger-hook*`, `break`, and `invoke-debugger`.

So far, we have used a stub implementation of `invoke-debugger` that was good enough for our use cases; it is now time to replace it with a fully standard-compliant function that first queries the debugger hook and calls it, and only then calls the standard debugger.

```lisp
(defvar *debugger-hook* nil)

(defun invoke-debugger (condition)
  (when *debugger-hook*
    (let ((hook *debugger-hook*)
          (*debugger-hook* nil))
      (funcall hook condition hook)))
  (standard-debugger condition))
```

This means that now we can use `invoke-debugger` instead of our custom `standard-debugger` function.

```lisp
PCS> (with-simple-restart (abort "Leave the debugger.")
       (invoke-debugger (make-condition 'condition)))
;; Debugger level 1 entered on CONDITION:
;; Condition CONDITION was signaled.
;; Type :HELP for available commands.
[1] Debug> :abort                                              ; user input here

NIL
T
```

The only remaining function is `break` that establishes a `continue` restart and directly calls the debugger with the provided format control and arguments, returning `nil`.

```lisp
(defun break (&optional (format-control "Break") &rest format-arguments)
  (let ((*debugger-hook* nil)
        (condition (make-condition 'simple-condition
                                   :format-control format-control
                                   :format-arguments format-arguments)))
    (with-simple-restart (continue "Return from BREAK.")
      (invoke-debugger condition))
    nil))
```

Finally, we may perform one final REPL test to complete the testing process of our portable condition system.

```lisp
PCS> (break)
;; Debugger level 1 entered on SIMPLE-CONDITION:
;; Break
;; Type :HELP for available commands.
[1] Debug> :continue                                           ; user input here

NIL
```

### Integration

Our condition system is now complete. If we are using this condition system in an implementation that does not have a condition system, we can e.g. use the condition types we defined, along with our `error` function, inside the implementation's arithmetic operations. This will ensure that our `division-by-zero` condition is signaled when we attempt to evaluate e.g. `(/ 2 0)`.

It is, however, more possible that the reader following the code in this book already has a fully conformant Common Lisp implementation that has a condition system of its own; therefore, this portable condition system which we have defined is, in a way, a piece of *guest* code that has no full control over the *host's* condition system; therefore, if the host Lisp system signals its own `division-by-zero` error, it is fully independent from our guest condition system and therefore will not be captured by our debugger or even recognized as an object of the `condition` type that we have defined ourselves.

The authors of this book have prepared [an ASDF system](https://github.com/phoe/portable-condition-system/tree/master/integration) named `portable-condition-system.integration` that provides a means of integrating the host's condition system with PCS. While we will not go into details of its inner functioning, we will nonetheless show an example debugger session where the host's condition object is wrapped in a condition object of our own and where the host's restarts are available for calling as if they were restarts defined with our own mechanism.

```lisp
;;; (ql:quickload :portable-condition-system.integration)
;;; (in-package :portable-condition-system.integration)

INTEGRATION> (restart-case (with-debugger (#'debugger) (/ 2 0))
               (return-42 () :report "Return 42 instead." 42))
;; Debugger level 1 entered on FOREIGN-ERROR:
;; Foreign condition FOREIGN-ERROR was signaled:
;; arithmetic error COMMON-LISP:DIVISION-BY-ZERO signalled
;; Operation was (/ 2 0).
;; Type :HELP for available commands.
[1] Debug> :restarts                                           ; user input here

;; Available restarts:
;;  0: [RETURN-42] Return 42 instead.
;;  1: [RETRY    ] (*) Retry SLIME REPL evaluation request.
;;  2: [ABORT    ] (*) Return to SLIME's top level.
;;  3: [ABORT    ] (*) abort thread (#<THREAD "repl-thread" RUNNING {100CC01DE3}>)
[1] Debug> 0                                                   ; user input here
42                                                             ; returned

INTEGRATION> (restart-case (with-debugger (#'debugger) (/ 2 0))
               (return-42 () :report "Return 42 instead." 42))
;; Debugger level 1 entered on FOREIGN-ERROR:
;; Foreign condition FOREIGN-ERROR was signaled:
;; arithmetic error COMMON-LISP:DIVISION-BY-ZERO signalled
;; Operation was (/ 2 0).
;; Type :HELP for available commands.
[1] Debug> :abort                                              ; user input here
; Evaluation aborted on #<DIVISION-BY-ZERO {1010F04FD3}>.
```

Please see the manual of the `portable-condition-system.integration` system for more details.

### Additional work

The condition system we have created in this book passes the series of tests from the ANSI-TEST suite related to the condition system and assertion operators. While it is compliant, it is not very robust when it comes to undefined behaviour; it is, for example, possible to pass some duplicate keyword arguments to `restart-bind` and `define-condition`, which, in turn, triggers undefined behaviour. For brevity and educational reasons, we have omitted multiple places where argument validation and type-checks could be introduced; the reader is welcome to look at the GitHub version of Portable Condition System and inspect the safety measures already present in the code, as well as suggest new ones to be added to the code.

This concludes our work on implementing a complete Common Lisp condition system.

FIXME re-verify if the above code passes the ANSI tests

## Wrapping up

During the first half of this book, we have introduced the concept of dynamic variables and showed how they can be used to implement new kinds of functionality: subsystems of hooks and choices whose behaviour depends on the dynamic context in which they are executed. We have drawn direct parallels between these two and the existing standard systems of Common Lisp condition handlers and restarts. We have shown how these tie in with the existing standard mechanisms of transferring control in Common Lisp programs; finally, we have described the interactive means via which the condition system and the Lisp debugger is allowed to interact with the programmer.

The second half has led us through the non-trivial task of implementing a complete, ANSI-compliant Common Lisp condition system, from the very definition of the first condition type to implementing the interactive debugger function. To do that, we have drawn parallels and gathered inspiration from the individual mechanisms we have devised during our work through the first half of the book.

It is very notable that we have written a Lisp condition system in Lisp itself. Multiple parts of Common Lisp - the condition system, Common Lisp Object System, `loop`, `format`, `print-object`, `documentation` - have historically been implemented this way, being layered as separate modules upon a smaller language core. This shows that it is easy to introduce new modules and paradigms into Lisp in a way that forms seamless integration with the rest of Lisp. We owe that to the nature of Lisp, which is a a dynamic, image-based language.

A particularly curious reader might want to compare the systems of hooks and choices implemented in this book with a description of [the original implementation](http://www.nhplace.com/kent/CL/Revision-18.txt) of the condition system written by Kent M. Pitman in 1988 for *Common Lisp the Language version 1*. (For posterity, [the code of that implementation](http://www.nhplace.com/kent/CL/Revision-18.lisp.txt) has also been copied into the code repository for Portable Condition System.)

These closing words of the book are meant to explain a few more aspects of the condition system that might seem more related to philosophy and design rather than concrete implementation. We will describe the difference between the binding and casing operators that are related to handlers and restarts, and then, we will criticize the approach we have taken in the first part of our book, as well as describe the pitfalls, inconsistencies and pain points of the condition system.

### Binding versus casing

The logical distinction between the `*-bind` macros (`handler-bind` and `restart-bind`) and the `*-case` macros (`handler-case` and `restart-case`) can be understood as an analogy between binding operators - such as `let`, `let*`, `flet`, `labels`, `macrolet`, `symbol-macrolet`, and so on - and the Lisp macro `case`, along with its standard siblings `ccase`, `ecase`, and `typecase`. By this distinction, `with-simple-restart` naturally falls into the bag of `*-case` macros, since it is a wrapper around `restart-case`; the same goes for the `handler-case*` macro which we have defined ourselves.

The role of a binding operator is to modify the environment in which other forms are run. In case of `let` and `let*`, the change in the environment is effective by means of introducing new variables. `flet` and `labels` introduce local function bindings, while `macrolet` and `symbol-macrolet` introduce bindings for, respectively, local macros and local symbol macros. It can be said that these operators affect the variable and function *namespaces* by introducing new bindings into it. (For clarity: binding new macros counts as adding new bindings to the function namespace, while new symbol macros - to the variable namespace.)

In this context, `handler-case` can be considered to add new bindings to the *handler* namespace. It is a somewhat unique namespace, since handlers do not have names of their own; they instead use are the condition types that the respective handler functions are bound to. On the other hand, the restart namespace is a fully fledged one: each restart bound by `restart-bind` has a name (even if it is `nil`) that leads to the value that name is bound to in the restart namespace - the restart object that we can invoke. In addition,

Theoretically speaking, the act of binding is essentially a no-op until our code utilizes the bindings in some way - regardless of the namespace we bind in. In case of variables and symbol macros, such an act of utilization is accessing the value cells of symbols that name these variables and symbol macros; in case of functions and macros, it is the act of trying to call them. In case of handlers, it is the act of signaling a condition whose type matches one of these handlers; finally, in case of restarts, it is the act of calling `compute-restarts` or `find-restart` in order to access the restart objects, or invoking these restart objects through any means.

The `case`-like operators, on the other hand, follow a different programming idiom. The standard use of `case` in Common Lisp is passing it a Lisp form, which is evaluated; its value is called a *test key*. The list of all cases is then checked in order to see if the test key is `eql` to any of the objects passed in the individual cases. If a match is found, then the forms associated with that particular case are executed, and their return value is the returned value of `case`.

`handler-case` and `restart-case` follow the same idiom as `case`, except they search for matches deeper than just at the point where their form returns a value. These operators await for, respectively, `signal` and `invoke-restart` calls issued within their dynamic scope, at which point they check their lists of, respectively, handler cases and restart cases. In case of handlers, the type of condition is matched against the type of each handler; in case of restarts, restart names are matched via `eql`, unless a restart object is invoked directly by retrieving it via `compute-restarts`. For both operators, if a match is found, control is immediately transferred out of the signaling or invoking form and into the respective matching case, whose forms are then executed and whose return value becomes the return value of the respective macro.

### Separation of concerns

In the first half of the book, we have translated the approach we have used with the hook and choice subsystems into the handler and restart subsystems. While this approach that we have taken in the book was (hopefully) suitable for teaching the basics of the two respective Common Lisp subsystems, it might be surprising to hear from the book's author that it would not the best choice for use in actual, real code.

The main argument of not using handlers and restarts in such a way is separation of concerns. The handler and restart systems have an already well-established meaning in Common Lisp. While condition handlers are known to sometimes *decline* to handle a condition by not transferring control (which option is explicitly mentioned in the ANSI standard), restarts in Common Lisp are most commonly known as "means of leaving the debugger", completely ignoring the fact that they can be used in the way we originally used our choice system for.

To elaborate, using `handler-bind` to evaluate code for signaled conditions is common in real-life code, but `restart-bind` is pretty much unused to establish restarts. This stands in direct clash to our initial implementation of the choice subsystem, which was a system of hooks that we could selectively call without transferring control. (In fact, we have explicitly mentioned in the respective chapter that using choices to transfer control was changing the structure of our code and creating tighter coupling between the place where our choices were bound and the place where they are computed and invoked.)

This means that using restarts for purpose of *not* restarting some abstract process in the program might violate the principle of least astonishment. Programmers reading our code might thus not expect to see `restart-bind` used in a non-restarting way and they might become confused by it.

Another question is whether such restarts should show up in the debugger at all. It is possible to avoid that by setting a test function that returns `nil` if the condition argument passed to the test function is non-`nil`, but this in turn might cause confusion if the programmer mistakenly calls `compute-restarts` or `find-restarts` without any condition object whatsoever and then sees some "means of leaving the debugger" which do not actually transfer control anywhere.

Summing up, one could argue that it might be preferable to leave the original, CL-provided handler and restart subsystems to their original roles, and instead create or use independent mechanisms for hooks and choices. They are easy enough to be constructed, as we have demonstrated in the earlier chapters; in fact, multiple implementations of hooks and events in Common Lisp have already been created and are available to be fetched from Quicklisp. These systems also feature better introspection for individual hook objects and the ability to declare hooks with indefinite scope. Some examples are [`cl-events`](https://github.com/deadtrickster/cl-events/), [`cl-hooks`](https://github.com/scymtym/architecture.hooks), and [`modularize-hooks`](https://github.com/Shinmera/modularize-hooks).

### Downsides of the condition system

As we mentioned before, the Common Lisp condition system is, like all of Common Lisp, a product of a standardization committee; the people who have designed, discussed, and agreed on Common Lisp over twenty fve years ago have been solving problems that we do not need to consider nowadays. That fact causes some aspects of the language - in our case, of the condition system - to stick out as weird, unusual, inconsistent, or otherwise "should have been done differently".

#### Separation from CLOS

`define-condition` and `defclass` are very similar to one another, but yet, they are distinct in the Common Lisp standard. This disparity is caused by a long history of the condition system; the original `define-condition` had been created in the times of *Common Lisp the Language version 1*, when CLOS did not yet exist and therefore `defclass` was not yet available. During the time of Common Lisp standardization, the proposal to fully merge the condition system into CLOS was rejected, resulting in a system of defining conditions that behaves like `defclass`, looks like `defclass`, is supposed to work like `defclass`, but is not `defclass`. A similar case occurs for the inheritance model of condition types, which is a multiple inheritance model identical to the one used by CLOS, but explicitly mentioned not to necessarily be CLOS-based.

In many Common Lisp implementations conditions are instances of the CLOS-defined `standard-class` and therefore `standard-object`s themselves; this, however, is not mandated by the standard, and therefore not followed by at least one popular Common Lisp implementation. One could daydream of the standard stating that condition classes could have a *metaclass* named `condition-class`; no such thing is required by the standard, however.

Because of that disparity, it is impossible to, among others: use the general statement "condition class" instead of "condition type" when referring to standard Common Lisp code; portably access slot values of condition object by means of `slot-value`; create mixin classes that can be added to standard classes and condition classes alike; define constructor functions for condition objects by defining `initialize-instance :after` methods on condition types. (The last fact can be worked around by using `make-instance` instead of `make-condition` for creating condition instances, which seems to work on that particular implementation; the author of this book shyly suggests that this behaviour could become standard in the implementation in question to bridge that gap.)

##### Making conditions of complex condition types

A somewhat unfortunate consequence of that fact is that the specification for `make-condition` requires its datum to be a specifier for a condition type. Given that the Common Lisp condition type is based on set theory, `(or condition integer)` could be argued to be a valid condition type (since any condition is of type `(or condition integer)`) and therefore it should be a valid argument to `make-condition`. What is an even more unusual consequence, it should be possible to instantiate condition types such as `(or program-error file-error)` or `(and warning condition)`; the mental gymnastics required to adapt `make-condition` to accept such condition type designators are left as an exercise for no one in particular.

#### Dynamic extent of restart objects

It is "not a bug" in a sense that is a feature, but nonetheless, the fact that restarts are defined to have dynamic extent in Common Lisp may nonetheless be confusing for newcomers and experienced Lisp programmers alike. While it is undefined behaviour to access any objects of dynamic extent outside the scope in which they're valid, it is nonetheless trivial to invoke that undefined behaviour in case of restarts, e.g. by evaluating `(compute-restarts)` in the REPL and allowing the resulting list to be printed. At the time of writing this book, evaluating the below one-liner causes at least one commonly used Lisp implementation to "halt and catch fire" by means of signaling memory corruption errors.

```lisp
((lambda () (print (restart-bind ((x (lambda ()))) (compute-restarts)))))
```

#### Speed

While [it is possible](http://directed-procrastination.blogspot.se/2011/05/backtracking-with-common-lisp-condition.html) to use the Common Lisp condition system for transferring control in case of algorithms that perform backtracking, [it might not be](https://gist.github.com/nikodemus/b461ab9146a3397dd93e) the best fit when it comes to performance of such a solution. Compated to the primitive Common Lisp operators for transfer of control, such as `tagbody`/`go`, `block`/`return-from`, or `catch`/`throw`, the condition system needs to perform more work in addition to using one of these primitives: a condition object needs to be constructed with the provided arguments; the list of handlers needs to be walked, with a runtime typecheck being done for each handler; all applicable handler functions needs to be called, until a transfer of control occurs. Therefore, heavily relying on the condition system in hot loops and places that require being optimized for speed may be a poor choice over using the Common Lisp primitives for performing NLToCs; if profiling such code shows that much time is spent in `signal` and respective handler functions.

#### Introspection

The introspection facility for the condition system is quite poor. There is no portable way of inspecting the full lists of handlers; that is matched with the fact that handlers are only defined in terms of matching handler types and their handler functions, and therefore were never meant to have distinct types of their own, like restarts do. While it is possible to compute a list of restarts applicable for a given situation, it is similarly impossible to get a list of *all* established restarts and manually inspect their report, interactive, or test functions.

In addition, the Common Lisp standard specifies nothing regarding backtrace information. Libraries such as [Dissect](https://github.com/Shinmera/dissect) have to resort to implementation-defined mechanisms in order to be able to fetch stack information across different Common Lisp implementations.

One more pain point is the fact that the debugger itself is not programmable. There exists the `*debugger-hook*` variable that is respected by all functions which invoke the debugger - except for `break` which binds the variable to `nil` before entering the debugger. Because of this, again, libraries such as [Swank](https://github.com/slime/slime/tree/master/swank), [Slynk](https://github.com/joaotavora/sly/blob/master/slynk/), or [`trivial-custom-debugger`](https://github.com/phoe/trivial-custom-debugger) that implement their own debuggers are required to hook into implementation-defined code.

One more issue related to signaling introspection is the fact that there is no indication of which operator has signaled a particular condition or any reason for why a debugger has been invoked. This prevents code from acting differently depending on whether a particular condition has been signaled using `signal`, `warn`, `error` or `cerror`, as well as a portable debugger to act differently based on whether it has been entered due to an unhandled error or e.g. due to `break`.

#### Concrete condition types

The tree of standard condition types is not very detailed. On one hand, we are missing some specific condition types that are more detailed, such as a serious condition `stack-overflow` that represents hitting a hardware limitation of the current platform, or more detailed variants of `file-error` that go into more details: the file is missing, the file is being edited by another user, the file is read-only, etc..; on the other hand, Common Lisp misses some useful variants of already existing types, such as many simple conditions like `simple-program-error` that would be directly useful for Common Lisp programmers.

This, again, is the result of the Common Lisp committee deciding on what would be the best for the language being standardized; defining too few condition types would cripple the condition functionality that the programmers could depend on, but defining too many of them would put more burden on implementors and people adapting existing programs into Common Lisp to ensure that the errors signaled by their code were of proper standard type. (One of the authors of the condition system mentioned that it was already a success to fit the existing set of condition types inside the Common Lisp standard.)

#### Warning conditions and #'WARN

While it is possible to signal any kind of condition object via `signal`, `error`, or `cerror`, the function `warn` is required to signal a `type-error` if the condition object supplied to it is not of type `warning`. This means that it is illegal to `warn` on conditions that are not warnings, even though the reasons for such a requirement are not clear.

#### Implementing custom SIGNAL-like operators

The function `coerce-to-condition` is not directly exposed nor programmable. This has two unfortunate consequences: first of all, it is impossible to provide data to the signaling operators that is not a condition, a string, a function, or a symbol, and second, it makes it impossible to portably write new signaling functions, such as `signal`, that hook into this functionality.

One example of the above that is exhibited by many Common Lisp implementations is signaling *compiler notes*, which are conditions of lesser relevance than `warning` or even `style-warning`. Compiler notes are information for the programmer that are related to valid Common Lisp code, but e.g. inform the programmer about dead code branches that are never reachable or about the fact that the compiler was unable to perform some requested optimizations. Not only compiler note condition types are not a part of the standard, but it is impossible to write a portable `compiler-notify` function that first coerces its data to a condition of type `compiler-note`, signals it, and then performs some additional reporting in case the condition was not handled.

#### Smaller issues

The condition system also has smaller issues and pet peeves that are only relevant in particular contexts.

* One such issue is the fact that a large part of the condition system depends upon parsing types at runtime, which makes it hard for implementors that work on *first class environments*, where type information is not stored globally, but per environment, and may vary between individual environments.
* Another one is the fact that the `:no-error` clause in `handler-case` should be named differently due to the fact that `handler-case` may handle conditions that are not errors.
* Yet another one is the fact that the clustering behaviour is not consistent between handlers and restarts: a handler may only invoke handlers that are "older" than it, while a restart has access to all restarts that have ever been bound in its dynamic environment.
* Another one, already mentioned before, is the difference in syntax between `handler-bind`/`restart-bind` (`:report-function`, `:interactive-function`, `:test-function`) and `handler-case`/`restart-case` (`:report`, `:interactive`, `:test`) and, in some cases, `define-condition` (which also accepts a `:report` option that is consistent with the `-case` operators). One set of keywords accepts a function object, the other - forms suitable for passing to the `function` special operator. This issue is related to the old feud between the proponents of `(lambda ...)` and the proponents of `#'(lambda ...)` that dates back to the times when older Common Lisp dialects were still alive and therefore `#'(lambda ...)` was used as a means to be compatible with them. Common Lisp defined the macro `(lambda ...)` that expands into `#'(lambda ...)` for this exact reason, and the `#'`-less form `(lambda ...)` is the only function designator suitable for use both in `:report-function`/`:interactive-function`/`:test-function` and in `:report`/`:interactive`/`:test`.

## Appendix A: HANDLER-CASE*

Let us introduce one more utility into the condition system. This utility is not defined in the standard; still, it is useful for a certain class of problems, which is why we will implement it ourselves within this book.

In the world of handlers, `handler-bind` is sometimes used to construct `handler-case*`, a non-standard operator with the same syntax as `handler-case`. `handler-case*`, upon reacting to a signaled condition, evaluates the handler body first, allowing it to operate within the dynamic environment in which the condition was signaled. *Only then* it unwinds the stack by transferring control outside of the handler. This approach is useful for e.g. allowing the handler to preserve the back trace in case of an error; using `handler-case` for such situations is useless, because the handler body needs to operate before the stack is unwound, and therefore before the back trace of the error site is destroyed.

An example implementation of `handler-case*`, adapted from the CMU CL source of `handler-case` and refactored for clarity, may look like the following:

```lisp
(defmacro handler-case* (form &rest cases)
  "A variant of HANDLER-CASE, in which the case forms are evaluating before
performing a transfer of control. This ensures that the case forms are evaluated
in the dynamic scope of the signaling form."
  (let ((no-error-case-count (count :no-error cases :key #'car)))
    (case no-error-case-count
      (0 (make-handler-case*-without-no-error-case form cases))
      (1 (make-handler-case*-with-no-error-case form cases))
      (t (error "Multiple :NO-ERROR cases found in HANDLER-CASE*.")))))

(defun make-handler-case*-with-no-error-case (form cases)
  (let* ((no-error-case (assoc :no-error cases))
         (other-cases (remove no-error-case cases)))
    (let ((normal-return (gensym "NORMAL-RETURN"))
          (error-return  (gensym "ERROR-RETURN")))
      `(block ,error-return
         (multiple-value-call (lambda ,@(cdr no-error-case))
           (block ,normal-return
             (return-from ,error-return
               (handler-case* (return-from ,normal-return ,form)
                 ,@other-cases))))))))

(defun make-handler-case*-without-no-error-case (form cases)
  (let ((block-name (gensym "HANDLER-CASE*-BLOCK")))
    (flet ((make-handler-binding (case)
             (destructuring-bind (type lambda-list . body) case
               `(,type (lambda ,lambda-list
                         (return-from ,block-name (locally ,@body)))))))
      (let ((bindings (mapcar #'make-handler-binding cases)))
        `(block ,block-name (handler-bind ,bindings ,form))))))
```

We can notice that the implementation is divided into two cases. One of them is utilized if a `:no-error` case is present; that case is treated specially and it delegates other, non-`:no-error` cases, to be handled via a recursive macro call to `handler-case*`. The case of `:no-error` is handled identically in `handler-case` and `handler-case*`, and the normal case is, thankfully, much less complex than the matching implementation of `handler-case`.

`handler-case*` needs to iterate through the cases provided to `handler-case*` and turn them into properly formatted bindings for `handler-bind`. The anonymous function within each binding needs to execute the body of each case and then return its value to the outermost block. Once all cases are iterated over and the list of bindings is created, it is spliced into `handler-bind`, and the resulting code is returned.

(For completeness: the `locally` form wrapped around the splicing of `body` is there to allow *local declarations* to work inside it. Declarations are, however, out of scope for this book.)

We may test a combination `handler-case*` and `unwind-protect` and compare the resulting order in which forms are executed to the one that results from using the standard macro `handler-case`.

```lisp
CL-USER> (handler-case
             (unwind-protect (signal 'condition)
               (format t ";; Going out of dynamic scope~%"))
           (condition (c) (format t ";; Handling condition ~S~%" c)))
;; Going out of dynamic scope
;; Handling condition #<CONDITION {1001BC4FA3}>
NIL

CL-USER> (handler-case*
             (unwind-protect (signal 'condition)
               (format t ";; Going out of dynamic scope~%"))
           (condition (c) (format t ";; Handling condition ~S~%" c)))
;; Handling condition #<CONDITION {1001C77553}>
;; Going out of dynamic scope
NIL
```

We may also check the macroexpansion of our `handler-case* form` from above to see what the final macroexpansion looks like and how it utilizes `handler-bind` internally. If we print the expansion it with `*print-gensym*` set to `nil`, we will be able to immediately evaluate this code after copy-pasting it into the read-eval-print loop.

```lisp
CL-USER> (BLOCK HANDLER-CASE*-BLOCK617
           (HANDLER-BIND ((CONDITION
                            (LAMBDA (C)
                              (RETURN-FROM HANDLER-CASE*-BLOCK617
                                (LOCALLY (FORMAT T ";; Handling condition ~S~%" C))))))
             (UNWIND-PROTECT (SIGNAL 'CONDITION)
               (FORMAT T ";; Going out of dynamic scope~%"))))
;; Handling condition #<CONDITION {100258ADF3}>
;; Going out of dynamic scope
NIL
```

## Appendix B: Lisp macros 101

A Common Lisp macro is a peculiar kind of function that transforms Lisp data into Lisp code. Wherever a function cannot be used to achieve a particular task within Lisp syntax, usually a macro can be employed in its place; this fact stems from two basic differences between how functions and macros are treated in Lisp.

The first basic difference from the outer point of view is that functions receive their arguments as values resulting after evaluating their forms; macros receive the unevaluated forms verbatim. This allows macros to skip the normal evaluation rules of forms and either evaluate them selectively or invent completely new syntax rules based on the composition of the subforms that they receive.

The second basic difference is that while return values of functions are returned, return values of macros are *inserted in place* of the macro call and evaluated again. When a macro is called, the macro call site is "replaced" with the code that it returns; this is why a macro is required to return valid Lisp code.

While ordinary functions are usually called at *execution time*, macros are usually called at *compilation time*. This is because the Lisp compiler, as it processes Lisp forms during compilation, is required to completely expand all macros before proceeding with compilations; it is not required to do the same with functions.

### Basics of macro writing

Let us write an example form that uses the standard macro `and`, which evaluates its arguments in turn until one of them returns false.

```lisp
(and (= 0 (random 6)) (error "Bang!"))
```

This very simple implementation of a russian roulette contains a macro call. When the compiler parses this form, it expands the `and` call and replaces the whole form with the result of the *macroexpansion function*. The concrete implementation of `and` depends on the Lisp implementation; an example expansion is shown below.

```lisp
(IF (= 0 (RANDOM 6)) (ERROR "Bang!"))
```

It is perhaps not very impressive like that, but we can arbitrarily chain arguments passed to it. Let us modify the game a bit and only make a bang when we hit the roulette three times in a row.

```lisp
(my-and (= 0 (random 6)) (= 0 (random 6)) (= 0 (random 6)) (error "Bang!"))
```

An example *recursive* expansion of the above can be seen below.

```lisp
(IF (= 0 (RANDOM 6))
    (AND (= 0 (RANDOM 6)) (= 0 (RANDOM 6)) (ERROR "Bang!")))
```

Using recursive macroexpansion, it is possible to reach the final, fully macroexpanded form:

```lisp
(IF (= 0 (RANDOM 6))
    (IF (= 0 (RANDOM 6))
        (IF (= 0 (RANDOM 6))
            (ERROR "Bang!"))))
```

Let us define our version of that macro, named `my-and`, which will implement the following scheme.

```lisp
(defmacro my-and (&rest forms)
  (cond ((null forms) 'nil)
        ((null (rest forms)) (first forms))
        (t (list 'if (first forms) (cons 'my-and (rest forms))))))
```

We can see that our macro has three cases. If the list of forms is empty (which means that the macro was called like `(my-and)`), it returns `nil`; if the list of forms contains only one element, then that element is returned. Otherwise (when the list of forms contains more than one element), a compound form is returned: a three-element list with the symbol `if` as its first element, the first form as its second element, and another compound form: a cons of the symbol `my-and` and the forms remaining after removing the first form. The form returned by the macro is then treated as Lisp code by the compiler.

### Backquote

The explicit list-and-symbols syntax in the macro is correct, but Common Lisp exposes one more kind of notation that resembles the final output of the macro somewhat better. It is called *backquote notation* or *quasiquote*. It consists of four (or, in practice, three) syntactic operators:

* `` ` `` *quasiquotes* an expression,
* `,` *unquotes* an expression,
* `,@` *splicing-unquotes an expression*,
* `,.` is a variant of `,@` that has generally fallen out of use, since it offers no benefits and many more pitfalls that `,@`.

Backquoting is similar to the quoting (`'`) operator of Common Lisp, as it preserves a form in its verbatim state; if we use a backquote in place of a quote, the two forms will be equivalent. The main difference is that backquote allows subforms of the backquoted expression to be selectively *unquoted*. Let us demonstrate this.

```lisp
CL-USER> (let ((args '(1 2 3)))
           '(funcall function args))
(FUNCALL FUNCTION ARGS)

CL-USER> (let ((args '(1 2 3)))
           `(funcall function args))
(FUNCALL FUNCTION ARGS)

CL-USER> (let ((args '(1 2 3)))
           `(funcall function ,args))
(FUNCALL FUNCTION (1 2 3))

CL-USER> (let ((args '(1 2 3)))
           `(funcall function ,@args))
(FUNCALL FUNCTION 1 2 3)
```

In the first two cases, the whole `funcall` form has been quoted verbatim. In the third case, we can see that the variable `body` has been replaced with its value, the list `(1 2 3)`. In the fourth case, we can see that the variable `body` has been replaced with *multiple values*; these values required `body` to be bound to a list, and the elements of these list have been spliced into the outer form.

In other words: in a backquoted expression, Lisp data which are not unquoted are quoted under normal rules and Lisp data which are unquoted are evaluated normally before being inserted into the resulting form.

(It is possible to nest backquotes into so-called *double* or *triple backquote* notation; describing this technique is out of scope of this book.)


### Symbol capture

One of the common pitfalls when writing macros is introducing *symbol capture*, which, in other words, is an adverse or unexpected effect that comes into effect when a symbol used in the macroexpansion clashes with a symbol

Let us consider a modified version of the `for` macro that Paul Graham in chapter 9.6 of *On Lisp*.

```lisp
(defmacro for ((var start stop) &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
         (limit ,stop))
         ((> ,var limit))
       ,@body)))
```

The macro seems to work correctly...

```lisp
CL-USER> (for (i 0 3)
           (format t ";; ~D~%" i))
;; 0
;; 1
;; 2
;; 3
NIL
```

...until it interacts with a variable named in a particular way - in our case, `limit`.

```lisp
CL-USER> (let ((limit 10))
           (for (i 0 3)
             (format t ";; ~D ~D~%" i limit)))
;; 0 3
;; 1 3
;; 2 3
;; 3 3
NIL
```

We have clearly defined `limit` to be `10`, and yet, the `format` call prints `3` instead. We can see why it happens when we decide to `macroexpand-1` our `for` macro:

```lisp
(let ((limit 10))
  (for (i 0 3)
    (format t ";; ~D ~D~%" i limit)))

(let ((limit 10))
  (DO ((I 0 (1+ I))
       (LIMIT 3))
      ((> I LIMIT))
    (FORMAT T ";; ~D ~D~%" I LIMIT)))
```

We can see that the outer `limit` binding is *shadowed* by an internal binding of the same symbol that is made inside `do`. In effect, the outer binding is unused, and the inner binding interferes with the `format` form that we want to execute.

A solution is to use *gensyms* inside our macroexpansion. Gensyms, short for "generated symbols", are freshly generated symbols that are completely unique: they have no relationship to any package and are distinct from all other symbols. Because of that, they are printed with a `#:` prefix.

(Providing detailed information about symbols, packages, and the relationship between the two is not in scope of this book. Reader may want to consult *The Complete Idiot’s Guide to Common Lisp Packages* by Erann Gat/Ron Garret, after disregarding the somewhat crude name of that article.)

The fixed version of our `for` macro may look like the following:

```lisp
(defmacro for ((var start stop) &body body)
  (let ((limit (gensym "LIMIT")))
    `(do ((,var ,start (1+ ,var))
          (,limit ,stop))
         ((> ,var ,limit))
       ,@body)))
```

We can now expand our example from earlier to see the difference:

```lisp
(let ((limit 10))
  (for (i 0 3)
    (format t ";; ~D ~D~%" i limit)))

(let ((limit 10))
  (DO ((I 0 (1+ I))
       (#:LIMIT737 3))
      ((> I #:LIMIT737))
    (FORMAT T ";; ~D ~D~%" I LIMIT)))
```

The former `limit` variable has been replaced with a gensym that is guaranteed not to collide with any existing symbols, no matter where in our Lisp world they might come from. We can also verify that our new form works:

```lisp
CL-USER> (let ((limit 10))
           (for (i 0 3)
             (format t ";; ~D ~D~%" i limit)))
;; 0 10
;; 1 10
;; 2 10
;; 3 10
NIL
```

The problem is not limited to variable names only; in certain situations, it is also possible to capture function names and interact with symbols in other Lisp namespaces. Using gensyms is a solution for the majority of all these cases.

### Order of evaluation

Macros should generally ensure that their arguments are evaluated in the proper order. Let us consider a small variation of the above `for` macro:

```lisp
(defmacro for ((var start stop) &body body)
  (let ((limit (gensym "LIMIT")))
    `(do ((,limit ,stop)
          (,var ,start (1+ ,var)))
         ((> ,var ,limit))
       ,@body)))
```

In this example, the bindings established by `for` are in inverse order: first we establish the limit, and only then we establish the iteration variable. Such behaviour might violate the programmer's expectations, especially when *side effects* are involved in computing the forms in question; such a side effect is, for example, printing to the screen. For instance, the programmer may expect `;; Returning 0!` to be printed *before* `;; Returning 3!`; the above version of the macro will violate this expectation.

```lisp
CL-USER> (flet ((return-0 () (format t ";; Returning 0!~%") 0)
                (return-3 () (format t ";; Returning 3!~%") 3))
           (for (i (return-0) (return-3))
             (format t ";; ~D~%" i)))
;; Returning 3!
;; Returning 0!
;; 0
;; 1
;; 2
;; 3
NIL
```

The solution in this case is to once again inverse the order in which variables are bound; for different macros, the required solutions will depend on the order in which the forms passed to the macro are evaluated.

### Multiple evaluation

Another mistake that is possible to make when writing the `for` macro is not introducing a `limit` variable at all, and passing the `stop` parameter directly into the body of `do`.

```lisp
(defmacro for ((var start stop) &body body)
  `(do ((,var ,start (1+ ,var)))
       ((> ,var ,stop))
     ,@body))
```

This causes the `(return-3)` form to be evaluated anew on every iteration, which might or might not be what we want for a particular macro to do. In our case, the effect will be printing the `;; Returning 3!` message on each iteration of the `do` macro.

```lisp
CL-USER> (flet ((return-0 () (format t ";; Returning 0!~%") 0)
                (return-3 () (format t ";; Returning 3!~%") 3))
           (for (i (return-0) (return-3))
             (format t ";; ~D~%" i)))
;; Returning 0!
;; Returning 3!
;; 0
;; Returning 3!
;; 1
;; Returning 3!
;; 2
;; Returning 3!
;; 3
;; Returning 3!
NIL
```

The solution for that is creating lexical variables within the macroexpansion and using the name of the variable instead of inserting the full form everywhere. This will ensure that the form is evaluated only once and that the resulting value is then used around the body of our macro. In this case, we need to - once again - step back and reintroduce our `limit` variable, also creating a gensym for it in order to avoid variable capture.

### When not to write macros

In short: only use macros if a function won't do. If a function is enough, you should write a function.

There two groups of situations for which macros are the proper tool to use. These are evaluation control and syntactic abstraction. An example of evaluation control is the `and` macro that we have demonstrated above and re-implemented as `my-and`; an example of syntactic abstraction is the `for` macro, also demonstrated above.

In other words, a macro should be used only when the syntax rules for functions are not enough: namely, when evaluating all forms before passing their values to the called function is *not* what we want.

This is because a macro is capable of receiving its arguments unevaluated and performing arbitrary transformations on them. The earlier example of `for` cannot be written as a function - the `(i 0 3)` form would have been interpreted as a call of an unknown function named `i` instead of being interpreted as a binding for an iteration variable.

However, macros also have their downsides: it is impossible to use the `#'` notation with them; for example, `#'and` is invalid syntax. Therefore, it is impossible to directly pass macro functions as values to e.g. `mapcar`. In addition, macro calls are invisible in debug traces, since they no longer exist in compiled code.

### Reference

For more information and detail on basics of macro writing, the reader should consult the "Macros: Defining Your Own" chapter of *Practical Common Lisp* by Peter Seibel, as well as chapters 9 and 10 of *On Lisp* by Paul Graham.

## Appendix C: condition system reference

TODO

## <3 :heart: <3 Hall of Fame <3 :heart: <3

Thanks to Lonjil, John Cowan, mtreis86, BrightLight, Elias Mårtenson, Adam Piekarczyk, Nisar Ahmad, Robert Strandh, Daniel Kochmański, Michael Fiano, cage, Bike, Shinmera, no-defun-allowed, Grue, Gnuxie, Tom Peoples for various remarks, suggestions, and fixes to the text of this book and to the code of `trivial-custom-debugger` and the Portable Condition System.

TODO ask everyone from the above list how they would like to be credited.

Thanks to Kent M. Pitman, Peter Seibel, Paul Graham, David Lamkins, Robin Schroer, Chaitanya Gupta, Justin Grant, Timmy Jose, and Jacek Złydach, as well as the Lisp Cookbook contributors, for their collective work on evolving, defining, teaching, and writing about the Common Lisp condition system.

Thanks to ein Strauch for financial support on Patreon. (If you like what I write, feel free to [tip me there](https://www.patreon.com/phoe_krk) yourself).

Thanks to you - the reader - for making your way through the text.

TODO research paper - integrating independent condition systems in Common Lisp
TODO once you write all of that, walk the whole book and evaluate and verify all the code blocks and all the REPL tests; mistakes and inconsistencies might have been made along the way

FIXME Perhaps the handlers and restarts chapter should be switched around, despite the bootstrapping issues that this will cause. Blah, we'll see during the review.

TODO quote (we do not want to be here)
