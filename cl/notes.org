* Create a new project
  1) Create project with quickproject
     http://www.xach.com/lisp/quickproject/
     (quickproject:make-project #p"~/src/myproject/" :depends-on '(drakma cxml))
     - package.lisp  (defpackage)
     - myproject.asd  (defsystem)
     - myproject.lisp  (code)
  2) Add code to myproject.lisp
     http://ackerleytng.github.io/new-project-common-lisp/

  3) Add dependency on cl-ppcre
     http://xach.livejournal.com/130040.html
     1) In package.lisp

     2) in helloworld.asd

     3) As cl-ppcre is not installed in the laptop yet, quickload
  4) Alternative
     https://gitlab.common-lisp.net/dkochmanski/sclp

* Clon vs Quicklisp vs Qlot
* library Version locking
  How does one ensure that it's safe to update a package?
* Environments
  - If the deployment is done with the image freezing, how does one
    make different behaviours depending on the environment? (no billing, for example)

* Balkanization
** pattern matching
*** let+
*** metabang
*** optima
    http://enthusiasm.cozy.org/archives/2013/07/optima
*** trivia
** fset
*** http://fare.livejournal.com/155094.html
*** fsets
** cl-continuation
** nondeterministic
*** https://nikodemus.github.io/screamer/#Overview

* style guides
  - http://labs.ariel-networks.com/cl-style-guide.html

*
