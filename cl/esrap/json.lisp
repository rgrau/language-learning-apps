(require :esrap)

(defpackage :json-grammar
  (:use :cl :esrap))

(in-package :json-grammar)

(defun not-doublequote (char)
  (not (eql #\" char)))

(defrule whitespace (+ (or #\space #\tab #\newline))
  (:constant nil))

(defrule alphanumeric (alphanumericp character))

(defrule string-char (or (not-doublequote character)
                         (and #\\ #\")))

(defrule json (and (? whitespace)
                   (or atom string array hash null)
                   (? whitespace))
  (:destructure (w s w2)
                s))

(defrule integer (+ (or "0" "1" "2" "3" "4" "5" "6" "7" "8" "9"))
  (:lambda (list)
    (parse-integer (text list) :radix 10)))

(defrule string (and #\" (* string-char) #\")
  (:destructure (q1 string q2)
                (declare (ignore q1 q2))
                (text string)))

(defrule atom (or integer string))

(defrule kv (and (? whitespace) string (? whitespace) #\: json)
  (:destructure (w k w2 colon v)
                (declare (ignore colon))
                (cons k v)))

(defrule hash (and #\{
                   (* kv)
                   #\})

  (:destructure (open cosas close)
                (declare (ignore open close))
                cosas))

(parse 'integer "123")
(parse 'string "\"hola\"")
(parse 'atom "\"hola\"")
(parse 'atom "123")
(parse 'json "123")
(parse 'hash "{ \"hola\" : 32}")
