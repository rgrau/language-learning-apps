
(require :esrap)
(defpackage :arithmetic
  (:use :cl :esrap))

(in-package :arithmetic)

(defun not-doublequote (char)
  (not (eql #\" char)))

(defrule integer (+ (or "0" "1" "2" "3" "4" "5" "6" "7" "8" "9"))
  (:lambda (list)
    (parse-integer (text list) :radix 10)))

(defrule whitespace (+ (or #\space #\tab #\newline))
  (:constant nil))

(defrule alphanumeric (alphanumericp character))

(defrule id (+ alphanumeric)
  (:text t))


(defrule string-char (or (not-doublequote character)
                         (and #\\ #\")))

(defrule expr (or (and #\( expr #\))
                  (and term (? (and #\+ expr))))
  (:destructure (match)
                (if (atom))
                ))

(defrule prod (and factor #\* term)
  (:destructure (f s te)
                (list '* f te)))

(defrule term (or prod factor))

(parse 'term "10*12^2")
(parse 'term "12^2")

(defrule exp (and atom #\^ atom)
  (:destructure (te e at)
                (list 'expt te at)))

(defrule factor (or exp atom)
  (:function identity))

(parse 'factor "2^10")
(parse 'factor "20")

(defrule atom (or integer id (and id \#( expr \#))))

(defrule pr (+ "a")
  )

(parse 'pr "a")

(parse 'atom "abe")
