;;; * Hola
(require :esrap)
(defpackage :filters-grammar
  (:use :cl :esrap))

(in-package :filters-grammar)

(defun not-doublequote (char)
  (not (eql #\" char)))

(defrule whitespace (+ (or #\space #\tab #\newline))
  (:constant nil))

(defrule alphanumeric (alphanumericp character))

(defrule string-char (or (not-doublequote character)
                         (and #\\ #\")))


(defrule input (+ expr))


(defrule chain (or "AND" "OR")
  (:text t))


(defrule kv (and col (? whitespace) op (? whitespace) val)
  (:destructure (col ws op ws2 val)
   (list :filter (intern op :keyword) col val)))

(defrule expr
    (and kv (* (and (? whitespace) chain (? whitespace) kv))))


(defrule list-of-filters (+ (or (and kv chain list-of-filters)
                                 kv))
  (:destructure (match)
    (if (= (car match) :filter)
        (list match)
        (destructuring-bind (int op list) match
          (list :op op int list)))))

(parse 'kv "foo = 34")
(parse 'expr1 "foo = 34")
(parse 'expr "foo = 34 AND bar=3")

(defrule op (or "="
                ">="
                ">"
                "<"
                ">="
                "!=")
  (:text t))


(defrule col (+ alphanumeric) (:text t))

(parse 'expr "foo = 34")
;;; * adeu
(parse 'col "a3")
(parse 'op ">=")

(defrule val (+ alphanumeric) (:text t))
