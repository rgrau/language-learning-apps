;;;; clos1.lisp

(in-package #:clos1)

(defclass project ()
  ((name :accessor name
         :initform 'name
         :initarg  :name)
   (stars)))

(defclass user ()
  ((projects :initform 'projects
             :initarg  :projects
             :accessor projects)
   (name :accessor name
         :initarg :name
         )))

(defclass null-user (user) ())

(defmethod print-object ((u user) stream)
  (print-unreadable-object (u stream :type t :identity t)
    (princ (name u) stream)))

(defmethod print-object ((p project) stream)
  (print-unreadable-object (p stream :type t :identity t)
    (princ (name p) stream)))

(defmethod foobar ((p project)  (u user))
  "yes")

(defmethod foobar ((p project)  (u null-user))
  "nope")

(defparameter *debvar* nil)

(defun deb (x)
  (setf *debvar* x)
  x)

(defmethod print-projects ((u user))
  ;(declare (optimize (speed 3)))
  (flet ((foo (a)
           (+ a 1)))
   (dolist (p (projects u))
     (format t "~a ~a~%"
             (foo 2) (name p))
     (sleep 1))))

(defparameter *user* nil)

(defun main ()
  (declare (optimize (debug 3)))
  (let* ((p1 (make-instance 'project :name "clasker"))
         (u  (make-instance 'user :name "rgrau" :projects (list
                                                           p1
                                                           (make-instance 'project :name "foo")
                                                           (make-instance 'project :name "bar")
                                                           (make-instance 'project :name "baz")))))
    ;(setf *user* u)
    (loop while t
          do (print-projects u))
    ))

;;; "clos1" goes here. Hacks and glory await!
