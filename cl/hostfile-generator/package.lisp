;;;; package.lisp

(defpackage #:hostfile-generator
  (:use #:cl)
  (:import-from #:drakma
                #:http-request
                #:url-encode))
