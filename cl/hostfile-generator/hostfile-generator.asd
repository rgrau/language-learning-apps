;;;; hostfile-generator.asd

(asdf:defsystem #:hostfile-generator
  :description "Generates a secure host file."
  :author "Raimon Grau"
  :license "MIT"
  :serial t
  :depends-on (#:alexandria #:drakma)
  :components ((:file "package")
               (:file "hostfile-generator")))
