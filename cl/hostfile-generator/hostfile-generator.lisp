;;;; hostfile-generator.lisp
(in-package #:hostfile-generator)

(defclass <source> ()
  ((url :initarg :url :reader url)
   (name :initarg :name)))

(defun make-source (name url)
  (make-instance '<source> :name name :url url))

(defmethod get-page ((source <source>))
  (multiple-value-bind (body status)
      (http-request (url source))
    (when (= status 200)
      body)))


(defvar *sources* (mapcar (alexandria:curry 'apply 'make-source)
                          '(("winhelp" "http://winhelp2002.mvps.org/hosts.txt")
                            ("yoyo" "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext")
                            ("someonewhocares" "http://someonewhocares.org/hosts/zero/hosts")
                            ("malwaredomainlist" "http://www.malwaredomainlist.com/hostslist/hosts.txt"))))

(defun main ()
  (with-open-file (s (merge-pathnames "/tmp/host-file" ) :direction :output )
    (format s "~a~%" (mapcan (lambda (s) (get-page s)) *sources*))))
