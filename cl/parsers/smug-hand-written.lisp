(defpackage :foo
  (:use :cl :smug :cl-ppcre))

(in-package :foo)

(defun .arith ()
  (.or
   (.let* ((a (.term))
           (_ (.string= "+"))
           (b (.arith)))
     (.identity (list :+ a b)))
   (.let* ((a (.term))
           (_ (.string= "-"))
           (b (.arith)))
     (.identity (list :- a b)))
   (.term)))

(defun .term ()
  (.or
   (.let* ((a (.number))
           (_ (.string= "*"))
           (b (.term)))
     (.identity (list :* a b)))
   (.let* ((a (.number))
           (_ (.string= "/"))
           (b (.term)))
     (.identity (list :/ a b)))
   (.number)))

(defun .digit-char-p ()
  (.is #'cl:digit-char-p))

(defun .number () (.or
                   (.prog2
                    (.string= "(")
                    (.arith)
                    (.string= ")"))
                   (.first (.map 'string (.digit-char-p)))))

(defvar *whitespace* '(#\space #\newline #\tab))

(defun .whitespace (&optional result-type)
  (.first (.map result-type (.is 'member *whitespace*))))


(parse (.arith) "(342-(32+2))*34")
(parse (.term) "A*A")

(parse (.term) "234*2")
