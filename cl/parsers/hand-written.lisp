(defpackage :hand
  (:use :cl :cl-ppcre))

(in-package :hand)

(defun lex (string)
  (let ((tokens nil)
        (content nil))
    (do-matches-as-strings (m  "(\\(|\\)|\\d+|\\w+|[\\-\\^\\+\\/\\*])"
                             string)
     (cond
       ((string= m "(")  (push (list :open "(") tokens))
       ((string= m ")")  (push (list :close ")") tokens))
       ((setf content
              (scan-to-strings "\\d+" m))
        (push (list :number content) tokens))
       ((setf content
              (scan-to-strings "\\w+" m))
        (push (list :id content) tokens))

       ((setf content
              (scan-to-strings "[\\-\\+\\/\\*\\^]" m))
        (push (list (intern content :keyword) content) tokens))
       (t (error "unrecognized token ~a" m))))
    (reverse tokens)))

(defstruct (node)
  type
  val)

;; (defun parse-plus (stream)
;;   (when (eq :+ (caar stream))
;;     (make-node :type :op :val :+)))

(defstruct (fail))
(defvar nothing
  (lambda (stream)
    (list nil stream)))

(defun look-for (what)
  (lambda (stream)
    (if (eq what (caar stream))
        (list (cadar stream)
              (cdr stream))
        (list (make-fail) nil))))

(defvar lookfor-plus (look-for :+))
(defvar lookfor-open (look-for :open))
(defvar lookfor-close (look-for :close ))
(defvar look-for-number  (tr (look-for :number)
                            (lambda (x) (parse-integer x))))

(funcall lookfor-plus (lex "+"))
(funcall lookfor-number (lex "123"))
(funcall lookfor-open (lex "("))


(defun conc (&rest parsers)
  (lambda (stream)
    (block fun
     (let ((results))
       (dolist (p parsers)
         (destructuring-bind (result new-stream)
             (funcall p stream)
           (when (fail-p result)
             (return-from fun (list (make-fail) nil)))
           (push result results)
           (setf stream new-stream)))
       (list (reverse results) stream)))))

(defun tr (parser tr-fn)
  (lambda (stream)
    (destructuring-bind (r to)
        (funcall parser stream)
      (if (fail-p r)
          (list r to)
          (list (apply tr-fn (if (listp r) r (list r))) to)))))

(funcall (tr (conc lookfor-number (alt lookfor-number))
             (lambda (f s) (list s f)))
         (lex "1 2"))

(funcall (tr (alt lookfor-number
                  nothing)
             (lambda (x) x))
         (lex "12"))

(funcall (tr (alt nothing nothing)
             (lambda ()))
         (lex ""))

(funcall (conc lookfor-number lookfor-number) (lex "1 2"))

(defun alt (&rest parsers)
  (lambda (stream)
    (block fun
      (dolist (p parsers)
        (destructuring-bind (result new-stream)
            (funcall p stream)
          (when (not (fail-p result))
            (return-from fun
                (list result new-stream)))))
        (list (make-fail) stream))))

(funcall nothing (lex "hola tiu"))

(defparameter funcs '(("sin" . sin)
                      ("abs" . abs)))

(defvar parse-atom (alt
                    (tr
                     (conc
                      (look-for :id)
                      (look-for :open)
                      (look-for :id)
                      (look-for :close))
                     (lambda (i o id2 c)
                       (if (assoc i funcs :test #'string=)
                           (list (cdr (assoc i funcs :test #'string=)) id2)
                           (error "incorrect fun"))))
                    (look-for :id)
                    look-for-number))

(funcall parse-atom (lex "sin(bar)"))
(funcall parse-atom (lex "foo"))
(funcall parse-atom (lex "12"))

(defvar %parse-term nothing)
(defvar parse-term (lambda (stream) (funcall %parse-term stream)))
(defvar %parse-term (conc
                    parse-factor
                    (alt (conc (look-for :*)
                               (alt parse-term
                                    nothing)))))

(defvar %parse-expr nothing)
(defvar parse-expr (lambda (stream) (funcall %parse-expr stream)))

(setf %parse-expr (alt (tr
                         (conc (look-for :open)
                               parse-expr
                               (look-for :open))
                         (lambda (_ e _1) e))
                        (conc
                         parse-term
                         (alt (conc (look-for :+)
                                    parse-expr)
                              nothing))))


(defvar parse-term (conc
                    parse-factor
                    (alt (conc (look-for :*)
                               (alt parse-term
                                    nothing)))))

(defvar parse-factor
  (tr
   (conc parse-atom
         (alt (conc (look-for :^)
                    look-for-number)
              nothing))
   (lambda (a n)
     (if (not (null (car n)))
         (list 'expt a (cadr n))
         a))))

(funcall
 (alt (conc (look-for :^)
            (look-for :number))
      nothing)
 (lex "^1"))

(funcall parse-factor (lex "12^3"))

(funcall parse-atom (lex "foo(bar)"))
(funcall parse-atom (lex "foo"))
(funcall parse-atom (lex "12"))

(defvar parse-plus
  (tr
   (conc (look-for :number)
         (look-for :+)
         (look-for :number))
  (lambda (f op s) (list op f s))))

(funcall
 (tr
  (conc (look-for :number)
        (look-for :+)
        (look-for :number))
  (lambda (f op s) (list op f s)))
 (lex "1+2"))

(funcall parse-plus (lex "32+12"))

(funcall (look-for :id) (lex "foo"))
(funcall (look-for :number) (lex "32"))

(defvar parse-expr (look-for :id))

(funcall (conc (look-for :id)
               (look-for :open)
               parse-expr
               (look-for :close))
         (lex "foo(id)"))

(funcall (conc (look-for :id)
               (look-for :open)
               (look-for :close))
         (lex "foo()"))

(funcall (look-for :id) (lex "foo"))
