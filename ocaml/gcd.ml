let rec gcd n m =
  let r = n mod m in
  if r = 0 then m else gcd m r ;;

let rec multiple_upto n r =
  let divisiblep n m = n mod m = 0 in
  if r = 2 then divisiblep n r else
  if divisiblep n r then true else
    multiple_upto n (r - 1 );;

let rec is_prime n =  if n = 2 then true else
  if n = 1 then false else
    not (multiple_upto n (n - 1));;
