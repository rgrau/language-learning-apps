#! /bin/bash

# Bash script to create a self signed CA and a server certificate
# This script is unattended and (for this reason), the CA key
# doesn't require any password to be used. This is __NOT__ a good 
# behaviour in a production environment so USE IT AT YOUR OWN RISK!

# Configure CA structure
mkdir -m 0700 -p \
./demoCA/private \
./demoCA/certs \
./demoCA/crl \
./demoCA/newcerts

# Serial and registry
echo "011E" > ./demoCA/serial
touch ./demoCA/index.txt

# Generate CA cert and key
# (You should send cacert.pem to the client)
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=CA/ST=Canada/L=Canada/O=IT/CN=CA" -keyout ./demoCA/private/cakey.pem  -out ./demoCA/cacert.pem
chmod 0400 ./demoCA/private/cakey.pem

# Verify purpose of a CA
# openssl x509 -purpose -in cacert.pem -inform PEM

# Configure server certificate
# (You should send server.csr to the CA)
openssl genrsa -out server.key 4096
openssl req -new -newkey rsa:4096 -key server.key -out server.csr -subj "/C=CA/ST=Canada/L=Canada/O=IT/CN=server.example.com"

# Sign the certificate!
openssl ca -in server.csr -out server.pem -batch -passin pass:keypass

